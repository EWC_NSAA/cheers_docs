USE [EWC_NSAA]
GO
/****** Object:  StoredProcedure [dbo].[usp_usp_GetSystemMessageDetails]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_usp_GetSystemMessageDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_usp_GetSystemMessageDetails]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateTransferStatus]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateTransferStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateTransferStatus]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateSolution]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateSolution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateSolution]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateServiceRecipient]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateServiceRecipient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateServiceRecipient]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateScreeningNav]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateScreeningNav]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateScreeningNav]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdatePartnersCollaborator]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdatePartnersCollaborator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdatePartnersCollaborator]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNSProvider]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNSProvider]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateNSProvider]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigatorTraining]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNavigatorTraining]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateNavigatorTraining]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigatorLanguage]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNavigatorLanguage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateNavigatorLanguage]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigator]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNavigator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateNavigator]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigationCycle]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNavigationCycle]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateNavigationCycle]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateHECActivity]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateHECActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateHECActivity]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateEncounter]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateEncounter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateEncounter]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateBarrier]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateBarrier]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateBarrier]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateAssistanceResolution]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateAssistanceResolution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateAssistanceResolution]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateAssistanceIssues]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateAssistanceIssues]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateAssistanceIssues]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateAssistance]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateAssistance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateAssistance]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectTransferReasons]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectTransferReasons]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectTransferReasons]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectSolutionsList]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectSolutionsList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectSolutionsList]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectServiceRecipient]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectServiceRecipient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectServiceRecipient]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectScreeningNav]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectScreeningNav]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectScreeningNav]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectRacePacIslander]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectRacePacIslander]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectRacePacIslander]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectRaceAsian]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectRaceAsian]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectRaceAsian]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectRace]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectRace]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectRace]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectNSProvider]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectNSProvider]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectNSProvider]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectNSProblem]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectNSProblem]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectNSProblem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectNavigator]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectNavigator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectNavigator]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectNavigationCycle]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectNavigationCycle]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectNavigationCycle]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectLanguages]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectLanguages]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectLanguages]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectHealthProgram]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectHealthProgram]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectHealthProgram]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectHealthInsurance]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectHealthInsurance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectHealthInsurance]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectEncounter]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectEncounter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectEncounter]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCountries]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectCountries]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectCountries]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCaregiverApproval]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectCaregiverApproval]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectCaregiverApproval]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCancerSite]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectCancerSite]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectCancerSite]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectBarrierType]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectBarrierType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectBarrierType]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectBarrierSolution]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectBarrierSolution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectBarrierSolution]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectBarrierList]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectBarrierList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectBarrierList]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectBarrier]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectBarrier]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectBarrier]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceResolutionList]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceResolutionList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceResolutionList]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceResolution]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceResolution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceResolution]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceRequestor]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceRequestor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceRequestor]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceReferralProvider]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceReferralProvider]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceReferralProvider]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceList]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceList]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceIssuesList]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceIssuesList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceIssuesList]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceEWCRecipient]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceEWCRecipient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceEWCRecipient]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceEWCPCP]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceEWCPCP]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceEWCPCP]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceEWCEnrollee]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceEWCEnrollee]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceEWCEnrollee]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceCommunication]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceCommunication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceCommunication]
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchRecipients]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SearchRecipients]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SearchRecipients]
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchAssistanceIncident]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SearchAssistanceIncident]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SearchAssistanceIncident]
GO
/****** Object:  StoredProcedure [dbo].[usp_SaveRace]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SaveRace]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SaveRace]
GO
/****** Object:  StoredProcedure [dbo].[usp_SaveCaregiverApproval]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SaveCaregiverApproval]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SaveCaregiverApproval]
GO
/****** Object:  StoredProcedure [dbo].[usp_RecipientReport]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RecipientReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RecipientReport]
GO
/****** Object:  StoredProcedure [dbo].[usp_NavigatorTransfer]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_NavigatorTransfer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_NavigatorTransfer]
GO
/****** Object:  StoredProcedure [dbo].[usp_MarkSystemMessageAsRead]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MarkSystemMessageAsRead]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MarkSystemMessageAsRead]
GO
/****** Object:  StoredProcedure [dbo].[usp_MarkMessageAsRead]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MarkMessageAsRead]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MarkMessageAsRead]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertUserAccess]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertUserAccess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertUserAccess]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertSolution]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertSolution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertSolution]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertServiceRecipient]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertServiceRecipient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertServiceRecipient]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertScreeningNav]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertScreeningNav]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertScreeningNav]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertPartnersCollaborator]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertPartnersCollaborator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertPartnersCollaborator]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNSProvider]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNSProvider]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertNSProvider]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNavigatorTraining]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNavigatorTraining]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertNavigatorTraining]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNavigatorLanguage]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNavigatorLanguage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertNavigatorLanguage]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNavigator]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNavigator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertNavigator]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNavigationCycle]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNavigationCycle]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertNavigationCycle]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertInstantMessage]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertInstantMessage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertInstantMessage]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertHECActivity]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertHECActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertHECActivity]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertEncounter]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertEncounter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertEncounter]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertBarrier]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertBarrier]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertBarrier]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertAssistanceResolution]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertAssistanceResolution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertAssistanceResolution]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertAssistanceIssues]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertAssistanceIssues]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertAssistanceIssues]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertAssistance]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertAssistance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertAssistance]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserUnreadMessageCount]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetUserUnreadMessageCount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetUserUnreadMessageCount]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserSystemInstanceMessages]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetUserSystemInstanceMessages]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetUserSystemInstanceMessages]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserSendInstanceMessages]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetUserSendInstanceMessages]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetUserSendInstanceMessages]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserInstanceMessages]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetUserInstanceMessages]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetUserInstanceMessages]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetTrainingsList]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetTrainingsList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetTrainingsList]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetSystemMessageDetails]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetSystemMessageDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetSystemMessageDetails]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetScreeningNav]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetScreeningNav]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetScreeningNav]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetRecipient]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetRecipient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetRecipient]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetRace]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetRace]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetRace]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPartnersAndCollaboratorDetails]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetPartnersAndCollaboratorDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetPartnersAndCollaboratorDetails]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetNavigationCycle]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetNavigationCycle]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetNavigationCycle]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetLanguagesList]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetLanguagesList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetLanguagesList]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetInstanceMessageDetails]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetInstanceMessageDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetInstanceMessageDetails]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetHECActivity]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetHECActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetHECActivity]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetEncounter]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetEncounter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetEncounter]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBarrier]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetBarrier]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetBarrier]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAssistanceResolution]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAssistanceResolution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetAssistanceResolution]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAssistanceIssues]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAssistanceIssues]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetAssistanceIssues]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAssistance]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAssistance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetAssistance]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAllPatnersAndCollaborators]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAllPatnersAndCollaborators]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetAllPatnersAndCollaborators]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAllHECActivities]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAllHECActivities]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetAllHECActivities]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteSolutions]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DeleteSolutions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_DeleteSolutions]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeletePartnersCollaborator]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DeletePartnersCollaborator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_DeletePartnersCollaborator]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteHECActivity]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DeleteHECActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_DeleteHECActivity]
GO
/****** Object:  StoredProcedure [dbo].[GetCaregiverApproval]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCaregiverApproval]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetCaregiverApproval]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tSystemMessage_tNavigator_rcvdBy]') AND parent_object_id = OBJECT_ID(N'[dbo].[tSystemMessage]'))
ALTER TABLE [dbo].[tSystemMessage] DROP CONSTRAINT [FK_tSystemMessage_tNavigator_rcvdBy]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tSolution_tBarrier]') AND parent_object_id = OBJECT_ID(N'[dbo].[tSolution]'))
ALTER TABLE [dbo].[tSolution] DROP CONSTRAINT [FK_tSolution_tBarrier]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tPartnersCollaboratorsService_tPartnersCollaborators]') AND parent_object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsService]'))
ALTER TABLE [dbo].[tPartnersCollaboratorsService] DROP CONSTRAINT [FK_tPartnersCollaboratorsService_tPartnersCollaborators]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tPartnersCollaboratorsContact_tPartnersCollaborators]') AND parent_object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsContact]'))
ALTER TABLE [dbo].[tPartnersCollaboratorsContact] DROP CONSTRAINT [FK_tPartnersCollaboratorsContact_tPartnersCollaborators]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigatorTraining_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigatorTraining]'))
ALTER TABLE [dbo].[tNavigatorTraining] DROP CONSTRAINT [FK_tNavigatorTraining_tNavigator]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle] DROP CONSTRAINT [FK_tNavigationCycle_tServiceRecipient]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNSProvider]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle] DROP CONSTRAINT [FK_tNavigationCycle_tNSProvider]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle] DROP CONSTRAINT [FK_tNavigationCycle_tNavigator]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNavigationCycle]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle] DROP CONSTRAINT [FK_tNavigationCycle_tNavigationCycle]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tInstantMessage_tNavigator_RcvdFrom]') AND parent_object_id = OBJECT_ID(N'[dbo].[tInstantMessage]'))
ALTER TABLE [dbo].[tInstantMessage] DROP CONSTRAINT [FK_tInstantMessage_tNavigator_RcvdFrom]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tInstantMessage_tNavigator_rcvdBy]') AND parent_object_id = OBJECT_ID(N'[dbo].[tInstantMessage]'))
ALTER TABLE [dbo].[tInstantMessage] DROP CONSTRAINT [FK_tInstantMessage_tNavigator_rcvdBy]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecScreeningEvent_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecScreeningEvent]'))
ALTER TABLE [dbo].[tHecScreeningEvent] DROP CONSTRAINT [FK_tHecScreeningEvent_tHecActivities]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecActivityParticipantInfo_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecActivityParticipantInfo]'))
ALTER TABLE [dbo].[tHecActivityParticipantInfo] DROP CONSTRAINT [FK_tHecActivityParticipantInfo_tHecActivities]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tEncounter_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tEncounter]'))
ALTER TABLE [dbo].[tEncounter] DROP CONSTRAINT [FK_tEncounter_tServiceRecipient]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tEncounter_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tEncounter]'))
ALTER TABLE [dbo].[tEncounter] DROP CONSTRAINT [FK_tEncounter_tNavigator]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tCaregiverApprovals_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tCaregiverApprovals]'))
ALTER TABLE [dbo].[tCaregiverApprovals] DROP CONSTRAINT [FK_tCaregiverApprovals_tServiceRecipient]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tBarrier_tEncounter]') AND parent_object_id = OBJECT_ID(N'[dbo].[tBarrier]'))
ALTER TABLE [dbo].[tBarrier] DROP CONSTRAINT [FK_tBarrier_tEncounter]
GO
/****** Object:  Table [dbo].[tUserAccess]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tUserAccess]') AND type in (N'U'))
DROP TABLE [dbo].[tUserAccess]
GO
/****** Object:  Table [dbo].[tSystemMessage]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tSystemMessage]') AND type in (N'U'))
DROP TABLE [dbo].[tSystemMessage]
GO
/****** Object:  Table [dbo].[tSolution]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tSolution]') AND type in (N'U'))
DROP TABLE [dbo].[tSolution]
GO
/****** Object:  Table [dbo].[tServiceRecipient]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tServiceRecipient]') AND type in (N'U'))
DROP TABLE [dbo].[tServiceRecipient]
GO
/****** Object:  Table [dbo].[tScreeningNav]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tScreeningNav]') AND type in (N'U'))
DROP TABLE [dbo].[tScreeningNav]
GO
/****** Object:  Table [dbo].[trSolutionImplementationStatus]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trSolutionImplementationStatus]') AND type in (N'U'))
DROP TABLE [dbo].[trSolutionImplementationStatus]
GO
/****** Object:  Table [dbo].[trServiceTerminatedReason]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trServiceTerminatedReason]') AND type in (N'U'))
DROP TABLE [dbo].[trServiceTerminatedReason]
GO
/****** Object:  Table [dbo].[trRacePacIslander]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trRacePacIslander]') AND type in (N'U'))
DROP TABLE [dbo].[trRacePacIslander]
GO
/****** Object:  Table [dbo].[trRaceAsian]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trRaceAsian]') AND type in (N'U'))
DROP TABLE [dbo].[trRaceAsian]
GO
/****** Object:  Table [dbo].[trRace]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trRace]') AND type in (N'U'))
DROP TABLE [dbo].[trRace]
GO
/****** Object:  Table [dbo].[trProvLanguages]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trProvLanguages]') AND type in (N'U'))
DROP TABLE [dbo].[trProvLanguages]
GO
/****** Object:  Table [dbo].[trOutcomeStatusReason]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trOutcomeStatusReason]') AND type in (N'U'))
DROP TABLE [dbo].[trOutcomeStatusReason]
GO
/****** Object:  Table [dbo].[trOutcomeStatus]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trOutcomeStatus]') AND type in (N'U'))
DROP TABLE [dbo].[trOutcomeStatus]
GO
/****** Object:  Table [dbo].[trNSProblem]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trNSProblem]') AND type in (N'U'))
DROP TABLE [dbo].[trNSProblem]
GO
/****** Object:  Table [dbo].[trNavigatorType]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trNavigatorType]') AND type in (N'U'))
DROP TABLE [dbo].[trNavigatorType]
GO
/****** Object:  Table [dbo].[trNavigatorTransferReason]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trNavigatorTransferReason]') AND type in (N'U'))
DROP TABLE [dbo].[trNavigatorTransferReason]
GO
/****** Object:  Table [dbo].[trHealthProgram]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trHealthProgram]') AND type in (N'U'))
DROP TABLE [dbo].[trHealthProgram]
GO
/****** Object:  Table [dbo].[trHealthInsuranceStatus]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trHealthInsuranceStatus]') AND type in (N'U'))
DROP TABLE [dbo].[trHealthInsuranceStatus]
GO
/****** Object:  Table [dbo].[trHealthInsurance]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trHealthInsurance]') AND type in (N'U'))
DROP TABLE [dbo].[trHealthInsurance]
GO
/****** Object:  Table [dbo].[trEncounterType]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trEncounterType]') AND type in (N'U'))
DROP TABLE [dbo].[trEncounterType]
GO
/****** Object:  Table [dbo].[trEncounterPurpose]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trEncounterPurpose]') AND type in (N'U'))
DROP TABLE [dbo].[trEncounterPurpose]
GO
/****** Object:  Table [dbo].[trCountries]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trCountries]') AND type in (N'U'))
DROP TABLE [dbo].[trCountries]
GO
/****** Object:  Table [dbo].[trCaregiverApproval]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trCaregiverApproval]') AND type in (N'U'))
DROP TABLE [dbo].[trCaregiverApproval]
GO
/****** Object:  Table [dbo].[trCancerSite]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trCancerSite]') AND type in (N'U'))
DROP TABLE [dbo].[trCancerSite]
GO
/****** Object:  Table [dbo].[trBarrierType]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trBarrierType]') AND type in (N'U'))
DROP TABLE [dbo].[trBarrierType]
GO
/****** Object:  Table [dbo].[trBarrierSolution]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trBarrierSolution]') AND type in (N'U'))
DROP TABLE [dbo].[trBarrierSolution]
GO
/****** Object:  Table [dbo].[trBarrier]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trBarrier]') AND type in (N'U'))
DROP TABLE [dbo].[trBarrier]
GO
/****** Object:  Table [dbo].[trAssistanceResolution]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceResolution]') AND type in (N'U'))
DROP TABLE [dbo].[trAssistanceResolution]
GO
/****** Object:  Table [dbo].[trAssistanceRequestor]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceRequestor]') AND type in (N'U'))
DROP TABLE [dbo].[trAssistanceRequestor]
GO
/****** Object:  Table [dbo].[trAssistanceReferralProvider]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceReferralProvider]') AND type in (N'U'))
DROP TABLE [dbo].[trAssistanceReferralProvider]
GO
/****** Object:  Table [dbo].[trAssistanceEWCRecipient]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceEWCRecipient]') AND type in (N'U'))
DROP TABLE [dbo].[trAssistanceEWCRecipient]
GO
/****** Object:  Table [dbo].[trAssistanceEWCPCP]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceEWCPCP]') AND type in (N'U'))
DROP TABLE [dbo].[trAssistanceEWCPCP]
GO
/****** Object:  Table [dbo].[trAssistanceEWCEnrollee]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceEWCEnrollee]') AND type in (N'U'))
DROP TABLE [dbo].[trAssistanceEWCEnrollee]
GO
/****** Object:  Table [dbo].[trAssistanceCommunication]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceCommunication]') AND type in (N'U'))
DROP TABLE [dbo].[trAssistanceCommunication]
GO
/****** Object:  Table [dbo].[tRace]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tRace]') AND type in (N'U'))
DROP TABLE [dbo].[tRace]
GO
/****** Object:  Table [dbo].[tPartnersCollaboratorsService]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsService]') AND type in (N'U'))
DROP TABLE [dbo].[tPartnersCollaboratorsService]
GO
/****** Object:  Table [dbo].[tPartnersCollaboratorsContact]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsContact]') AND type in (N'U'))
DROP TABLE [dbo].[tPartnersCollaboratorsContact]
GO
/****** Object:  Table [dbo].[tPartnersCollaborators]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tPartnersCollaborators]') AND type in (N'U'))
DROP TABLE [dbo].[tPartnersCollaborators]
GO
/****** Object:  Table [dbo].[tNSProvider]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNSProvider]') AND type in (N'U'))
DROP TABLE [dbo].[tNSProvider]
GO
/****** Object:  Table [dbo].[tNetworking]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNetworking]') AND type in (N'U'))
DROP TABLE [dbo].[tNetworking]
GO
/****** Object:  Table [dbo].[tNavigatorTransfer]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigatorTransfer]') AND type in (N'U'))
DROP TABLE [dbo].[tNavigatorTransfer]
GO
/****** Object:  Table [dbo].[tNavigatorTraining]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigatorTraining]') AND type in (N'U'))
DROP TABLE [dbo].[tNavigatorTraining]
GO
/****** Object:  Table [dbo].[tNavigatorLanguages]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigatorLanguages]') AND type in (N'U'))
DROP TABLE [dbo].[tNavigatorLanguages]
GO
/****** Object:  Table [dbo].[tNavigator]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigator]') AND type in (N'U'))
DROP TABLE [dbo].[tNavigator]
GO
/****** Object:  Table [dbo].[tNavigationCycle]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]') AND type in (N'U'))
DROP TABLE [dbo].[tNavigationCycle]
GO
/****** Object:  Table [dbo].[tInstantMessage]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tInstantMessage]') AND type in (N'U'))
DROP TABLE [dbo].[tInstantMessage]
GO
/****** Object:  Table [dbo].[tHecScreeningEvent]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecScreeningEvent]') AND type in (N'U'))
DROP TABLE [dbo].[tHecScreeningEvent]
GO
/****** Object:  Table [dbo].[tHecActivityParticipantInfo]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecActivityParticipantInfo]') AND type in (N'U'))
DROP TABLE [dbo].[tHecActivityParticipantInfo]
GO
/****** Object:  Table [dbo].[tHecActivities]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecActivities]') AND type in (N'U'))
DROP TABLE [dbo].[tHecActivities]
GO
/****** Object:  Table [dbo].[tEncounter]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tEncounter]') AND type in (N'U'))
DROP TABLE [dbo].[tEncounter]
GO
/****** Object:  Table [dbo].[tCaregiverApprovals]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tCaregiverApprovals]') AND type in (N'U'))
DROP TABLE [dbo].[tCaregiverApprovals]
GO
/****** Object:  Table [dbo].[tBarrier]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tBarrier]') AND type in (N'U'))
DROP TABLE [dbo].[tBarrier]
GO
/****** Object:  Table [dbo].[tAssistanceResolution]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tAssistanceResolution]') AND type in (N'U'))
DROP TABLE [dbo].[tAssistanceResolution]
GO
/****** Object:  Table [dbo].[tAssistanceIssues]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tAssistanceIssues]') AND type in (N'U'))
DROP TABLE [dbo].[tAssistanceIssues]
GO
/****** Object:  Table [dbo].[tAssistance]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tAssistance]') AND type in (N'U'))
DROP TABLE [dbo].[tAssistance]
GO
/****** Object:  UserDefinedFunction [dbo].[GetSolutionsById]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSolutionsById]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetSolutionsById]
GO
/****** Object:  UserDefinedFunction [dbo].[GetImplementationsById]    Script Date: 5/10/2018 12:06:11 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetImplementationsById]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetImplementationsById]
GO
/****** Object:  UserDefinedFunction [dbo].[GetImplementationsById]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetImplementationsById]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[GetImplementationsById]
(
    @BarrierID int
)
RETURNS varchar(max)
AS
BEGIN

	declare @CLRF char(2)
	set @CLRF = CHAR(13) + CHAR(10)

    declare @output varchar(max)
    select @output = COALESCE(@output + ''; '', '''') + CASE SolutionImplementationStatus WHEN 1 THEN ''Implemented'' WHEN 2 THEN ''Not Implemented'' END
    from tSolution
    where FK_BarrierID = @BarrierID

    return @output
END;


' 
END

GO
/****** Object:  UserDefinedFunction [dbo].[GetSolutionsById]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSolutionsById]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE FUNCTION [dbo].[GetSolutionsById]
(
    @BarrierID int
)
RETURNS varchar(max)
AS
BEGIN
    declare @output varchar(max)
    select @output = COALESCE(@output + '', '', '''') + BarrierSolutionDescription 
    from tSolution
	JOIN trBarrierSolution
	ON SolutionOffered = BarrierSolutionCode
    where FK_BarrierID = @BarrierID

    return @output
END;


' 
END

GO
/****** Object:  Table [dbo].[tAssistance]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tAssistance]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tAssistance](
	[AssistanceID] [int] IDENTITY(1,1) NOT NULL,
	[IncidentNumber] [char](10) NOT NULL,
	[IncidentDt] [smalldatetime] NOT NULL,
	[FK_CommunicationCode] [smallint] NULL,
	[FirstName] [varchar](20) NOT NULL,
	[Lastname] [varchar](30) NOT NULL,
	[Telephone] [char](10) NOT NULL,
	[Email] [varchar](45) NULL,
	[RecipID] [char](14) NULL,
	[FK_RequestorCode] [smallint] NOT NULL,
	[NPI] [varchar](10) NULL,
	[NPIOwner] [varchar](2) NULL,
	[NPISvcLoc] [varchar](3) NULL,
	[BusinessName] [varchar](100) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[FK_NavigatorID] [int] NOT NULL,
 CONSTRAINT [PK_tAssistance] PRIMARY KEY CLUSTERED 
(
	[AssistanceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tAssistanceIssues]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tAssistanceIssues]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tAssistanceIssues](
	[IssueID] [int] IDENTITY(1,1) NOT NULL,
	[FK_AssistanceID] [int] NOT NULL,
	[IncidentNumber] [char](10) NOT NULL,
	[FK_EWCRecipientCode] [smallint] NULL,
	[FK_EWCPCPCode] [smallint] NULL,
	[FK_ReferralProviderCode] [smallint] NULL,
	[FK_EWCEnrolleeCode] [smallint] NULL,
	[OtherIssues] [varchar](500) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
 CONSTRAINT [PK_tAssistanceIssues] PRIMARY KEY CLUSTERED 
(
	[IssueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tAssistanceResolution]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tAssistanceResolution]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tAssistanceResolution](
	[ResolutionID] [int] IDENTITY(1,1) NOT NULL,
	[FK_AssistanceID] [int] NOT NULL,
	[IncidentNumber] [char](10) NOT NULL,
	[ResolutionInitiationDt] [smalldatetime] NULL,
	[FK_ResolutionCode] [smallint] NULL,
	[ResolutionEndDt] [smalldatetime] NULL,
	[OtherResolution] [varchar](500) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
 CONSTRAINT [PK_tAssistanceResolution] PRIMARY KEY CLUSTERED 
(
	[ResolutionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tBarrier]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tBarrier]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tBarrier](
	[BarrierID] [int] IDENTITY(1,1) NOT NULL,
	[FK_EncounterID] [int] NOT NULL,
	[BarrierType] [int] NOT NULL,
	[BarrierAssessed] [int] NOT NULL,
	[FollowUpDt] [smalldatetime] NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[Other] [varchar](2000) NULL,
 CONSTRAINT [PK_tBarrier] PRIMARY KEY CLUSTERED 
(
	[BarrierID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tCaregiverApprovals]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tCaregiverApprovals]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tCaregiverApprovals](
	[ApprovalID] [int] IDENTITY(1,1) NOT NULL,
	[FK_NSRecipientID] [int] NOT NULL,
	[CaregiverApproval] [int] NOT NULL,
	[CaregiverApprovalOther] [varchar](20) NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
 CONSTRAINT [PK_tCaregiverApprovals] PRIMARY KEY CLUSTERED 
(
	[ApprovalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tEncounter]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tEncounter]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tEncounter](
	[EncounterID] [int] IDENTITY(1,1) NOT NULL,
	[FK_NavigatorID] [int] NOT NULL,
	[FK_RecipientID] [int] NOT NULL,
	[EncounterNumber] [int] NOT NULL,
	[EncounterDt] [smalldatetime] NULL,
	[EncounterStartTime] [varchar](20) NULL,
	[EncounterEndTime] [varchar](20) NULL,
	[EncounterType] [int] NOT NULL,
	[EncounterReason] [int] NOT NULL,
	[MissedAppointment] [bit] NULL,
	[SvcOutcomeStatus] [int] NULL,
	[SvcOutcomeStatusReason] [varchar](200) NULL,
	[NextAppointmentDt] [smalldatetime] NULL,
	[NextAppointmentTime] [varchar](20) NULL,
	[NextAppointmentType] [int] NULL,
	[ServicesTerminated] [bit] NULL,
	[ServicesTerminatedDt] [smalldatetime] NULL,
	[ServicesTerminatedReason] [varchar](200) NULL,
	[Notes] [varchar](2000) NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[EncounterPurpose] [int] NULL,
 CONSTRAINT [PK_tEncounter] PRIMARY KEY CLUSTERED 
(
	[EncounterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tHecActivities]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecActivities]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tHecActivities](
	[ActivityId] [int] IDENTITY(1,1) NOT NULL,
	[ActivityDate] [datetime] NULL,
	[ActivityType] [smallint] NULL,
	[NameOrPurpose] [varchar](250) NULL,
	[CollaboratorId] [int] NULL,
	[CollaboratorContributionId] [int] NULL,
	[Address1] [nvarchar](100) NULL,
	[Address2] [nvarchar](100) NULL,
	[CityId] [int] NULL,
	[Zip] [varchar](5) NULL,
	[CHW] [int] NULL,
	[OtherAttendee] [nvarchar](100) NULL,
	[Population] [varchar](50) NULL,
	[LanguageId] [int] NULL,
	[Discussed] [int] NULL,
	[PrePostTest] [int] NULL,
	[Result] [int] NULL,
	[MyRole] [int] NULL,
	[Attendee] [bit] NULL,
	[AnnoucementDoc] [varchar](300) NULL,
	[Travel] [bit] NULL,
	[Notes] [nvarchar](250) NULL,
 CONSTRAINT [PK_tHecActivities] PRIMARY KEY CLUSTERED 
(
	[ActivityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tHecActivityParticipantInfo]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecActivityParticipantInfo]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tHecActivityParticipantInfo](
	[ActivityId] [int] NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Month] [smallint] NULL,
	[Year] [int] NULL,
	[RaceOrEthinicity] [int] NULL,
	[Address1] [nvarchar](100) NULL,
	[Address2] [nvarchar](100) NULL,
	[CityId] [int] NULL,
	[Zip] [varchar](5) NULL,
	[Telephone] [varchar](50) NULL,
	[Email] [varchar](150) NULL,
	[LanguageId] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tHecScreeningEvent]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecScreeningEvent]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tHecScreeningEvent](
	[ActivityId] [int] NOT NULL,
	[BusinessName] [nvarchar](100) NULL,
	[NPI] [varchar](50) NULL,
	[ProviderFirstName] [nvarchar](50) NULL,
	[ProviderLastName] [nvarchar](50) NULL,
	[EwcProvider] [smallint] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tInstantMessage]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tInstantMessage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tInstantMessage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [nvarchar](250) NULL,
	[Message] [nvarchar](max) NULL,
	[ReceivedOn] [datetime] NOT NULL,
	[ReceivedFrom] [int] NOT NULL,
	[ReceivedBy] [int] NOT NULL,
	[IsRead] [bit] NOT NULL,
	[ParentId] [int] NULL,
 CONSTRAINT [PK_tInstantMessage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tNavigationCycle]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tNavigationCycle](
	[NavigationCycleID] [int] IDENTITY(1,1) NOT NULL,
	[FK_RecipientID] [int] NOT NULL,
	[FK_ProviderID] [int] NULL,
	[FK_ReferralID] [int] NULL,
	[CancerSite] [int] NOT NULL,
	[NSProblem] [int] NOT NULL,
	[HealthProgram] [int] NOT NULL,
	[HealthInsuranceStatus] [int] NOT NULL,
	[HealthInsurancePlan] [int] NULL,
	[HealthInsurancePlanNumber] [varchar](15) NULL,
	[ContactPrefDays] [varchar](15) NULL,
	[ContactPrefHours] [varchar](15) NULL,
	[ContactPrefHoursOther] [varchar](20) NULL,
	[ContactPrefType] [int] NULL,
	[SRIndentifierCodeGenerated] [bit] NULL,
	[FK_NavigatorID] [int] NOT NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
 CONSTRAINT [PK_tNavigationCycle] PRIMARY KEY CLUSTERED 
(
	[NavigationCycleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tNavigator]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigator]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tNavigator](
	[NavigatorID] [int] IDENTITY(5000,1) NOT NULL,
	[DomainName] [varchar](20) NOT NULL,
	[Type] [int] NOT NULL,
	[LastName] [varchar](30) NOT NULL,
	[FirstName] [varchar](20) NOT NULL,
	[Address1] [varchar](30) NOT NULL,
	[Address2] [varchar](30) NULL,
	[City] [varchar](25) NOT NULL,
	[State] [char](2) NOT NULL,
	[Zip] [char](5) NOT NULL,
	[BusinessTelephone] [char](10) NOT NULL,
	[MobileTelephone] [char](10) NULL,
	[Email] [varchar](45) NOT NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[Region] [smallint] NULL,
 CONSTRAINT [PK_tNavigator] PRIMARY KEY CLUSTERED 
(
	[NavigatorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tNavigatorLanguages]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigatorLanguages]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tNavigatorLanguages](
	[NavigatorLanguageID] [int] IDENTITY(1,1) NOT NULL,
	[FK_NavigatorID] [int] NOT NULL,
	[LanguageCode] [int] NOT NULL,
	[SpeakingScore] [int] NULL,
	[ListeningScore] [int] NULL,
	[ReadingScore] [int] NULL,
	[WritingScore] [int] NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
 CONSTRAINT [PK_tNavigatorLanguages] PRIMARY KEY CLUSTERED 
(
	[NavigatorLanguageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tNavigatorTraining]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigatorTraining]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tNavigatorTraining](
	[TrainingID] [int] IDENTITY(1,1) NOT NULL,
	[FK_NavigatorID] [int] NOT NULL,
	[CourseName] [varchar](100) NOT NULL,
	[Organization] [varchar](100) NOT NULL,
	[CompletionDt] [smalldatetime] NOT NULL,
	[Certificate] [bit] NOT NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[BeginDt] [smalldatetime] NULL,
	[CMEAwarded] [bit] NULL,
 CONSTRAINT [PK_tNavigatorTraining] PRIMARY KEY CLUSTERED 
(
	[TrainingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tNavigatorTransfer]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigatorTransfer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tNavigatorTransfer](
	[TransferID] [int] IDENTITY(1,1) NOT NULL,
	[FK_RecipientID] [int] NOT NULL,
	[CurrentNavigatorID] [int] NOT NULL,
	[NewNavigatorID] [int] NOT NULL,
	[NavigatorTransferReason] [varchar](200) NULL,
	[NavigatorTransferDt] [smalldatetime] NULL,
	[TransferStatus] [int] NULL,
	[TransferRefusedReason] [varchar](200) NULL,
	[TransferRefusedDt] [smalldatetime] NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
PRIMARY KEY CLUSTERED 
(
	[TransferID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tNetworking]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNetworking]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tNetworking](
	[NetworkingId] [int] IDENTITY(1,1) NOT NULL,
	[Startdate] [datetime] NULL,
	[Enddate] [datetime] NULL,
	[NameOrPurpose] [nvarchar](200) NULL,
	[Collaborator] [int] NULL,
	[CollaboratorOther] [nvarchar](100) NULL,
	[Address1] [nvarchar](50) NULL,
	[Address2] [nvarchar](50) NULL,
	[City] [varchar](50) NULL,
	[Zip] [int] NULL,
	[Role] [int] NULL,
	[ConfirmAttendance] [bit] NULL,
	[AnnocementDoc] [nvarchar](300) NULL,
	[Travel] [bit] NULL,
	[Notes] [nvarchar](500) NULL,
 CONSTRAINT [PK_tNetworking] PRIMARY KEY CLUSTERED 
(
	[NetworkingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tNSProvider]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNSProvider]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tNSProvider](
	[NSProviderID] [int] IDENTITY(1,1) NOT NULL,
	[PCPName] [varchar](100) NOT NULL,
	[PCP_NPI] [varchar](10) NULL,
	[PCP_Owner] [varchar](2) NULL,
	[PCP_Location] [varchar](3) NULL,
	[PCPAddress1] [varchar](30) NOT NULL,
	[PCPAddress2] [varchar](30) NULL,
	[PCPCity] [varchar](24) NOT NULL,
	[PCPState] [char](2) NOT NULL,
	[PCPZip] [char](5) NOT NULL,
	[PCPContactFName] [varchar](50) NULL,
	[PCPContactLName] [varchar](50) NULL,
	[PCPContactTitle] [varchar](50) NULL,
	[PCPContactTelephone] [char](10) NULL,
	[PCPContactEmail] [varchar](45) NULL,
	[Medical] [bit] NOT NULL,
	[MedicalSpecialty] [int] NULL,
	[MedicalSpecialtyOther] [varchar](100) NULL,
	[ManagedCarePlan] [int] NULL,
	[ManagedCarePlanOther] [varchar](100) NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[EWCPCP] [bit] NULL,
 CONSTRAINT [PK_tNSProvider] PRIMARY KEY CLUSTERED 
(
	[NSProviderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tPartnersCollaborators]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tPartnersCollaborators]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tPartnersCollaborators](
	[PartnersCollaboratorsId] [int] IDENTITY(1,1) NOT NULL,
	[NameofOrg1] [nvarchar](100) NULL,
	[NameOfOrg2] [nvarchar](100) NULL,
	[Abbreviation] [nvarchar](50) NULL,
	[Address1] [nvarchar](50) NULL,
	[Address2] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[ZipCode] [int] NULL,
	[Phone] [varchar](15) NULL,
	[Email] [varchar](100) NULL,
	[WebAddress] [varchar](250) NULL,
 CONSTRAINT [PK_tPartnersCollaborators] PRIMARY KEY CLUSTERED 
(
	[PartnersCollaboratorsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tPartnersCollaboratorsContact]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsContact]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tPartnersCollaboratorsContact](
	[PartnersCollaboratorsId] [int] NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Title] [nvarchar](10) NULL,
	[Phone] [varchar](15) NULL,
	[Ext] [int] NULL,
	[Email] [varchar](100) NULL,
	[DateOfLastContact] [datetime] NULL,
 CONSTRAINT [PK_tPartnersCollaboratorsContact] PRIMARY KEY CLUSTERED 
(
	[PartnersCollaboratorsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tPartnersCollaboratorsService]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsService]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tPartnersCollaboratorsService](
	[PartnersCollaboratorsId] [int] NOT NULL,
	[OrgType1] [int] NULL,
	[OrgType2] [int] NULL,
	[ServiceCategory1] [int] NULL,
	[ServiceCaregory2] [int] NULL,
	[ServicePopulation1] [int] NULL,
	[ServicePopulation2] [int] NULL,
	[Language1] [int] NULL,
	[Language2] [int] NULL,
	[Area] [varchar](50) NULL,
	[Region] [int] NULL,
	[Country1] [int] NULL,
	[Country2] [int] NULL,
 CONSTRAINT [PK_tPartnersCollaboratorsService] PRIMARY KEY CLUSTERED 
(
	[PartnersCollaboratorsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tRace]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tRace]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tRace](
	[RaceID] [int] IDENTITY(1,1) NOT NULL,
	[FK_NSRecipientID] [int] NOT NULL,
	[RaceCode] [int] NOT NULL,
	[RaceOther] [varchar](20) NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
 CONSTRAINT [PK_tRace] PRIMARY KEY CLUSTERED 
(
	[RaceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trAssistanceCommunication]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceCommunication]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trAssistanceCommunication](
	[CommunicationCode] [smallint] NOT NULL,
	[CommunicationDescription] [varchar](200) NOT NULL,
 CONSTRAINT [PK_trAssistanceCommunication] PRIMARY KEY CLUSTERED 
(
	[CommunicationCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trAssistanceEWCEnrollee]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceEWCEnrollee]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trAssistanceEWCEnrollee](
	[EWCEnrolleeCode] [smallint] NOT NULL,
	[EWCEnrolleeDescription] [varchar](200) NOT NULL,
 CONSTRAINT [PK_trAssistanceEWCEnrollee] PRIMARY KEY CLUSTERED 
(
	[EWCEnrolleeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trAssistanceEWCPCP]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceEWCPCP]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trAssistanceEWCPCP](
	[EWCPCPCode] [smallint] NOT NULL,
	[EWCPCPDescription] [varchar](200) NOT NULL,
 CONSTRAINT [PK_trAssistanceEWCPCP] PRIMARY KEY CLUSTERED 
(
	[EWCPCPCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trAssistanceEWCRecipient]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceEWCRecipient]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trAssistanceEWCRecipient](
	[EWCRecipientCode] [smallint] NOT NULL,
	[EWCRecipientDescription] [varchar](200) NOT NULL,
 CONSTRAINT [PK_trAssistanceEWCRecipient] PRIMARY KEY CLUSTERED 
(
	[EWCRecipientCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trAssistanceReferralProvider]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceReferralProvider]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trAssistanceReferralProvider](
	[ReferralProviderCode] [smallint] NOT NULL,
	[ReferralProviderDescription] [varchar](200) NOT NULL,
 CONSTRAINT [PK_trAssistanceReferralProvider] PRIMARY KEY CLUSTERED 
(
	[ReferralProviderCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trAssistanceRequestor]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceRequestor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trAssistanceRequestor](
	[RequestorCode] [smallint] NOT NULL,
	[RequestorDescription] [varchar](200) NOT NULL,
 CONSTRAINT [PK_trAssistanceRequestor] PRIMARY KEY CLUSTERED 
(
	[RequestorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trAssistanceResolution]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceResolution]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trAssistanceResolution](
	[ResolutionCode] [smallint] NOT NULL,
	[ResolutionDescription] [varchar](200) NOT NULL,
 CONSTRAINT [PK_trAssistanceResolution] PRIMARY KEY CLUSTERED 
(
	[ResolutionCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trBarrier]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trBarrier]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trBarrier](
	[BarrierCode] [int] NOT NULL,
	[BarrierDescription] [varchar](100) NOT NULL,
	[BarrierTypeCode] [int] NULL,
 CONSTRAINT [PK_trBarrier] PRIMARY KEY CLUSTERED 
(
	[BarrierCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trBarrierSolution]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trBarrierSolution]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trBarrierSolution](
	[BarrierSolutionCode] [int] NOT NULL,
	[BarrierSolutionDescription] [varchar](100) NOT NULL,
	[BarrierTypeCode] [int] NULL,
 CONSTRAINT [PK_trBarrierSolution] PRIMARY KEY CLUSTERED 
(
	[BarrierSolutionCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trBarrierType]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trBarrierType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trBarrierType](
	[BarrierTypeCode] [int] NOT NULL,
	[BarrierTypeDescription] [varchar](50) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trCancerSite]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trCancerSite]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trCancerSite](
	[CancerSiteCode] [int] NOT NULL,
	[CancerSiteName] [varchar](20) NOT NULL,
 CONSTRAINT [PK_trCancerSite] PRIMARY KEY CLUSTERED 
(
	[CancerSiteCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trCaregiverApproval]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trCaregiverApproval]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trCaregiverApproval](
	[CaregiverApprovalCode] [int] NOT NULL,
	[CaregiverApprovalDescription] [varchar](100) NOT NULL,
 CONSTRAINT [PK_trCaregiverApproval] PRIMARY KEY CLUSTERED 
(
	[CaregiverApprovalCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trCountries]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trCountries]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trCountries](
	[CountryCode] [int] NOT NULL,
	[CountryName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_trCountries] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trEncounterPurpose]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trEncounterPurpose]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trEncounterPurpose](
	[EncounterPurposeCode] [int] NOT NULL,
	[EncounterPurposeDescription] [varchar](50) NOT NULL,
 CONSTRAINT [PK_trEncounterPurpose] PRIMARY KEY CLUSTERED 
(
	[EncounterPurposeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trEncounterType]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trEncounterType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trEncounterType](
	[EncounterTypeCode] [int] NOT NULL,
	[EncounterTypeDescription] [varchar](50) NOT NULL,
 CONSTRAINT [PK_trEncounterType] PRIMARY KEY CLUSTERED 
(
	[EncounterTypeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trHealthInsurance]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trHealthInsurance]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trHealthInsurance](
	[HealthInsuranceCode] [int] NOT NULL,
	[HealthInsuranceName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_trHealthInsurance] PRIMARY KEY CLUSTERED 
(
	[HealthInsuranceCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trHealthInsuranceStatus]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trHealthInsuranceStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trHealthInsuranceStatus](
	[HealthInsuranceStatusCode] [int] NOT NULL,
	[HealthInsuranceStatusName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_trHealthInsuranceStatus] PRIMARY KEY CLUSTERED 
(
	[HealthInsuranceStatusCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trHealthProgram]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trHealthProgram]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trHealthProgram](
	[HealthProgramCode] [int] NOT NULL,
	[HealthProgramName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_trHealthProgram] PRIMARY KEY CLUSTERED 
(
	[HealthProgramCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trNavigatorTransferReason]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trNavigatorTransferReason]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trNavigatorTransferReason](
	[NavigatorTransferReasonCode] [int] NOT NULL,
	[NavigatorTransferReason] [varchar](200) NOT NULL,
 CONSTRAINT [PK_trNavigatorTransferReason] PRIMARY KEY CLUSTERED 
(
	[NavigatorTransferReasonCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trNavigatorType]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trNavigatorType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trNavigatorType](
	[NavigatorTypeCode] [int] NOT NULL,
	[NavigatorTypeDescription] [varchar](50) NOT NULL,
 CONSTRAINT [PK_trNavigatorType] PRIMARY KEY CLUSTERED 
(
	[NavigatorTypeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trNSProblem]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trNSProblem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trNSProblem](
	[NSProblemCode] [int] NOT NULL,
	[NSProblemDescription] [varchar](100) NOT NULL,
 CONSTRAINT [PK_trNSProblem] PRIMARY KEY CLUSTERED 
(
	[NSProblemCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trOutcomeStatus]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trOutcomeStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trOutcomeStatus](
	[OutcomeStatusCode] [int] NOT NULL,
	[OutcomeStatusDescription] [varchar](50) NOT NULL,
 CONSTRAINT [PK_trOutcomeStatus] PRIMARY KEY CLUSTERED 
(
	[OutcomeStatusCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trOutcomeStatusReason]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trOutcomeStatusReason]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trOutcomeStatusReason](
	[OutcomeStatusReasonCode] [int] NOT NULL,
	[OutcomeStatusReason] [varchar](50) NOT NULL,
 CONSTRAINT [PK_trOutcomeStatusReason] PRIMARY KEY CLUSTERED 
(
	[OutcomeStatusReasonCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trProvLanguages]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trProvLanguages]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trProvLanguages](
	[LanguageCode] [int] NOT NULL,
	[LanguageName] [varchar](30) NOT NULL,
 CONSTRAINT [PK_trProvLanguages] PRIMARY KEY CLUSTERED 
(
	[LanguageCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trRace]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trRace]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trRace](
	[RaceCode] [int] NOT NULL,
	[RaceName] [varchar](30) NOT NULL,
 CONSTRAINT [PK_trRace] PRIMARY KEY CLUSTERED 
(
	[RaceCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trRaceAsian]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trRaceAsian]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trRaceAsian](
	[RaceAsianCode] [int] NOT NULL,
	[RaceAsianName] [varchar](30) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trRacePacIslander]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trRacePacIslander]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trRacePacIslander](
	[RacePacIslanderCode] [int] NOT NULL,
	[RacePacIslanderName] [varchar](30) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trServiceTerminatedReason]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trServiceTerminatedReason]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trServiceTerminatedReason](
	[ServiceTerminatedReasonCode] [int] NOT NULL,
	[ServiceTerminatedReason] [varchar](50) NOT NULL,
 CONSTRAINT [PK_trServiceTerminatedReason] PRIMARY KEY CLUSTERED 
(
	[ServiceTerminatedReasonCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[trSolutionImplementationStatus]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trSolutionImplementationStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trSolutionImplementationStatus](
	[SolutionImplementationStatusCode] [int] NOT NULL,
	[SolutionImplementationStatus] [varchar](100) NOT NULL,
 CONSTRAINT [PK_trSolutionImplementationStatus] PRIMARY KEY CLUSTERED 
(
	[SolutionImplementationStatusCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tScreeningNav]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tScreeningNav]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tScreeningNav](
	[SN_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_NavigatorID] [int] NOT NULL,
	[SN_CODE1] [varchar](11) NOT NULL,
	[SN_CODE2] [smallint] NOT NULL,
	[LastName] [varchar](30) NOT NULL,
	[FirstName] [varchar](20) NULL,
	[DOB] [smalldatetime] NULL,
	[Address1] [varchar](30) NULL,
	[Address2] [varchar](30) NULL,
	[City] [varchar](25) NULL,
	[State] [char](2) NULL,
	[Zip] [char](5) NULL,
	[HomeTelephone] [char](10) NULL,
	[Cellphone] [char](10) NULL,
	[Email] [varchar](45) NULL,
	[Computer] [bit] NULL,
	[TextMessage] [char](1) NULL,
	[DateOfContact1] [smalldatetime] NULL,
	[ApptScreen1] [char](1) NULL,
	[SvcDate1] [smalldatetime] NULL,
	[SvcType1] [char](2) NULL,
	[Response1] [char](2) NULL,
	[DateOfContact2] [smalldatetime] NULL,
	[ApptScreen2] [char](1) NULL,
	[SvcDate2] [smalldatetime] NULL,
	[SvcType2] [char](2) NULL,
	[Response2] [char](2) NULL,
	[NSEnrollment] [char](1) NULL,
	[EnrollmentDate] [smalldatetime] NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
PRIMARY KEY CLUSTERED 
(
	[SN_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tServiceRecipient]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tServiceRecipient]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tServiceRecipient](
	[NSRecipientID] [int] IDENTITY(1,1) NOT NULL,
	[FK_NavigatorID] [int] NOT NULL,
	[FK_RecipID] [char](14) NULL,
	[LastName] [varchar](30) NOT NULL,
	[FirstName] [varchar](20) NOT NULL,
	[MiddleInitial] [varchar](1) NULL,
	[DOB] [smalldatetime] NOT NULL,
	[AddressNotAvailable] [bit] NULL,
	[Address1] [varchar](30) NULL,
	[Address2] [varchar](30) NULL,
	[City] [varchar](25) NULL,
	[State] [char](2) NULL,
	[Zip] [char](5) NULL,
	[Phone1] [char](10) NOT NULL,
	[P1PhoneType] [smallint] NOT NULL,
	[P1PersonalMessage] [bit] NOT NULL,
	[Phone2] [char](10) NULL,
	[P2PhoneType] [smallint] NULL,
	[P2PhoneTypeOther] [varchar](20) NULL,
	[P2PersonalMessage] [bit] NULL,
	[MotherMaidenName] [varchar](20) NULL,
	[Email] [varchar](45) NULL,
	[SSN] [char](10) NULL,
	[ImmigrantStatus] [bit] NULL,
	[CountryOfOrigin] [smallint] NULL,
	[Gender] [char](1) NULL,
	[Ethnicity] [bit] NULL,
	[PrimaryLanguage] [smallint] NOT NULL,
	[AgreeToSpeakEnglish] [bit] NOT NULL,
	[CaregiverName] [varchar](50) NULL,
	[Relationship] [smallint] NULL,
	[RelationshipOther] [varchar](30) NULL,
	[CaregiverPhone] [char](10) NULL,
	[CaregiverPhoneType] [smallint] NULL,
	[CaregiverPhonePersonalMessage] [bit] NULL,
	[CaregiverEmail] [varchar](45) NULL,
	[CaregiverContactPreference] [int] NULL,
	[Comments] [varchar](500) NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[NavigatorTransferDt] [smalldatetime] NULL,
	[NavigatorTransferReason] [varchar](200) NULL,
	[OldNavigatorID] [int] NULL,
 CONSTRAINT [PK_tServiceRecipient] PRIMARY KEY CLUSTERED 
(
	[NSRecipientID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tSolution]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tSolution]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tSolution](
	[SolutionID] [int] IDENTITY(1,1) NOT NULL,
	[FK_BarrierID] [int] NOT NULL,
	[SolutionOffered] [int] NOT NULL,
	[SolutionImplementationStatus] [int] NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[FollowUpDt] [smalldatetime] NULL,
	[Solution] [varchar](2000) NULL,
 CONSTRAINT [PK_tSolution] PRIMARY KEY CLUSTERED 
(
	[SolutionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tSystemMessage]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tSystemMessage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tSystemMessage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [nvarchar](250) NULL,
	[Message] [nvarchar](max) NULL,
	[ReceivedOn] [datetime] NOT NULL,
	[ReceivedBy] [int] NOT NULL,
	[IsRead] [bit] NOT NULL,
 CONSTRAINT [PK_tSystemMessage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tUserAccess]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tUserAccess]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tUserAccess](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DomainName] [varchar](20) NOT NULL,
	[LastAccess] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_tUserAccess] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tBarrier] ON 

INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (1, 1, 3, 9, NULL, NULL, CAST(N'2017-10-16 14:47:00' AS SmallDateTime), N'User input', CAST(N'2017-10-16 14:48:00' AS SmallDateTime), N'User update', NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (6, 6, 5, 15, NULL, NULL, CAST(N'2018-02-26 14:32:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (7, 8, 7, 23, NULL, NULL, CAST(N'2018-02-27 16:37:00' AS SmallDateTime), N'User input', CAST(N'2018-03-05 20:14:00' AS SmallDateTime), N'User update', N'ioudkjckkcskcsk')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (8, 9, 7, 23, NULL, NULL, CAST(N'2018-02-27 16:54:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (9, 10, 7, 23, NULL, NULL, CAST(N'2018-02-27 17:02:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (10, 11, 3, 10, NULL, NULL, CAST(N'2018-02-28 12:32:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (11, 12, 7, 23, NULL, NULL, CAST(N'2018-02-28 13:03:00' AS SmallDateTime), N'User input', CAST(N'2018-02-28 14:02:00' AS SmallDateTime), N'User update', NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (12, 14, 7, 23, NULL, NULL, CAST(N'2018-02-28 14:43:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (13, 15, 7, 23, NULL, NULL, CAST(N'2018-02-28 14:49:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (14, 16, 7, 23, NULL, NULL, CAST(N'2018-03-01 09:19:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (15, 17, 7, 23, NULL, NULL, CAST(N'2018-03-01 09:29:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (16, 18, 7, 23, NULL, NULL, CAST(N'2018-03-01 09:37:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (17, 19, 7, 23, NULL, NULL, CAST(N'2018-03-01 09:52:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (18, 20, 7, 23, NULL, NULL, CAST(N'2018-03-01 10:02:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (19, 21, 7, 23, NULL, NULL, CAST(N'2018-03-01 10:03:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (20, 22, 7, 23, NULL, NULL, CAST(N'2018-03-01 10:09:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (21, 23, 7, 23, NULL, NULL, CAST(N'2018-03-01 12:37:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (22, 24, 7, 23, NULL, NULL, CAST(N'2018-03-01 13:09:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (23, 25, 7, 23, NULL, NULL, CAST(N'2018-03-02 19:59:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (24, 26, 7, 23, NULL, NULL, CAST(N'2018-03-02 20:04:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (25, 26, 7, 23, NULL, NULL, CAST(N'2018-03-02 20:05:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (26, 26, 7, 23, NULL, NULL, CAST(N'2018-03-02 20:07:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (27, 27, 7, 23, NULL, NULL, CAST(N'2018-03-03 11:48:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (28, 28, 5, 16, NULL, NULL, CAST(N'2018-03-03 13:23:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (29, 29, 6, 20, NULL, NULL, CAST(N'2018-03-03 14:11:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (30, 30, 7, 23, NULL, NULL, CAST(N'2018-03-03 14:23:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (31, 31, 2, 7, NULL, NULL, CAST(N'2018-03-03 15:09:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (32, 32, 7, 23, NULL, NULL, CAST(N'2018-03-03 15:11:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (33, 33, 7, 23, NULL, NULL, CAST(N'2018-03-03 15:28:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (34, 34, 7, 23, NULL, NULL, CAST(N'2018-03-03 15:28:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (35, 35, 7, 23, NULL, NULL, CAST(N'2018-03-03 15:35:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (36, 36, 7, 23, NULL, NULL, CAST(N'2018-03-03 15:42:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (37, 37, 7, 23, NULL, NULL, CAST(N'2018-03-03 15:46:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (38, 38, 7, 23, NULL, NULL, CAST(N'2018-03-03 15:53:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (39, 39, 7, 23, NULL, NULL, CAST(N'2018-03-03 16:04:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (40, 40, 7, 23, NULL, NULL, CAST(N'2018-03-03 16:10:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (41, 41, 7, 23, NULL, NULL, CAST(N'2018-03-03 16:20:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (42, 42, 7, 23, NULL, NULL, CAST(N'2018-03-03 16:34:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (43, 43, 7, 23, NULL, NULL, CAST(N'2018-03-03 16:42:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (44, 44, 7, 23, NULL, NULL, CAST(N'2018-03-03 16:54:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (45, 45, 7, 23, NULL, NULL, CAST(N'2018-03-03 17:03:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (46, 46, 7, 23, NULL, NULL, CAST(N'2018-03-03 17:12:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (47, 47, 7, 23, NULL, NULL, CAST(N'2018-03-04 09:36:00' AS SmallDateTime), N'User input', NULL, NULL, N'OOOOOOOOOOOOOOO')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (48, 48, 7, 23, NULL, NULL, CAST(N'2018-03-04 09:44:00' AS SmallDateTime), N'User input', NULL, NULL, N'3/4/18  9:39 Other')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (49, 49, 7, 23, NULL, NULL, CAST(N'2018-03-04 09:48:00' AS SmallDateTime), N'User input', NULL, NULL, N'3/4/18 9:46  Other')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (50, 50, 7, 23, NULL, NULL, CAST(N'2018-03-04 12:55:00' AS SmallDateTime), N'User input', NULL, NULL, N'3/4/18 12:53PM Other stuff')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (51, 50, 7, 23, NULL, NULL, CAST(N'2018-03-04 12:56:00' AS SmallDateTime), N'User input', NULL, NULL, N'3/4/18 12:55PM Other stuff')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (52, 50, 7, 23, NULL, NULL, CAST(N'2018-03-04 13:09:00' AS SmallDateTime), N'User input', NULL, NULL, N'3/4/18 1:08 PM Other')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (53, 50, 1, 2, NULL, NULL, CAST(N'2018-03-04 13:15:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (54, 50, 7, 23, NULL, NULL, CAST(N'2018-03-04 13:16:00' AS SmallDateTime), N'User input', CAST(N'2018-03-04 14:44:00' AS SmallDateTime), N'User update', N'TTTTTTTTTTTTTTTTTTTTTTTTTTT')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (55, 50, 7, 23, NULL, NULL, CAST(N'2018-03-04 13:30:00' AS SmallDateTime), N'User input', NULL, NULL, N'3/4/18 1:30 PM Other')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (56, 50, 7, 23, NULL, NULL, CAST(N'2018-03-04 13:31:00' AS SmallDateTime), N'User input', CAST(N'2018-03-04 14:41:00' AS SmallDateTime), N'User update', N'3/4/18 1:32 PM OtherRRR')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (57, 50, 7, 23, NULL, NULL, CAST(N'2018-03-04 13:32:00' AS SmallDateTime), N'User input', CAST(N'2018-03-04 14:42:00' AS SmallDateTime), N'User update', N'QQQQQQQQQQQQQQQQQQQQQQQQQQQQ')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (58, 50, 2, 7, NULL, NULL, CAST(N'2018-03-04 14:45:00' AS SmallDateTime), N'User input', CAST(N'2018-03-04 14:46:00' AS SmallDateTime), N'User update', NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (59, 51, 7, 23, NULL, NULL, CAST(N'2018-03-05 20:16:00' AS SmallDateTime), N'User input', CAST(N'2018-03-06 09:03:00' AS SmallDateTime), N'User update', N'Other than Other')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (60, 52, 7, 23, NULL, NULL, CAST(N'2018-03-06 17:08:00' AS SmallDateTime), N'User input', CAST(N'2018-03-06 17:27:00' AS SmallDateTime), N'User update', N'kjfkjkjcskjsfd')
SET IDENTITY_INSERT [dbo].[tBarrier] OFF
SET IDENTITY_INSERT [dbo].[tCaregiverApprovals] ON 

INSERT [dbo].[tCaregiverApprovals] ([ApprovalID], [FK_NSRecipientID], [CaregiverApproval], [CaregiverApprovalOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (1, 2, 74, N' ', NULL, CAST(N'2017-10-10 08:26:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tCaregiverApprovals] ([ApprovalID], [FK_NSRecipientID], [CaregiverApproval], [CaregiverApprovalOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (26, 8, 72, N' ', NULL, CAST(N'2017-11-15 15:26:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tCaregiverApprovals] ([ApprovalID], [FK_NSRecipientID], [CaregiverApproval], [CaregiverApprovalOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (27, 8, 73, N' ', NULL, CAST(N'2017-11-15 15:26:00' AS SmallDateTime), N'User Input', NULL, NULL)
SET IDENTITY_INSERT [dbo].[tCaregiverApprovals] OFF
SET IDENTITY_INSERT [dbo].[tEncounter] ON 

INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (1, 5000, 1, 1, CAST(N'2017-10-16 00:00:00' AS SmallDateTime), N'2:46 PM', NULL, 2, 0, 0, NULL, NULL, CAST(N'2017-10-25 00:00:00' AS SmallDateTime), N'9.00 AM', 1, 0, NULL, NULL, NULL, NULL, CAST(N'2017-10-16 14:47:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (2, 5000, 8, 1, CAST(N'2017-11-09 00:00:00' AS SmallDateTime), N'1:56 PM', N'3.00 PM', 1, 0, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-11-09 13:57:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (3, 5000, 8, 2, CAST(N'2017-11-09 00:00:00' AS SmallDateTime), N'4:27 PM', N'5.00 PM', 2, 2, NULL, 1, N'Test', CAST(N'2017-11-15 00:00:00' AS SmallDateTime), N'9.00 AM', 1, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-11-09 16:29:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (4, 5000, 8, 3, CAST(N'2017-11-09 00:00:00' AS SmallDateTime), N'6:00 PM', N'7.00 PM', 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-11-09 18:01:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (5, 5000, 8, 4, CAST(N'2017-11-16 00:00:00' AS SmallDateTime), N'2:43 PM', NULL, 1, 1, NULL, NULL, NULL, CAST(N'2017-11-22 00:00:00' AS SmallDateTime), N'9.00 AM', 1, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-11-16 14:45:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (6, 5001, 11, 1, CAST(N'2018-02-26 00:00:00' AS SmallDateTime), N'2:30 PM', N'3:01 PM', 4, 0, 0, NULL, N'done', CAST(N'2018-03-02 00:00:00' AS SmallDateTime), N'3:00 PM', 4, 0, NULL, NULL, N'noted', NULL, CAST(N'2018-02-26 14:32:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (7, 5001, 11, 2, CAST(N'2018-02-27 00:00:00' AS SmallDateTime), N'4:29 PM', N'6:00 PM', 4, 0, 0, NULL, NULL, CAST(N'2018-03-05 00:00:00' AS SmallDateTime), N'10:00 AM', 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-02-27 16:31:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (8, 5001, 11, 3, CAST(N'2018-02-27 00:00:00' AS SmallDateTime), N'4:36 PM', N'6:30 PM', 4, 0, 0, NULL, NULL, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'9:00 AM', 4, 0, NULL, NULL, N'A check of the Note text', NULL, CAST(N'2018-02-27 16:37:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (9, 5001, 11, 4, CAST(N'2018-02-27 00:00:00' AS SmallDateTime), N'4:52 PM', N'6:00 PM', 4, 0, 0, NULL, NULL, CAST(N'2018-03-02 00:00:00' AS SmallDateTime), N'10:00 AM', 4, 0, NULL, NULL, N' A note test', NULL, CAST(N'2018-02-27 16:54:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (10, 5001, 11, 5, CAST(N'2018-02-27 00:00:00' AS SmallDateTime), N'5:01 PM', NULL, 4, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-02-27 17:02:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (11, 5001, 11, 6, CAST(N'2018-02-28 00:00:00' AS SmallDateTime), N'12:27 PM', N'1:00 PM', 4, 0, 0, NULL, NULL, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'2:00 PM', 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-02-28 12:30:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (12, 5001, 11, 7, CAST(N'2018-02-28 00:00:00' AS SmallDateTime), N'1:01 PM', NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-02-28 13:03:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (13, 5001, 11, 8, CAST(N'2018-02-28 00:00:00' AS SmallDateTime), N'2:30 PM', N'4:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-02 00:00:00' AS SmallDateTime), NULL, 2, 0, NULL, NULL, NULL, NULL, CAST(N'2018-02-28 14:31:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (14, 5001, 11, 9, CAST(N'2018-02-28 00:00:00' AS SmallDateTime), N'2:42 PM', NULL, 1, 0, 0, NULL, NULL, CAST(N'2018-03-02 00:00:00' AS SmallDateTime), NULL, 2, 0, NULL, NULL, N'motes', NULL, CAST(N'2018-02-28 14:43:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (15, 5001, 11, 8, CAST(N'2018-02-28 00:00:00' AS SmallDateTime), N'2:47 PM', N'4:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-05 00:00:00' AS SmallDateTime), N'3:00PM', 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-02-28 14:49:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (16, 5001, 12, 1, CAST(N'2018-03-01 00:00:00' AS SmallDateTime), N'9:17 AM', N'10:00 AM', 2, 0, 0, NULL, NULL, CAST(N'2018-03-02 00:00:00' AS SmallDateTime), N'9:00 AM', 1, 0, NULL, NULL, N'Noted Jack', NULL, CAST(N'2018-03-01 09:19:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (17, 5001, 12, 2, CAST(N'2018-03-01 00:00:00' AS SmallDateTime), N'9:26 AM', N'10:00 AM', 3, 0, 0, NULL, NULL, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'11:00 AM', 1, 0, NULL, NULL, N'enc 2 notes', NULL, CAST(N'2018-03-01 09:29:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (18, 5001, 12, 3, CAST(N'2018-03-01 00:00:00' AS SmallDateTime), N'9:35 AM', N'12: 00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-04 00:00:00' AS SmallDateTime), N'8:00 AM', 2, 0, NULL, NULL, N'enc 3 MORE NOTES ', NULL, CAST(N'2018-03-01 09:37:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (19, 5001, 12, 4, CAST(N'2018-03-01 00:00:00' AS SmallDateTime), N'9:50 AM', N'11:30 AM', 2, 0, 0, NULL, NULL, CAST(N'2018-03-05 00:00:00' AS SmallDateTime), N'9:00 AM', 1, 0, NULL, NULL, N'ENC 4', NULL, CAST(N'2018-03-01 09:52:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (20, 5001, 12, 5, CAST(N'2018-03-01 00:00:00' AS SmallDateTime), N'10:00 AM', N'11:00 AM', 3, 0, 0, NULL, NULL, CAST(N'2018-03-06 00:00:00' AS SmallDateTime), N'8:00 AM', 4, 0, NULL, NULL, N'ENC 5 notes', NULL, CAST(N'2018-03-01 10:02:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (21, 5001, 12, 6, CAST(N'2018-03-01 00:00:00' AS SmallDateTime), N'10:02 AM', N'11:00 AM', 4, 0, 0, NULL, NULL, CAST(N'2018-03-07 00:00:00' AS SmallDateTime), N'8:30 AM', 1, 0, NULL, NULL, N'ENC 6 Notes', NULL, CAST(N'2018-03-01 10:03:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (22, 5001, 12, 7, CAST(N'2018-03-01 00:00:00' AS SmallDateTime), N'10:07 AM', N'10:30 AM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-02 00:00:00' AS SmallDateTime), N'7:00 AM', 4, 0, NULL, NULL, N'ENC 7 note', NULL, CAST(N'2018-03-01 10:09:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (23, 5001, 11, 9, CAST(N'2018-03-01 00:00:00' AS SmallDateTime), N'12:36 PM', N'1:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-02 00:00:00' AS SmallDateTime), N'2:00 PM', 3, 0, NULL, NULL, N'ENC 10 Note', NULL, CAST(N'2018-03-01 12:37:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (24, 5001, 12, 8, CAST(N'2018-03-01 00:00:00' AS SmallDateTime), N'1:08 PM', N'2:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-04 00:00:00' AS SmallDateTime), N'9:00 AM', 1, 0, NULL, NULL, N'ENC 8 Notes', NULL, CAST(N'2018-03-01 13:09:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (25, 5001, 11, 2, CAST(N'2018-03-02 00:00:00' AS SmallDateTime), N'7:57 PM', N'9:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-05 00:00:00' AS SmallDateTime), N'10:00 AM', 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-02 19:59:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (26, 5001, 11, 2, CAST(N'2018-03-02 00:00:00' AS SmallDateTime), N'8:02 PM', N'9:00 PM', 3, 0, 0, NULL, NULL, CAST(N'2018-03-05 00:00:00' AS SmallDateTime), N'11:00 AM', 4, 0, NULL, NULL, N'Note 3/2/18 8:03 PM', NULL, CAST(N'2018-03-02 20:04:00' AS SmallDateTime), N'User Input', CAST(N'2018-03-02 20:07:00' AS SmallDateTime), N'User Update', NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (27, 5001, 11, 3, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'11:46 AM', N'1:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-07 00:00:00' AS SmallDateTime), N'9:00 AM', 4, 0, NULL, NULL, N'ENC 10 3/3/18 Note text', NULL, CAST(N'2018-03-03 11:48:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (28, 5001, 11, 3, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'1:20 PM', N'3:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-05 00:00:00' AS SmallDateTime), N'8:00 AM', 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 13:23:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (29, 5001, 11, 5, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'2:08 PM', N'3:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-09 00:00:00' AS SmallDateTime), N'2:00 PM', NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 14:11:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (30, 5001, 11, 8, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'2:18 PM', N'3:30 PM', 4, 0, 0, NULL, NULL, CAST(N'2018-03-07 00:00:00' AS SmallDateTime), N'8:30 AM', 1, 0, NULL, NULL, N'ENC 8 note 3/3 2:18PM', NULL, CAST(N'2018-03-03 14:21:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (31, 5001, 11, 3, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'3:07 PM', N'4:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-08 00:00:00' AS SmallDateTime), N'1:00 PM', 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 15:09:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (32, 5001, 11, 7, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'3:09 PM', N'5:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-07 00:00:00' AS SmallDateTime), N'9:00 AM', 3, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 15:11:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (33, 5001, 11, 3, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'3:26 PM', NULL, 2, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 15:28:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (34, 5001, 11, 2, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'3:27 PM', NULL, 2, 0, 0, NULL, NULL, CAST(N'2018-03-07 00:00:00' AS SmallDateTime), NULL, 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 15:28:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (35, 5001, 11, 2, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'3:34 PM', NULL, 1, 0, 0, NULL, NULL, CAST(N'2018-03-07 00:00:00' AS SmallDateTime), NULL, 3, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 15:35:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (36, 5001, 11, 2, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'3:40 PM', N'4:00 PM', 3, 0, 0, NULL, NULL, CAST(N'2018-03-05 00:00:00' AS SmallDateTime), NULL, 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 15:42:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (37, 5001, 11, 2, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'3:45 PM', N'4:00 PM', 4, 0, 0, NULL, NULL, CAST(N'2018-03-04 00:00:00' AS SmallDateTime), NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 15:46:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (38, 5001, 11, 4, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'3:52 PM', N'4:00 PM', 2, 0, 0, NULL, NULL, CAST(N'2018-03-08 00:00:00' AS SmallDateTime), NULL, 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 15:53:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (39, 5001, 11, 7, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'4:02 PM', N'5:00PM', 2, 0, 0, NULL, NULL, CAST(N'2018-03-07 00:00:00' AS SmallDateTime), NULL, 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 16:04:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (40, 5001, 11, 3, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'4:09 PM', NULL, 4, 0, 0, NULL, NULL, CAST(N'2018-03-07 00:00:00' AS SmallDateTime), NULL, 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 16:10:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (41, 5001, 11, 3, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'4:18 PM', NULL, 1, 0, 0, NULL, NULL, CAST(N'2018-03-08 00:00:00' AS SmallDateTime), NULL, 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 16:20:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (42, 5001, 12, 3, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'4:33 PM', NULL, 1, 0, 0, NULL, NULL, NULL, NULL, 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 16:34:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (43, 5001, 12, 9, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'4:40 PM', N'5:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-08 00:00:00' AS SmallDateTime), NULL, 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 16:42:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (44, 5001, 12, 2, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'4:53 PM', NULL, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 16:54:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (45, 5001, 12, 7, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'5:02 PM', NULL, 4, 0, 0, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 17:03:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (46, 5001, 12, 6, CAST(N'2018-03-03 00:00:00' AS SmallDateTime), N'5:11 PM', NULL, 1, 0, 0, NULL, NULL, NULL, NULL, 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03 17:12:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (47, 5001, 11, 5, CAST(N'2018-03-04 00:00:00' AS SmallDateTime), N'9:34 AM', N'10:00 AM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-08 00:00:00' AS SmallDateTime), N'9:00 AM', 4, 0, NULL, NULL, N'NNNNNNNNNNNNNN', NULL, CAST(N'2018-03-04 09:36:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (48, 5001, 11, 7, CAST(N'2018-03-04 00:00:00' AS SmallDateTime), N'9:39 AM', N'11:00 AM', 2, 0, 0, NULL, NULL, CAST(N'2018-03-06 00:00:00' AS SmallDateTime), N'8:00 AM', 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04 09:42:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (49, 5001, 11, 3, CAST(N'2018-03-04 00:00:00' AS SmallDateTime), N'9:45 AM', N'10:30 AM', 4, 0, 0, NULL, NULL, CAST(N'2018-03-08 00:00:00' AS SmallDateTime), N'7:00 AM', 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04 09:48:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (50, 5001, 13, 1, CAST(N'2018-03-04 00:00:00' AS SmallDateTime), N'12:53 PM', N'2:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-08 00:00:00' AS SmallDateTime), N'10:00 AM', 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04 12:55:00' AS SmallDateTime), N'User Input', CAST(N'2018-03-04 14:45:00' AS SmallDateTime), N'User Update', NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (51, 5001, 18, 1, CAST(N'2018-03-05 00:00:00' AS SmallDateTime), N'8:14 PM', N'9:00 PM', 4, 0, 0, NULL, N'Done', CAST(N'2018-03-10 00:00:00' AS SmallDateTime), N'9:00 AM', 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-05 20:16:00' AS SmallDateTime), N'User Input', CAST(N'2018-03-05 20:27:00' AS SmallDateTime), N'User Update', 2)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (52, 5001, 13, 2, CAST(N'2018-03-06 00:00:00' AS SmallDateTime), N'5:06 PM', NULL, 1, 0, 0, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-06 17:08:00' AS SmallDateTime), N'User Input', NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[tEncounter] OFF
SET IDENTITY_INSERT [dbo].[tInstantMessage] ON 

INSERT [dbo].[tInstantMessage] ([Id], [Subject], [Message], [ReceivedOn], [ReceivedFrom], [ReceivedBy], [IsRead], [ParentId]) VALUES (1, N'test Message 1', N'test Message 1', CAST(N'2018-03-18 20:08:30.130' AS DateTime), 5006, 5000, 1, NULL)
INSERT [dbo].[tInstantMessage] ([Id], [Subject], [Message], [ReceivedOn], [ReceivedFrom], [ReceivedBy], [IsRead], [ParentId]) VALUES (2, N'Re:test Message 1', N'test Message 1 reply', CAST(N'2018-03-18 20:09:11.183' AS DateTime), 5006, 5006, 1, 1)
INSERT [dbo].[tInstantMessage] ([Id], [Subject], [Message], [ReceivedOn], [ReceivedFrom], [ReceivedBy], [IsRead], [ParentId]) VALUES (1002, N'test compose', N'adasdas', CAST(N'2018-03-24 16:50:47.850' AS DateTime), 5006, 5000, 0, NULL)
SET IDENTITY_INSERT [dbo].[tInstantMessage] OFF
SET IDENTITY_INSERT [dbo].[tNavigationCycle] ON 

INSERT [dbo].[tNavigationCycle] ([NavigationCycleID], [FK_RecipientID], [FK_ProviderID], [FK_ReferralID], [CancerSite], [NSProblem], [HealthProgram], [HealthInsuranceStatus], [HealthInsurancePlan], [HealthInsurancePlanNumber], [ContactPrefDays], [ContactPrefHours], [ContactPrefHoursOther], [ContactPrefType], [SRIndentifierCodeGenerated], [FK_NavigatorID], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (1, 2, 1, NULL, 1, 1, 1, 0, NULL, NULL, N' ', N' ', NULL, NULL, NULL, 5000, NULL, CAST(N'2017-10-10 08:26:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigationCycle] ([NavigationCycleID], [FK_RecipientID], [FK_ProviderID], [FK_ReferralID], [CancerSite], [NSProblem], [HealthProgram], [HealthInsuranceStatus], [HealthInsurancePlan], [HealthInsurancePlanNumber], [ContactPrefDays], [ContactPrefHours], [ContactPrefHoursOther], [ContactPrefType], [SRIndentifierCodeGenerated], [FK_NavigatorID], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (2, 4, NULL, NULL, 1, 1, 1, 0, NULL, NULL, N' ', N' ', NULL, NULL, NULL, 5000, NULL, CAST(N'2017-10-17 11:41:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigationCycle] ([NavigationCycleID], [FK_RecipientID], [FK_ProviderID], [FK_ReferralID], [CancerSite], [NSProblem], [HealthProgram], [HealthInsuranceStatus], [HealthInsurancePlan], [HealthInsurancePlanNumber], [ContactPrefDays], [ContactPrefHours], [ContactPrefHoursOther], [ContactPrefType], [SRIndentifierCodeGenerated], [FK_NavigatorID], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (5, 8, 2, NULL, 1, 1, 1, 0, 1, N'123456', N'2,4', N'4', NULL, 1, NULL, 5000, NULL, CAST(N'2017-10-25 14:07:00' AS SmallDateTime), N'User Input', CAST(N'2017-11-15 15:27:00' AS SmallDateTime), N'User Update')
INSERT [dbo].[tNavigationCycle] ([NavigationCycleID], [FK_RecipientID], [FK_ProviderID], [FK_ReferralID], [CancerSite], [NSProblem], [HealthProgram], [HealthInsuranceStatus], [HealthInsurancePlan], [HealthInsurancePlanNumber], [ContactPrefDays], [ContactPrefHours], [ContactPrefHoursOther], [ContactPrefType], [SRIndentifierCodeGenerated], [FK_NavigatorID], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (6, 9, NULL, NULL, 1, 1, 1, 0, NULL, NULL, N' ', N' ', NULL, NULL, NULL, 5000, NULL, CAST(N'2018-01-10 14:40:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigationCycle] ([NavigationCycleID], [FK_RecipientID], [FK_ProviderID], [FK_ReferralID], [CancerSite], [NSProblem], [HealthProgram], [HealthInsuranceStatus], [HealthInsurancePlan], [HealthInsurancePlanNumber], [ContactPrefDays], [ContactPrefHours], [ContactPrefHoursOther], [ContactPrefType], [SRIndentifierCodeGenerated], [FK_NavigatorID], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (7, 10, NULL, NULL, 1, 1, 1, 0, NULL, NULL, N' ', N' ', NULL, NULL, NULL, 5000, NULL, CAST(N'2018-01-10 14:55:00' AS SmallDateTime), N'User Input', NULL, NULL)
SET IDENTITY_INSERT [dbo].[tNavigationCycle] OFF
SET IDENTITY_INSERT [dbo].[tNavigator] ON 

INSERT [dbo].[tNavigator] ([NavigatorID], [DomainName], [Type], [LastName], [FirstName], [Address1], [Address2], [City], [State], [Zip], [BusinessTelephone], [MobileTelephone], [Email], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Region]) VALUES (5000, N'DHSINTRA\bbuchake', 1, N'Buchake', N'Bilwa', N'664 Cygnus Ln', NULL, N'Foster City', N'CA', N'94404', N'9167868942', NULL, N'bilwa.buchake@dhcs.ca.gov', NULL, CAST(N'2017-09-18 13:26:00' AS SmallDateTime), N'User Input', CAST(N'2017-10-11 17:01:00' AS SmallDateTime), N'User Input', 4)
INSERT [dbo].[tNavigator] ([NavigatorID], [DomainName], [Type], [LastName], [FirstName], [Address1], [Address2], [City], [State], [Zip], [BusinessTelephone], [MobileTelephone], [Email], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Region]) VALUES (5001, N'DHSINTRA\grichey', 1, N'richey', N'gordon', N'1501 Capitol Ave', NULL, N'sacramento', N'CA', N'95818', N'9163417342', NULL, N'grichey@dhcs.ca.gov', NULL, CAST(N'2018-02-22 14:59:00' AS SmallDateTime), N'User Input', NULL, NULL, 5)
INSERT [dbo].[tNavigator] ([NavigatorID], [DomainName], [Type], [LastName], [FirstName], [Address1], [Address2], [City], [State], [Zip], [BusinessTelephone], [MobileTelephone], [Email], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Region]) VALUES (5002, N'DHSINTRA\rperumal', 1, N'Mani', N'Retna', N'1501 Capitol Ave', NULL, N'sacramento', N'CA', N'95818', N'9163417342', NULL, N'grichey@dhcs.ca.gov', NULL, CAST(N'2018-02-22 14:59:00' AS SmallDateTime), N'User Input', NULL, NULL, 5)
INSERT [dbo].[tNavigator] ([NavigatorID], [DomainName], [Type], [LastName], [FirstName], [Address1], [Address2], [City], [State], [Zip], [BusinessTelephone], [MobileTelephone], [Email], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Region]) VALUES (5006, N'dell\user', 1, N'Thomas', N'Anish', N'1501 Capitol', NULL, N'Foster City', N'CA', N'95818', N'9163417341', NULL, N'a@a.com', NULL, CAST(N'2018-03-22 00:00:00' AS SmallDateTime), N'User Input', NULL, NULL, 4)
SET IDENTITY_INSERT [dbo].[tNavigator] OFF
SET IDENTITY_INSERT [dbo].[tNavigatorLanguages] ON 

INSERT [dbo].[tNavigatorLanguages] ([NavigatorLanguageID], [FK_NavigatorID], [LanguageCode], [SpeakingScore], [ListeningScore], [ReadingScore], [WritingScore], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (3, 5000, 28, 3, 3, 3, 3, NULL, CAST(N'2017-09-27 15:58:00' AS SmallDateTime), N'User Input', CAST(N'2017-10-10 16:39:00' AS SmallDateTime), N'User Update')
INSERT [dbo].[tNavigatorLanguages] ([NavigatorLanguageID], [FK_NavigatorID], [LanguageCode], [SpeakingScore], [ListeningScore], [ReadingScore], [WritingScore], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (4, 5000, 14, 3, 3, 4, 2, NULL, CAST(N'2017-10-09 16:07:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigatorLanguages] ([NavigatorLanguageID], [FK_NavigatorID], [LanguageCode], [SpeakingScore], [ListeningScore], [ReadingScore], [WritingScore], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (5, 5000, 13, 2, 3, 3, 2, NULL, CAST(N'2017-10-10 15:12:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigatorLanguages] ([NavigatorLanguageID], [FK_NavigatorID], [LanguageCode], [SpeakingScore], [ListeningScore], [ReadingScore], [WritingScore], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (6, 5000, 7, 2, 2, 2, 2, NULL, CAST(N'2017-10-10 16:40:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigatorLanguages] ([NavigatorLanguageID], [FK_NavigatorID], [LanguageCode], [SpeakingScore], [ListeningScore], [ReadingScore], [WritingScore], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (8, 5000, 20, 2, 2, 2, 2, NULL, CAST(N'2017-10-16 14:35:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigatorLanguages] ([NavigatorLanguageID], [FK_NavigatorID], [LanguageCode], [SpeakingScore], [ListeningScore], [ReadingScore], [WritingScore], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (9, 5000, 17, 2, 2, 2, 2, NULL, CAST(N'2017-10-16 14:38:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigatorLanguages] ([NavigatorLanguageID], [FK_NavigatorID], [LanguageCode], [SpeakingScore], [ListeningScore], [ReadingScore], [WritingScore], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (10, 0, 1, 0, 0, 0, 0, NULL, CAST(N'2018-03-26 23:28:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigatorLanguages] ([NavigatorLanguageID], [FK_NavigatorID], [LanguageCode], [SpeakingScore], [ListeningScore], [ReadingScore], [WritingScore], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (11, 0, 2, 1, 3, 2, 3, NULL, CAST(N'2018-03-26 23:29:00' AS SmallDateTime), N'User Input', NULL, NULL)
SET IDENTITY_INSERT [dbo].[tNavigatorLanguages] OFF
SET IDENTITY_INSERT [dbo].[tNavigatorTraining] ON 

INSERT [dbo].[tNavigatorTraining] ([TrainingID], [FK_NavigatorID], [CourseName], [Organization], [CompletionDt], [Certificate], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [BeginDt], [CMEAwarded]) VALUES (1, 5000, N'ABC Course', N'ABC Org', CAST(N'2017-09-07 00:00:00' AS SmallDateTime), 1, NULL, CAST(N'2017-09-27 16:01:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tNavigatorTraining] ([TrainingID], [FK_NavigatorID], [CourseName], [Organization], [CompletionDt], [Certificate], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [BeginDt], [CMEAwarded]) VALUES (2, 5000, N'XYZ Course', N'XYZ Org', CAST(N'2017-10-01 00:00:00' AS SmallDateTime), 1, NULL, CAST(N'2017-10-10 15:13:00' AS SmallDateTime), N'User Input', CAST(N'2017-10-10 16:40:00' AS SmallDateTime), N'User Update', NULL, NULL)
INSERT [dbo].[tNavigatorTraining] ([TrainingID], [FK_NavigatorID], [CourseName], [Organization], [CompletionDt], [Certificate], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [BeginDt], [CMEAwarded]) VALUES (3, 5000, N'LMN Course', N'LMN Org', CAST(N'2017-10-02 00:00:00' AS SmallDateTime), 0, NULL, CAST(N'2017-10-10 16:41:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tNavigatorTraining] ([TrainingID], [FK_NavigatorID], [CourseName], [Organization], [CompletionDt], [Certificate], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [BeginDt], [CMEAwarded]) VALUES (4, 5000, N'ABC Course', N'ABC Org', CAST(N'2017-10-16 00:00:00' AS SmallDateTime), 0, NULL, CAST(N'2017-10-16 14:38:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tNavigatorTraining] OFF
SET IDENTITY_INSERT [dbo].[tNSProvider] ON 

INSERT [dbo].[tNSProvider] ([NSProviderID], [PCPName], [PCP_NPI], [PCP_Owner], [PCP_Location], [PCPAddress1], [PCPAddress2], [PCPCity], [PCPState], [PCPZip], [PCPContactFName], [PCPContactLName], [PCPContactTitle], [PCPContactTelephone], [PCPContactEmail], [Medical], [MedicalSpecialty], [MedicalSpecialtyOther], [ManagedCarePlan], [ManagedCarePlanOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EWCPCP]) VALUES (1, N'EWC', N'0099211928', N'01', N'001', N'1501 Capitol Ave', NULL, N'Sacramento', N'CA', N'95814', N'Bilwa', N'Buchake', N'SPA', N'(916) 786-', N'bilwa.buchake@dhcs.ca.gov', 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-10-10 08:26:00' AS SmallDateTime), N'User Input', NULL, NULL, 0)
INSERT [dbo].[tNSProvider] ([NSProviderID], [PCPName], [PCP_NPI], [PCP_Owner], [PCP_Location], [PCPAddress1], [PCPAddress2], [PCPCity], [PCPState], [PCPZip], [PCPContactFName], [PCPContactLName], [PCPContactTitle], [PCPContactTelephone], [PCPContactEmail], [Medical], [MedicalSpecialty], [MedicalSpecialtyOther], [ManagedCarePlan], [ManagedCarePlanOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EWCPCP]) VALUES (2, N'EWCPCP', N'0099211928', N'01', N'001', N'1500 Capitol Ave', N'1111', N'Sacramento', N'CA', N'95814', N'TestContact', N'TestLContact', N'ABC', N'3456789087', N'contacttest@dhcs.ca.gov', 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-10-30 13:45:00' AS SmallDateTime), N'User Input', CAST(N'2017-11-15 15:26:00' AS SmallDateTime), N'User Update', 1)
SET IDENTITY_INSERT [dbo].[tNSProvider] OFF
SET IDENTITY_INSERT [dbo].[tRace] ON 

INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (2, 2, 0, N' ', NULL, CAST(N'2017-10-10 08:26:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (3, 3, 0, N' ', NULL, CAST(N'2017-10-17 11:20:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (4, 4, 0, N' ', NULL, CAST(N'2017-10-17 11:41:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (5, 5, 1, N' ', NULL, CAST(N'2017-10-19 16:05:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (6, 6, 0, N' ', NULL, CAST(N'2017-10-24 10:02:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (7, 7, 0, N' ', NULL, CAST(N'2017-10-24 10:11:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (36, 8, 3, N' ', NULL, CAST(N'2017-11-15 15:26:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (37, 9, 1, N' ', NULL, CAST(N'2018-01-10 14:40:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (38, 10, 1, N' ', NULL, CAST(N'2018-01-10 14:54:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (39, 14, 6, N' ', NULL, CAST(N'2018-03-04 15:18:00' AS SmallDateTime), N'User Input', NULL, NULL)
SET IDENTITY_INSERT [dbo].[tRace] OFF
INSERT [dbo].[trAssistanceCommunication] ([CommunicationCode], [CommunicationDescription]) VALUES (1, N'Phone Call')
INSERT [dbo].[trAssistanceCommunication] ([CommunicationCode], [CommunicationDescription]) VALUES (2, N'Face-to-Face')
INSERT [dbo].[trAssistanceCommunication] ([CommunicationCode], [CommunicationDescription]) VALUES (3, N'Translate')
INSERT [dbo].[trAssistanceCommunication] ([CommunicationCode], [CommunicationDescription]) VALUES (4, N'Email')
INSERT [dbo].[trAssistanceCommunication] ([CommunicationCode], [CommunicationDescription]) VALUES (5, N'Other')
INSERT [dbo].[trAssistanceEWCEnrollee] ([EWCEnrolleeCode], [EWCEnrolleeDescription]) VALUES (1, N'EWC Program Information')
INSERT [dbo].[trAssistanceEWCEnrollee] ([EWCEnrolleeCode], [EWCEnrolleeDescription]) VALUES (2, N'Courtesy Enrollment procedure question')
INSERT [dbo].[trAssistanceEWCEnrollee] ([EWCEnrolleeCode], [EWCEnrolleeDescription]) VALUES (3, N'Request Navigation (e.g., BCCTP or other)')
INSERT [dbo].[trAssistanceEWCEnrollee] ([EWCEnrolleeCode], [EWCEnrolleeDescription]) VALUES (4, N'Invitation to meeting or conference')
INSERT [dbo].[trAssistanceEWCEnrollee] ([EWCEnrolleeCode], [EWCEnrolleeDescription]) VALUES (5, N'Other or Notes for Selected Problem/Issue')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (1, N'Recipient Issue')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (2, N'Billing Issue')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (3, N'DETEC or data entry issue')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (4, N'Clarification of Program or Clinical Issue')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (5, N'Request for Case Management Navigation')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (6, N'PCP Enrollment/Disenrollment, Addition/Removal of unregistered or Satellite Facility')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (7, N'Other or Notes for Selected Problem/Issue')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (21, N'How to bill')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (22, N'Claims denial')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (31, N'Training request, DETEC')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (32, N'Error Remediation')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (41, N'Covered benefits')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (42, N'Program policy')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (43, N'Training Request, CPPI')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (44, N'Training Request, Clinical Algorithm')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (1, N'Contacting an EWC provider, (i,e., phone number or wrong address; no longer in EWC)')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (2, N'Obtaining an appointment with an EWC provider (i,e., provider not taking new clients or not taking EWC clients)')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (3, N'Billing issue i,e., recipient has been billed for services')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (4, N'Access to a specialist or getting a follow-up appointment with a specialist')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (5, N'Provider or staff behavior (i,e., inappropriate provider and staff behavior)')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (6, N'Program eligibility/enrollment question')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (7, N'Request for Navigation')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (8, N'Other or Notes for Selected Problem/Issue')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (61, N'EWC')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (62, N'BCCTP')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (63, N'Medi Cal')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (64, N'Other')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (1, N'Recipient Issue')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (2, N'Billing Issue')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (3, N'Clarification of program or clinical issue')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (4, N'Request for Navigation')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (5, N'Other or Notes for Selected Problem/Issue')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (21, N'How to bill')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (22, N'Claims denial')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (31, N'Covered benefits')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (32, N'Program policy')
INSERT [dbo].[trAssistanceRequestor] ([RequestorCode], [RequestorDescription]) VALUES (1, N'EWC Recipient or designated representative')
INSERT [dbo].[trAssistanceRequestor] ([RequestorCode], [RequestorDescription]) VALUES (2, N'EWC PCP')
INSERT [dbo].[trAssistanceRequestor] ([RequestorCode], [RequestorDescription]) VALUES (3, N'Referral Provider')
INSERT [dbo].[trAssistanceRequestor] ([RequestorCode], [RequestorDescription]) VALUES (4, N'Program or Community Partner, Public, Potential EWC Enrollees')
INSERT [dbo].[trAssistanceRequestor] ([RequestorCode], [RequestorDescription]) VALUES (5, N'Other')
INSERT [dbo].[trAssistanceResolution] ([ResolutionCode], [ResolutionDescription]) VALUES (1, N'Answered question')
INSERT [dbo].[trAssistanceResolution] ([ResolutionCode], [ResolutionDescription]) VALUES (2, N'Accepted Navigation referral')
INSERT [dbo].[trAssistanceResolution] ([ResolutionCode], [ResolutionDescription]) VALUES (3, N'Sent informtion/made referral, specify')
INSERT [dbo].[trAssistanceResolution] ([ResolutionCode], [ResolutionDescription]) VALUES (4, N'PCP/Referral provider TA')
INSERT [dbo].[trAssistanceResolution] ([ResolutionCode], [ResolutionDescription]) VALUES (5, N'PCP/Referral provider referred to QAP online or Live Training')
INSERT [dbo].[trAssistanceResolution] ([ResolutionCode], [ResolutionDescription]) VALUES (6, N'PCP Training')
INSERT [dbo].[trAssistanceResolution] ([ResolutionCode], [ResolutionDescription]) VALUES (7, N'Unable to resolve, why')
INSERT [dbo].[trAssistanceResolution] ([ResolutionCode], [ResolutionDescription]) VALUES (8, N'Other')
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (1, N'No car/access to or can’t afford public transportation', 1)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (2, N'Conflict between PCP and work schedules; can’t take time off work', 1)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (3, N'Lack of coordination between services or service locations', 1)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (4, N'Needs child or elder-care', 1)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (5, N'Disabled', 1)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (6, N'Difficulty speaking or reading English', 2)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (7, N'Cultural beliefs inconsistent with exam, test, procedure or treatment', 2)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (8, N'Difficulty communicating needs or desires to PCP or staff', 2)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (9, N'No health insurance', 3)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (10, N'Can’t afford co-pay /share-of-cost/ deductible', 3)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (11, N'Health not a priority', 4)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (12, N'Confused/doesn’t understand', 4)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (13, N'Difficulty completing forms; gathering additional information', 4)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (14, N'Problem contacting PCP (i.e., phone number or address incorrect)', 5)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (15, N'Problem making appointment (i.e., not accepting new/program clients)', 5)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (16, N'Billing issue (i.e., client billed for services)', 5)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (17, N'Problem, access or follow-up appointment with specialist', 5)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (18, N'Problem, PCP or staff behavior', 5)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (19, N'Program eligibility question', 5)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (20, N'Avoidance or refusal of care due to fear or stress', 6)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (21, N'Lack of family or social support', 6)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (22, N'Unstable living situation / homeless', 6)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (23, N'Other', 7)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (1, N'Find alternative PCP', 1)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (2, N'Discuss/identity public/personal transportation options', 1)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (3, N'Discuss options for child/elder care; or identify PCP with appropriate service', 1)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (4, N'Assist coordinating appointments/travel; or arrival of records/information', 1)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (5, N'Coach on how to or communicate with PCP about special needs', 1)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (6, N'Refer to culturally competent/linguistically appropriate PCP or with translation service', 2)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (7, N'Coach on how to or communicate with PCP about special needs', 2)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (8, N'If ineligible for Covered California, identify appropriate safety net program', 3)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (9, N'Educate - cancer, healthcare system, test, procedures, diagnoses and/or treatments', 3)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (10, N'Refer to culturally competent/linguistically appropriate PCP or with translation service', 4)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (11, N'Assist in completing forms; explain additional information needed', 4)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (12, N'Find correct phone number/address or PCP accepting clients, then report to EWC, PSU', 4)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (13, N'Investigate, billing or behavior issue, then report to EWC, CSU', 5)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (14, N'Assess eligibility', 5)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (15, N'Educate - cancer, healthcare system, test, procedures, diagnoses and/or treatments', 5)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (16, N'Refer to support groups or counseling services; or appropriate authority', 6)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (17, N'Other', 6)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (18, N'Other Solution', 7)
INSERT [dbo].[trBarrierType] ([BarrierTypeCode], [BarrierTypeDescription]) VALUES (1, N'Transportation and Logistics')
INSERT [dbo].[trBarrierType] ([BarrierTypeCode], [BarrierTypeDescription]) VALUES (2, N'Language, Culture and Literacy')
INSERT [dbo].[trBarrierType] ([BarrierTypeCode], [BarrierTypeDescription]) VALUES (3, N'Financial')
INSERT [dbo].[trBarrierType] ([BarrierTypeCode], [BarrierTypeDescription]) VALUES (4, N'Health Literacy or Education')
INSERT [dbo].[trBarrierType] ([BarrierTypeCode], [BarrierTypeDescription]) VALUES (5, N'PCP or Referral PCP')
INSERT [dbo].[trBarrierType] ([BarrierTypeCode], [BarrierTypeDescription]) VALUES (6, N'Social Support or Distress')
INSERT [dbo].[trBarrierType] ([BarrierTypeCode], [BarrierTypeDescription]) VALUES (7, N'Other')
INSERT [dbo].[trCancerSite] ([CancerSiteCode], [CancerSiteName]) VALUES (1, N'Breast Cancer')
INSERT [dbo].[trCancerSite] ([CancerSiteCode], [CancerSiteName]) VALUES (2, N'Cervical Cancer')
INSERT [dbo].[trCancerSite] ([CancerSiteCode], [CancerSiteName]) VALUES (3, N'Breast and Cervical')
INSERT [dbo].[trCancerSite] ([CancerSiteCode], [CancerSiteName]) VALUES (4, N'Other')
INSERT [dbo].[trCaregiverApproval] ([CaregiverApprovalCode], [CaregiverApprovalDescription]) VALUES (71, N'Translate')
INSERT [dbo].[trCaregiverApproval] ([CaregiverApprovalCode], [CaregiverApprovalDescription]) VALUES (72, N'Make appointments')
INSERT [dbo].[trCaregiverApproval] ([CaregiverApprovalCode], [CaregiverApprovalDescription]) VALUES (73, N'Take messages')
INSERT [dbo].[trCaregiverApproval] ([CaregiverApprovalCode], [CaregiverApprovalDescription]) VALUES (74, N'Co-decision maker')
INSERT [dbo].[trCaregiverApproval] ([CaregiverApprovalCode], [CaregiverApprovalDescription]) VALUES (75, N'Other')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (1, N'Afghanistan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (2, N'Aland Islands (Finland)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (3, N'Albania')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (4, N'Algeria')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (5, N'American Samoa (USA)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (6, N'Andorra')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (7, N'Angola')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (8, N'Anguilla (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (9, N'Antigua and Barbuda')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (10, N'Argentina')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (11, N'Armenia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (12, N'Aruba (Netherlands)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (13, N'Australia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (14, N'Austria')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (15, N'Azerbaijan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (16, N'Bahamas')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (17, N'Bahrain')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (18, N'Bangladesh')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (19, N'Barbados')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (20, N'Belarus')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (21, N'Belgium')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (22, N'Belize')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (23, N'Benin')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (24, N'Bermuda (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (25, N'Bhutan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (26, N'Bolivia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (27, N'Bosnia and Herzegovina')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (28, N'Botswana')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (29, N'Brazil')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (30, N'British Virgin Islands (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (31, N'Brunei')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (32, N'Bulgaria')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (33, N'Burkina Faso')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (34, N'Burma')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (35, N'Burundi')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (36, N'Cambodia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (37, N'Cameroon')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (38, N'Canada')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (39, N'Cape Verde')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (40, N'Caribbean Netherlands (Netherlands)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (41, N'Cayman Islands (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (42, N'Central African Republic')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (43, N'Chad')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (44, N'Chile')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (45, N'China')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (46, N'Christmas Island (Australia)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (47, N'Cocos (Keeling) Islands (Australia)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (48, N'Colombia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (49, N'Comoros')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (50, N'Cook Islands (NZ)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (51, N'Costa Rica')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (52, N'Croatia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (53, N'Cuba')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (54, N'Curacao (Netherlands)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (55, N'Cyprus')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (56, N'Czech Republic')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (57, N'Democratic Republic of the Congo')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (58, N'Denmark')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (59, N'Djibouti')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (60, N'Dominica')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (61, N'Dominican Republic')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (62, N'Ecuador')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (63, N'Egypt')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (64, N'El Salvador')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (65, N'Equatorial Guinea')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (66, N'Eritrea')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (67, N'Estonia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (68, N'Ethiopia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (69, N'Falkland Islands (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (70, N'Faroe Islands (Denmark)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (71, N'Federated States of Micronesia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (72, N'Fiji')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (73, N'Finland')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (74, N'France')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (75, N'French Guiana (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (76, N'French Polynesia (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (77, N'Gabon')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (78, N'Gambia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (79, N'Georgia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (80, N'Germany')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (81, N'Ghana')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (82, N'Gibraltar (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (83, N'Greece')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (84, N'Greenland (Denmark)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (85, N'Grenada')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (86, N'Guadeloupe (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (87, N'Guam (USA)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (88, N'Guatemala')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (89, N'Guernsey (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (90, N'Guinea')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (91, N'Guinea-Bissau')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (92, N'Guyana')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (93, N'Haiti')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (94, N'Honduras')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (95, N'Hong Kong (China)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (96, N'Hungary')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (97, N'Iceland')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (98, N'India')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (99, N'Indonesia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (100, N'Iran')
GO
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (101, N'Iraq')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (102, N'Ireland')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (103, N'Isle of Man (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (104, N'Israel')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (105, N'Italy')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (106, N'Ivory Coast')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (107, N'Jamaica')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (108, N'Japan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (109, N'Jersey (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (110, N'Jordan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (111, N'Kazakhstan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (112, N'Kenya')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (113, N'Kiribati')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (114, N'Kosovo')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (115, N'Kuwait')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (116, N'Kyrgyzstan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (117, N'Laos')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (118, N'Latvia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (119, N'Lebanon')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (120, N'Lesotho')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (121, N'Liberia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (122, N'Libya')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (123, N'Liechtenstein')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (124, N'Lithuania')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (125, N'Luxembourg')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (126, N'Macau (China)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (127, N'Macedonia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (128, N'Madagascar')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (129, N'Malawi')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (130, N'Malaysia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (131, N'Maldives')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (132, N'Mali')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (133, N'Malta')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (134, N'Marshall Islands')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (135, N'Martinique (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (136, N'Mauritania')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (137, N'Mauritius')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (138, N'Mayotte (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (139, N'Mexico')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (140, N'Moldov')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (141, N'Monaco')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (142, N'Mongolia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (143, N'Montenegro')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (144, N'Montserrat (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (145, N'Morocco')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (146, N'Mozambique')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (147, N'Namibia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (148, N'Nauru')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (149, N'Nepal')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (150, N'Netherlands')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (151, N'New Caledonia (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (152, N'New Zealand')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (153, N'Nicaragua')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (154, N'Niger')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (155, N'Nigeria')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (156, N'Niue (NZ)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (157, N'Norfolk Island (Australia)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (158, N'North Korea')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (159, N'Northern Mariana Islands (USA)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (160, N'Norway')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (161, N'Oman')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (162, N'Pakistan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (163, N'Palau')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (164, N'Palestine')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (165, N'Panama')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (166, N'Papua New Guinea')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (167, N'Paraguay')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (168, N'Peru')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (169, N'Philippines')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (170, N'Pitcairn Islands (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (171, N'Poland')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (172, N'Portugal')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (173, N'Puerto Rico')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (174, N'Qatar')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (175, N'Republic of the Congo')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (176, N'Reunion (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (177, N'Romania')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (178, N'Russia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (179, N'Rwanda')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (180, N'Saint Barthelemy (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (181, N'Saint Helena, Ascension and Tristan da Cunha (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (182, N'Saint Kitts and Nevis')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (183, N'Saint Lucia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (184, N'Saint Martin (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (185, N'Saint Pierre and Miquelon (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (186, N'Saint Vincent and the Grenadines')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (187, N'Samoa')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (188, N'San Marino')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (189, N'Sao Tom and Principe')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (190, N'Saudi Arabia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (191, N'Senegal')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (192, N'Serbia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (193, N'Seychelles')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (194, N'Sierra Leone')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (195, N'Singapore')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (196, N'Sint Maarten (Netherlands)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (197, N'Slovakia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (198, N'Slovenia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (199, N'Solomon Islands')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (200, N'Somalia')
GO
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (201, N'South Africa')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (202, N'South Korea')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (203, N'South Sudan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (204, N'Spain')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (205, N'Sri Lanka')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (206, N'Sudan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (207, N'Suriname')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (208, N'Svalbard and Jan Mayen (Norway)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (209, N'Swaziland')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (210, N'Sweden')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (211, N'Switzerland')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (212, N'Syria')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (213, N'Taiwan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (214, N'Tajikistan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (215, N'Tanzania')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (216, N'Thailand')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (217, N'Timor-Leste')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (218, N'Togo')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (219, N'Tokelau (NZ)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (220, N'Tonga')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (221, N'Trinidad and Tobago')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (222, N'Tunisia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (223, N'Turkey')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (224, N'Turkmenistan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (225, N'Turks and Caicos Islands (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (226, N'Tuvalu')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (227, N'Uganda')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (228, N'Ukraine')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (229, N'United Arab Emirates')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (230, N'United Kingdom')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (231, N'United States')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (232, N'United States Virgin Islands (USA)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (233, N'Uruguay')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (234, N'Uzbekistan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (235, N'Vanuatu')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (236, N'Vatican City')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (237, N'Venezuela')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (238, N'Vietnam')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (239, N'Wallis and Futuna (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (240, N'Western Sahara')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (241, N'Yemen')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (242, N'Zambia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (243, N'Zimbabwe')
INSERT [dbo].[trEncounterPurpose] ([EncounterPurposeCode], [EncounterPurposeDescription]) VALUES (1, N'Assessment')
INSERT [dbo].[trEncounterPurpose] ([EncounterPurposeCode], [EncounterPurposeDescription]) VALUES (2, N'Check-in')
INSERT [dbo].[trHealthInsurance] ([HealthInsuranceCode], [HealthInsuranceName]) VALUES (1, N'Medi-Cal')
INSERT [dbo].[trHealthInsurance] ([HealthInsuranceCode], [HealthInsuranceName]) VALUES (2, N'Adventist Health Plan')
INSERT [dbo].[trHealthInsurance] ([HealthInsuranceCode], [HealthInsuranceName]) VALUES (3, N'Aetna Health of California, Inc.')
INSERT [dbo].[trHealthInsurance] ([HealthInsuranceCode], [HealthInsuranceName]) VALUES (4, N'Kaiser Permanente Hospitals of California - San luis Obispo Town Center Medical Clinic')
INSERT [dbo].[trHealthProgram] ([HealthProgramCode], [HealthProgramName]) VALUES (1, N'EWC')
INSERT [dbo].[trHealthProgram] ([HealthProgramCode], [HealthProgramName]) VALUES (2, N'BCCTP')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (11, N'Recipient has specific or complex medical issue, including previous diagnosis of breast and/or cervical cancer')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (12, N'Recipient has complex insurance/EWC billing issues')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (13, N'Recipient expressed difficulty with PCP/staff behavior')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (14, N'CC is a more appropriate language/social/cultural match')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (21, N'Recipient requires only basic health, breast and/or cervical cancer education for navigation')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (22, N'HE is a more appropriate language/social/cultural match')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (31, N'Recipient moves to other region and wishes to change Navigator')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (32, N'Other region has more appropriate or specific resources')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (33, N'HE/CC is a more appropriate language/social/cultural match')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (34, N'Region has reached caseload maximum or has strained staffing')
INSERT [dbo].[trNSProblem] ([NSProblemCode], [NSProblemDescription]) VALUES (1, N'Screening')
INSERT [dbo].[trNSProblem] ([NSProblemCode], [NSProblemDescription]) VALUES (2, N'Case Management')
INSERT [dbo].[trNSProblem] ([NSProblemCode], [NSProblemDescription]) VALUES (3, N'Treatment Referral')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (1, N'Asian Languages')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (2, N'Cambodian                     ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (3, N'Cantonese                     ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (5, N'Farsi                         ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (7, N'French                        ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (8, N'German                        ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (9, N'Hmong                         ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (10, N'Italian                       ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (11, N'Korean                        ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (13, N'Punjabi                       ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (14, N'Spanish                       ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (15, N'Tagalog                       ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (16, N'Vietnamese                    ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (17, N'Yiddish                       ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (18, N'Hebrew                        ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (20, N'Russian                       ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (21, N'Japanese                      ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (22, N'Mandarin                      ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (24, N'American Sign                 ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (25, N'Arabic                        ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (26, N'Armenian                      ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (28, N'Hindi                         ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (31, N'Portuguese                    ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (32, N'Thai                          ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (33, N'Urdu                          ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (34, N'Greek                         ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (35, N'Afrikaans')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (36, N'Albanian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (37, N'Aymara')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (38, N'Bengali')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (39, N'Bulgarian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (40, N'Cakchiquel')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (41, N'Catalan')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (42, N'Cebuano')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (43, N'Croatian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (44, N'Czech')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (45, N'Danish')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (46, N'Dutch')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (47, N'Egyptian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (48, N'Esperanto')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (49, N'Finnish')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (50, N'Galego')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (51, N'Haitian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (52, N'Hausa')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (53, N'Hungarian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (54, N'Icelandic')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (55, N'Indonesian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (56, N'Irish')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (57, N'Khmer')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (58, N'Lao')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (59, N'Latin')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (60, N'Latvian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (61, N'Malay')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (62, N'Maori')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (63, N'Marathi')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (64, N'Mixteco')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (65, N'Navajo')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (66, N'Nepali')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (67, N'Norwegian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (68, N'Polish')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (69, N'Rarotongan')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (70, N'Romanian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (71, N'Samoan')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (72, N'Serbo-Croatian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (73, N'Slovak')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (74, N'Somali')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (75, N'Swahili')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (76, N'Swedish')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (77, N'Tahitian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (78, N'Tamil')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (79, N'Tibetan')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (80, N'Tongan')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (81, N'Turkish')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (82, N'Ukrainian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (83, N'Welsh')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (84, N'Translation Service')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (85, N'Burmese')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (86, N'Kanjobal')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (87, N'Lau')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (88, N'Mien')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (89, N'Sinhalese')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (90, N'Sudanese')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (91, N'Taiwanese')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (92, N'Teo-Chow')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (93, N'Triki')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (99, N'English')
INSERT [dbo].[trRace] ([RaceCode], [RaceName]) VALUES (1, N'White')
INSERT [dbo].[trRace] ([RaceCode], [RaceName]) VALUES (2, N'African American')
INSERT [dbo].[trRace] ([RaceCode], [RaceName]) VALUES (3, N'Asian')
INSERT [dbo].[trRace] ([RaceCode], [RaceName]) VALUES (4, N'Pacific Islander')
INSERT [dbo].[trRace] ([RaceCode], [RaceName]) VALUES (5, N'American Indian/Alaskan Native')
INSERT [dbo].[trRace] ([RaceCode], [RaceName]) VALUES (6, N'Other')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (11, N'Asian Indian')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (12, N'Cambodian')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (13, N'Chinese')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (14, N'Filipino')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (15, N'Hmong')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (16, N'Japanese')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (17, N'Korean')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (18, N'Laotian')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (19, N'Vietnamese')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (20, N'Other Asian')
INSERT [dbo].[trRacePacIslander] ([RacePacIslanderCode], [RacePacIslanderName]) VALUES (21, N'Native Hawaiian')
INSERT [dbo].[trRacePacIslander] ([RacePacIslanderCode], [RacePacIslanderName]) VALUES (22, N'Guamanian or Chamorro')
INSERT [dbo].[trRacePacIslander] ([RacePacIslanderCode], [RacePacIslanderName]) VALUES (23, N'Samoan')
INSERT [dbo].[trRacePacIslander] ([RacePacIslanderCode], [RacePacIslanderName]) VALUES (24, N'Other Pacific Islander')
SET IDENTITY_INSERT [dbo].[tScreeningNav] ON 

INSERT [dbo].[tScreeningNav] ([SN_ID], [FK_NavigatorID], [SN_CODE1], [SN_CODE2], [LastName], [FirstName], [DOB], [Address1], [Address2], [City], [State], [Zip], [HomeTelephone], [Cellphone], [Email], [Computer], [TextMessage], [DateOfContact1], [ApptScreen1], [SvcDate1], [SvcType1], [Response1], [DateOfContact2], [ApptScreen2], [SvcDate2], [SvcType2], [Response2], [NSEnrollment], [EnrollmentDate], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (2, 5000, N'04BB111317A', 1, N'Kopti', N'Mary', CAST(N'1960-09-01 00:00:00' AS SmallDateTime), NULL, NULL, NULL, NULL, NULL, NULL, N'1234567890', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-11-13 13:50:00' AS SmallDateTime), N'User Input', CAST(N'2017-11-13 16:32:00' AS SmallDateTime), N'User Input')
INSERT [dbo].[tScreeningNav] ([SN_ID], [FK_NavigatorID], [SN_CODE1], [SN_CODE2], [LastName], [FirstName], [DOB], [Address1], [Address2], [City], [State], [Zip], [HomeTelephone], [Cellphone], [Email], [Computer], [TextMessage], [DateOfContact1], [ApptScreen1], [SvcDate1], [SvcType1], [Response1], [DateOfContact2], [ApptScreen2], [SvcDate2], [SvcType2], [Response2], [NSEnrollment], [EnrollmentDate], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (3, 5000, N'04BB111317B', 1, N'Herg', N'Jessica', CAST(N'1956-10-01 00:00:00' AS SmallDateTime), N'123 First Street', N'Suite B', N'Sacramento', N'CA', N'95814', N'0987654321', N'1234567890', N'jess@gmail.com', 1, N'U', CAST(N'2017-10-10 00:00:00' AS SmallDateTime), N'A', CAST(N'2017-10-15 00:00:00' AS SmallDateTime), N'C ', N'NO', CAST(N'2017-10-12 00:00:00' AS SmallDateTime), N'S', CAST(N'2017-10-15 00:00:00' AS SmallDateTime), N'C ', N'NR', N'Y', CAST(N'2017-10-01 00:00:00' AS SmallDateTime), NULL, CAST(N'2017-11-13 21:03:00' AS SmallDateTime), N'User Input', NULL, NULL)
SET IDENTITY_INSERT [dbo].[tScreeningNav] OFF
SET IDENTITY_INSERT [dbo].[tServiceRecipient] ON 

INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (1, 5000, NULL, N'Conway', N'Alexandra', NULL, CAST(N'1950-06-01 00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, NULL, NULL, N'1234567890', 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, NULL, NULL, 99, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, CAST(N'2017-10-09 14:44:00' AS SmallDateTime), N'User input', CAST(N'2017-10-10 16:41:00' AS SmallDateTime), N'User update', NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (2, 5000, NULL, N'Conway', N'Alexandra', NULL, CAST(N'1960-06-01 00:00:00' AS SmallDateTime), NULL, NULL, NULL, NULL, NULL, NULL, N'1234567890', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 99, 0, N'Bill Conway', NULL, NULL, N'3456789876', 2, NULL, N'bill.conway@gmail.com', NULL, NULL, NULL, CAST(N'2017-10-10 08:26:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (3, 5000, NULL, N'TestLast', N'Test', NULL, CAST(N'1960-10-11 00:00:00' AS SmallDateTime), NULL, NULL, NULL, NULL, NULL, NULL, N'1234567890', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 99, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-10-17 11:20:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (4, 5000, NULL, N'TestLast', N'Test', NULL, CAST(N'1960-10-11 00:00:00' AS SmallDateTime), NULL, NULL, NULL, NULL, NULL, NULL, N'1234567890', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 99, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-10-17 11:41:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (5, 5000, NULL, N'TestLast', N'TestA', NULL, CAST(N'1960-10-17 00:00:00' AS SmallDateTime), NULL, NULL, NULL, NULL, NULL, NULL, N'1234567890', 1, 1, N'          ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 99, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-10-19 16:05:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (8, 5000, N'129A1234567890', N'Necessary', N'Minimum', N'R', CAST(N'1960-10-10 00:00:00' AS SmallDateTime), NULL, N'123 First St', N'B', N'San Mateo', N'CA', N'94404', N'1234567890', 2, 1, N'5678905678', 1, NULL, 1, NULL, N'min.necessary@gmail.com', NULL, 1, 98, NULL, 0, 99, 1, N'Contact Name', 1, NULL, N'1234567890', 2, 1, N'contact@gmail.com', 4, N'</br>This is a new comment.
This is a new comment and it is a long one.
This is one more new comment


', NULL, CAST(N'2017-10-25 14:07:00' AS SmallDateTime), N'User input', CAST(N'2017-11-15 15:26:00' AS SmallDateTime), N'User update', NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (9, 5000, NULL, N'Necessary', N'Minimum', NULL, CAST(N'1960-01-01 00:00:00' AS SmallDateTime), 0, N'123 First Street', NULL, N'Sacramento', N'CA', N'95814', N'1234567890', 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 11, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-01-10 14:40:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (10, 5000, NULL, N'ABC', N'XYZ', NULL, CAST(N'1960-01-01 00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'1234567890', 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 7, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-01-10 14:54:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (11, 5001, N'6777777       ', N'doe', N'jane', NULL, CAST(N'1977-07-07 00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'4156969788', 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 14, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-02-22 15:06:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (12, 5001, NULL, N'doe', N'jack', NULL, CAST(N'1999-12-12 00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'6555829797', 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 14, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-02-22 15:10:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (13, 5001, N'9912399       ', N'turner', N'Ted', NULL, CAST(N'1992-09-05 00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'9163586585', 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 99, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04 12:51:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (14, 5001, NULL, N'Stone', N'John', NULL, CAST(N'1983-03-05 00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'4159697865', 4, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 231, NULL, NULL, 99, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04 15:18:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (15, 5001, NULL, N'Edwards', N'Don', NULL, CAST(N'1990-06-05 00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'9167894563', 4, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 99, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04 15:27:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (16, 5001, NULL, N'Roberts', N'Bob', NULL, CAST(N'1976-08-02 00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'5667988222', 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 99, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04 15:30:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (17, 5001, NULL, N'Shane', N'Sam', NULL, CAST(N'1993-03-03 00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'4445556666', 4, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 8, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04 15:36:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (18, 5001, NULL, N'James', N'Carl', NULL, CAST(N'1966-06-06 00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'9161234568', 4, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 22, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04 15:49:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (19, 5001, NULL, N'Bowman', N'Chad', NULL, CAST(N'1978-07-05 00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'9166397452', 4, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, CAST(N'2018-03-05 20:29:00' AS SmallDateTime), N'User input', CAST(N'2018-03-05 20:35:00' AS SmallDateTime), N'User update', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tServiceRecipient] OFF
SET IDENTITY_INSERT [dbo].[tSolution] ON 

INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (2, 1, 8, 1, NULL, CAST(N'2017-10-16 14:48:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2017-10-20 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (3, 1, 9, 1, NULL, CAST(N'2017-10-16 14:48:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2017-10-28 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (4, 6, 13, 1, NULL, CAST(N'2018-02-26 14:32:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-01 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (5, 12, 0, NULL, NULL, CAST(N'2018-02-28 14:43:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (6, 13, 0, NULL, NULL, CAST(N'2018-02-28 14:49:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (7, 14, 0, NULL, NULL, CAST(N'2018-03-01 09:19:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (8, 15, 0, NULL, NULL, CAST(N'2018-03-01 09:29:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (9, 16, 0, NULL, NULL, CAST(N'2018-03-01 09:37:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (10, 17, 0, NULL, NULL, CAST(N'2018-03-01 09:52:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (11, 18, 0, NULL, NULL, CAST(N'2018-03-01 10:02:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (12, 19, 0, NULL, NULL, CAST(N'2018-03-01 10:03:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (13, 20, 0, NULL, NULL, CAST(N'2018-03-01 10:09:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (14, 21, 0, NULL, NULL, CAST(N'2018-03-01 12:37:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (15, 22, 0, NULL, NULL, CAST(N'2018-03-01 13:09:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (16, 23, 0, NULL, NULL, CAST(N'2018-03-02 19:59:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (17, 24, 0, NULL, NULL, CAST(N'2018-03-02 20:04:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (18, 27, 0, NULL, NULL, CAST(N'2018-03-03 11:48:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (19, 28, 0, NULL, NULL, CAST(N'2018-03-03 13:23:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (20, 29, 0, NULL, NULL, CAST(N'2018-03-03 14:12:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (21, 30, 18, 1, NULL, CAST(N'2018-03-03 14:24:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-06 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (22, 31, 6, 1, NULL, CAST(N'2018-03-03 15:09:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-07 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (23, 32, 18, 1, NULL, CAST(N'2018-03-03 15:11:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-05 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (24, 36, 18, 1, NULL, CAST(N'2018-03-03 15:42:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-05 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (25, 37, 18, 1, NULL, CAST(N'2018-03-03 15:46:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-08 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (26, 38, 18, 2, NULL, CAST(N'2018-03-03 15:53:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-18 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (27, 39, 18, 2, NULL, CAST(N'2018-03-03 16:04:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-18 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (28, 40, 18, 2, NULL, CAST(N'2018-03-03 16:10:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-05 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (29, 41, 18, 2, NULL, CAST(N'2018-03-03 16:20:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-06 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (30, 42, 18, 2, NULL, CAST(N'2018-03-03 16:34:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-08 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (31, 43, 18, 1, NULL, CAST(N'2018-03-03 16:42:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-10 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (32, 44, 18, 2, NULL, CAST(N'2018-03-03 16:54:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-09 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (33, 45, 18, 1, NULL, CAST(N'2018-03-03 17:03:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-12 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (34, 46, 18, 2, NULL, CAST(N'2018-03-03 17:12:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-08 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (35, 47, 18, 1, NULL, CAST(N'2018-03-04 09:36:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-10 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (36, 48, 18, 2, NULL, CAST(N'2018-03-04 09:44:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-09 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (37, 49, 18, 1, NULL, CAST(N'2018-03-04 09:48:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-07 00:00:00' AS SmallDateTime), N'3/4/18 9:46  Solution')
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (38, 50, 18, 2, NULL, CAST(N'2018-03-04 12:55:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-05 00:00:00' AS SmallDateTime), N'3/4/18 12:53PM Solution stuff')
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (39, 53, 2, NULL, NULL, CAST(N'2018-03-04 13:15:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (41, 55, 18, 1, NULL, CAST(N'2018-03-04 13:30:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-09 00:00:00' AS SmallDateTime), N'3/4/18 1:30 PM Solution')
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (48, 56, 18, 2, NULL, CAST(N'2018-03-04 14:41:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-08 00:00:00' AS SmallDateTime), N'3/4/18 1:32 PM SolutioNNN')
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (49, 57, 18, 1, NULL, CAST(N'2018-03-04 14:42:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-07 00:00:00' AS SmallDateTime), N'WWWWWWWWWWWWWWWWWWWWWWWWW')
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (51, 54, 18, 2, NULL, CAST(N'2018-03-04 14:44:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-10 00:00:00' AS SmallDateTime), N'3/4/18 1:15 PM Solution stuff')
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (53, 58, 6, 2, NULL, CAST(N'2018-03-04 14:46:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-17 00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (54, 7, 18, 1, NULL, CAST(N'2018-03-05 20:14:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-08 00:00:00' AS SmallDateTime), N'plifcsljofcsll;ad vllad')
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (61, 59, 18, 1, NULL, CAST(N'2018-03-06 09:03:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-09 00:00:00' AS SmallDateTime), N'Solution to Solutionz')
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (64, 60, 18, 1, NULL, CAST(N'2018-03-06 17:27:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-05 00:00:00' AS SmallDateTime), N'po[weoiiouwre')
SET IDENTITY_INSERT [dbo].[tSolution] OFF
SET IDENTITY_INSERT [dbo].[tSystemMessage] ON 

INSERT [dbo].[tSystemMessage] ([Id], [Subject], [Message], [ReceivedOn], [ReceivedBy], [IsRead]) VALUES (1, N'testSub', N'test SysMsg', CAST(N'2018-03-03 00:00:00.000' AS DateTime), 5006, 1)
SET IDENTITY_INSERT [dbo].[tSystemMessage] OFF
SET IDENTITY_INSERT [dbo].[tUserAccess] ON 

INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1, N'DHSINTRA\bbuchake', CAST(N'2017-09-18 13:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2, N'DHSINTRA\bbuchake', CAST(N'2017-09-18 13:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3, N'DHSINTRA\bbuchake', CAST(N'2017-09-19 10:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4, N'DHSINTRA\bbuchake', CAST(N'2017-09-19 10:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (5, N'DHSINTRA\bbuchake', CAST(N'2017-09-26 10:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (6, N'DHSINTRA\bbuchake', CAST(N'2017-09-27 08:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (7, N'DHSINTRA\bbuchake', CAST(N'2017-09-27 09:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (8, N'DHSINTRA\bbuchake', CAST(N'2017-09-27 15:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (9, N'DHSINTRA\bbuchake', CAST(N'2017-09-27 15:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (10, N'DHSINTRA\bbuchake', CAST(N'2017-09-27 15:54:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (11, N'DHSINTRA\bbuchake', CAST(N'2017-09-27 15:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (12, N'DHSINTRA\bbuchake', CAST(N'2017-09-27 16:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (13, N'DHSINTRA\bbuchake', CAST(N'2017-09-28 17:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (14, N'DHSINTRA\bbuchake', CAST(N'2017-10-02 13:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (15, N'DHSINTRA\bbuchake', CAST(N'2017-10-03 08:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (16, N'DHSINTRA\bbuchake', CAST(N'2017-10-03 09:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (17, N'DHSINTRA\bbuchake', CAST(N'2017-10-03 13:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (18, N'DHSINTRA\bbuchake', CAST(N'2017-10-03 15:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (19, N'DHSINTRA\pchinnap', CAST(N'2017-10-06 10:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (20, N'DHSINTRA\pchinnap', CAST(N'2017-10-06 10:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (21, N'DHSINTRA\bbuchake', CAST(N'2017-10-06 10:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (22, N'DHSINTRA\pchinnap', CAST(N'2017-10-06 10:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (23, N'DHSINTRA\pchinnap', CAST(N'2017-10-06 10:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (24, N'DHSINTRA\bbuchake', CAST(N'2017-10-06 10:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (25, N'DHSINTRA\pchinnap', CAST(N'2017-10-06 11:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (26, N'DHSINTRA\bbuchake', CAST(N'2017-10-06 13:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (27, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 11:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (28, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 14:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (29, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 15:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (30, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 15:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (31, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 15:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (32, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 15:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (33, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 15:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (34, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 15:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (35, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 15:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (36, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 15:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (37, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 15:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (38, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 15:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (39, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 15:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (40, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 15:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (41, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 15:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (42, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 16:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (43, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 16:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (44, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 16:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (45, N'DHSINTRA\bbuchake', CAST(N'2017-10-09 16:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (46, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 08:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (47, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (48, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (49, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (50, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (51, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (52, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (53, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (54, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (55, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (56, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (57, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (58, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (59, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (60, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (61, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (62, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (63, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (64, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (65, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (66, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (67, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (68, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (69, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (70, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 09:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (71, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 10:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (72, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 10:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (73, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 10:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (74, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 10:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (75, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 10:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (76, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 10:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (77, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 10:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (78, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 10:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (79, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 10:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (80, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 10:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (81, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 10:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (82, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 10:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (83, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 10:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (84, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 10:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (85, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 10:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (86, N'DHSINTRA\pchinnap', CAST(N'2017-10-10 11:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (87, N'DHSINTRA\pchinnap', CAST(N'2017-10-10 11:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (88, N'DHSINTRA\pchinnap', CAST(N'2017-10-10 11:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (89, N'DHSINTRA\pchinnap', CAST(N'2017-10-10 11:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (90, N'DHSINTRA\pchinnap', CAST(N'2017-10-10 11:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (91, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 14:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (92, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 14:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (93, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 14:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (94, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 14:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (95, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 14:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (96, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 14:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (97, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 14:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (98, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 14:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (99, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 14:56:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (100, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (101, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (102, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (103, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (104, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (105, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (106, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (107, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (108, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (109, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (110, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (111, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (112, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (113, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (114, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (115, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (116, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (117, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (118, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (119, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (120, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (121, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (122, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (123, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (124, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (125, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (126, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (127, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (128, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (129, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (130, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (131, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (132, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (133, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 15:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (134, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 16:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (135, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 16:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (136, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 16:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (137, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 16:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (138, N'DHSINTRA\bbuchake', CAST(N'2017-10-10 16:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (139, N'DHSINTRA\bbuchake', CAST(N'2017-10-11 14:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (140, N'DHSINTRA\bbuchake', CAST(N'2017-10-11 15:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (141, N'DHSINTRA\bbuchake', CAST(N'2017-10-11 15:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (142, N'DHSINTRA\bbuchake', CAST(N'2017-10-11 16:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (143, N'DHSINTRA\bbuchake', CAST(N'2017-10-11 16:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (144, N'DHSINTRA\bbuchake', CAST(N'2017-10-11 16:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (145, N'DHSINTRA\bbuchake', CAST(N'2017-10-11 16:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (146, N'DHSINTRA\bbuchake', CAST(N'2017-10-11 16:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (147, N'DHSINTRA\bbuchake', CAST(N'2017-10-11 17:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (148, N'DHSINTRA\bbuchake', CAST(N'2017-10-12 10:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (149, N'DHSINTRA\bbuchake', CAST(N'2017-10-12 10:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (150, N'DHSINTRA\bbuchake', CAST(N'2017-10-12 11:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (151, N'DHSINTRA\bbuchake', CAST(N'2017-10-12 15:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (152, N'DHSINTRA\pchinnap', CAST(N'2017-10-13 11:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (153, N'DHSINTRA\pchinnap', CAST(N'2017-10-13 11:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (154, N'DHSINTRA\pchinnap', CAST(N'2017-10-16 11:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (155, N'DHSINTRA\pchinnap', CAST(N'2017-10-16 11:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (156, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (157, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (158, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (159, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (160, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (161, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (162, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (163, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (164, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (165, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (166, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (167, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (168, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (169, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (170, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (171, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (172, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (173, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (174, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (175, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (176, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (177, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (178, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (179, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (180, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (181, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (182, N'DHSINTRA\pchinnap', CAST(N'2017-10-16 14:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (183, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (184, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (185, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (186, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (187, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (188, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (189, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (190, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (191, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (192, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (193, N'DHSINTRA\bbuchake', CAST(N'2017-10-16 14:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (194, N'DHSINTRA\pchinnap', CAST(N'2017-10-17 05:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (195, N'DHSINTRA\pchinnap', CAST(N'2017-10-17 05:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (196, N'DHSINTRA\pchinnap', CAST(N'2017-10-17 05:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (197, N'DHSINTRA\pchinnap', CAST(N'2017-10-17 05:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (198, N'DHSINTRA\pchinnap', CAST(N'2017-10-17 05:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (199, N'DHSINTRA\pchinnap', CAST(N'2017-10-17 05:41:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (200, N'DHSINTRA\pchinnap', CAST(N'2017-10-17 07:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (201, N'DHSINTRA\bbuchake', CAST(N'2017-10-17 10:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (202, N'DHSINTRA\bbuchake', CAST(N'2017-10-17 11:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (203, N'DHSINTRA\bbuchake', CAST(N'2017-10-17 11:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (204, N'DHSINTRA\bbuchake', CAST(N'2017-10-17 11:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (205, N'DHSINTRA\bbuchake', CAST(N'2017-10-17 11:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (206, N'DHSINTRA\bbuchake', CAST(N'2017-10-17 12:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (207, N'DHSINTRA\bbuchake', CAST(N'2017-10-17 13:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (208, N'DHSINTRA\bbuchake', CAST(N'2017-10-17 14:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (209, N'DHSINTRA\bbuchake', CAST(N'2017-10-17 14:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (210, N'DHSINTRA\pchinnap', CAST(N'2017-10-17 14:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (211, N'DHSINTRA\bbuchake', CAST(N'2017-10-18 10:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (212, N'DHSINTRA\bbuchake', CAST(N'2017-10-18 10:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (213, N'DHSINTRA\bbuchake', CAST(N'2017-10-19 14:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (214, N'DHSINTRA\bbuchake', CAST(N'2017-10-19 14:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (215, N'DHSINTRA\bbuchake', CAST(N'2017-10-19 14:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (216, N'DHSINTRA\bbuchake', CAST(N'2017-10-19 15:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (217, N'DHSINTRA\bbuchake', CAST(N'2017-10-19 15:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (218, N'DHSINTRA\bbuchake', CAST(N'2017-10-19 16:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (219, N'DHSINTRA\bbuchake', CAST(N'2017-10-19 16:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (220, N'DHSINTRA\bbuchake', CAST(N'2017-10-20 11:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (221, N'DHSINTRA\bbuchake', CAST(N'2017-10-20 14:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (222, N'DHSINTRA\bbuchake', CAST(N'2017-10-20 15:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (223, N'DHSINTRA\bbuchake', CAST(N'2017-10-24 09:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (224, N'DHSINTRA\bbuchake', CAST(N'2017-10-24 10:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (225, N'DHSINTRA\bbuchake', CAST(N'2017-10-24 11:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (226, N'DHSINTRA\bbuchake', CAST(N'2017-10-24 11:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (227, N'DHSINTRA\bbuchake', CAST(N'2017-10-24 11:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (228, N'DHSINTRA\bbuchake', CAST(N'2017-10-24 11:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (229, N'DHSINTRA\pchinnap', CAST(N'2017-10-24 12:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (230, N'DHSINTRA\bbuchake', CAST(N'2017-10-25 11:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (231, N'DHSINTRA\bbuchake', CAST(N'2017-10-25 14:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (232, N'DHSINTRA\bbuchake', CAST(N'2017-10-25 14:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (233, N'DHSINTRA\bbuchake', CAST(N'2017-10-25 14:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (234, N'DHSINTRA\bbuchake', CAST(N'2017-10-25 15:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (235, N'DHSINTRA\bbuchake', CAST(N'2017-10-25 15:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (236, N'DHSINTRA\bbuchake', CAST(N'2017-10-25 15:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (237, N'DHSINTRA\bbuchake', CAST(N'2017-10-25 15:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (238, N'DHSINTRA\bbuchake', CAST(N'2017-10-25 15:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (239, N'DHSINTRA\bbuchake', CAST(N'2017-10-25 15:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (240, N'DHSINTRA\bbuchake', CAST(N'2017-10-25 15:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (241, N'DHSINTRA\bbuchake', CAST(N'2017-10-25 15:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (242, N'DHSINTRA\bbuchake', CAST(N'2017-10-25 16:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (243, N'DHSINTRA\bbuchake', CAST(N'2017-10-26 15:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (244, N'DHSINTRA\bbuchake', CAST(N'2017-10-26 15:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (245, N'DHSINTRA\bbuchake', CAST(N'2017-10-26 16:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (246, N'DHSINTRA\bbuchake', CAST(N'2017-10-26 16:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (247, N'DHSINTRA\bbuchake', CAST(N'2017-10-26 16:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (248, N'DHSINTRA\bbuchake', CAST(N'2017-10-26 16:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (249, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 08:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (250, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 09:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (251, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 09:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (252, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 09:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (253, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 09:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (254, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 09:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (255, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 09:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (256, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 09:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (257, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 09:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (258, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 09:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (259, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 09:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (260, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 09:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (261, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 09:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (262, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 09:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (263, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 10:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (264, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 10:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (265, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 10:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (266, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 10:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (267, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 10:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (268, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 10:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (269, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 10:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (270, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 11:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (271, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 11:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (272, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 11:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (273, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 13:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (274, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 13:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (275, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 13:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (276, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 13:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (277, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 13:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (278, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 13:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (279, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 14:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (280, N'DHSINTRA\bbuchake', CAST(N'2017-10-27 14:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (281, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 11:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (282, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 12:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (283, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 12:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (284, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 12:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (285, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 13:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (286, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 13:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (287, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 13:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (288, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 13:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (289, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 13:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (290, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 13:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (291, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 13:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (292, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 14:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (293, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 14:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (294, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 15:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (295, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 15:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (296, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 15:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (297, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 15:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (298, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 15:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (299, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 15:51:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (300, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 15:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (301, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 15:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (302, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 16:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (303, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 16:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (304, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 16:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (305, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 16:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (306, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 16:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (307, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 16:54:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (308, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 16:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (309, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 17:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (310, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 17:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (311, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 17:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (312, N'DHSINTRA\bbuchake', CAST(N'2017-10-30 17:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (313, N'DHSINTRA\bbuchake', CAST(N'2017-11-02 09:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (314, N'DHSINTRA\bbuchake', CAST(N'2017-11-02 10:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (315, N'DHSINTRA\bbuchake', CAST(N'2017-11-02 13:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (316, N'DHSINTRA\bbuchake', CAST(N'2017-11-02 13:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (317, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 12:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (318, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 13:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (319, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 13:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (320, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 13:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (321, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 13:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (322, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 13:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (323, N'DHSINTRA\pchinnap', CAST(N'2017-11-03 14:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (324, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (325, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (326, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (327, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (328, N'DHSINTRA\pchinnap', CAST(N'2017-11-03 14:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (329, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 15:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (330, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 15:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (331, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 16:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (332, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 16:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (333, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 16:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (334, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 16:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (335, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 16:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (336, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 18:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (337, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 18:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (338, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 18:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (339, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 18:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (340, N'DHSINTRA\bbuchake', CAST(N'2017-11-03 18:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (341, N'DHSINTRA\pchinnap', CAST(N'2017-11-07 10:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (342, N'DHSINTRA\pchinnap', CAST(N'2017-11-07 13:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (343, N'DHSINTRA\pchinnap', CAST(N'2017-11-07 13:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (344, N'DHSINTRA\pchinnap', CAST(N'2017-11-07 13:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (345, N'DHSINTRA\pchinnap', CAST(N'2017-11-08 09:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (346, N'DHSINTRA\pchinnap', CAST(N'2017-11-08 10:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (347, N'DHSINTRA\pchinnap', CAST(N'2017-11-08 10:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (348, N'DHSINTRA\pchinnap', CAST(N'2017-11-08 10:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (349, N'DHSINTRA\bbuchake', CAST(N'2017-11-08 13:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (350, N'DHSINTRA\pchinnap', CAST(N'2017-11-08 13:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (351, N'DHSINTRA\bbuchake', CAST(N'2017-11-08 15:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (352, N'DHSINTRA\bbuchake', CAST(N'2017-11-08 15:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (353, N'DHSINTRA\bbuchake', CAST(N'2017-11-08 15:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (354, N'DHSINTRA\bbuchake', CAST(N'2017-11-08 15:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (355, N'DHSINTRA\bbuchake', CAST(N'2017-11-08 15:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (356, N'DHSINTRA\bbuchake', CAST(N'2017-11-08 15:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (357, N'DHSINTRA\bbuchake', CAST(N'2017-11-08 16:06:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (358, N'DHSINTRA\bbuchake', CAST(N'2017-11-08 16:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (359, N'DHSINTRA\bbuchake', CAST(N'2017-11-08 16:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (360, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 10:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (361, N'DHSINTRA\pchinnap', CAST(N'2017-11-09 11:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (362, N'DHSINTRA\pchinnap', CAST(N'2017-11-09 11:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (363, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 11:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (364, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 11:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (365, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 11:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (366, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 11:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (367, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 11:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (368, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 11:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (369, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 11:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (370, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 12:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (371, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 12:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (372, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 12:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (373, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 12:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (374, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 12:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (375, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 13:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (376, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 13:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (377, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 13:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (378, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 13:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (379, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 13:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (380, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 13:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (381, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 13:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (382, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 13:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (383, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 13:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (384, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 13:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (385, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 13:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (386, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 13:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (387, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 13:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (388, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 13:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (389, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 13:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (390, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 13:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (391, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 13:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (392, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 14:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (393, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 14:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (394, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 14:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (395, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 15:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (396, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 15:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (397, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 15:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (398, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 15:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (399, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 15:55:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (400, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 15:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (401, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 16:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (402, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 16:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (403, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 16:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (404, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 16:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (405, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 16:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (406, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 16:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (407, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 16:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (408, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 16:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (409, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 16:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (410, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 16:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (411, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 17:06:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (412, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 17:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (413, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 18:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (414, N'DHSINTRA\bbuchake', CAST(N'2017-11-09 18:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (415, N'DHSINTRA\bbuchake', CAST(N'2017-11-10 10:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (416, N'DHSINTRA\bbuchake', CAST(N'2017-11-10 11:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (417, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 12:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (418, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 13:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (419, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 13:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (420, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 13:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (421, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 13:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (422, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 13:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (423, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 13:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (424, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 13:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (425, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 13:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (426, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 13:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (427, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 13:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (428, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 13:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (429, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 13:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (430, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 13:54:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (431, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 14:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (432, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 14:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (433, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 14:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (434, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 14:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (435, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 15:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (436, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 15:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (437, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 15:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (438, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 16:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (439, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 16:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (440, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 16:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (441, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 16:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (442, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 16:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (443, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 16:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (444, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 16:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (445, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 17:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (446, N'DHSINTRA\bbuchake', CAST(N'2017-11-13 21:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (447, N'DHSINTRA\bbuchake', CAST(N'2017-11-14 10:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (448, N'DHSINTRA\bbuchake', CAST(N'2017-11-14 11:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (449, N'DHSINTRA\bbuchake', CAST(N'2017-11-14 13:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (450, N'DHSINTRA\bbuchake', CAST(N'2017-11-14 13:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (451, N'DHSINTRA\bbuchake', CAST(N'2017-11-14 13:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (452, N'DHSINTRA\bbuchake', CAST(N'2017-11-14 14:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (453, N'DHSINTRA\bbuchake', CAST(N'2017-11-14 15:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (454, N'DHSINTRA\bbuchake', CAST(N'2017-11-14 15:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (455, N'DHSINTRA\bbuchake', CAST(N'2017-11-14 15:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (456, N'DHSINTRA\bbuchake', CAST(N'2017-11-14 15:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (457, N'DHSINTRA\bbuchake', CAST(N'2017-11-14 15:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (458, N'DHSINTRA\bbuchake', CAST(N'2017-11-14 16:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (459, N'DHSINTRA\bbuchake', CAST(N'2017-11-14 16:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (460, N'DHSINTRA\bbuchake', CAST(N'2017-11-14 16:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (461, N'DHSINTRA\bbuchake', CAST(N'2017-11-14 17:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (462, N'DHSINTRA\bbuchake', CAST(N'2017-11-15 09:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (463, N'DHSINTRA\bbuchake', CAST(N'2017-11-15 09:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (464, N'DHSINTRA\bbuchake', CAST(N'2017-11-15 14:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (465, N'DHSINTRA\bbuchake', CAST(N'2017-11-15 14:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (466, N'DHSINTRA\bbuchake', CAST(N'2017-11-15 14:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (467, N'DHSINTRA\bbuchake', CAST(N'2017-11-15 15:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (468, N'DHSINTRA\bbuchake', CAST(N'2017-11-15 15:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (469, N'DHSINTRA\bbuchake', CAST(N'2017-11-15 15:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (470, N'DHSINTRA\bbuchake', CAST(N'2017-11-15 15:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (471, N'DHSINTRA\bbuchake', CAST(N'2017-11-15 15:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (472, N'DHSINTRA\bbuchake', CAST(N'2017-11-15 15:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (473, N'DHSINTRA\bbuchake', CAST(N'2017-11-15 16:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (474, N'DHSINTRA\bbuchake', CAST(N'2017-11-15 16:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (475, N'DHSINTRA\bbuchake', CAST(N'2017-11-15 16:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (476, N'DHSINTRA\bbuchake', CAST(N'2017-11-15 16:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (477, N'DHSINTRA\bbuchake', CAST(N'2017-11-16 10:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (478, N'DHSINTRA\bbuchake', CAST(N'2017-11-16 10:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (479, N'DHSINTRA\bbuchake', CAST(N'2017-11-16 14:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (480, N'DHSINTRA\bbuchake', CAST(N'2017-11-16 14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (481, N'DHSINTRA\bbuchake', CAST(N'2017-11-16 14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (482, N'DHSINTRA\bbuchake', CAST(N'2017-11-16 14:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (483, N'DHSINTRA\bbuchake', CAST(N'2017-11-16 16:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (484, N'DHSINTRA\bbuchake', CAST(N'2017-11-16 16:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (485, N'DHSINTRA\bbuchake', CAST(N'2017-11-16 16:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (486, N'DHSINTRA\bbuchake', CAST(N'2017-11-16 16:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (487, N'DHSINTRA\bbuchake', CAST(N'2017-11-16 16:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (488, N'DHSINTRA\bbuchake', CAST(N'2017-11-16 16:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (489, N'DHSINTRA\bbuchake', CAST(N'2017-11-16 16:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (490, N'DHSINTRA\bbuchake', CAST(N'2017-11-17 08:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (491, N'DHSINTRA\bbuchake', CAST(N'2017-11-17 16:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (492, N'DHSINTRA\bbuchake', CAST(N'2017-11-17 16:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (493, N'DHSINTRA\bbuchake', CAST(N'2017-11-20 09:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (494, N'DHSINTRA\bbuchake', CAST(N'2017-11-20 09:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (495, N'DHSINTRA\bbuchake', CAST(N'2017-11-20 10:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (496, N'DHSINTRA\bbuchake', CAST(N'2017-11-20 14:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (497, N'DHSINTRA\bbuchake', CAST(N'2017-11-20 14:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (498, N'DHSINTRA\bbuchake', CAST(N'2017-11-21 14:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (499, N'DHSINTRA\bbuchake', CAST(N'2017-11-21 14:06:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (500, N'DHSINTRA\bbuchake', CAST(N'2017-11-21 14:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (501, N'DHSINTRA\bbuchake', CAST(N'2017-11-21 15:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (502, N'DHSINTRA\bbuchake', CAST(N'2017-11-21 15:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (503, N'DHSINTRA\bbuchake', CAST(N'2017-11-21 15:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (504, N'DHSINTRA\bbuchake', CAST(N'2017-11-28 12:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (505, N'DHSINTRA\bbuchake', CAST(N'2017-12-08 10:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (506, N'DHSINTRA\bbuchake', CAST(N'2017-12-08 10:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (507, N'DHSINTRA\bbuchake', CAST(N'2017-12-08 11:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (508, N'DHSINTRA\bbuchake', CAST(N'2017-12-08 13:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (509, N'DHSINTRA\bbuchake', CAST(N'2017-12-08 13:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (510, N'DHSINTRA\bbuchake', CAST(N'2017-12-08 13:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (511, N'DHSINTRA\bbuchake', CAST(N'2017-12-08 13:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (512, N'DHSINTRA\bbuchake', CAST(N'2017-12-08 13:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (513, N'DHSINTRA\bbuchake', CAST(N'2017-12-08 13:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (514, N'DHSINTRA\bbuchake', CAST(N'2017-12-08 14:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (515, N'DHSINTRA\bbuchake', CAST(N'2017-12-08 14:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (516, N'DHSINTRA\bbuchake', CAST(N'2017-12-08 14:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (517, N'DHSINTRA\bbuchake', CAST(N'2017-12-11 12:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (518, N'DHSINTRA\bbuchake', CAST(N'2017-12-11 15:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (519, N'DHSINTRA\bbuchake', CAST(N'2017-12-11 15:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (520, N'DHSINTRA\bbuchake', CAST(N'2017-12-12 14:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (521, N'DHSINTRA\bbuchake', CAST(N'2018-01-03 08:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (522, N'DHSINTRA\bbuchake', CAST(N'2018-01-03 13:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (523, N'DHSINTRA\bbuchake', CAST(N'2018-01-03 13:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (524, N'DHSINTRA\bbuchake', CAST(N'2018-01-03 13:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (525, N'DHSINTRA\bbuchake', CAST(N'2018-01-03 13:06:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (526, N'DHSINTRA\bbuchake', CAST(N'2018-01-03 13:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (527, N'DHSINTRA\bbuchake', CAST(N'2018-01-08 14:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (528, N'DHSINTRA\bbuchake', CAST(N'2018-01-08 15:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (529, N'DHSINTRA\bbuchake', CAST(N'2018-01-08 15:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (530, N'DHSINTRA\bbuchake', CAST(N'2018-01-08 15:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (531, N'DHSINTRA\bbuchake', CAST(N'2018-01-08 15:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (532, N'DHSINTRA\bbuchake', CAST(N'2018-01-09 08:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (533, N'DHSINTRA\bbuchake', CAST(N'2018-01-09 08:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (534, N'DHSINTRA\bbuchake', CAST(N'2018-01-09 08:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (535, N'DHSINTRA\bbuchake', CAST(N'2018-01-09 09:06:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (536, N'DHSINTRA\bbuchake', CAST(N'2018-01-09 10:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (537, N'DHSINTRA\bbuchake', CAST(N'2018-01-09 10:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (538, N'DHSINTRA\bbuchake', CAST(N'2018-01-10 14:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (539, N'DHSINTRA\bbuchake', CAST(N'2018-01-10 14:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (540, N'DHSINTRA\bbuchake', CAST(N'2018-01-10 14:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (541, N'DHSINTRA\bbuchake', CAST(N'2018-01-10 14:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (542, N'DHSINTRA\bbuchake', CAST(N'2018-01-10 14:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (543, N'DHSINTRA\bbuchake', CAST(N'2018-01-10 14:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (544, N'DHSINTRA\bbuchake', CAST(N'2018-01-10 14:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (545, N'DHSINTRA\bbuchake', CAST(N'2018-01-10 14:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (546, N'DHSINTRA\bbuchake', CAST(N'2018-01-10 14:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (547, N'DHSINTRA\bbuchake', CAST(N'2018-01-10 14:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (548, N'DHSINTRA\bbuchake', CAST(N'2018-01-10 14:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (549, N'DHSINTRA\bbuchake', CAST(N'2018-01-10 14:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (550, N'DHSINTRA\bbuchake', CAST(N'2018-01-11 14:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (551, N'DHSINTRA\bbuchake', CAST(N'2018-01-11 14:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (552, N'DHSINTRA\bbuchake', CAST(N'2018-01-11 14:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (553, N'DHSINTRA\bbuchake', CAST(N'2018-01-11 14:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (554, N'DHSINTRA\pchinnap', CAST(N'2018-01-11 15:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (555, N'DHSINTRA\bbuchake', CAST(N'2018-01-12 09:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (556, N'DHSINTRA\pchinnap', CAST(N'2018-01-25 13:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (557, N'DHSINTRA\pchinnap', CAST(N'2018-01-25 14:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (558, N'DHSINTRA\vgrenz', CAST(N'2018-01-29 11:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (559, N'DHSINTRA\vgrenz', CAST(N'2018-01-29 11:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (560, N'DHSINTRA\vgrenz', CAST(N'2018-01-29 11:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (561, N'DHSINTRA\vgrenz', CAST(N'2018-01-29 13:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (562, N'DHSINTRA\vgrenz', CAST(N'2018-01-29 14:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (563, N'DHSINTRA\vgrenz', CAST(N'2018-01-30 11:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (564, N'DHSINTRA\vgrenz', CAST(N'2018-01-30 11:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (565, N'DHSINTRA\vgrenz', CAST(N'2018-01-30 11:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (566, N'DHSINTRA\vgrenz', CAST(N'2018-01-30 12:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (567, N'DHSINTRA\grichey', CAST(N'2018-02-20 12:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (568, N'DHSINTRA\grichey', CAST(N'2018-02-20 12:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (569, N'DHSINTRA\grichey', CAST(N'2018-02-20 12:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (570, N'DHSINTRA\grichey', CAST(N'2018-02-20 12:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (571, N'DHSINTRA\grichey', CAST(N'2018-02-20 12:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (572, N'DHSINTRA\grichey', CAST(N'2018-02-20 12:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (573, N'DHSINTRA\grichey', CAST(N'2018-02-20 12:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (574, N'DHSINTRA\grichey', CAST(N'2018-02-20 13:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (575, N'DHSINTRA\grichey', CAST(N'2018-02-20 13:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (576, N'DHSINTRA\grichey', CAST(N'2018-02-20 14:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (577, N'DHSINTRA\grichey', CAST(N'2018-02-20 14:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (578, N'DHSINTRA\grichey', CAST(N'2018-02-20 15:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (579, N'DHSINTRA\grichey', CAST(N'2018-02-20 15:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (580, N'DHSINTRA\grichey', CAST(N'2018-02-20 15:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (581, N'DHSINTRA\grichey', CAST(N'2018-02-20 15:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (582, N'DHSINTRA\pchinnap', CAST(N'2018-02-21 08:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (583, N'DHSINTRA\grichey', CAST(N'2018-02-21 08:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (584, N'DHSINTRA\grichey', CAST(N'2018-02-21 08:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (585, N'DHSINTRA\grichey', CAST(N'2018-02-21 08:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (586, N'DHSINTRA\grichey', CAST(N'2018-02-21 09:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (587, N'DHSINTRA\grichey', CAST(N'2018-02-21 09:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (588, N'DHSINTRA\grichey', CAST(N'2018-02-21 09:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (589, N'DHSINTRA\grichey', CAST(N'2018-02-21 09:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (590, N'DHSINTRA\grichey', CAST(N'2018-02-21 09:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (591, N'DHSINTRA\bbuchake', CAST(N'2018-02-21 09:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (592, N'DHSINTRA\grichey', CAST(N'2018-02-21 09:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (593, N'DHSINTRA\grichey', CAST(N'2018-02-21 09:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (594, N'DHSINTRA\grichey', CAST(N'2018-02-21 09:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (595, N'DHSINTRA\grichey', CAST(N'2018-02-21 09:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (596, N'DHSINTRA\grichey', CAST(N'2018-02-21 10:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (597, N'DHSINTRA\grichey', CAST(N'2018-02-21 10:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (598, N'DHSINTRA\grichey', CAST(N'2018-02-21 10:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (599, N'DHSINTRA\grichey', CAST(N'2018-02-21 10:29:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (600, N'DHSINTRA\grichey', CAST(N'2018-02-21 10:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (601, N'DHSINTRA\grichey', CAST(N'2018-02-21 10:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (602, N'DHSINTRA\grichey', CAST(N'2018-02-21 12:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (603, N'DHSINTRA\grichey', CAST(N'2018-02-21 12:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (604, N'DHSINTRA\grichey', CAST(N'2018-02-21 12:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (605, N'DHSINTRA\grichey', CAST(N'2018-02-21 12:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (606, N'DHSINTRA\grichey', CAST(N'2018-02-21 12:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (607, N'DHSINTRA\grichey', CAST(N'2018-02-21 12:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (608, N'DHSINTRA\grichey', CAST(N'2018-02-21 13:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (609, N'DHSINTRA\grichey', CAST(N'2018-02-21 13:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (610, N'DHSINTRA\grichey', CAST(N'2018-02-21 13:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (611, N'DHSINTRA\grichey', CAST(N'2018-02-21 14:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (612, N'DHSINTRA\grichey', CAST(N'2018-02-21 14:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (613, N'DHSINTRA\grichey', CAST(N'2018-02-22 08:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (614, N'DHSINTRA\GRichey', CAST(N'2018-02-22 09:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (615, N'DHSINTRA\grichey', CAST(N'2018-02-22 09:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (616, N'DHSINTRA\grichey', CAST(N'2018-02-22 09:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (617, N'DHSINTRA\grichey', CAST(N'2018-02-22 09:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (618, N'DHSINTRA\GRichey', CAST(N'2018-02-22 09:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (619, N'DHSINTRA\grichey', CAST(N'2018-02-22 09:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (620, N'DHSINTRA\grichey', CAST(N'2018-02-22 10:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (621, N'DHSINTRA\grichey', CAST(N'2018-02-22 10:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (622, N'DHSINTRA\grichey', CAST(N'2018-02-22 10:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (623, N'NT AUTHORITY\IUSR', CAST(N'2018-02-22 10:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (624, N'DHSINTRA\GRichey', CAST(N'2018-02-22 10:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (625, N'DHSINTRA\grichey', CAST(N'2018-02-22 12:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (626, N'DHSINTRA\grichey', CAST(N'2018-02-22 12:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (627, N'DHSINTRA\grichey', CAST(N'2018-02-22 12:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (628, N'DHSINTRA\grichey', CAST(N'2018-02-22 12:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (629, N'DHSINTRA\grichey', CAST(N'2018-02-22 12:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (630, N'DHSINTRA\grichey', CAST(N'2018-02-22 12:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (631, N'DHSINTRA\grichey', CAST(N'2018-02-22 12:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (632, N'DHSINTRA\grichey', CAST(N'2018-02-22 13:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (633, N'DHSINTRA\grichey', CAST(N'2018-02-22 13:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (634, N'DHSINTRA\grichey', CAST(N'2018-02-22 13:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (635, N'DHSINTRA\grichey', CAST(N'2018-02-22 13:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (636, N'DHSINTRA\grichey', CAST(N'2018-02-22 13:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (637, N'DHSINTRA\grichey', CAST(N'2018-02-22 14:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (638, N'IIS APPPOOL\NSAA', CAST(N'2018-02-22 14:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (639, N'IIS APPPOOL\NSAA', CAST(N'2018-02-22 14:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (640, N'IIS APPPOOL\NSAA', CAST(N'2018-02-22 14:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (641, N'IIS APPPOOL\NSAA', CAST(N'2018-02-22 14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (642, N'IIS APPPOOL\NSAA', CAST(N'2018-02-22 14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (643, N'IIS APPPOOL\NSAA', CAST(N'2018-02-22 14:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (644, N'NT AUTHORITY\NETWORK', CAST(N'2018-02-22 14:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (645, N'DHSINTRA\grichey', CAST(N'2018-02-22 14:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (646, N'DHSINTRA\grichey', CAST(N'2018-02-22 14:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (647, N'DHSINTRA\grichey', CAST(N'2018-02-22 15:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (648, N'DHSINTRA\grichey', CAST(N'2018-02-22 15:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (649, N'DHSINTRA\grichey', CAST(N'2018-02-23 08:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (650, N'DHSINTRA\grichey', CAST(N'2018-02-23 08:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (651, N'DHSINTRA\grichey', CAST(N'2018-02-23 09:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (652, N'DHSINTRA\grichey', CAST(N'2018-02-23 12:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (653, N'DHSINTRA\grichey', CAST(N'2018-02-23 12:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (654, N'DHSINTRA\grichey', CAST(N'2018-02-23 12:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (655, N'DHSINTRA\grichey', CAST(N'2018-02-23 12:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (656, N'DHSINTRA\grichey', CAST(N'2018-02-23 12:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (657, N'DHSINTRA\grichey', CAST(N'2018-02-23 12:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (658, N'DHSINTRA\grichey', CAST(N'2018-02-23 13:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (659, N'DHSINTRA\grichey', CAST(N'2018-02-23 13:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (660, N'DHSINTRA\grichey', CAST(N'2018-02-23 13:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (661, N'DHSINTRA\grichey', CAST(N'2018-02-23 13:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (662, N'DHSINTRA\grichey', CAST(N'2018-02-23 14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (663, N'DHSINTRA\grichey', CAST(N'2018-02-23 15:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (664, N'DHSINTRA\grichey', CAST(N'2018-02-23 15:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (665, N'DHSINTRA\grichey', CAST(N'2018-02-25 09:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (666, N'DHSINTRA\grichey', CAST(N'2018-02-25 10:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (667, N'DHSINTRA\grichey', CAST(N'2018-02-25 11:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (668, N'DHSINTRA\grichey', CAST(N'2018-02-25 13:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (669, N'DHSINTRA\grichey', CAST(N'2018-02-25 13:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (670, N'DHSINTRA\grichey', CAST(N'2018-02-25 13:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (671, N'DHSINTRA\grichey', CAST(N'2018-02-25 14:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (672, N'DHSINTRA\grichey', CAST(N'2018-02-26 14:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (673, N'DHSINTRA\grichey', CAST(N'2018-02-27 07:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (674, N'DHSINTRA\grichey', CAST(N'2018-02-27 12:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (675, N'DHSINTRA\vgrenz', CAST(N'2018-02-27 12:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (676, N'DHSINTRA\vgrenz', CAST(N'2018-02-27 13:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (677, N'DHSINTRA\vgrenz', CAST(N'2018-02-27 13:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (678, N'DHSINTRA\vgrenz', CAST(N'2018-02-27 13:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (679, N'DHSINTRA\vgrenz', CAST(N'2018-02-27 13:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (680, N'DHSINTRA\vgrenz', CAST(N'2018-02-27 13:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (681, N'DHSINTRA\vgrenz', CAST(N'2018-02-27 13:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (682, N'DHSINTRA\vgrenz', CAST(N'2018-02-27 14:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (683, N'DHSINTRA\vgrenz', CAST(N'2018-02-27 14:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (684, N'DHSINTRA\vgrenz', CAST(N'2018-02-27 14:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (685, N'DHSINTRA\grichey', CAST(N'2018-02-27 14:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (686, N'DHSINTRA\grichey', CAST(N'2018-02-27 15:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (687, N'DHSINTRA\RPerumal', CAST(N'2018-02-27 16:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (688, N'DHSINTRA\grichey', CAST(N'2018-02-27 16:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (689, N'DHSINTRA\grichey', CAST(N'2018-02-27 16:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (690, N'DHSINTRA\RPerumal', CAST(N'2018-02-27 19:06:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (691, N'DHSINTRA\RPerumal', CAST(N'2018-02-27 19:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (692, N'DHSINTRA\RPerumal', CAST(N'2018-02-27 19:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (694, N'DHSINTRA\RPerumal', CAST(N'2018-02-27 00:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (695, N'DHSINTRA\RPerumal', CAST(N'2018-02-27 19:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (696, N'DHSINTRA\RPerumal', CAST(N'2018-02-27 19:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (697, N'DHSINTRA\RPerumal', CAST(N'2018-02-27 19:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (698, N'DHSINTRA\RPerumal', CAST(N'2018-02-27 20:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (699, N'DHSINTRA\RPerumal', CAST(N'2018-02-27 20:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (700, N'DHSINTRA\grichey', CAST(N'2018-02-28 08:11:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (701, N'DHSINTRA\RPerumal', CAST(N'2018-02-28 08:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (702, N'DHSINTRA\grichey', CAST(N'2018-02-28 08:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (703, N'DHSINTRA\grichey', CAST(N'2018-02-28 09:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (704, N'DHSINTRA\grichey', CAST(N'2018-02-28 09:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (705, N'DHSINTRA\grichey', CAST(N'2018-02-28 09:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (706, N'DHSINTRA\grichey', CAST(N'2018-02-28 10:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (707, N'DHSINTRA\RPerumal', CAST(N'2018-02-28 10:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (708, N'DHSINTRA\RPerumal', CAST(N'2018-02-28 10:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (709, N'DHSINTRA\RPerumal', CAST(N'2018-02-28 10:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (710, N'DHSINTRA\RPerumal', CAST(N'2018-02-28 10:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (711, N'DHSINTRA\RPerumal', CAST(N'2018-02-28 10:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (712, N'DHSINTRA\RPerumal', CAST(N'2018-02-28 10:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (713, N'DHSINTRA\RPerumal', CAST(N'2018-02-28 10:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (714, N'DHSINTRA\RPerumal', CAST(N'2018-02-28 11:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (716, N'DHSINTRA\RPerumal', CAST(N'2018-02-28 11:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (718, N'DHSINTRA\RPerumal', CAST(N'2018-02-28 11:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (719, N'DHSINTRA\RPerumal', CAST(N'2018-02-28 11:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (720, N'DHSINTRA\RPerumal', CAST(N'2018-02-28 11:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (721, N'DHSINTRA\RPerumal', CAST(N'2018-02-28 11:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (722, N'DHSINTRA\grichey', CAST(N'2018-02-28 12:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (723, N'DHSINTRA\grichey', CAST(N'2018-02-28 12:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (724, N'DHSINTRA\grichey', CAST(N'2018-02-28 13:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (725, N'DHSINTRA\RPerumal', CAST(N'2018-02-28 13:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (726, N'DHSINTRA\grichey', CAST(N'2018-02-28 13:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (727, N'DHSINTRA\grichey', CAST(N'2018-02-28 14:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (728, N'DHSINTRA\grichey', CAST(N'2018-02-28 14:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (729, N'DHSINTRA\grichey', CAST(N'2018-02-28 14:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (730, N'DHSINTRA\RPerumal', CAST(N'2018-02-28 16:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (731, N'DHSINTRA\RPerumal', CAST(N'2018-03-01 08:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (732, N'DHSINTRA\RPerumal', CAST(N'2018-03-01 08:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (733, N'DHSINTRA\RPerumal', CAST(N'2018-03-01 08:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (734, N'DHSINTRA\grichey', CAST(N'2018-03-01 09:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (735, N'DHSINTRA\grichey', CAST(N'2018-03-01 09:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (736, N'DHSINTRA\grichey', CAST(N'2018-03-01 10:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (737, N'DHSINTRA\grichey', CAST(N'2018-03-01 10:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (738, N'DHSINTRA\grichey', CAST(N'2018-03-01 10:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (739, N'DHSINTRA\RPerumal', CAST(N'2018-03-01 10:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (740, N'DHSINTRA\grichey', CAST(N'2018-03-01 12:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (741, N'DHSINTRA\grichey', CAST(N'2018-03-01 13:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (742, N'DHSINTRA\grichey', CAST(N'2018-03-01 13:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (743, N'DHSINTRA\grichey', CAST(N'2018-03-01 14:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (744, N'DHSINTRA\grichey', CAST(N'2018-03-01 15:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (745, N'DHSINTRA\grichey', CAST(N'2018-03-02 08:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (746, N'DHSINTRA\grichey', CAST(N'2018-03-02 11:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (747, N'DHSINTRA\grichey', CAST(N'2018-03-02 11:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (748, N'DHSINTRA\grichey', CAST(N'2018-03-02 12:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (749, N'DHSINTRA\grichey', CAST(N'2018-03-02 13:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (750, N'DHSINTRA\grichey', CAST(N'2018-03-02 14:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (751, N'DHSINTRA\grichey', CAST(N'2018-03-02 15:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (752, N'DHSINTRA\grichey', CAST(N'2018-03-02 17:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (753, N'DHSINTRA\grichey', CAST(N'2018-03-02 17:54:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (754, N'DHSINTRA\grichey', CAST(N'2018-03-02 18:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (755, N'DHSINTRA\grichey', CAST(N'2018-03-02 19:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (756, N'DHSINTRA\grichey', CAST(N'2018-03-03 10:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (757, N'DHSINTRA\grichey', CAST(N'2018-03-03 11:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (758, N'DHSINTRA\grichey', CAST(N'2018-03-03 12:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (759, N'DHSINTRA\grichey', CAST(N'2018-03-03 12:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (760, N'DHSINTRA\grichey', CAST(N'2018-03-03 13:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (761, N'DHSINTRA\grichey', CAST(N'2018-03-03 14:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (762, N'DHSINTRA\grichey', CAST(N'2018-03-03 14:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (763, N'DHSINTRA\grichey', CAST(N'2018-03-03 14:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (764, N'DHSINTRA\grichey', CAST(N'2018-03-03 16:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (765, N'DHSINTRA\grichey', CAST(N'2018-03-04 08:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (766, N'DHSINTRA\grichey', CAST(N'2018-03-04 09:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (767, N'DHSINTRA\grichey', CAST(N'2018-03-04 09:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (768, N'DHSINTRA\grichey', CAST(N'2018-03-04 10:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (769, N'DHSINTRA\grichey', CAST(N'2018-03-04 10:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (770, N'DHSINTRA\grichey', CAST(N'2018-03-04 10:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (771, N'DHSINTRA\grichey', CAST(N'2018-03-04 10:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (772, N'DHSINTRA\grichey', CAST(N'2018-03-04 11:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (773, N'DHSINTRA\grichey', CAST(N'2018-03-04 12:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (774, N'DHSINTRA\grichey', CAST(N'2018-03-04 12:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (775, N'DHSINTRA\grichey', CAST(N'2018-03-04 12:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (776, N'DHSINTRA\grichey', CAST(N'2018-03-04 13:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (777, N'DHSINTRA\grichey', CAST(N'2018-03-04 13:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (778, N'DHSINTRA\grichey', CAST(N'2018-03-04 13:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (779, N'DHSINTRA\grichey', CAST(N'2018-03-04 13:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (780, N'DHSINTRA\grichey', CAST(N'2018-03-04 14:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (781, N'DHSINTRA\grichey', CAST(N'2018-03-04 14:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (782, N'DHSINTRA\grichey', CAST(N'2018-03-04 14:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (783, N'DHSINTRA\grichey', CAST(N'2018-03-04 15:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (784, N'DHSINTRA\grichey', CAST(N'2018-03-04 15:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (785, N'DHSINTRA\grichey', CAST(N'2018-03-04 17:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (786, N'DHSINTRA\grichey', CAST(N'2018-03-05 10:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (787, N'DHSINTRA\grichey', CAST(N'2018-03-05 14:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (788, N'DHSINTRA\grichey', CAST(N'2018-03-05 15:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (789, N'DHSINTRA\vgrenz', CAST(N'2018-03-05 15:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (790, N'DHSINTRA\vgrenz', CAST(N'2018-03-05 15:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (791, N'DHSINTRA\grichey', CAST(N'2018-03-05 16:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (792, N'DHSINTRA\vgrenz', CAST(N'2018-03-05 16:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (793, N'DHSINTRA\grichey', CAST(N'2018-03-05 16:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (794, N'DHSINTRA\vgrenz', CAST(N'2018-03-05 17:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (795, N'DHSINTRA\grichey', CAST(N'2018-03-05 18:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (796, N'DHSINTRA\grichey', CAST(N'2018-03-05 18:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (797, N'DHSINTRA\grichey', CAST(N'2018-03-05 19:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (798, N'DHSINTRA\grichey', CAST(N'2018-03-05 20:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (799, N'DHSINTRA\grichey', CAST(N'2018-03-05 20:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (800, N'DHSINTRA\grichey', CAST(N'2018-03-06 08:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (801, N'DHSINTRA\grichey', CAST(N'2018-03-06 09:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (802, N'DHSINTRA\grichey', CAST(N'2018-03-06 12:24:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (803, N'DHSINTRA\grichey', CAST(N'2018-03-06 12:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (804, N'DHSINTRA\grichey', CAST(N'2018-03-06 13:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (805, N'DHSINTRA\grichey', CAST(N'2018-03-06 14:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (806, N'DHSINTRA\grichey', CAST(N'2018-03-06 14:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (807, N'DHSINTRA\grichey', CAST(N'2018-03-06 14:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (808, N'DHSINTRA\grichey', CAST(N'2018-03-06 14:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (809, N'DHSINTRA\grichey', CAST(N'2018-03-06 14:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (810, N'DHSINTRA\grichey', CAST(N'2018-03-06 15:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (811, N'DHSINTRA\grichey', CAST(N'2018-03-06 15:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (812, N'DHSINTRA\grichey', CAST(N'2018-03-06 15:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (813, N'DHSINTRA\grichey', CAST(N'2018-03-06 15:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (814, N'DHSINTRA\grichey', CAST(N'2018-03-06 16:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (815, N'DHSINTRA\grichey', CAST(N'2018-03-06 16:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (816, N'dell\user', CAST(N'2018-03-10 00:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (817, N'dell\user', CAST(N'2018-03-10 02:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (818, N'dell\user', CAST(N'2018-03-10 02:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (819, N'dell\user', CAST(N'2018-03-10 02:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (820, N'dell\user', CAST(N'2018-03-10 02:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (821, N'dell\user', CAST(N'2018-03-11 13:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (822, N'dell\user', CAST(N'2018-03-11 13:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (823, N'dell\user', CAST(N'2018-03-11 13:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (824, N'dell\user', CAST(N'2018-03-11 13:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (825, N'dell\user', CAST(N'2018-03-11 13:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (826, N'dell\user', CAST(N'2018-03-11 13:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (827, N'dell\user', CAST(N'2018-03-11 14:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (828, N'dell\user', CAST(N'2018-03-11 14:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (829, N'dell\user', CAST(N'2018-03-11 14:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (830, N'dell\user', CAST(N'2018-03-11 14:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (831, N'dell\user', CAST(N'2018-03-11 14:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (832, N'dell\user', CAST(N'2018-03-11 14:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (833, N'dell\user', CAST(N'2018-03-11 14:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (834, N'dell\user', CAST(N'2018-03-11 15:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (835, N'dell\user', CAST(N'2018-03-11 15:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (836, N'dell\user', CAST(N'2018-03-11 15:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (837, N'dell\user', CAST(N'2018-03-11 16:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (838, N'dell\user', CAST(N'2018-03-11 16:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (839, N'dell\user', CAST(N'2018-03-11 16:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (840, N'dell\user', CAST(N'2018-03-11 16:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (841, N'dell\user', CAST(N'2018-03-11 16:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (842, N'dell\user', CAST(N'2018-03-11 16:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (843, N'dell\user', CAST(N'2018-03-11 16:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (844, N'dell\user', CAST(N'2018-03-11 16:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (845, N'dell\user', CAST(N'2018-03-11 17:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (846, N'dell\user', CAST(N'2018-03-11 17:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (847, N'dell\user', CAST(N'2018-03-11 17:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (848, N'dell\user', CAST(N'2018-03-11 18:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (849, N'dell\user', CAST(N'2018-03-11 18:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (850, N'dell\user', CAST(N'2018-03-11 19:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (851, N'dell\user', CAST(N'2018-03-11 19:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (852, N'dell\user', CAST(N'2018-03-11 19:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (853, N'dell\user', CAST(N'2018-03-11 19:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (854, N'dell\user', CAST(N'2018-03-11 19:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (855, N'dell\user', CAST(N'2018-03-11 19:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (856, N'dell\user', CAST(N'2018-03-11 19:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (857, N'dell\user', CAST(N'2018-03-11 19:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (858, N'dell\user', CAST(N'2018-03-11 19:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (859, N'dell\user', CAST(N'2018-03-11 19:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (860, N'dell\user', CAST(N'2018-03-11 19:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (861, N'dell\user', CAST(N'2018-03-11 19:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (862, N'dell\user', CAST(N'2018-03-11 19:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (863, N'dell\user', CAST(N'2018-03-11 19:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (864, N'dell\user', CAST(N'2018-03-11 20:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (865, N'dell\user', CAST(N'2018-03-11 20:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (866, N'dell\user', CAST(N'2018-03-11 20:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (867, N'dell\user', CAST(N'2018-03-11 20:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (868, N'dell\user', CAST(N'2018-03-11 20:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (869, N'dell\user', CAST(N'2018-03-11 20:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (870, N'dell\user', CAST(N'2018-03-11 21:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (871, N'dell\user', CAST(N'2018-03-11 21:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (872, N'dell\user', CAST(N'2018-03-11 21:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (873, N'dell\user', CAST(N'2018-03-11 21:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (874, N'dell\user', CAST(N'2018-03-11 21:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (875, N'dell\user', CAST(N'2018-03-11 21:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (876, N'dell\user', CAST(N'2018-03-11 21:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (877, N'dell\user', CAST(N'2018-03-11 21:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (878, N'dell\user', CAST(N'2018-03-11 21:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (879, N'dell\user', CAST(N'2018-03-11 21:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (880, N'dell\user', CAST(N'2018-03-11 21:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (881, N'dell\user', CAST(N'2018-03-11 21:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (882, N'dell\user', CAST(N'2018-03-12 22:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (883, N'dell\user', CAST(N'2018-03-12 22:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (884, N'dell\user', CAST(N'2018-03-12 23:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (885, N'dell\user', CAST(N'2018-03-12 23:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (886, N'dell\user', CAST(N'2018-03-12 23:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (887, N'dell\user', CAST(N'2018-03-12 23:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (888, N'dell\user', CAST(N'2018-03-12 23:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (889, N'dell\user', CAST(N'2018-03-12 23:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (890, N'dell\user', CAST(N'2018-03-12 23:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (891, N'dell\user', CAST(N'2018-03-12 23:54:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (892, N'dell\user', CAST(N'2018-03-13 00:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (893, N'dell\user', CAST(N'2018-03-13 00:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (894, N'dell\user', CAST(N'2018-03-13 00:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (895, N'dell\user', CAST(N'2018-03-13 00:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (896, N'dell\user', CAST(N'2018-03-13 00:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (897, N'dell\user', CAST(N'2018-03-13 00:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (898, N'dell\user', CAST(N'2018-03-13 00:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (899, N'dell\user', CAST(N'2018-03-13 00:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (900, N'dell\user', CAST(N'2018-03-13 00:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (901, N'dell\user', CAST(N'2018-03-13 00:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (902, N'dell\user', CAST(N'2018-03-14 22:00:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (903, N'dell\user', CAST(N'2018-03-14 22:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (904, N'dell\user', CAST(N'2018-03-14 22:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (905, N'dell\user', CAST(N'2018-03-14 22:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (906, N'dell\user', CAST(N'2018-03-14 22:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (907, N'dell\user', CAST(N'2018-03-14 22:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (908, N'dell\user', CAST(N'2018-03-14 22:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (909, N'dell\user', CAST(N'2018-03-14 22:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (910, N'dell\user', CAST(N'2018-03-14 22:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (911, N'dell\user', CAST(N'2018-03-14 22:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (912, N'dell\user', CAST(N'2018-03-14 22:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (913, N'dell\user', CAST(N'2018-03-14 23:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (914, N'dell\user', CAST(N'2018-03-14 23:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (915, N'dell\user', CAST(N'2018-03-14 23:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (916, N'dell\user', CAST(N'2018-03-14 23:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (917, N'dell\user', CAST(N'2018-03-14 23:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (918, N'dell\user', CAST(N'2018-03-15 00:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (919, N'dell\user', CAST(N'2018-03-15 00:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (920, N'dell\user', CAST(N'2018-03-15 00:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (921, N'dell\user', CAST(N'2018-03-15 00:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (922, N'dell\user', CAST(N'2018-03-15 00:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (923, N'dell\user', CAST(N'2018-03-15 00:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (924, N'dell\user', CAST(N'2018-03-15 00:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (925, N'dell\user', CAST(N'2018-03-15 00:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (926, N'dell\user', CAST(N'2018-03-15 00:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (927, N'dell\user', CAST(N'2018-03-15 21:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (928, N'dell\user', CAST(N'2018-03-15 21:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (929, N'dell\user', CAST(N'2018-03-15 21:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (930, N'dell\user', CAST(N'2018-03-15 21:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (931, N'dell\user', CAST(N'2018-03-15 21:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (932, N'dell\user', CAST(N'2018-03-15 22:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (933, N'dell\user', CAST(N'2018-03-15 22:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (934, N'dell\user', CAST(N'2018-03-15 22:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (935, N'dell\user', CAST(N'2018-03-16 00:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (936, N'dell\user', CAST(N'2018-03-16 00:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (937, N'dell\user', CAST(N'2018-03-16 00:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (938, N'dell\user', CAST(N'2018-03-16 00:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (939, N'dell\user', CAST(N'2018-03-16 00:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (940, N'dell\user', CAST(N'2018-03-16 00:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (941, N'dell\user', CAST(N'2018-03-16 01:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (942, N'dell\user', CAST(N'2018-03-16 01:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (943, N'dell\user', CAST(N'2018-03-16 01:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (944, N'dell\user', CAST(N'2018-03-16 01:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (945, N'dell\user', CAST(N'2018-03-16 01:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (946, N'dell\user', CAST(N'2018-03-16 01:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (947, N'dell\user', CAST(N'2018-03-16 01:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (948, N'dell\user', CAST(N'2018-03-16 01:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (949, N'dell\user', CAST(N'2018-03-16 01:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (950, N'dell\user', CAST(N'2018-03-17 10:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (951, N'dell\user', CAST(N'2018-03-17 10:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (952, N'dell\user', CAST(N'2018-03-17 10:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (953, N'dell\user', CAST(N'2018-03-17 10:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (954, N'dell\user', CAST(N'2018-03-17 10:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (955, N'dell\user', CAST(N'2018-03-17 10:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (956, N'dell\user', CAST(N'2018-03-17 11:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (957, N'dell\user', CAST(N'2018-03-17 11:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (958, N'dell\user', CAST(N'2018-03-17 11:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (959, N'dell\user', CAST(N'2018-03-17 11:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (960, N'dell\user', CAST(N'2018-03-17 11:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (961, N'dell\user', CAST(N'2018-03-17 11:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (962, N'dell\user', CAST(N'2018-03-17 11:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (963, N'dell\user', CAST(N'2018-03-18 20:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (964, N'dell\user', CAST(N'2018-03-18 20:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (965, N'dell\user', CAST(N'2018-03-18 22:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (966, N'dell\user', CAST(N'2018-03-18 22:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (967, N'dell\user', CAST(N'2018-03-18 22:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (968, N'dell\user', CAST(N'2018-03-18 22:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1963, N'dell\user', CAST(N'2018-03-24 16:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1964, N'dell\user', CAST(N'2018-03-26 23:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1965, N'dell\user', CAST(N'2018-03-26 23:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1966, N'dell\user', CAST(N'2018-03-26 23:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1967, N'dell\user', CAST(N'2018-03-26 23:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1968, N'dell\user', CAST(N'2018-03-26 23:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1969, N'dell\user', CAST(N'2018-03-26 23:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1970, N'dell\user', CAST(N'2018-03-26 23:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1971, N'dell\user', CAST(N'2018-03-26 23:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1972, N'dell\user', CAST(N'2018-03-26 23:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1973, N'dell\user', CAST(N'2018-03-28 00:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1974, N'dell\user', CAST(N'2018-03-29 09:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1975, N'dell\user', CAST(N'2018-03-29 10:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1976, N'dell\user', CAST(N'2018-03-29 10:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1977, N'dell\user', CAST(N'2018-04-10 00:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1978, N'dell\user', CAST(N'2018-04-10 21:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1979, N'dell\user', CAST(N'2018-04-10 21:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1980, N'dell\user', CAST(N'2018-04-10 21:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1981, N'dell\user', CAST(N'2018-04-10 22:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1982, N'dell\user', CAST(N'2018-04-10 22:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1983, N'dell\user', CAST(N'2018-04-13 21:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1984, N'dell\user', CAST(N'2018-04-13 22:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1985, N'dell\user', CAST(N'2018-04-13 22:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1986, N'dell\user', CAST(N'2018-04-13 22:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1987, N'dell\user', CAST(N'2018-04-13 22:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1988, N'dell\user', CAST(N'2018-04-13 22:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1989, N'dell\user', CAST(N'2018-04-13 22:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1990, N'dell\user', CAST(N'2018-04-13 22:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1991, N'dell\user', CAST(N'2018-04-13 22:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1992, N'dell\user', CAST(N'2018-04-13 22:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1993, N'dell\user', CAST(N'2018-04-13 22:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1994, N'dell\user', CAST(N'2018-04-13 22:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1995, N'dell\user', CAST(N'2018-04-13 22:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1996, N'dell\user', CAST(N'2018-04-13 22:36:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1997, N'dell\user', CAST(N'2018-04-13 22:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1998, N'dell\user', CAST(N'2018-04-13 22:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1999, N'dell\user', CAST(N'2018-04-13 22:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2000, N'dell\user', CAST(N'2018-04-13 22:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2001, N'dell\user', CAST(N'2018-04-13 23:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2002, N'dell\user', CAST(N'2018-04-13 23:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2003, N'dell\user', CAST(N'2018-04-13 23:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2004, N'dell\user', CAST(N'2018-04-13 23:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2005, N'dell\user', CAST(N'2018-04-13 23:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2006, N'dell\user', CAST(N'2018-04-14 00:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2007, N'dell\user', CAST(N'2018-04-15 19:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2008, N'dell\user', CAST(N'2018-04-15 19:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2009, N'dell\user', CAST(N'2018-04-15 19:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2010, N'dell\user', CAST(N'2018-04-15 20:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2011, N'dell\user', CAST(N'2018-04-15 20:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2012, N'dell\user', CAST(N'2018-04-15 20:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2013, N'dell\user', CAST(N'2018-04-15 21:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2014, N'dell\user', CAST(N'2018-04-15 21:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2015, N'dell\user', CAST(N'2018-04-15 22:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2016, N'dell\user', CAST(N'2018-04-15 22:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2017, N'dell\user', CAST(N'2018-04-17 22:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3007, N'dell\user', CAST(N'2018-05-07 22:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3008, N'dell\user', CAST(N'2018-05-07 23:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3009, N'dell\user', CAST(N'2018-05-07 23:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3010, N'dell\user', CAST(N'2018-05-07 23:51:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[tUserAccess] OFF
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tBarrier_tEncounter]') AND parent_object_id = OBJECT_ID(N'[dbo].[tBarrier]'))
ALTER TABLE [dbo].[tBarrier]  WITH CHECK ADD  CONSTRAINT [FK_tBarrier_tEncounter] FOREIGN KEY([FK_EncounterID])
REFERENCES [dbo].[tEncounter] ([EncounterID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tBarrier_tEncounter]') AND parent_object_id = OBJECT_ID(N'[dbo].[tBarrier]'))
ALTER TABLE [dbo].[tBarrier] CHECK CONSTRAINT [FK_tBarrier_tEncounter]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tCaregiverApprovals_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tCaregiverApprovals]'))
ALTER TABLE [dbo].[tCaregiverApprovals]  WITH CHECK ADD  CONSTRAINT [FK_tCaregiverApprovals_tServiceRecipient] FOREIGN KEY([FK_NSRecipientID])
REFERENCES [dbo].[tServiceRecipient] ([NSRecipientID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tCaregiverApprovals_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tCaregiverApprovals]'))
ALTER TABLE [dbo].[tCaregiverApprovals] CHECK CONSTRAINT [FK_tCaregiverApprovals_tServiceRecipient]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tEncounter_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tEncounter]'))
ALTER TABLE [dbo].[tEncounter]  WITH CHECK ADD  CONSTRAINT [FK_tEncounter_tNavigator] FOREIGN KEY([FK_NavigatorID])
REFERENCES [dbo].[tNavigator] ([NavigatorID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tEncounter_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tEncounter]'))
ALTER TABLE [dbo].[tEncounter] CHECK CONSTRAINT [FK_tEncounter_tNavigator]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tEncounter_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tEncounter]'))
ALTER TABLE [dbo].[tEncounter]  WITH CHECK ADD  CONSTRAINT [FK_tEncounter_tServiceRecipient] FOREIGN KEY([FK_RecipientID])
REFERENCES [dbo].[tServiceRecipient] ([NSRecipientID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tEncounter_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tEncounter]'))
ALTER TABLE [dbo].[tEncounter] CHECK CONSTRAINT [FK_tEncounter_tServiceRecipient]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecActivityParticipantInfo_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecActivityParticipantInfo]'))
ALTER TABLE [dbo].[tHecActivityParticipantInfo]  WITH CHECK ADD  CONSTRAINT [FK_tHecActivityParticipantInfo_tHecActivities] FOREIGN KEY([ActivityId])
REFERENCES [dbo].[tHecActivities] ([ActivityId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecActivityParticipantInfo_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecActivityParticipantInfo]'))
ALTER TABLE [dbo].[tHecActivityParticipantInfo] CHECK CONSTRAINT [FK_tHecActivityParticipantInfo_tHecActivities]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecScreeningEvent_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecScreeningEvent]'))
ALTER TABLE [dbo].[tHecScreeningEvent]  WITH CHECK ADD  CONSTRAINT [FK_tHecScreeningEvent_tHecActivities] FOREIGN KEY([ActivityId])
REFERENCES [dbo].[tHecActivities] ([ActivityId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecScreeningEvent_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecScreeningEvent]'))
ALTER TABLE [dbo].[tHecScreeningEvent] CHECK CONSTRAINT [FK_tHecScreeningEvent_tHecActivities]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tInstantMessage_tNavigator_rcvdBy]') AND parent_object_id = OBJECT_ID(N'[dbo].[tInstantMessage]'))
ALTER TABLE [dbo].[tInstantMessage]  WITH CHECK ADD  CONSTRAINT [FK_tInstantMessage_tNavigator_rcvdBy] FOREIGN KEY([ReceivedBy])
REFERENCES [dbo].[tNavigator] ([NavigatorID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tInstantMessage_tNavigator_rcvdBy]') AND parent_object_id = OBJECT_ID(N'[dbo].[tInstantMessage]'))
ALTER TABLE [dbo].[tInstantMessage] CHECK CONSTRAINT [FK_tInstantMessage_tNavigator_rcvdBy]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tInstantMessage_tNavigator_RcvdFrom]') AND parent_object_id = OBJECT_ID(N'[dbo].[tInstantMessage]'))
ALTER TABLE [dbo].[tInstantMessage]  WITH CHECK ADD  CONSTRAINT [FK_tInstantMessage_tNavigator_RcvdFrom] FOREIGN KEY([ReceivedFrom])
REFERENCES [dbo].[tNavigator] ([NavigatorID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tInstantMessage_tNavigator_RcvdFrom]') AND parent_object_id = OBJECT_ID(N'[dbo].[tInstantMessage]'))
ALTER TABLE [dbo].[tInstantMessage] CHECK CONSTRAINT [FK_tInstantMessage_tNavigator_RcvdFrom]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNavigationCycle]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle]  WITH CHECK ADD  CONSTRAINT [FK_tNavigationCycle_tNavigationCycle] FOREIGN KEY([NavigationCycleID])
REFERENCES [dbo].[tNavigationCycle] ([NavigationCycleID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNavigationCycle]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle] CHECK CONSTRAINT [FK_tNavigationCycle_tNavigationCycle]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle]  WITH CHECK ADD  CONSTRAINT [FK_tNavigationCycle_tNavigator] FOREIGN KEY([FK_NavigatorID])
REFERENCES [dbo].[tNavigator] ([NavigatorID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle] CHECK CONSTRAINT [FK_tNavigationCycle_tNavigator]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNSProvider]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle]  WITH CHECK ADD  CONSTRAINT [FK_tNavigationCycle_tNSProvider] FOREIGN KEY([FK_ProviderID])
REFERENCES [dbo].[tNSProvider] ([NSProviderID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNSProvider]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle] CHECK CONSTRAINT [FK_tNavigationCycle_tNSProvider]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle]  WITH CHECK ADD  CONSTRAINT [FK_tNavigationCycle_tServiceRecipient] FOREIGN KEY([FK_RecipientID])
REFERENCES [dbo].[tServiceRecipient] ([NSRecipientID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle] CHECK CONSTRAINT [FK_tNavigationCycle_tServiceRecipient]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigatorTraining_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigatorTraining]'))
ALTER TABLE [dbo].[tNavigatorTraining]  WITH CHECK ADD  CONSTRAINT [FK_tNavigatorTraining_tNavigator] FOREIGN KEY([FK_NavigatorID])
REFERENCES [dbo].[tNavigator] ([NavigatorID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigatorTraining_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigatorTraining]'))
ALTER TABLE [dbo].[tNavigatorTraining] CHECK CONSTRAINT [FK_tNavigatorTraining_tNavigator]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tPartnersCollaboratorsContact_tPartnersCollaborators]') AND parent_object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsContact]'))
ALTER TABLE [dbo].[tPartnersCollaboratorsContact]  WITH CHECK ADD  CONSTRAINT [FK_tPartnersCollaboratorsContact_tPartnersCollaborators] FOREIGN KEY([PartnersCollaboratorsId])
REFERENCES [dbo].[tPartnersCollaborators] ([PartnersCollaboratorsId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tPartnersCollaboratorsContact_tPartnersCollaborators]') AND parent_object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsContact]'))
ALTER TABLE [dbo].[tPartnersCollaboratorsContact] CHECK CONSTRAINT [FK_tPartnersCollaboratorsContact_tPartnersCollaborators]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tPartnersCollaboratorsService_tPartnersCollaborators]') AND parent_object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsService]'))
ALTER TABLE [dbo].[tPartnersCollaboratorsService]  WITH CHECK ADD  CONSTRAINT [FK_tPartnersCollaboratorsService_tPartnersCollaborators] FOREIGN KEY([PartnersCollaboratorsId])
REFERENCES [dbo].[tPartnersCollaborators] ([PartnersCollaboratorsId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tPartnersCollaboratorsService_tPartnersCollaborators]') AND parent_object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsService]'))
ALTER TABLE [dbo].[tPartnersCollaboratorsService] CHECK CONSTRAINT [FK_tPartnersCollaboratorsService_tPartnersCollaborators]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tSolution_tBarrier]') AND parent_object_id = OBJECT_ID(N'[dbo].[tSolution]'))
ALTER TABLE [dbo].[tSolution]  WITH CHECK ADD  CONSTRAINT [FK_tSolution_tBarrier] FOREIGN KEY([FK_BarrierID])
REFERENCES [dbo].[tBarrier] ([BarrierID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tSolution_tBarrier]') AND parent_object_id = OBJECT_ID(N'[dbo].[tSolution]'))
ALTER TABLE [dbo].[tSolution] CHECK CONSTRAINT [FK_tSolution_tBarrier]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tSystemMessage_tNavigator_rcvdBy]') AND parent_object_id = OBJECT_ID(N'[dbo].[tSystemMessage]'))
ALTER TABLE [dbo].[tSystemMessage]  WITH CHECK ADD  CONSTRAINT [FK_tSystemMessage_tNavigator_rcvdBy] FOREIGN KEY([ReceivedBy])
REFERENCES [dbo].[tNavigator] ([NavigatorID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tSystemMessage_tNavigator_rcvdBy]') AND parent_object_id = OBJECT_ID(N'[dbo].[tSystemMessage]'))
ALTER TABLE [dbo].[tSystemMessage] CHECK CONSTRAINT [FK_tSystemMessage_tNavigator_rcvdBy]
GO
/****** Object:  StoredProcedure [dbo].[GetCaregiverApproval]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCaregiverApproval]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetCaregiverApproval] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 07/13/2016
-- Description:	Get caregiver approval values for a NS Recipient
-- =============================================
ALTER PROCEDURE [dbo].[GetCaregiverApproval] 
	@NSRecipientID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT A.[ApprovalID]
		  ,A.[FK_NSRecipientID]
		  ,A.[CaregiverApproval]
		  ,CA.CaregiverApprovalDescription
		  ,A.[CaregiverApprovalOther]
		  ,A.[xtag]
		  ,A.[DateCreated]
		  ,A.[CreatedBy]
		  ,A.[LastUpdated]
		  ,A.[UpdatedBy]
	  FROM [dbo].[tCaregiverApprovals] A
	  LEFT JOIN trCaregiverApproval CA
	  ON A.CaregiverApproval = CA.CaregiverApprovalCode
	  WHERE FK_NSRecipientID = @NSRecipientID



    
END

/****** Object:  StoredProcedure [dbo].[usp_SelectNSProvider]    Script Date: 7/15/2016 10:38:55 AM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteHECActivity]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DeleteHECActivity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_DeleteHECActivity] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_DeleteHECActivity]
@ActivityId int
AS
BEGIN
 IF(@ActivityId<>Null)
 Begin
	DELETE fROM tHecScreeningEvent WHERE ActivityId = @ActivityId
	DELETE FROM tHecActivityParticipantInfo WHERE ActivityId = @ActivityId
	DELETE FROM tHecActivities WHERE ActivityId = @ActivityId
 End
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeletePartnersCollaborator]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DeletePartnersCollaborator]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_DeletePartnersCollaborator] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_DeletePartnersCollaborator]
@PartnersCollaboratorsId int
AS
BEGIN
DELETE FROM [dbo].[tPartnersCollaboratorsContact] where PartnersCollaboratorsId = @PartnersCollaboratorsId
DELETE FROM [dbo].[tPartnersCollaboratorsService] where PartnersCollaboratorsId = @PartnersCollaboratorsId
DELETE FROM [dbo].[tPartnersCollaborators] where PartnersCollaboratorsId = @PartnersCollaboratorsId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteSolutions]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DeleteSolutions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_DeleteSolutions] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 08/17/2016
-- Description:	Delete all Solutions for a Barrier
-- =============================================
ALTER PROCEDURE [dbo].[usp_DeleteSolutions] 
	@FK_BarrierID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM tSolution WHERE FK_BarrierID = @FK_BarrierID

END


/****** Object:  StoredProcedure [dbo].[usp_InsertEncounter]    Script Date: 8/19/2016 11:51:21 AM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_GetAllHECActivities]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAllHECActivities]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetAllHECActivities] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_GetAllHECActivities]
AS
BEGIN

SELECT hecmaster.*, hecpartInfo.*,hecscrnevent.* FROM tHecActivities hecmaster
left outer join tHecActivityParticipantInfo hecpartInfo on hecmaster.ActivityId = hecpartInfo.ActivityId
left outer join tHecScreeningEvent hecscrnevent on hecmaster.ActivityId = hecscrnevent.ActivityId


END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAllPatnersAndCollaborators]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAllPatnersAndCollaborators]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetAllPatnersAndCollaborators] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_GetAllPatnersAndCollaborators]
AS
BEGIN

SELECT pmaster.*,pcontract.*,pservice.* from [dbo].[tPartnersCollaborators] pmaster
left outer join [dbo].[tPartnersCollaboratorsContact] pcontract on pmaster.PartnersCollaboratorsId = pcontract.PartnersCollaboratorsId
left outer join [dbo].[tPartnersCollaboratorsService] pservice on pmaster.PartnersCollaboratorsId = pservice.PartnersCollaboratorsId

END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAssistance]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAssistance]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetAssistance] AS' 
END
GO

-- =====================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to get Assistance using AssistanceID
-- Edited by Bilwa Buchake: Added FK_NavigatorID column
-- =====================================================================
ALTER PROCEDURE [dbo].[usp_GetAssistance] 
	-- Add the parameters for the stored procedure here
	@AssistanceID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [AssistanceID] ,
	[FK_NavigatorID] ,
	[IncidentNumber] ,
	[IncidentDt] ,
	[FK_CommunicationCode] ,
	[FirstName] ,
	[Lastname] ,
	[Telephone] ,
	[Email] ,
	[RecipID] ,
	[FK_RequestorCode] ,
	[NPI] ,
	[NPIOwner] ,
	[NPISvcLoc] ,
	[BusinessName] ,
	[DateCreated] ,
	[CreatedBy] ,
	[LastUpdated] ,
	[UpdatedBy] 
FROM [dbo].[tAssistance]
	  WHERE [AssistanceID] = @AssistanceID

END


SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_GetAssistanceIssues]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAssistanceIssues]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetAssistanceIssues] AS' 
END
GO


-- =====================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to get Assistance using IncidentNumber
-- =====================================================================
ALTER PROCEDURE [dbo].[usp_GetAssistanceIssues] 
	-- Add the parameters for the stored procedure here
	@IssueID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [IssueID] ,
	[FK_AssistanceID] ,
	[IncidentNumber] ,
	[FK_EWCRecipientCode] ,
	[FK_EWCPCPCode] ,
	[FK_ReferralProviderCode] ,
	[FK_EWCEnrolleeCode] ,
	[OtherIssues] ,
	[DateCreated] ,
	[CreatedBy] ,
	[LastUpdated] ,
	[UpdatedBy]	 
FROM [dbo].[tAssistanceIssues]
	  WHERE [IssueID] = @IssueID

END




GO
/****** Object:  StoredProcedure [dbo].[usp_GetAssistanceResolution]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAssistanceResolution]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetAssistanceResolution] AS' 
END
GO



-- ==============================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to get Assistance resolution using [ResolutionID]
-- ==============================================================================
ALTER PROCEDURE [dbo].[usp_GetAssistanceResolution] 
	-- Add the parameters for the stored procedure here
	@ResolutionID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [ResolutionID] ,
	[FK_AssistanceID] ,
	[IncidentNumber] ,
	[ResolutionInitiationDt] ,
	[FK_ResolutionCode] ,
	[ResolutionEndDt] ,
	[OtherResolution] ,
	[DateCreated] ,
	[CreatedBy] ,
	[LastUpdated] ,
	[UpdatedBy] 
FROM [dbo].[tAssistanceResolution]
	  WHERE [ResolutionID] = @ResolutionID

END





GO
/****** Object:  StoredProcedure [dbo].[usp_GetBarrier]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetBarrier]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetBarrier] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 06/20/2016
-- Description:	Stored Procedure to get Encounter using EncounterID
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetBarrier] 
	-- Add the parameters for the stored procedure here
	@BarrierID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [BarrierID]
		  ,[FK_EncounterID]
		  ,[BarrierType]
		  ,[BarrierAssessed]
		  ,[FollowUpDt]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
		  ,[Other]
	  FROM [dbo].[tBarrier]
	  WHERE BarrierID = @BarrierID

END


GO
/****** Object:  StoredProcedure [dbo].[usp_GetEncounter]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetEncounter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetEncounter] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 06/20/2016
-- Description:	Stored Procedure to get Encounter using EncounterID
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetEncounter] 
	-- Add the parameters for the stored procedure here
	@EncounterID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [EncounterID]
		  ,[FK_NavigatorID]
		  ,[FK_RecipientID]
		  ,[EncounterNumber]
		  ,[EncounterDt]
		  ,[EncounterStartTime]
		  ,[EncounterEndTime]
		  ,[EncounterType]
		  ,[EncounterReason]
		  ,[MissedAppointment]
		  ,[SvcOutcomeStatus]
		  ,[SvcOutcomeStatusReason]
		  ,[NextAppointmentDt]
		  ,[NextAppointmentTime]
		  ,[NextAppointmentType]
		  ,[ServicesTerminated]
		  ,[ServicesTerminatedDt]
		  ,[ServicesTerminatedReason]
		  ,[Notes]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
		  ,[EncounterPurpose]
	  FROM [dbo].[tEncounter]
	  WHERE EncounterID = @EncounterID

END


/****** Object:  StoredProcedure [dbo].[usp_InsertEncounter]    Script Date: 9/14/2016 9:30:04 AM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_GetHECActivity]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetHECActivity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetHECActivity] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_GetHECActivity]
@ActivityId int
AS
BEGIN

IF(@ActivityId<>NUll)
Begin
SELECT hecmaster.*, hecpartInfo.*,hecscrnevent.* FROM tHecActivities hecmaster
left outer join tHecActivityParticipantInfo hecpartInfo on hecmaster.ActivityId = hecpartInfo.ActivityId
left outer join tHecScreeningEvent hecscrnevent on hecmaster.ActivityId = hecscrnevent.ActivityId
where hecmaster.ActivityId = @ActivityId
END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetInstanceMessageDetails]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetInstanceMessageDetails]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetInstanceMessageDetails] AS' 
END
GO
ALTER procedure [dbo].[usp_GetInstanceMessageDetails]
@messageId int
As
Begin

 Select Id,Subject,Message,ReceivedOn,ReceivedFrom,ReceivedBy,IsRead,
	nav1.LastName+','+nav1.FirstName  as SenderName,
	nav2.LastName+','+nav2.FirstName  as ReceiverName
	 from 
	tInstantMessage im
	inner join tNavigator nav1 on nav1.NavigatorID = im.ReceivedFrom
	inner join tNavigator nav2 on nav2.NavigatorID = im.ReceivedBy
	 where Id = @messageId


End

GO
/****** Object:  StoredProcedure [dbo].[usp_GetLanguagesList]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetLanguagesList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetLanguagesList] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 12/01/2016
-- Description:	Get a list of languages from the NavigatorLanguages table
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetLanguagesList]
	@FK_NavigatorID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- Insert statements for procedure here

	SELECT [NavigatorLanguageID]
      ,[FK_NavigatorID]
      ,L.[LanguageCode]
	  ,L.[LanguageName]
      ,[SpeakingScore]
      ,[ListeningScore]
      ,[ReadingScore]
      ,[WritingScore]
      ,[xtag]
      ,[DateCreated]
      ,[CreatedBy]
      ,[LastUpdated]
      ,[UpdatedBy]
	FROM [EWC_NSAA].[dbo].[tNavigatorLanguages] N
	JOIN trProvLanguages L
	ON N.LanguageCode = L.LanguageCode
	  WHERE FK_NavigatorID = @FK_NavigatorID
	  
END


SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_GetNavigationCycle]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetNavigationCycle]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetNavigationCycle] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 06/21/2016
-- Description:	Stored Procedure to get Navigation Cycle using NavigationCycleID
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetNavigationCycle]
	@mode int,
	@NavigationCycleID int,
	@NSRecipientID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if @mode = 0
	begin
		SELECT [NavigationCycleID]
			  ,[FK_RecipientID]
			  ,[FK_ProviderID]
			  ,[FK_ReferralID]
			  ,[CancerSite]
			  ,[NSProblem]
			  ,[HealthProgram]
			  ,[HealthInsuranceStatus]
			  ,[HealthInsurancePlan]
			  ,[HealthInsurancePlanNumber]
			  ,[ContactPrefDays]
			  ,[ContactPrefHours]
			  ,[ContactPrefHoursOther]
			  ,[ContactPrefType]
			  ,[SRIndentifierCodeGenerated]
			  ,[FK_NavigatorID]
			  ,[xtag]
			  ,[DateCreated]
			  ,[CreatedBy]
			  ,[LastUpdated]
			  ,[UpdatedBy]
		  FROM [dbo].[tNavigationCycle]
		  WHERE NavigationCycleID = @NavigationCycleID
	  end

	  if @mode = 1
	begin
		SELECT [NavigationCycleID]
			  ,[FK_RecipientID]
			  ,[FK_ProviderID]
			  ,[FK_ReferralID]
			  ,[CancerSite]
			  ,[NSProblem]
			  ,[HealthProgram]
			  ,[HealthInsuranceStatus]
			  ,[HealthInsurancePlan]
			  ,[HealthInsurancePlanNumber]
			  ,[ContactPrefDays]
			  ,[ContactPrefHours]
			  ,[ContactPrefHoursOther]
			  ,[ContactPrefType]
			  ,[SRIndentifierCodeGenerated]
			  ,[FK_NavigatorID]
			  ,[xtag]
			  ,[DateCreated]
			  ,[CreatedBy]
			  ,[LastUpdated]
			  ,[UpdatedBy]
		  FROM [dbo].[tNavigationCycle]
		  WHERE FK_RecipientID = @NSRecipientID
	  end

END


/****** Object:  StoredProcedure [dbo].[usp_RecipientReport]    Script Date: 11/2/2016 3:01:55 PM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_GetPartnersAndCollaboratorDetails]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetPartnersAndCollaboratorDetails]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetPartnersAndCollaboratorDetails] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_GetPartnersAndCollaboratorDetails]
@PartnersCollaboratorsId int
AS
BEGIN
IF(@PartnersCollaboratorsId<>null)
BEGIN
SELECT pmaster.*,pcontract.*,pservice.* from [dbo].[tPartnersCollaborators] pmaster
left outer join [dbo].[tPartnersCollaboratorsContact] pcontract on pmaster.PartnersCollaboratorsId = pcontract.PartnersCollaboratorsId
left outer join [dbo].[tPartnersCollaboratorsService] pservice on pmaster.PartnersCollaboratorsId = pservice.PartnersCollaboratorsId
WHERE pmaster.PartnersCollaboratorsId = @PartnersCollaboratorsId
END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetRace]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetRace]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetRace] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 03/28/2016
-- Description:	Get race values for a NS Recipient
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetRace] 
	@NSRecipientID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT R.[RaceID]
      ,R.[FK_NSRecipientID]
      ,R.[RaceCode]
      ,RR.[RaceName]
      ,R.[RaceOther]
      ,R.[xtag]
      ,R.[DateCreated]
      ,R.[CreatedBy]
      ,R.[LastUpdated]
      ,R.[UpdatedBy]
  FROM [EWC_NSAA].[dbo].[tRace] R
  LEFT JOIN trRace RR
  ON R.RaceCode = RR.RaceCode
  WHERE FK_NSRecipientID = @NSRecipientID

    
END


/****** Object:  StoredProcedure [dbo].[usp_SelectCaregiverApproval]    Script Date: 7/12/2016 1:57:58 PM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_GetRecipient]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetRecipient]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetRecipient] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 06/20/2016
-- Description:	Stored Procedure to get Service Recipient using NSID
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetRecipient]
	@NSRecipientID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [NSRecipientID]
		  ,[FK_NavigatorID]
		  ,[FK_RecipID]
		  ,[LastName]
		  ,[FirstName]
		  ,[MiddleInitial]
		  ,[DOB]
		  ,[AddressNotAvailable]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[Zip]
		  ,[Phone1]
		  ,[P1PhoneType]
		  ,[P1PersonalMessage]
		  ,[Phone2]
		  ,[P2PhoneType]
		  ,[P2PhoneTypeOther]
		  ,[P2PersonalMessage]
		  ,[MotherMaidenName]
		  ,[Email]
		  ,[SSN]
		  ,[ImmigrantStatus]
		  ,[CountryOfOrigin]
		  ,[Gender]
		  ,[Ethnicity]
		  ,[PrimaryLanguage]
		  ,[AgreeToSpeakEnglish]
		  ,[CaregiverName]
		  ,[Relationship]
		  ,[RelationshipOther]
		  ,[CaregiverPhone]
		  ,[CaregiverPhoneType]
		  ,[CaregiverPhonePersonalMessage]
		  ,[CaregiverEmail]
		  ,[CaregiverContactPreference]
		  ,[Comments]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
	  FROM [dbo].[tServiceRecipient]
	  WHERE NSRecipientID = @NSRecipientID

END



GO
/****** Object:  StoredProcedure [dbo].[usp_GetScreeningNav]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetScreeningNav]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetScreeningNav] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 08/01/2017
-- Description:	Stored Procedure to get list of Screening Nav using FK_NavigatorID
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetScreeningNav]
	@FK_NavigatorID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT [SN_ID]
      ,[FK_NavigatorID]
      ,[SN_CODE1]
      ,[SN_CODE2]
      ,[LastName]
      ,[FirstName]
      ,[DOB]
      ,[Address1]
      ,[Address2]
      ,[City]
      ,[State]
      ,[Zip]
      ,[HomeTelephone]
      ,[Cellphone]
      ,[Email]
      ,[Computer]
      ,[TextMessage]
      ,[DateOfContact1]
      ,[ApptScreen1]
      ,[SvcDate1]
      ,[SvcType1]
      ,[Response1]
      ,[DateOfContact2]
      ,[ApptScreen2]
      ,[SvcDate2]
      ,[SvcType2]
      ,[Response2]
      ,[NSEnrollment]
      ,[EnrollmentDate]
      ,[xtag]
      ,[DateCreated]
      ,[CreatedBy]
      ,[LastUpdated]
      ,[UpdatedBy]
  FROM [dbo].[tScreeningNav]
  WHERE FK_NavigatorID = @FK_NavigatorID
  ORDER BY SN_ID DESC



END




GO
/****** Object:  StoredProcedure [dbo].[usp_GetSystemMessageDetails]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetSystemMessageDetails]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetSystemMessageDetails] AS' 
END
GO
ALTER procedure [dbo].[usp_GetSystemMessageDetails]
@messageId int
As
Begin

Select * from tSystemMessage where Id = @messageId


End

GO
/****** Object:  StoredProcedure [dbo].[usp_GetTrainingsList]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetTrainingsList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetTrainingsList] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 11/15/2016
-- Description:	get list of Navigator Trainings
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetTrainingsList]
	@FK_NavigatorID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [TrainingID]
		  ,[FK_NavigatorID]
		  ,[CourseName]
		  ,[Organization]
		  ,[CompletionDt]
		  ,[Certificate]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
	  FROM [dbo].[tNavigatorTraining]
	  WHERE FK_NavigatorID = @FK_NavigatorID


 
END

/****** Object:  StoredProcedure [dbo].[usp_InsertEncounter]    Script Date: 11/17/2016 1:19:25 PM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserInstanceMessages]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetUserInstanceMessages]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetUserInstanceMessages] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_GetUserInstanceMessages] 
	-- Add the parameters for the stored procedure here
	@NavigatorId int
AS
BEGIN
	
	SET NOCOUNT ON;

	Select Id,Subject,Message,ReceivedOn,ReceivedFrom,ReceivedBy,IsRead,
	nav1.LastName+','+nav1.FirstName  as SenderName,
	nav2.LastName+','+nav2.FirstName  as ReceiverName
	 from 
	tInstantMessage im
	inner join tNavigator nav1 on nav1.NavigatorID = im.ReceivedFrom
	inner join tNavigator nav2 on nav2.NavigatorID = im.ReceivedBy
	where ReceivedBy=@NavigatorId
	order by im.ReceivedOn desc
	
	

END


GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserSendInstanceMessages]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetUserSendInstanceMessages]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetUserSendInstanceMessages] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_GetUserSendInstanceMessages] 
	-- Add the parameters for the stored procedure here
	@NavigatorId int
AS
BEGIN
	
	SET NOCOUNT ON;

	Select Id,Subject,Message,ReceivedOn,ReceivedFrom,ReceivedBy,IsRead,
	nav1.LastName+','+nav1.FirstName as SenderName,
	nav2.LastName+','+nav2.FirstName as ReceiverName
	 from 
	tInstantMessage im
	inner join tNavigator nav1 on nav1.NavigatorID = im.ReceivedFrom
	inner join tNavigator nav2 on nav2.NavigatorID = im.ReceivedBy
	where ReceivedFrom=@NavigatorId  
	order by im.ReceivedOn desc

END



GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserSystemInstanceMessages]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetUserSystemInstanceMessages]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetUserSystemInstanceMessages] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_GetUserSystemInstanceMessages] 
	-- Add the parameters for the stored procedure here
	@NavigatorId varchar(150)
AS
BEGIN
	
	SET NOCOUNT ON;

	Select Id,Subject,Message,ReceivedOn,ReceivedBy,IsRead from 
	tSystemMessage where ReceivedBy=@NavigatorId  

	

END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserUnreadMessageCount]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetUserUnreadMessageCount]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetUserUnreadMessageCount] AS' 
END
GO

ALTER PROCEDURE [dbo].[usp_GetUserUnreadMessageCount] 	
	@NavigatorId int
AS
BEGIN
	
	SET NOCOUNT ON;

	select count(1) from tInstantMessage where ReceivedBy = @NavigatorId	
	

END


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertAssistance]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertAssistance]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertAssistance] AS' 
END
GO

-- ======================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to insert Assistance 
-- Edited by Bilwa Buchake: Added FK_NavigatorID column
-- ======================================================
ALTER PROCEDURE [dbo].[usp_InsertAssistance] 
	-- Add the parameters for the stored procedure here
	@AssistanceID int OUTPUT,
	@FK_NavigatorID int,
	@IncidentNumber char(10),
	@IncidentDt smalldatetime ,
	@FK_CommunicationCode smallint ,
	@FirstName varchar(20) ,
	@Lastname varchar(30) ,
	@Telephone char(10) ,
	@Email varchar(45) ,
	@RecipID char(14) ,
	@FK_RequestorCode smallint ,
	@NPI varchar(10) ,
	@NPIOwner varchar(2) ,
	@NPISvcLoc varchar(3) ,
	@BusinessName varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [EWC_NSAA].[dbo].[tAssistance] (
	[FK_NavigatorID],
	[IncidentNumber] ,
	[IncidentDt] ,
	[FK_CommunicationCode] ,
	[FirstName] ,
	[Lastname] ,
	[Telephone] ,
	[Email] ,
	[RecipID] ,
	[FK_RequestorCode] ,
	[NPI] ,
	[NPIOwner] ,
	[NPISvcLoc] ,
	[BusinessName] ,
	[DateCreated],
	[CreatedBy],
	[LastUpdated],
	[UpdatedBy])

VALUES
  (
	@FK_NavigatorID , 
  	@IncidentNumber ,
	@IncidentDt  ,
	@FK_CommunicationCode  ,
	@FirstName  ,
	@Lastname  ,
	@Telephone  ,
	@Email  ,
	@RecipID  ,
	@FK_RequestorCode  ,
	@NPI ,
	@NPIOwner  ,
	@NPISvcLoc  ,
	@BusinessName,
	getdate() ,
	'User input' ,
	NULL ,
	NULL) 

	SELECT @AssistanceID = SCOPE_IDENTITY()

END

SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertAssistanceIssues]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertAssistanceIssues]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertAssistanceIssues] AS' 
END
GO


-- ======================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to insert Assistance 
-- ======================================================
ALTER PROCEDURE [dbo].[usp_InsertAssistanceIssues] 
	-- Add the parameters for the stored procedure here
	@IssueID int OUTPUT,
	@FK_AssistanceID int,
	@IncidentNumber char(10),
	@FK_EWCRecipientCode smallint ,
	@FK_EWCPCPCode smallint ,
	@FK_ReferralProviderCode smallint ,
	@FK_EWCEnrolleeCode smallint,
	@OtherIssues varchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [EWC_NSAA].[dbo].[tAssistanceIssues] (
	[FK_AssistanceID] ,
	[IncidentNumber] ,
	[FK_EWCRecipientCode] ,
	[FK_EWCPCPCode] ,
	[FK_ReferralProviderCode] ,
	[FK_EWCEnrolleeCode] ,
	[OtherIssues] ,
	[DateCreated] ,
	[CreatedBy] ,
	[LastUpdated] ,
	[UpdatedBy])

VALUES
  (
  	@FK_AssistanceID ,
	@IncidentNumber  ,
	@FK_EWCRecipientCode  ,
	@FK_EWCPCPCode  ,
	@FK_ReferralProviderCode  ,
	@FK_EWCEnrolleeCode  ,
	@OtherIssues,
	getdate() ,
	'User input' ,
	NULL ,
	NULL) 

	SELECT @IssueID = SCOPE_IDENTITY()

END




GO
/****** Object:  StoredProcedure [dbo].[usp_InsertAssistanceResolution]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertAssistanceResolution]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertAssistanceResolution] AS' 
END
GO



-- ======================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to insert Assistance 
-- ======================================================
ALTER PROCEDURE [dbo].[usp_InsertAssistanceResolution] 
	-- Add the parameters for the stored procedure here
	@ResolutionID int OUTPUT,
	@FK_AssistanceID int ,
	@IncidentNumber char(10),
	@ResolutionInitiationDt smalldatetime,
	@FK_ResolutionCode smallint,
	@ResolutionEndDt smalldatetime,
	@OtherResolution varchar(500) 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [EWC_NSAA].[dbo].[tAssistanceResolution] (
	[FK_AssistanceID] ,
	[IncidentNumber] ,
	[ResolutionInitiationDt] ,
	[FK_ResolutionCode] ,
	[ResolutionEndDt] ,
	[OtherResolution] ,
	[DateCreated] ,
	[CreatedBy] ,
	[LastUpdated] ,
	[UpdatedBy] )

VALUES
  (
  	@FK_AssistanceID ,
	@IncidentNumber ,
	@ResolutionInitiationDt  ,
	@FK_ResolutionCode  ,
	@ResolutionEndDt  ,
	@OtherResolution,
	getdate() ,
	'User input' ,
	NULL ,
	NULL) 

	SELECT @ResolutionID = SCOPE_IDENTITY()

END





GO
/****** Object:  StoredProcedure [dbo].[usp_InsertBarrier]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertBarrier]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertBarrier] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Insert new Barrier
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertBarrier]
	-- Add the parameters for the stored procedure here
	@BarrierID int OUTPUT,
	@FK_EncounterID int
    ,@BarrierType int
    ,@BarrierAssessed int
    ,@FollowUpDt smalldatetime
	,@Other varchar(2000)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	INSERT INTO [dbo].[tBarrier]
           ([FK_EncounterID]
           ,[BarrierType]
           ,[BarrierAssessed]
           ,[FollowUpDt]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy]
		   ,[Other])
     VALUES
           (@FK_EncounterID
           ,@BarrierType
           ,@BarrierAssessed
           ,@FollowUpDt
           ,NULL
           ,GETDATE()
           ,'User input'
           ,NULL
           ,NULL
		   ,@Other)

		   Select @BarrierID = SCOPE_IDENTITY()

END




GO
/****** Object:  StoredProcedure [dbo].[usp_InsertEncounter]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertEncounter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertEncounter] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/17/2016
-- Description:	Insert new Encounter record
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertEncounter]
	-- Add the parameters for the stored procedure here
			@EncounterID int OUTPUT,
			@FK_NavigatorID int
           ,@FK_RecipientID int
           ,@EncounterNumber int
           ,@EncounterDt smalldatetime
           ,@EncounterStartTime varchar(20)
           ,@EncounterEndTime varchar(20)
           ,@EncounterType int
		   ,@EncounterReason int
           ,@MissedAppointment bit
           ,@SvcOutcomeStatus int
           ,@SvcOutcomeStatusReason varchar(200)
           ,@NextAppointmentDt smalldatetime
           ,@NextAppointmentTime varchar(20)
           ,@NextAppointmentType int
           ,@ServicesTerminated bit
           ,@ServicesTerminatedDt smalldatetime
           ,@ServicesTerminatedReason varchar(200)
           ,@Notes varchar(2000)
		   ,@EncounterPurpose int
           
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	INSERT INTO [dbo].[tEncounter]
           ([FK_NavigatorID]
           ,[FK_RecipientID]
           ,[EncounterNumber]
           ,[EncounterDt]
           ,[EncounterStartTime]
           ,[EncounterEndTime]
           ,[EncounterType]
		   ,[EncounterReason]
           ,[MissedAppointment]
           ,[SvcOutcomeStatus]
           ,[SvcOutcomeStatusReason]
           ,[NextAppointmentDt]
           ,[NextAppointmentTime]
           ,[NextAppointmentType]
           ,[ServicesTerminated]
           ,[ServicesTerminatedDt]
           ,[ServicesTerminatedReason]
           ,[Notes]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy]
		   ,[EncounterPurpose])
     VALUES
           (@FK_NavigatorID
           ,@FK_RecipientID
           ,@EncounterNumber
           ,@EncounterDt
           ,@EncounterStartTime
           ,@EncounterEndTime
           ,@EncounterType
		   ,@EncounterReason
           ,@MissedAppointment
           ,@SvcOutcomeStatus
           ,@SvcOutcomeStatusReason
           ,@NextAppointmentDt
           ,@NextAppointmentTime
           ,@NextAppointmentType
           ,@ServicesTerminated
           ,@ServicesTerminatedDt
           ,@ServicesTerminatedReason
           ,@Notes
           ,NULL
           ,GETDATE()
           ,'User Input'
           ,NULL
           ,NULL
		   ,@EncounterPurpose)

		   Select @EncounterID = SCOPE_IDENTITY()

END



/****** Object:  StoredProcedure [dbo].[usp_UpdateEncounter]    Script Date: 9/14/2016 9:31:04 AM ******/
SET ANSI_NULLS ON


/****** Object:  StoredProcedure [dbo].[usp_UpdateEncounter]    Script Date: 11/17/2016 1:20:54 PM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertHECActivity]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertHECActivity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertHECActivity] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_InsertHECActivity]
@ActivityDate	datetime,	
@ActivityType	smallint,
@NameOrPurpose	varchar(250),	
@CollaboratorId	int,	
@CollaboratorContributionId	int,	
@Address1	nvarchar(100),
@Address2	nvarchar(100),	
@CityId	int,	
@Zip	varchar(5),	
@CHW	int,
@OtherAttendee	nvarchar(100),	
@Population	varchar(50),	
@LanguageId	int,	
@Discussed	int,	
@PrePostTest	int,	
@Result	int,	
@MyRole	int,	
@Attendee	bit,	
@AnnoucementDoc	varchar(300),	
@Travel	bit,	
@Notes	nvarchar(250),

@FirstName	nvarchar(50),	
@LastName	nvarchar(50),	
@Month	smallint,	
@Year	int,	
@RaceOrEthinicity	int,	
@Part_Info_Address1	nvarchar(100),	
@Part_Info_Address2	nvarchar(100),	
@Part_Info_CityId	int,
@Part_Info_Zip	varchar(5),	
@Telephone	varchar(50),	
@Email	varchar(150),	
@Part_Info_LanguageId	int,	

@BusinessName	nvarchar(100),	
@NPI	varchar(50),	
@ProviderFirstName	nvarchar(50),	
@ProviderLastName	nvarchar(50),	
@EwcProvider	smallint

AS
Begin

DECLARE @ActivityId int = null
INSERT INTO tHecActivities( ActivityDate,  ActivityType, NameOrPurpose,  CollaboratorId,  CollaboratorContributionId,  Address1,  Address2,  CityId,  Zip,  CHW,  OtherAttendee,
 Population,  LanguageId,  Discussed,  PrePostTest,  Result,  MyRole,  Attendee,  AnnoucementDoc,  Travel,  Notes)
 VALUES (@ActivityDate,@ActivityType	,@NameOrPurpose	,@CollaboratorId	,@CollaboratorContributionId	,@Address1	,@Address2	,@CityId	,	
@Zip	,@CHW	,@OtherAttendee	,	@Population	,	@LanguageId	,@Discussed	,@PrePostTest	,	@Result	,@MyRole	,@Attendee	,	@AnnoucementDoc	,	
@Travel	,	@Notes)

set @ActivityId = SCOPE_IDENTITY()

IF(@ActivityId<>NULL)
Begin
INSERT INTO tHecActivityParticipantInfo(ActivityId,FirstName,LastName,Month,Year,RaceOrEthinicity,Address1,Address2,CityId,Zip,Telephone,Email,LanguageId)
VALUES (@ActivityId,@FirstName,@LastName	,@Month	,@Year	,@RaceOrEthinicity	,	@Part_Info_Address1	,@Part_Info_Address2,	@Part_Info_CityId	,
@Part_Info_Zip,	@Telephone,@Email	,@Part_Info_LanguageId)

INSERT INTO tHecScreeningEvent(ActivityId,BusinessName,NPI,ProviderFirstName,ProviderLastName,EwcProvider)
VALUES (@ActivityId,@BusinessName,@NPI,@ProviderFirstName,@ProviderLastName,@EwcProvider)


END



END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertInstantMessage]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertInstantMessage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertInstantMessage] AS' 
END
GO
ALTER Procedure [dbo].[usp_InsertInstantMessage]
@subject nvarchar(150),
@message nvarchar(max),
@receivedfrom int,
@receivedby int,
@ParentId int
As
Begin


insert into tInstantMessage(Subject,
Message,
ReceivedOn,
ReceivedFrom,
ReceivedBy,
IsRead,ParentId)
values ( @subject,@message,GETDATE(),@receivedfrom,@receivedby,0,IIF(@ParentId=0,null,@ParentId))


End



GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNavigationCycle]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNavigationCycle]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertNavigationCycle] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/12/2016
-- Description:	Insert a new Navigation Cycle
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertNavigationCycle] 
			@NavigationCycleID int OUTPUT,
			@FK_RecipientID int
           ,@FK_ProviderID int
           ,@FK_ReferralID int
           ,@CancerSite int
           ,@NSProblem int
           ,@HealthProgram int
           ,@HealthInsuranceStatus int
           ,@HealthInsurancePlan int
           ,@HealthInsurancePlanNumber varchar(15)
           ,@ContactPrefDays varchar(15)
           ,@ContactPrefHours varchar(15)
           ,@ContactPrefHoursOther varchar(20)
           ,@ContactPrefType int
           ,@SRIndentifierCodeGenerated bit
           ,@FK_NavigatorID int
           
           
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [EWC_NSAA].[dbo].[tNavigationCycle]
           ([FK_RecipientID]
           ,[FK_ProviderID]
           ,[FK_ReferralID]
           ,[CancerSite]
           ,[NSProblem]
           ,[HealthProgram]
           ,[HealthInsuranceStatus]
           ,[HealthInsurancePlan]
           ,[HealthInsurancePlanNumber]
           ,[ContactPrefDays]
           ,[ContactPrefHours]
           ,[ContactPrefHoursOther]
           ,[ContactPrefType]
           ,[SRIndentifierCodeGenerated]
           ,[FK_NavigatorID]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy])
     VALUES
           (@FK_RecipientID
           ,@FK_ProviderID
           ,@FK_ReferralID
           ,@CancerSite
           ,@NSProblem
           ,@HealthProgram
           ,@HealthInsuranceStatus
           ,@HealthInsurancePlan
           ,@HealthInsurancePlanNumber
           ,@ContactPrefDays
           ,@ContactPrefHours
           ,@ContactPrefHoursOther
           ,@ContactPrefType
           ,@SRIndentifierCodeGenerated
           ,@FK_NavigatorID
           ,NULL
           ,getdate()
           ,'User Input'
           ,NULL
           ,NULL)

		   Select @NavigationCycleID = SCOPE_IDENTITY()
END

/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigationCycle]    Script Date: 8/10/2016 2:46:18 PM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNavigator]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNavigator]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertNavigator] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/05/2016	
-- Description:	Insert a new record in Navigator table
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertNavigator]
	-- Add the parameters for the stored procedure here
			@NavigatorID int OUTPUT
		   ,@DomainName varchar(20)
		   ,@Region smallint
           ,@Type int
           ,@LastName varchar(30)
           ,@FirstName varchar(20)
           ,@Address1 varchar(30)
           ,@Address2 varchar(30)
           ,@City varchar(25)
           ,@State char(2)
           ,@Zip char(5)
           ,@BusinessTelephone char(10)
		   ,@MobileTelephone char(10)
           ,@Email varchar(45)
           
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	INSERT INTO [dbo].[tNavigator]
           ([DomainName]
		   ,[Region]
           ,[Type]
           ,[LastName]
           ,[FirstName]
           ,[Address1]
           ,[Address2]
           ,[City]
           ,[State]
           ,[Zip]
           ,[BusinessTelephone]
		   ,[MobileTelephone]
           ,[Email]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy])
     VALUES
           (@DomainName
		   ,@Region
           ,@Type
           ,@LastName
           ,@FirstName
           ,@Address1
           ,@Address2
           ,@City
           ,@State
           ,@Zip
           ,@BusinessTelephone
		   ,@MobileTelephone
           ,@Email
           ,NULL
           ,getdate()
           ,'User Input'
           ,NULL
           ,NULL)

		   Select @NavigatorID = SCOPE_IDENTITY()
END


/****** Object:  StoredProcedure [dbo].[usp_SelectNavigator]    Script Date: 7/5/2016 2:16:15 PM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNavigatorLanguage]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNavigatorLanguage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertNavigatorLanguage] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 12/12/2016
-- Description:	Insert a record in the Navigator Languages table
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertNavigatorLanguage]
	-- Add the parameters for the stored procedure here

	@FK_NavigatorID int,
	@LanguageCode int,
	@SpeakingScore int,
	@ListeningScore int,
	@ReadingScore int,
	@WritingScore int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[tNavigatorLanguages]
			   ([FK_NavigatorID]
			   ,[LanguageCode]
			   ,[SpeakingScore]
			   ,[ListeningScore]
			   ,[ReadingScore]
			   ,[WritingScore]
			   ,[DateCreated]
			   ,[CreatedBy]
			   )
		 VALUES
			   (@FK_NavigatorID
			   ,@LanguageCode
			   ,@SpeakingScore
			   ,@ListeningScore
			   ,@ReadingScore
			   ,@WritingScore
			   ,GETDATE()
			   ,'User Input')

END


/****** Object:  StoredProcedure [dbo].[usp_SelectTransferReasons]    Script Date: 12/27/2016 11:01:57 AM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNavigatorTraining]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNavigatorTraining]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertNavigatorTraining] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 11/17/2016
-- Description:	Insert a new Navigator Training record
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertNavigatorTraining] 
	@FK_NavigatorID int,
	@CourseName varchar(100),
	@Organization varchar(100),
	@CompletionDt smalldatetime,
	@Certificate bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	INSERT INTO [dbo].[tNavigatorTraining]
           ([FK_NavigatorID]
           ,[CourseName]
           ,[Organization]
           ,[CompletionDt]
           ,[Certificate]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy])
     VALUES
           (@FK_NavigatorID
           ,@CourseName
           ,@Organization
           ,@CompletionDt
           ,@Certificate
           ,NULL
           ,GETDATE()
           ,'User Input'
           ,NULL
           ,NULL)



END


SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNSProvider]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNSProvider]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertNSProvider] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 04/28/2016
-- Description:	Stored Procedure for inserting a new PCP record
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertNSProvider] 
	-- Add the parameters for the stored procedure here
	@NSProviderID int OUTPUT,
	@PCPName varchar(100),
	@EWCPCP bit,
    @PCP_NPI varchar(10),
    @PCP_Owner varchar(2),
    @PCP_Location varchar(3),
    @PCPAddress1 varchar(30),
    @PCPAddress2 varchar(30),
    @PCPCity varchar(24),
    @PCPState char(2),
    @PCPZip char(5),
    @PCPContactFName varchar(50),
    @PCPContactLName varchar(50),
    @PCPContactTitle varchar(50),
    @PCPContactTelephone char(10),
    @PCPContactEmail varchar(45),
    @Medical bit,
    @MedicalSpecialty int,
    @MedicalSpecialtyOther varchar(100),
    @ManagedCarePlan int,
    @ManagedCarePlanOther varchar(100)
    
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [EWC_NSAA].[dbo].[tNSProvider]
           ([PCPName]
		   ,[EWCPCP]
           ,[PCP_NPI]
           ,[PCP_Owner]
           ,[PCP_Location]
           ,[PCPAddress1]
           ,[PCPAddress2]
           ,[PCPCity]
           ,[PCPState]
           ,[PCPZip]
           ,[PCPContactFName]
           ,[PCPContactLName]
           ,[PCPContactTitle]
           ,[PCPContactTelephone]
           ,[PCPContactEmail]
           ,[Medical]
           ,[MedicalSpecialty]
           ,[MedicalSpecialtyOther]
           ,[ManagedCarePlan]
           ,[ManagedCarePlanOther]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy])
     VALUES
           (@PCPName
		   ,@EWCPCP
           ,@PCP_NPI
           ,@PCP_Owner
           ,@PCP_Location
           ,@PCPAddress1
           ,@PCPAddress2
           ,@PCPCity
           ,@PCPState
           ,@PCPZip
           ,@PCPContactFName
           ,@PCPContactLName
           ,@PCPContactTitle
           ,@PCPContactTelephone
           ,@PCPContactEmail
           ,@Medical
           ,@MedicalSpecialty
           ,@MedicalSpecialtyOther
           ,@ManagedCarePlan
           ,@ManagedCarePlanOther
           ,NULL
           ,getdate()
           ,'User Input'
           ,NULL
           ,NULL)

		   Select @NSProviderID = SCOPE_IDENTITY()
END







GO
/****** Object:  StoredProcedure [dbo].[usp_InsertPartnersCollaborator]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertPartnersCollaborator]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertPartnersCollaborator] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_InsertPartnersCollaborator]
@NameofOrg1	nvarchar(200),
@NameOfOrg2	nvarchar(200),
@Abbreviation	nvarchar(100),
@Address1	nvarchar(100),
@Address2	nvarchar(100),
@City	nvarchar(100),
@ZipCode int,
@Phone	varchar(15),
@Email	varchar(100),
@WebAddress	varchar	(250),

@FirstName	nvarchar(100),
@LastName	nvarchar(100),
@Title	nvarchar(20),
@Contact_Phone	varchar(15),
@Ext	int,
@Contact_Email	varchar(100),
@DateOfLastContact	datetime,

@OrgType1	int	,
@OrgType2	int	,
@ServiceCategory1	int	,
@ServiceCaregory2	int	,
@ServicePopulation1	int	,
@ServicePopulation2	int	,
@Language1	int	,
@Language2	int,
@Area	varchar	(50),
@Region	int	,
@Country1	int	,
@Country2	int	

AS
BEGIN
DECLARE @PartnersCollaboratorsId int
INSERT INTO tPartnersCollaborators(NameofOrg1,NameOfOrg2,Abbreviation,Address1,Address2,City,ZipCode,Phone,Email,WebAddress)
VALUES(@NameofOrg1,@NameOfOrg2,@Abbreviation,@Address1,@Address2,@City,@ZipCode,@Phone,@Email,@WebAddress)

SET @PartnersCollaboratorsId= SCOPE_IDENTITY()

IF ( @PartnersCollaboratorsId<>null)
BEGIN

INSERT INTO tPartnersCollaboratorsContact(PartnersCollaboratorsId,FirstName,LastName,Title,Phone,Ext,Email,DateOfLastContact)
VALUES (@PartnersCollaboratorsId,@FirstName,@LastName,@Title,@Contact_Phone,@Ext,@Contact_Email,@DateOfLastContact)
END


INSERT INTO tPartnersCollaboratorsService(PartnersCollaboratorsId,OrgType1,OrgType2,ServiceCategory1,ServiceCaregory2,ServicePopulation1,
ServicePopulation2,Language1,Language2,Area,Region,Country1,Country2)
VALUES(@PartnersCollaboratorsId,@OrgType1,@OrgType2,@ServiceCategory1,@ServiceCaregory2,@ServicePopulation1,
@ServicePopulation2,@Language1,@Language2,@Area,@Region,@Country1,@Country2)

END

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertScreeningNav]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertScreeningNav]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertScreeningNav] AS' 
END
GO


-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 07/31/2017	
-- Description:	Insert a new record in Screening Navigation table
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertScreeningNav]
	-- Add the parameters for the stored procedure here
		   @SN_ID int OUTPUT
		   ,@FK_NavigatorID int
		   ,@SN_CODE1 varchar(11)
           ,@SN_CODE2 smallint
           ,@LastName varchar(30)
           ,@FirstName varchar(20)
           ,@DOB smalldatetime
           ,@Address1 varchar(30)
           ,@Address2 varchar(30)
           ,@City varchar(25)
           ,@State char(2)
           ,@Zip char(5)
           ,@HomeTelephone char(10)
           ,@Cellphone char(10)
           ,@Email varchar(45)
           ,@Computer bit
           ,@TextMessage char(1)
           ,@DateOfContact1 smalldatetime
           ,@ApptScreen1 char(1)
           ,@SvcDate1 smalldatetime
           ,@SvcType1 char(2)
           ,@Response1 char(2)
           ,@DateOfContact2 smalldatetime
           ,@ApptScreen2 char(1)
           ,@SvcDate2 smalldatetime
           ,@SvcType2 char(2)
           ,@Response2 char(2)
           ,@NSEnrollment char(1)
           ,@EnrollmentDate smalldatetime
           
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SET @SN_CODE2 = (SELECT ISNULL(MAX(SN_Code2),0) FROM tScreeningNav WHERE SN_CODE1 = @SN_CODE1) + 1
	

	INSERT INTO [dbo].[tScreeningNav]
           (FK_NavigatorID
		   ,[SN_CODE1]
           ,[SN_CODE2]
           ,[LastName]
           ,[FirstName]
           ,[DOB]
           ,[Address1]
           ,[Address2]
           ,[City]
           ,[State]
           ,[Zip]
           ,[HomeTelephone]
           ,[Cellphone]
           ,[Email]
           ,[Computer]
           ,[TextMessage]
           ,[DateOfContact1]
           ,[ApptScreen1]
           ,[SvcDate1]
           ,[SvcType1]
           ,[Response1]
           ,[DateOfContact2]
           ,[ApptScreen2]
           ,[SvcDate2]
           ,[SvcType2]
           ,[Response2]
           ,[NSEnrollment]
           ,[EnrollmentDate]
		   ,[DateCreated]
		   ,[CreatedBy])
     VALUES
           (@FK_NavigatorID
		   ,@SN_CODE1
           ,@SN_CODE2
           ,@LastName
           ,@FirstName
           ,@DOB
           ,@Address1
           ,@Address2
           ,@City
           ,@State
           ,@Zip
           ,@HomeTelephone
           ,@Cellphone
           ,@Email
           ,@Computer
           ,@TextMessage
           ,@DateOfContact1
           ,@ApptScreen1
           ,@SvcDate1
           ,@SvcType1
           ,@Response1
           ,@DateOfContact2
           ,@ApptScreen2
           ,@SvcDate2
           ,@SvcType2
           ,@Response2
           ,@NSEnrollment
           ,@EnrollmentDate
		   ,GETDATE()
		   ,'User Input')


		   Select @SN_ID = SCOPE_IDENTITY()
END



SET ANSI_NULLS ON



GO
/****** Object:  StoredProcedure [dbo].[usp_InsertServiceRecipient]    Script Date: 5/10/2018 12:06:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertServiceRecipient]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertServiceRecipient] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 03/14/2016
-- Description:	Add a new Recipient Record
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertServiceRecipient] 
			@NSRecipientID int OUTPUT,
		   @FK_NavigatorID int,
		   @FK_RecipID char(14),
           @LastName varchar(30),
           @FirstName varchar(20),
           @MiddleInitial varchar(1),
           @DOB smalldatetime,
           @AddressNotAvailable bit,
           @Address1 varchar(30),
           @Address2 varchar(30),
           @City varchar(25),
           @State char(2),
           @Zip char(5),
           @Phone1 char(10),
           @P1PhoneType smallint,
           @P1PersonalMessage bit,
           @Phone2 char(10),
           @P2PhoneType smallint,
           @P2PersonalMessage bit,
           @MotherMaidenName varchar(20),
           @Email varchar(45),
           @SSN char(10),
           @ImmigrantStatus bit,
           @CountryOfOrigin smallint,
           @Gender char(1),
           @Ethnicity bit,
           @PrimaryLanguage smallint,
           @AgreeToSpeakEnglish bit,
           @CaregiverName varchar(50),
           @Relationship smallint,
           @RelationshipOther varchar(30),
           @CaregiverPhone char(10),
           @CaregiverPhoneType smallint,
           @CaregiverPhonePersonalMessage bit,
           @CaregiverEmail varchar(45),
           @CaregiverContactPreference int,
           @Comments varchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [EWC_NSAA].[dbo].[tServiceRecipient]
           ([FK_NavigatorID]
		   ,[FK_RecipID]
           ,[LastName]
           ,[FirstName]
           ,[MiddleInitial]
           ,[DOB]
           ,[AddressNotAvailable]
           ,[Address1]
           ,[Address2]
           ,[City]
           ,[State]
           ,[Zip]
           ,[Phone1]
           ,[P1PhoneType]
           ,[P1PersonalMessage]
           ,[Phone2]
           ,[P2PhoneType]
           ,[P2PersonalMessage]
           ,[MotherMaidenName]
           ,[Email]
           ,[SSN]
           ,[ImmigrantStatus]
           ,[CountryOfOrigin]
           ,[Gender]
           ,[Ethnicity]
           ,[PrimaryLanguage]
           ,[AgreeToSpeakEnglish]
           ,[CaregiverName]
           ,[Relationship]
           ,[RelationshipOther]
           ,[CaregiverPhone]
           ,[CaregiverPhoneType]
           ,[CaregiverPhonePersonalMessage]
           ,[CaregiverEmail]
           ,[CaregiverContactPreference]
           ,[Comments]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy])
     VALUES
           (@FK_NavigatorID,
		   @FK_RecipID,
           @LastName,
           @FirstName,
           @MiddleInitial,
           @DOB,
           @AddressNotAvailable,
           @Address1,
           @Address2,
           @City,
           @State,
           @Zip,
           @Phone1,
           @P1PhoneType,
           @P1PersonalMessage,
           @Phone2,
           @P2PhoneType,
           @P2PersonalMessage,
           @MotherMaidenName,
           @Email,
           @SSN,
           @ImmigrantStatus,
           @CountryOfOrigin,
           @Gender,
           @Ethnicity,
           @PrimaryLanguage,
           @AgreeToSpeakEnglish,
           @CaregiverName,
           @Relationship,
           @RelationshipOther,
           @CaregiverPhone,
           @CaregiverPhoneType,
           @CaregiverPhonePersonalMessage,
           @CaregiverEmail,
           @CaregiverContactPreference,
           @Comments,
           NULL,
           getdate(),
           'User input',
           NULL,
           NULL)

		   Select @NSRecipientID = SCOPE_IDENTITY()

END



GO
/****** Object:  StoredProcedure [dbo].[usp_InsertSolution]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertSolution]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertSolution] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Insert New Solution 
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertSolution]
	-- Add the parameters for the stored procedure here
	@FK_BarrierID int
    ,@SolutionOffered int
    ,@SolutionImplementationStatus int
	,@FollowUpDt smalldatetime
	,@Solution varchar(2000)
    AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	INSERT INTO [dbo].[tSolution]
           ([FK_BarrierID]
           ,[SolutionOffered]
           ,[SolutionImplementationStatus]
		   ,[FollowUpDt]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy]
		   ,[Solution])
     VALUES
           (@FK_BarrierID
           ,@SolutionOffered
           ,@SolutionImplementationStatus
		   ,@FollowUpDt
           ,NULL
           ,GETDATE()
           ,'User input'
           ,NULL
           ,NULL
		   ,@Solution)

END


/****** Object:  StoredProcedure [dbo].[usp_SelectSolutionsList]    Script Date: 8/4/2016 8:18:07 AM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertUserAccess]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertUserAccess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertUserAccess] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 06/09/2016
-- Description:	Log user access
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertUserAccess] 
	@DomainName varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	INSERT INTO [dbo].[tUserAccess]
           ([DomainName]
           ,[LastAccess])
     VALUES
           (@DomainName,
			GETDATE())


END




GO
/****** Object:  StoredProcedure [dbo].[usp_MarkMessageAsRead]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MarkMessageAsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_MarkMessageAsRead] AS' 
END
GO
ALTER procedure [dbo].[usp_MarkMessageAsRead]
@messageId int
As
Begin
Update tInstantMessage set IsRead =1 where Id=@messageId
End

GO
/****** Object:  StoredProcedure [dbo].[usp_MarkSystemMessageAsRead]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MarkSystemMessageAsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_MarkSystemMessageAsRead] AS' 
END
GO

ALTER procedure [dbo].[usp_MarkSystemMessageAsRead]
@messageId int
As
Begin
Update tSystemMessage set IsRead =1 where Id=@messageId
End

GO
/****** Object:  StoredProcedure [dbo].[usp_NavigatorTransfer]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_NavigatorTransfer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_NavigatorTransfer] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 08/09/2016
-- Description:	Stored Procedure for transferring a Service Recipient between Navigators
-- =============================================
ALTER PROCEDURE [dbo].[usp_NavigatorTransfer] 
	@mode int,
	@NSRecipientID int,
	@OldNavigatorID int,
	@NewNavigatorID int,
	@NavigatorTransferDt smalldatetime,
	@NavigatorTransferReason varchar(200)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Select Old Navigator details
	if @mode = 1
	begin
		SELECT LastName, FirstName, Region, [Type] 
		FROM tNavigator 
		WHERE NavigatorID = (SELECT FK_NavigatorID FROM tServiceRecipient WHERE NSRecipientID = @NSRecipientID)
	end

	-- Update New Navigator ID
	if @mode = 2
	begin
		
		--Update tServiceRecipient
		UPDATE tServiceRecipient
		SET FK_NavigatorID = @NewNavigatorID,
		NavigatorTransferDt = @NavigatorTransferDt,
		NavigatorTransferReason = @NavigatorTransferReason,
		OldNavigatorID = @OldNavigatorID
		WHERE NSRecipientID = @NSRecipientID

		--Update tNavigationCycle
		UPDATE tNavigationCycle
		SET FK_NavigatorID = @NewNavigatorID
		WHERE FK_RecipientID = @NSRecipientID

		--Update tEncounter
		UPDATE tNavigationCycle
		SET FK_NavigatorID = @NewNavigatorID
		WHERE FK_RecipientID = @NSRecipientID


	end

	--Get list of Navigators to choose from
	if @mode = 3
	begin
		SELECT NavigatorID, FirstName, LastName FROM tNavigator
	end

	-- Select Recipient details
	if @mode = 4
	begin
		SELECT R.LastName, R.FirstName, L.LanguageName As PrimaryLanguage, 
		AgreeToSpeakEnglish = CASE AgreeToSpeakEnglish WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' END 
		FROM tServiceRecipient R
		JOIN trProvLanguages L
			ON R.PrimaryLanguage = L.LanguageCode
		WHERE NSRecipientID = @NSRecipientID

	end
	
	--Save Interim transfer
	if @mode = 5
	begin
		
		INSERT INTO [EWC_NSAA].[dbo].[tNavigatorTransfer]
           ([FK_RecipientID]
           ,[CurrentNavigatorID]
           ,[NewNavigatorID]
           ,[NavigatorTransferReason]
           ,[NavigatorTransferDt]
           ,[DateCreated]
           ,[CreatedBy])
     VALUES
           (@NSRecipientID
           ,@OldNavigatorID
           ,@NewNavigatorID
           ,@NavigatorTransferReason
           ,@NavigatorTransferDt
           ,getdate()
           ,'User Input')
		
				
	end

	--List Interim transfers
	if @mode = 6
	begin

		SELECT [TransferID]
			  ,[FK_RecipientID]
			  ,[CurrentNavigatorID]
			  ,[NewNavigatorID]
			  ,[NavigatorTransferReason]
			  ,[NavigatorTransferDt]
			  ,[TransferStatus]
			  ,[TransferRefusedReason]
			  ,[TransferRefusedDt]
			  ,[xtag]
			  ,[DateCreated]
			  ,[CreatedBy]
			  ,[LastUpdated]
			  ,[UpdatedBy]
		  FROM [dbo].[tNavigatorTransfer]
			WHERE CurrentNavigatorID = @OldNavigatorID
				
	end

	--List Interim transfers
	if @mode = 7
	begin

		SELECT [TransferID]
			  ,[FK_RecipientID]
			  ,[CurrentNavigatorID]
			  ,[NewNavigatorID]
			  ,[NavigatorTransferReason]
			  ,[NavigatorTransferDt]
			  ,[TransferStatus]
			  ,[TransferRefusedReason]
			  ,[TransferRefusedDt]
			  ,[xtag]
			  ,[DateCreated]
			  ,[CreatedBy]
			  ,[LastUpdated]
			  ,[UpdatedBy]
		  FROM [dbo].[tNavigatorTransfer]
			WHERE NewNavigatorID = @OldNavigatorID
				
	end

END


SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_RecipientReport]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RecipientReport]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_RecipientReport] AS' 
END
GO
-- =============================================
-- Author:		Bilwa BUchake
-- Create date: 10/05/2016
-- Description:	Stored Procedure for generating Recipient Report
-- =============================================
ALTER PROCEDURE [dbo].[usp_RecipientReport] 
	@mode int,
	@NSRecipientID int,
	@NSProviderID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if @mode = 1
	begin
	SELECT [NSRecipientID]
		  ,[FK_NavigatorID]
		  ,[FK_RecipID]
		  ,[LastName]
		  ,[FirstName]
		  ,[MiddleInitial]
		  ,[DOB]
		  ,[AddressNotAvailable]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[Zip]
		  ,[Phone1]
		  ,CASE P1PhoneType WHEN 1 THEN 'Home' WHEN 2 THEN 'Cell/Mobile' WHEN 3 THEN 'Message' WHEN 4 THEN 'Work' END AS P1PhoneType
		  ,CASE P1PersonalMessage WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' END AS P1PersonalMessage
		  ,[Phone2]
		  ,CASE P2PhoneType WHEN 1 THEN 'Home' WHEN 2 THEN 'Cell/Mobile' WHEN 3 THEN 'Message' WHEN 4 THEN 'Work' END AS P2PhoneType
		  ,[P2PhoneTypeOther]
		  ,CASE [P2PersonalMessage] WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' END AS [P2PersonalMessage]
		  ,[MotherMaidenName]
		  ,[Email]
		  ,[SSN]
		  ,CASE [ImmigrantStatus] WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' END AS [ImmigrantStatus]
		  ,[CountryName] As CountryOfOrigin
		  ,[Gender]
		  ,CASE [Ethnicity] WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' END AS [Ethnicity]
		  ,[LanguageName] AS PrimaryLanguage
		  ,CASE AgreeToSpeakEnglish WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' END AS AgreeToSpeakEnglish
		  ,[CaregiverName]
		  ,CASE [Relationship] WHEN 1 THEN 'Family Support' WHEN 2 THEN 'Community Support' WHEN 3 THEN 'Professional Caretaker' WHEN 4 THEN 'Shelter' WHEN 4 THEN 'Shelter' WHEN 5 THEN 'Other' END AS [Relationship]
		  ,[RelationshipOther]
		  ,[CaregiverPhone]
		  ,CASE [CaregiverPhoneType] WHEN 1 THEN 'Home' WHEN 2 THEN 'Cell/Mobile' WHEN 3 THEN 'Message' WHEN 4 THEN 'Work' END AS [CaregiverPhoneType]
		  ,CASE [CaregiverPhonePersonalMessage] WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' END AS [CaregiverPhonePersonalMessage]
		  ,[CaregiverEmail]
		  ,CASE [CaregiverContactPreference] WHEN 1 THEN 'Phone Call' WHEN 2 THEN 'Email' WHEN 3 THEN 'Text' WHEN 4 THEN 'Face-to-Face' END AS [CaregiverContactPreference]
		  ,[Comments]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
		  ,[NavigatorTransferDt]
		  ,[NavigatorTransferReason]
		  ,[OldNavigatorID]
	  FROM [dbo].[tServiceRecipient]
	  LEFT JOIN trProvLanguages 
			ON PrimaryLanguage = LanguageCode
	  LEFT JOIN trCountries
			ON CountryOfOrigin = CountryCode
	  WHERE NSRecipientID = @NSRecipientID
	end

	if @mode = 2
	begin

		SELECT [NSProviderID]
			  ,[PCPName]
			  ,[PCP_NPI]
			  ,[PCPAddress1]
			  ,[PCPAddress2]
			  ,[PCPCity]
			  ,[PCPState]
			  ,[PCPZip]
			  ,[PCPContactFName]
			  ,[PCPContactLName]
			  ,[PCPContactTitle]
			  ,[PCPContactTelephone]
			  ,[PCPContactEmail]
			  ,[xtag]
			  ,[DateCreated]
			  ,[CreatedBy]
			  ,[LastUpdated]
			  ,[UpdatedBy]
			  ,[EWCPCP]
			FROM [dbo].[tNSProvider]
			WHERE NSProviderID = @NSProviderID

		end

		if @mode = 3
		begin

		SELECT [NavigationCycleID]
			  ,[FK_RecipientID]
			  ,[FK_ProviderID]
			  ,[FK_ReferralID]
			  ,CancerSiteName AS [CancerSite] 
			  ,NSProblemDescription AS [NSProblem] 
			  ,HealthProgramName As HealthProgram
			  ,CASE [HealthInsuranceStatus] WHEN 0 THEN 'Uninsured' wHEN 1 THEN 'Insured' END AS [HealthInsuranceStatus]
			  ,[HealthInsurancePlan]
			  ,[HealthInsurancePlanNumber]
			  ,[ContactPrefDays]
			  ,[ContactPrefHours]
			  ,[ContactPrefHoursOther]
			  ,CASE [ContactPrefType] WHEN 1 THEN 'Phone Call' WHEN 2 THEN 'Email' WHEN 3 THEN 'Text' WHEN 4 THEN 'Face-to-Face' END AS [ContactPrefType]
			  ,[SRIndentifierCodeGenerated]
			  ,[FK_NavigatorID]
			  ,[xtag]
			  ,[DateCreated]
			  ,[CreatedBy]
			  ,[LastUpdated]
			  ,[UpdatedBy]
		  FROM [dbo].[tNavigationCycle]
		  LEFT JOIN trHealthProgram
			ON HealthProgram = HealthProgramCode
		  LEFT JOIN trCancerSite
			ON CancerSite = CancerSiteCode
		  LEFT JOIN trNSProblem
			 ON NSProblem = NSProblemCode
		  WHERE FK_RecipientID = @NSRecipientID


		end

		if @mode = 4
		begin

			SELECT B.[BarrierID]
				  ,B.[FK_EncounterID]
				  ,BarrierTypeDescription As BarrierType
				  ,BarrierDescription As BarrierAssessed
				  ,S.SolutionID
				  ,S.FK_BarrierID
				  ,dbo.GetSolutionsById(BarrierID) As SolutionOffered
				  ,dbo.GetImplementationsById(BarrierID) AS SolutionImplementationStatus
				  ,S.FollowUpDt
				  ,E.EncounterDt
			  FROM [dbo].[tBarrier] B
				  JOIN tSolution S
					ON BarrierID = FK_BarrierID
				  JOIN trBarrierType
					ON BarrierType = BarrierTypeCode
				  JOIN trBarrier
					ON BarrierAssessed = BarrierCode
				  JOIN trBarrierSolution
					ON SolutionOffered = BarrierSolutionCode
				  JOIN tEncounter E
					ON B.FK_EncounterID = E.EncounterID
				  JOIN tServiceRecipient SR
					ON E.FK_RecipientID = SR.NSRecipientID
			  WHERE FK_RecipientID = @NSRecipientID

		end

		if @mode = 5
		begin

			SELECT Count(DISTINCT EncounterID) As encounters from tEncounter
			WHERE FK_RecipientID = @NSRecipientID

		end

		if @mode = 6
		begin

			SELECT Count(MissedAppointment) As missedappts from tEncounter
			WHERE FK_RecipientID = @NSRecipientID
			and MissedAppointment = 1

		end

		if @mode = 7
		begin

			SELECT MAX(EncounterDt) As LastEncounter
			from tEncounter
			WHERE FK_RecipientID = @NSRecipientID
			
		end

		if @mode = 8
		begin

			SELECT MAX(NextAppointmentDt) As NextEncounter 
			from tEncounter
			WHERE FK_RecipientID = @NSRecipientID
			--AND NextAppointmentDt > getdate()
			
		end
		
		if @mode = 9
		begin

			SELECT SvcOutcomeStatus, LastUpdated, ServicesTerminated, ServicesTerminatedDt 
			from tEncounter
			WHERE FK_RecipientID = @NSRecipientID
			AND EncounterDt = (SELECT MAX(EncounterDt) FROM tEncounter WHERE FK_RecipientID = @NSRecipientID)
			
		end

END



SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_SaveCaregiverApproval]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SaveCaregiverApproval]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SaveCaregiverApproval] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 07/12/2016
-- Description:	Save Caregiver Approval values to database
-- =============================================
ALTER PROCEDURE [dbo].[usp_SaveCaregiverApproval]
	@FK_NSRecipientID int,
	@ApprovalCodeList varchar(20),
	@ApprovalOther varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    --Delete all Race values for FK_NSRecipientID
    DELETE FROM tCaregiverApprovals WHERE FK_NSRecipientID = @FK_NSRecipientID
    
    
	--Begin string splitting function
	
	DECLARE @Index      smallint,
                    @Start      smallint,
                    @DelSize    smallint,
					@Delimiter char(1),
					@DelimitedString varchar(10)

	declare @ApprovalCode int


	DECLARE @tblArray TABLE
		(
		   Element     varchar(2)               -- Array element contents
		)


	SET @DelimitedString = @ApprovalCodeList

	SET @Delimiter = ','

    SET @DelSize = LEN(@Delimiter)

    -- Loop through source string and add elements to destination table array
    -- ----------------------------------------------------------------------
    WHILE LEN(@DelimitedString) > 0
    BEGIN

        SET @Index = CHARINDEX(@Delimiter, @DelimitedString)

        IF @Index = 0
            BEGIN

                INSERT INTO
                    @tblArray 
                    (Element)
                VALUES
                    (LTRIM(RTRIM(@DelimitedString)))

                BREAK
            END
        ELSE
            BEGIN

                INSERT INTO
                    @tblArray 
                    (Element)
                VALUES
                    (LTRIM(RTRIM(SUBSTRING(@DelimitedString, 1,@Index - 1))))

                SET @Start = @Index + @DelSize
                SET @DelimitedString = SUBSTRING(@DelimitedString, @Start , LEN(@DelimitedString) - @Start + 1)

            END
    END


	--End string splitting function

    DECLARE csr_CaregiverApprovalInsert CURSOR FOR (SELECT * FROM @tblArray)

	OPEN csr_CaregiverApprovalInsert

	FETCH NEXT FROM csr_CaregiverApprovalInsert INTO @ApprovalCode

	WHILE @@FETCH_STATUS = 0  

	BEGIN
	--Insert Caregiver Approval values

	INSERT INTO [dbo].[tCaregiverApprovals]
			   ([FK_NSRecipientID]
			   ,[CaregiverApproval]
			   ,[CaregiverApprovalOther]
			   ,[xtag]
			   ,[DateCreated]
			   ,[CreatedBy]
			   ,[LastUpdated]
			   ,[UpdatedBy])
		 VALUES
			   (@FK_NSRecipientID
			   ,@ApprovalCode
			   ,@ApprovalOther
			   ,NULL
			   ,getdate()
			   ,'User Input'
			   ,NULL
			   ,NULL)


	FETCH NEXT FROM csr_CaregiverApprovalInsert INTO @ApprovalCode

	END

	CLOSE csr_CaregiverApprovalInsert
	DEALLOCATE csr_CaregiverApprovalInsert


END


/****** Object:  StoredProcedure [dbo].[GetCaregiverApproval]    Script Date: 7/14/2016 2:43:13 PM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_SaveRace]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SaveRace]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SaveRace] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 04/20/2016
-- Description:	Save Race values to database
-- =============================================
ALTER PROCEDURE [dbo].[usp_SaveRace]
	@FK_NSRecipientID int,
	@RaceCodeList varchar(10),
	@RaceOther varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    --Delete all Race values for FK_NSRecipientID
    DELETE FROM tRace WHERE FK_NSRecipientID = @FK_NSRecipientID
    
    
	--Begin string splitting function
	
	DECLARE @Index      smallint,
                    @Start      smallint,
                    @DelSize    smallint,
					@Delimiter char(1),
					@DelimitedString varchar(10)

	declare @RaceCode int


	DECLARE @tblArray TABLE
		(
		   Element     varchar(2)               -- Array element contents
		)


	SET @DelimitedString = @RaceCodeList

	SET @Delimiter = ','

    SET @DelSize = LEN(@Delimiter)

    -- Loop through source string and add elements to destination table array
    -- ----------------------------------------------------------------------
    WHILE LEN(@DelimitedString) > 0
    BEGIN

        SET @Index = CHARINDEX(@Delimiter, @DelimitedString)

        IF @Index = 0
            BEGIN

                INSERT INTO
                    @tblArray 
                    (Element)
                VALUES
                    (LTRIM(RTRIM(@DelimitedString)))

                BREAK
            END
        ELSE
            BEGIN

                INSERT INTO
                    @tblArray 
                    (Element)
                VALUES
                    (LTRIM(RTRIM(SUBSTRING(@DelimitedString, 1,@Index - 1))))

                SET @Start = @Index + @DelSize
                SET @DelimitedString = SUBSTRING(@DelimitedString, @Start , LEN(@DelimitedString) - @Start + 1)

            END
    END


	--End string splitting function

    DECLARE csr_RaceInsert CURSOR FOR (SELECT * FROM @tblArray)

	OPEN csr_RaceInsert

	FETCH NEXT FROM csr_RaceInsert INTO @RaceCode

	WHILE @@FETCH_STATUS = 0  

	BEGIN
		--Insert Race values
    INSERT INTO [EWC_NSAA].[dbo].[tRace]
           ([FK_NSRecipientID]
           ,[RaceCode]
           ,[RaceOther]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy])
     VALUES
           (@FK_NSRecipientID
           ,@RaceCode
           ,@RaceOther
           ,NULL
           ,getdate()
           ,'User Input'
           ,NULL
           ,NULL)

	FETCH NEXT FROM csr_RaceInsert INTO @RaceCode

	END

	CLOSE csr_RaceInsert
	DEALLOCATE csr_RaceInsert


END


/****** Object:  StoredProcedure [dbo].[usp_GetRace]    Script Date: 7/12/2016 10:41:42 AM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_SearchAssistanceIncident]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SearchAssistanceIncident]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SearchAssistanceIncident] AS' 
END
GO


-- =====================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to search incident using IncidentNumber
-- =====================================================================
ALTER PROCEDURE [dbo].[usp_SearchAssistanceIncident] 
	-- Add the parameters for the stored procedure here
	@AssistanceID int,
	@IncidentNumber char(10),
	@Lastname varchar(30),
	@IncidentDt smalldatetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [AssistanceID] ,
	[IncidentNumber] ,
	[IncidentDt] ,
	[FK_CommunicationCode] ,
	[FirstName] ,
	[Lastname] ,
	[Telephone] ,
	[Email] ,
	[RecipID] ,
	[FK_RequestorCode] ,
	[NPI] ,
	[NPIOwner] ,
	[NPISvcLoc] ,
	[BusinessName] ,
	[DateCreated] ,
	[CreatedBy] ,
	[LastUpdated] ,
	[UpdatedBy] 
FROM [dbo].[tAssistance]
WHERE ([AssistanceID] = @AssistanceID OR
IncidentNumber = @IncidentNumber
	  OR  Lastname = @Lastname OR [IncidentDt] = @IncidentDt)

END




GO
/****** Object:  StoredProcedure [dbo].[usp_SearchRecipients]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SearchRecipients]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SearchRecipients] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 10/27/2016
-- Description:	Search for Recipients
-- =============================================
ALTER PROCEDURE [dbo].[usp_SearchRecipients] 
	@NSRecipientID int,
	@LastName varchar(30),
	@DOB smalldatetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [NSRecipientID]
		  ,[FK_NavigatorID]
		  ,[FK_RecipID]
		  ,[LastName]
		  ,[FirstName]
		  ,[MiddleInitial]
		  ,[DOB]
		  ,[AddressNotAvailable]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[Zip]
		  ,[Phone1]
		  ,[P1PhoneType]
		  ,[P1PersonalMessage]
		  ,[Phone2]
		  ,[P2PhoneType]
		  ,[P2PhoneTypeOther]
		  ,[P2PersonalMessage]
		  ,[MotherMaidenName]
		  ,[Email]
		  ,[SSN]
		  ,[ImmigrantStatus]
		  ,[CountryOfOrigin]
		  ,[Gender]
		  ,[Ethnicity]
		  ,[PrimaryLanguage]
		  ,[AgreeToSpeakEnglish]
		  ,[CaregiverName]
		  ,[Relationship]
		  ,[RelationshipOther]
		  ,[CaregiverPhone]
		  ,[CaregiverPhoneType]
		  ,[CaregiverPhonePersonalMessage]
		  ,[CaregiverEmail]
		  ,[CaregiverContactPreference]
		  ,[Comments]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
	  FROM [dbo].[tServiceRecipient]
	  WHERE (NSRecipientID = @NSRecipientID)
	  OR (LastName = @LastName AND DOB = @DOB)
END


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceCommunication]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceCommunication]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceCommunication] AS' 
END
GO

-- ===================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to select Assistance communication
-- ===================================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceCommunication] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [CommunicationCode]
      ,[CommunicationDescription]
FROM [dbo].[trAssistanceCommunication]
	 

END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceEWCEnrollee]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceEWCEnrollee]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceEWCEnrollee] AS' 
END
GO

-- ===================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to select Assistance Enrollee
-- ===================================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceEWCEnrollee] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [EWCEnrolleeCode]
      ,[EWCEnrolleeDescription]
  FROM [EWC_NSAA].[dbo].[trAssistanceEWCEnrollee]
	 

END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceEWCPCP]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceEWCPCP]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceEWCPCP] AS' 
END
GO

-- ==========================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to select Assistance PCP
-- ==========================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceEWCPCP] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [EWCPCPCode]
      ,[EWCPCPDescription]
  FROM [EWC_NSAA].[dbo].[trAssistanceEWCPCP]
	 

END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceEWCRecipient]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceEWCRecipient]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceEWCRecipient] AS' 
END
GO

-- ==========================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to select Assistance PCP
-- ==========================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceEWCRecipient] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [EWCRecipientCode]
      ,[EWCRecipientDescription]
  FROM [EWC_NSAA].[dbo].[trAssistanceEWCRecipient]
	 

END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceIssuesList]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceIssuesList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceIssuesList] AS' 
END
GO


-- ================================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/08/2017
-- Description:	Stored Procedure to select AssistanceIssues based on @FK_AssistanceID
-- ================================================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceIssuesList] 
	-- Add the parameters for the stored procedure here
	@FK_AssistanceID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT
	I.[IssueID] ,
	I.[FK_AssistanceID] ,
	I.[IncidentNumber] ,
	I.[FK_EWCRecipientCode] ,
	I.[FK_EWCPCPCode] ,
	I.[FK_ReferralProviderCode] ,
	I.[FK_EWCEnrolleeCode] ,
	I.[OtherIssues] ,
	I.[DateCreated] ,
	I.[CreatedBy] ,
	I.[LastUpdated] ,
	I.[UpdatedBy]	 
FROM [dbo].[tAssistanceIssues] I
JOIN tAssistance A
ON I.FK_AssistanceID = A.AssistanceID
WHERE I.FK_AssistanceID = @FK_AssistanceID
	  
END


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceList]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceList] AS' 
END
GO

-- =========================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/08/2017
-- Description:	Stored Procedure to get list of Assistance records using FK_NavigatorID 
-- Edited by Bilwa Buchake: Added FK_NavigatorID column and changed WHERE clause to use FK_NavigatorID and not IncidentNumber
-- =========================================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceList] 
	-- Add the parameters for the stored procedure here
	@FK_NavigatorID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT
	[AssistanceID] ,
	[FK_NavigatorID],
	[IncidentNumber] ,
	[IncidentDt] ,
	[FK_CommunicationCode] ,
	[FirstName] ,
	[Lastname] ,
	[Telephone] ,
	[Email] ,
	[RecipID] ,
	[FK_RequestorCode] ,
	[NPI] ,
	[NPIOwner] ,
	[NPISvcLoc] ,
	[BusinessName] ,
	[DateCreated] ,
	[CreatedBy] ,
	[LastUpdated] ,
	[UpdatedBy] 
	FROM [dbo].[tAssistance]
WHERE  [FK_NavigatorID] = @FK_NavigatorID

END


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceReferralProvider]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceReferralProvider]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceReferralProvider] AS' 
END
GO

-- ===================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to select Assistance ReferralProvider
-- ===================================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceReferralProvider] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [ReferralProviderCode]
      ,[ReferralProviderDescription]
  FROM [EWC_NSAA].[dbo].[trAssistanceReferralProvider]
	 

END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceRequestor]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceRequestor]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceRequestor] AS' 
END
GO

-- ===================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to select Assistance Requestor
-- ===================================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceRequestor] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [RequestorCode]
      ,[RequestorDescription]
  FROM [EWC_NSAA].[dbo].[trAssistanceRequestor]
	 

END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceResolution]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceResolution]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceResolution] AS' 
END
GO

-- ===================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to select Assistance Resolution
-- ===================================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceResolution] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [ResolutionCode]
      ,[ResolutionDescription]
  FROM [EWC_NSAA].[dbo].[trAssistanceResolution]
	 

END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceResolutionList]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceResolutionList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceResolutionList] AS' 
END
GO



-- ======================================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/08/2017
-- Description:	Stored Procedure to select AssistanceResolution based on @FK_AssistanceID
-- ======================================================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceResolutionList] 
	-- Add the parameters for the stored procedure here
	@FK_AssistanceID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT
	R.[FK_AssistanceID] ,
	R.[IncidentNumber] ,
	R.[ResolutionInitiationDt] ,
	R.[FK_ResolutionCode] ,
	R.[ResolutionEndDt] ,
	R.[OtherResolution] ,
	R.[DateCreated] ,
	R.[CreatedBy] ,
	R.[LastUpdated] ,
	R.[UpdatedBy] 	 
FROM [dbo].[tAssistanceResolution] R
JOIN tAssistance A
ON R.FK_AssistanceID = A.AssistanceID
WHERE R.FK_AssistanceID = @FK_AssistanceID
	  

END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectBarrier]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectBarrier]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectBarrier] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Select Barrier 
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectBarrier]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [BarrierCode]
		  ,[BarrierDescription]
		  ,[BarrierTypeCode]
	  FROM [dbo].[trBarrier]
	  --WHERE BarrierTypeCode = @BarrierTypeCode

END


/****** Object:  StoredProcedure [dbo].[usp_SelectBarrierSolution]    Script Date: 7/19/2016 10:10:06 AM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectBarrierList]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectBarrierList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectBarrierList] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 06/07/2016
-- Description:	Get list of Barriers for an Encounter
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectBarrierList] 
	@FK_EncounterID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @NSRecipientID int
	SET @NSRecipientID = (SELECT FK_RecipientID FROM tEncounter WHERE EncounterID = @FK_EncounterID)

    -- Insert statements for procedure here
	SELECT B.[BarrierID]
      ,B.[FK_EncounterID]
      ,B.[BarrierType]
      ,B.[BarrierAssessed]
      ,B.[FollowUpDt]
      ,B.[xtag]
      ,B.[DateCreated]
      ,B.[CreatedBy]
      ,B.[LastUpdated]
      ,B.[UpdatedBy]
	  ,B.[Other]
  FROM [EWC_NSAA].[dbo].[tBarrier] B
  JOIN tEncounter E
  ON B.FK_EncounterID = E.EncounterID
	JOIN tServiceRecipient SR
	ON E.FK_RecipientID = SR.NSRecipientID
  WHERE E.FK_RecipientID = @NSRecipientID


END





GO
/****** Object:  StoredProcedure [dbo].[usp_SelectBarrierSolution]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectBarrierSolution]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectBarrierSolution] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Select Barrier Solution
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectBarrierSolution]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [BarrierSolutionCode]
		  ,[BarrierSolutionDescription]
		  ,[BarrierTypeCode]
	  FROM [dbo].[trBarrierSolution]
	  --WHERE BarrierTypeCode = @BarrierTypeCode

END


/****** Object:  StoredProcedure [dbo].[usp_InsertSolution]    Script Date: 8/4/2016 8:16:52 AM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectBarrierType]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectBarrierType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectBarrierType] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Select Barrier Type
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectBarrierType]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [BarrierTypeCode]
		  ,[BarrierTypeDescription]
	  FROM [dbo].[trBarrierType]
	


END




GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCancerSite]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectCancerSite]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectCancerSite] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Select Cancer Site values
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectCancerSite] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [CancerSiteCode]
			,[CancerSiteName]
		FROM [dbo].[trCancerSite]



END




GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCaregiverApproval]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectCaregiverApproval]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectCaregiverApproval] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 03/28/2016
-- Description:	Get list of Caregiver codes and names from lookup table
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectCaregiverApproval]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [CaregiverApprovalCode]
		  ,[CaregiverApprovalDescription]
	  FROM [dbo].[trCaregiverApproval]


END

/****** Object:  StoredProcedure [dbo].[usp_SaveRace]    Script Date: 7/13/2016 2:50:01 PM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCountries]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectCountries]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectCountries] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 03/22/2016
-- Description:	Select Countries from lookup table
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectCountries] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [CountryCode]
      ,[CountryName]
	FROM [EWC_NSAA].[dbo].[trCountries]


END




GO
/****** Object:  StoredProcedure [dbo].[usp_SelectEncounter]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectEncounter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectEncounter] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake	
-- Create date: 05/26/2016
-- Description:	Select Assessments for a Navigation Cycle
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectEncounter] 
	@FK_RecipientID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [EncounterID]
		  ,[FK_NavigatorID]
		  ,[FK_RecipientID]
		  ,[EncounterNumber]
		  ,[EncounterDt]
		  ,[EncounterStartTime]
		  ,[EncounterEndTime]
		  ,[EncounterType]
		  ,[EncounterReason]
		  ,[MissedAppointment]
		  ,[SvcOutcomeStatus]
		  ,[SvcOutcomeStatusReason]
		  ,[NextAppointmentDt]
		  ,[NextAppointmentTime]
		  ,[NextAppointmentType]
		  ,[ServicesTerminated]
		  ,[ServicesTerminatedDt]
		  ,[ServicesTerminatedReason]
		  ,[Notes]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
		  ,[EncounterPurpose]
	  FROM [dbo].[tEncounter]
	  WHERE FK_RecipientID = @FK_RecipientID

END


/****** Object:  StoredProcedure [dbo].[usp_GetEncounter]    Script Date: 9/14/2016 9:29:09 AM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectHealthInsurance]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectHealthInsurance]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectHealthInsurance] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Select Health Insurance Plan values
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectHealthInsurance] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [HealthInsuranceCode]
			,[HealthInsuranceName]
		FROM [dbo].[trHealthInsurance]



END














GO
/****** Object:  StoredProcedure [dbo].[usp_SelectHealthProgram]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectHealthProgram]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectHealthProgram] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Select Health Program values
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectHealthProgram] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [HealthProgramCode]
			,[HealthProgramName]
		FROM [dbo].[trHealthProgram]



END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectLanguages]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectLanguages]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectLanguages] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake	
-- Create date: 4/20/2016
-- Description:	Stored Procedure to get all languages from trLanguages table
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectLanguages] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [LanguageCode]
      ,[LanguageName]
  FROM [EWC_NSAA].[dbo].[trProvLanguages]


END




GO
/****** Object:  StoredProcedure [dbo].[usp_SelectNavigationCycle]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectNavigationCycle]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectNavigationCycle] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/26/2016
-- Description:	Select Navigation Cycles for a Recipient
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectNavigationCycle] 
	@FK_RecipientID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [NavigationCycleID]
		  ,[FK_RecipientID]
		  ,[FK_ProviderID]
		  ,[FK_ReferralID]
		  ,[CancerSite]
		  ,[NSProblem]
		  ,[HealthProgram]
		  ,[HealthInsuranceStatus]
		  ,[HealthInsurancePlan]
		  ,[HealthInsurancePlanNumber]
		  ,[ContactPrefDays]
		  ,[ContactPrefHours]
		  ,[ContactPrefHoursOther]
		  ,[ContactPrefType]
		  ,[SRIndentifierCodeGenerated]
		  ,[FK_NavigatorID]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
	  FROM [dbo].[tNavigationCycle] 
	  WHERE FK_RecipientID = @FK_RecipientID

END


/****** Object:  StoredProcedure [dbo].[usp_GetNavigationCycle]    Script Date: 8/11/2016 3:50:13 PM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectNavigator]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectNavigator]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectNavigator] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/05/2016
-- Description:	Select a Navigator record based on domain name
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectNavigator] 
	@DomainName varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [NavigatorID]
		  ,[DomainName]
		  ,[Region]
		  ,[Type]
		  ,[LastName]
		  ,[FirstName]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[Zip]
		  ,[BusinessTelephone]
		  ,[MobileTelephone]
		  ,[Email]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
	  FROM [dbo].[tNavigator]
	  WHERE DomainName = @DomainName



END


/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigator]    Script Date: 7/5/2016 2:18:08 PM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectNSProblem]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectNSProblem]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectNSProblem] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Select NSProblem values
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectNSProblem] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [NSProblemCode]
		  ,[NSProblemDescription]
	  FROM [dbo].[trNSProblem]


END




GO
/****** Object:  StoredProcedure [dbo].[usp_SelectNSProvider]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectNSProvider]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectNSProvider] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake	
-- Create date: 04/29/2016
-- Description:	Stored Procedure for getting a Provider record
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectNSProvider] 
	@NSProvideriD int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [NSProviderID]
      ,[PCPName]
	  ,[EWCPCP]
      ,[PCP_NPI]
      ,[PCP_Owner]
      ,[PCP_Location]
      ,[PCPAddress1]
      ,[PCPAddress2]
      ,[PCPCity]
      ,[PCPState]
      ,[PCPZip]
      ,[PCPContactFName]
      ,[PCPContactLName]
      ,[PCPContactTitle]
      ,[PCPContactTelephone]
      ,[PCPContactEmail]
      ,[Medical]
      ,[MedicalSpecialty]
      ,[MedicalSpecialtyOther]
      ,[ManagedCarePlan]
      ,[ManagedCarePlanOther]
      ,[xtag]
      ,[DateCreated]
      ,[CreatedBy]
      ,[LastUpdated]
      ,[UpdatedBy]
  FROM [EWC_NSAA].[dbo].[tNSProvider]
  WHERE NSProviderID = @NSProviderID


END


/****** Object:  StoredProcedure [dbo].[usp_UpdateNSProvider]    Script Date: 7/15/2016 10:39:56 AM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectRace]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectRace]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectRace] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 03/28/2016
-- Description:	Get list of Race codes and names from lookup table
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectRace]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [RaceCode]
      ,[RaceName]
	FROM [EWC_NSAA].[dbo].[trRace]



END




GO
/****** Object:  StoredProcedure [dbo].[usp_SelectRaceAsian]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectRaceAsian]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectRaceAsian] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 07/07/2016
-- Description:	Get list of Race Asian
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectRaceAsian]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [RaceAsianCode]
		  ,[RaceAsianName]
	  FROM [dbo].[trRaceAsian]

END


SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectRacePacIslander]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectRacePacIslander]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectRacePacIslander] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 07/07/2016
-- Description:	Get list of Race Pac Islander
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectRacePacIslander]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [RacePacIslanderCode]
		  ,[RacePacIslanderName]
	  FROM [dbo].[trRacePacIslander]

END

/****** Object:  StoredProcedure [dbo].[usp_SaveRace]    Script Date: 7/11/2016 1:59:32 PM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectScreeningNav]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectScreeningNav]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectScreeningNav] AS' 
END
GO


-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 07/31/2017
-- Description:	Select a Screening Nav record based on SN_ID
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectScreeningNav] 
	@SN_ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

SELECT [SN_ID]
	  ,[FK_NavigatorID]
      ,[SN_CODE1]
      ,[SN_CODE2]
      ,[LastName]
      ,[FirstName]
      ,[DOB]
      ,[Address1]
      ,[Address2]
      ,[City]
      ,[State]
      ,[Zip]
      ,[HomeTelephone]
      ,[Cellphone]
      ,[Email]
      ,[Computer]
      ,[TextMessage]
      ,[DateOfContact1]
      ,[ApptScreen1]
      ,[SvcDate1]
      ,[SvcType1]
      ,[Response1]
      ,[DateOfContact2]
      ,[ApptScreen2]
      ,[SvcDate2]
      ,[SvcType2]
      ,[Response2]
      ,[NSEnrollment]
      ,[EnrollmentDate]
      ,[xtag]
      ,[DateCreated]
      ,[CreatedBy]
      ,[LastUpdated]
      ,[UpdatedBy]
  FROM [dbo].[tScreeningNav]
  WHERE SN_ID = @SN_ID
END



SET ANSI_NULLS ON



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectServiceRecipient]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectServiceRecipient]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectServiceRecipient] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 03/22/2016
-- Description:	Select Recipients from NS Recipients table
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectServiceRecipient]
	-- Add the parameters for the stored procedure here
	@FK_NavigatorID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [NSRecipientID]
	  ,[FK_NavigatorID]
      ,[FK_RecipID]
      ,[LastName]
      ,[FirstName]
      ,[MiddleInitial]
      ,[DOB]
      ,[AddressNotAvailable]
      ,[Address1]
      ,[Address2]
      ,[City]
      ,[State]
      ,[Zip]
      ,[Phone1]
      ,[P1PhoneType]
      ,[P1PersonalMessage]
      ,[Phone2]
      ,[P2PhoneType]
      ,[P2PersonalMessage]
      ,[MotherMaidenName]
      ,[Email]
      ,[SSN]
      ,[ImmigrantStatus]
      ,[CountryOfOrigin]
      ,[Gender]
      ,[Ethnicity]
      ,[PrimaryLanguage]
      ,[AgreeToSpeakEnglish]
      ,[CaregiverName]
      ,[Relationship]
      ,[RelationshipOther]
      ,[CaregiverPhone]
      ,[CaregiverPhoneType]
      ,[CaregiverPhonePersonalMessage]
      ,[CaregiverEmail]
      ,[CaregiverContactPreference]
      ,[Comments]
      ,[xtag]
      ,[DateCreated]
      ,[CreatedBy]
      ,[LastUpdated]
      ,[UpdatedBy]
  FROM [EWC_NSAA].[dbo].[tServiceRecipient]
  WHERE FK_NavigatorID = @FK_NavigatorID
  



END




GO
/****** Object:  StoredProcedure [dbo].[usp_SelectSolutionsList]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectSolutionsList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectSolutionsList] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 06/10/2016
-- Description:	List of Solutions for a Barrier
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectSolutionsList] 
	-- Add the parameters for the stored procedure here
	@FK_BarrierID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [SolutionID]
      ,[FK_BarrierID]
      ,[SolutionOffered]
      ,[SolutionImplementationStatus]
	  ,[FollowUpDt]
      ,[xtag]
      ,[DateCreated]
      ,[CreatedBy]
      ,[LastUpdated]
      ,[UpdatedBy]
	  ,[Solution]
  FROM [EWC_NSAA].[dbo].[tSolution]
WHERE FK_BarrierID = @FK_BarrierID


END



/****** Object:  StoredProcedure [dbo].[usp_UpdateSolution]    Script Date: 8/4/2016 8:18:47 AM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectTransferReasons]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectTransferReasons]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectTransferReasons] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 12/19/2016
-- Description:	Get a list of Navigator Transfer Reasons
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectTransferReasons]
	@NavigatorID1 int,
	@NavigatorID2 int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @NavigatorType1 int
	declare @NavigatorType2 int

	set @NavigatorType1 = (SELECT [Type] FROM tNavigator WHERE NavigatorID = @NavigatorID1)
	set @NavigatorType2 = (SELECT [Type] FROM tNavigator WHERE NavigatorID = @NavigatorID2)

    -- Insert statements for procedure here

	if @NavigatorType1 = 1 and @NavigatorType2 = 2 --HE to CC
	begin

	SELECT [NavigatorTransferReasonCode]
		  ,[NavigatorTransferReason]
	  FROM [dbo].[trNavigatorTransferReason]
	  WHERE NavigatorTransferReasonCode BETWEEN 10 and 20
	  
	end

	if @NavigatorType1 = 2 and @NavigatorType2 = 1 --CC to HE
	begin

	SELECT [NavigatorTransferReasonCode]
		  ,[NavigatorTransferReason]
	  FROM [dbo].[trNavigatorTransferReason]
	  WHERE NavigatorTransferReasonCode BETWEEN 20 and 30
	  
	end

	if @NavigatorType1 = 2 and @NavigatorType2 = 2 --CC to CC
	begin

	SELECT [NavigatorTransferReasonCode]
		  ,[NavigatorTransferReason]
	  FROM [dbo].[trNavigatorTransferReason]
	  WHERE NavigatorTransferReasonCode BETWEEN 30 and 40
	  
	end

	if @NavigatorType1 = 1 and @NavigatorType2 = 1 --HE to HE
	begin

	SELECT [NavigatorTransferReasonCode]
		  ,[NavigatorTransferReason]
	  FROM [dbo].[trNavigatorTransferReason]
	  
	end

	if (@NavigatorID1 = 0 and @NavigatorID2 = 0) or (@NavigatorType1 > 2) or (@NavigatorType2 > 2)
	begin

	SELECT [NavigatorTransferReasonCode]
		  ,[NavigatorTransferReason]
	  FROM [dbo].[trNavigatorTransferReason]
	  
	end

END



GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateAssistance]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateAssistance]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateAssistance] AS' 
END
GO

-- ====================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/08/2017
-- Description:	Stored Procedure to update Assistance  
-- Edited by Bilwa Buchake: Added FK_NavigatorID column
-- =====================================================
ALTER PROCEDURE [dbo].[usp_UpdateAssistance] 
	-- Add the parameters for the stored procedure here
	@AssistanceID int,
	@FK_NavigatorID int,
	@IncidentNumber char(10),
	@IncidentDt smalldatetime ,
	@FK_CommunicationCode smallint ,
	@FirstName varchar(20) ,
	@Lastname varchar(30) ,
	@Telephone char(10) ,
	@Email varchar(45) ,
	@RecipID char(14) ,
	@FK_RequestorCode smallint ,
	@NPI varchar(10) ,
	@NPIOwner varchar(2) ,
	@NPISvcLoc varchar(3) ,
	@BusinessName varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Update statement
UPDATE [EWC_NSAA].[dbo].[tAssistance] 
	SET [FK_NavigatorID] = @FK_NavigatorID,
	[IncidentNumber] = @IncidentNumber,
	[IncidentDt] = @IncidentDt ,
	[FK_CommunicationCode] = @FK_CommunicationCode,
	[FirstName] = @FirstName,
	[Lastname] = @Lastname,
	[Telephone] = @Telephone ,
	[Email] = @Email,
	[RecipID] = @RecipID,
	[FK_RequestorCode] = @FK_RequestorCode,
	[NPI] = @NPI,
	[NPIOwner] = @NPIOwner,
	[NPISvcLoc] = @NPISvcLoc,
	[BusinessName] = @BusinessName,
	[LastUpdated] = getdate(),
	[UpdatedBy] = 'User update'
WHERE AssistanceID = @AssistanceID

END

SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateAssistanceIssues]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateAssistanceIssues]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateAssistanceIssues] AS' 
END
GO


-- =========================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/08/2017
-- Description:	Stored Procedure to update AssistanceIssues  
-- ==========================================================
ALTER PROCEDURE [dbo].[usp_UpdateAssistanceIssues] 
	-- Add the parameters for the stored procedure here
	@IssueID int ,
	@FK_AssistanceID int,
	@IncidentNumber char(10),
	@FK_EWCRecipientCode smallint ,
	@FK_EWCPCPCode smallint ,
	@FK_ReferralProviderCode smallint ,
	@FK_EWCEnrolleeCode smallint,
	@OtherIssues varchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Update statement
UPDATE [EWC_NSAA].[dbo].[tAssistanceIssues] 
	SET [FK_AssistanceID] = @FK_AssistanceID,
	[IncidentNumber] = @IncidentNumber,
	[FK_EWCRecipientCode] = @FK_EWCRecipientCode,
	[FK_EWCPCPCode] = @FK_EWCPCPCode,
	[FK_ReferralProviderCode] = @FK_ReferralProviderCode,
	[FK_EWCEnrolleeCode] = @FK_EWCEnrolleeCode,
	[OtherIssues] =@OtherIssues ,
	[LastUpdated] = getdate(),
    [UpdatedBy] = 'User update'

WHERE IssueID = @IssueID

END


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateAssistanceResolution]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateAssistanceResolution]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateAssistanceResolution] AS' 
END
GO



-- ==============================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/08/2017
-- Description:	Stored Procedure to update AssistanceResolution 
-- ==============================================================
ALTER PROCEDURE [dbo].[usp_UpdateAssistanceResolution] 
	-- Add the parameters for the stored procedure here
	@ResolutionID int,
	@FK_AssistanceID int ,
	@IncidentNumber char(10),
	@ResolutionInitiationDt smalldatetime,
	@FK_ResolutionCode smallint,
	@ResolutionEndDt smalldatetime,
	@OtherResolution varchar(500) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Update statement
UPDATE [EWC_NSAA].[dbo].[tAssistanceResolution] 
	SET [FK_AssistanceID] = @FK_AssistanceID ,
	[IncidentNumber] = @IncidentNumber ,
	[ResolutionInitiationDt] = @ResolutionInitiationDt ,
	[FK_ResolutionCode] = @FK_ResolutionCode,
	[ResolutionEndDt] = @ResolutionEndDt,
	[OtherResolution] = @OtherResolution ,
	[LastUpdated] = getdate(),
    [UpdatedBy] = 'User update'

WHERE ResolutionID = @ResolutionID

END



GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateBarrier]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateBarrier]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateBarrier] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description: Update a Barrier record
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateBarrier]
	-- Add the parameters for the stored procedure here
	@BarrierID int
	,@FK_EncounterID int
    ,@BarrierType int
    ,@BarrierAssessed int
    ,@FollowUpDt smalldatetime
	,@Other varchar(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	UPDATE [dbo].[tBarrier]
	   SET [FK_EncounterID] = @FK_EncounterID
		  ,[BarrierType] = @BarrierType
		  ,[BarrierAssessed] = @BarrierAssessed
		  ,[FollowUpDt] = @FollowUpDt
		  ,[LastUpdated] = GETDATE()
		  ,[UpdatedBy] = 'User update'
		  ,[Other] = @Other
	 WHERE BarrierID = @BarrierID



END



GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateEncounter]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateEncounter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateEncounter] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/17/2016
-- Description:	Update an Encounter
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateEncounter] 
	-- Add the parameters for the stored procedure here
	@EncounterID int
	,@FK_NavigatorID int
    ,@FK_RecipientID int
    ,@EncounterNumber int
    ,@EncounterDt smalldatetime
    ,@EncounterStartTime varchar(20)
    ,@EncounterEndTime varchar(20)
    ,@EncounterType int
	,@EncounterReason int
    ,@MissedAppointment bit
    ,@SvcOutcomeStatus int
    ,@SvcOutcomeStatusReason varchar(200)
    ,@NextAppointmentDt smalldatetime
    ,@NextAppointmentTime varchar(20)
    ,@NextAppointmentType int
    ,@ServicesTerminated bit
    ,@ServicesTerminatedDt smalldatetime
    ,@ServicesTerminatedReason varchar(200)
    ,@Notes varchar(2000)
    ,@EncounterPurpose int
           
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


	UPDATE [dbo].[tEncounter]
	   SET [FK_NavigatorID] = @FK_NavigatorID
		  ,[FK_RecipientID] = @FK_RecipientID
		  ,[EncounterNumber] = @EncounterNumber
		  ,[EncounterDt] = @EncounterDt
		  ,[EncounterStartTime] = @EncounterStartTime
		  ,[EncounterEndTime] = @EncounterEndTime
		  ,[EncounterType] = @EncounterType
		  ,[EncounterReason] = @EncounterReason
		  ,[MissedAppointment] = @MissedAppointment
		  ,[SvcOutcomeStatus] = @SvcOutcomeStatus
		  ,[SvcOutcomeStatusReason] = @SvcOutcomeStatusReason
		  ,[NextAppointmentDt] = @NextAppointmentDt
		  ,[NextAppointmentTime] = @NextAppointmentTime
		  ,[NextAppointmentType] = @NextAppointmentType
		  ,[ServicesTerminated] = @ServicesTerminated
		  ,[ServicesTerminatedDt] = @ServicesTerminatedDt
		  ,[ServicesTerminatedReason] = @ServicesTerminatedReason
		  ,[Notes] = @Notes
		  ,[LastUpdated] = GETDATE()
		  ,[UpdatedBy] = 'User Update'
		  ,[EncounterPurpose] = @EncounterPurpose
	 WHERE EncounterID = @EncounterID



END



SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateHECActivity]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateHECActivity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateHECActivity] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_UpdateHECActivity]
@ActivityId int,
@ActivityDate	datetime,	
@ActivityType	smallint,
@NameOrPurpose	varchar(250),	
@CollaboratorId	int,	
@CollaboratorContributionId	int,	
@Address1	nvarchar(100),
@Address2	nvarchar(100),	
@CityId	int,	
@Zip	varchar(5),	
@CHW	int,
@OtherAttendee	nvarchar(100),	
@Population	varchar(50),	
@LanguageId	int,	
@Discussed	int,	
@PrePostTest	int,	
@Result	int,	
@MyRole	int,	
@Attendee	bit,	
@AnnoucementDoc	varchar(300),	
@Travel	bit,	
@Notes	nvarchar(250),

@FirstName	nvarchar(50),	
@LastName	nvarchar(50),	
@Month	smallint,	
@Year	int,	
@RaceOrEthinicity	int,	
@Part_Info_Address1	nvarchar(100),	
@Part_Info_Address2	nvarchar(100),	
@Part_Info_CityId	int,
@Part_Info_Zip	varchar(5),	
@Telephone	varchar(50),	
@Email	varchar(150),	
@Part_Info_LanguageId	int,	

@BusinessName	nvarchar(100),	
@NPI	varchar(50),	
@ProviderFirstName	nvarchar(50),	
@ProviderLastName	nvarchar(50),	
@EwcProvider	smallint

AS
Begin

IF(@ActivityId<>NULL)
Begin

UPDATE tHecActivities
SET ActivityDate=@ActivityDate ,
  ActivityType=@ActivityType, 
  NameOrPurpose=@NameOrPurpose,  
  CollaboratorId=@CollaboratorId,  
  CollaboratorContributionId=@CollaboratorContributionId,  
  Address1=@Address1,  
  Address2=@Address2,  
  CityId=@CityId,  
  Zip=@Zip,  
  CHW=@CHW,  
  OtherAttendee=@OtherAttendee,
 Population=@Population,  
 LanguageId=@LanguageId,  
 Discussed=@Discussed,  
 PrePostTest=@PrePostTest,  
 Result=@Result,  
 MyRole=@MyRole,  
 Attendee=@Attendee,  
 AnnoucementDoc=@AnnoucementDoc,  
 Travel=@Travel,  
 Notes=@Notes
 where ActivityId = @ActivityId

UPDATE tHecActivityParticipantInfo
set FirstName=@FirstName,
LastName=@LastName,
Month=@Month,
Year=@Year,
RaceOrEthinicity=@RaceOrEthinicity,
Address1=@Part_Info_Address1,
Address2=@Part_Info_Address2,
CityId=@Part_Info_CityId,
Zip=@Part_Info_Zip,
Telephone=@Telephone,
Email=@Email,
LanguageId=@Part_Info_LanguageId


UPDATE tHecScreeningEvent
SET BusinessName=@BusinessName,
NPI=@NPI,
ProviderFirstName=@ProviderFirstName,
ProviderLastName=@ProviderLastName,
EwcProvider=@EwcProvider 
where ActivityId = @ActivityId


END



END
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigationCycle]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNavigationCycle]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateNavigationCycle] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/12/2016
-- Description:	Update a Navigation Cycle
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateNavigationCycle] 
	-- Add the parameters for the stored procedure here
			@NavigationCycleID int
		   ,@FK_RecipientID int
           ,@FK_ProviderID int
           ,@FK_ReferralID int
           ,@CancerSite int
           ,@NSProblem int
           ,@HealthProgram int
           ,@HealthInsuranceStatus int
           ,@HealthInsurancePlan int
           ,@HealthInsurancePlanNumber varchar(15)
           ,@ContactPrefDays varchar(15)
           ,@ContactPrefHours varchar(15)
           ,@ContactPrefHoursOther varchar(20)
           ,@ContactPrefType int
           ,@SRIndentifierCodeGenerated bit
           ,@FK_NavigatorID int
           
           
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [EWC_NSAA].[dbo].[tNavigationCycle]
   SET [FK_RecipientID] = @FK_RecipientID
      ,[FK_ProviderID] = @FK_ProviderID
      ,[FK_ReferralID] = @FK_ReferralID
      ,[CancerSite] = @CancerSite
      ,[NSProblem] = @NSProblem
      ,[HealthProgram] = @HealthProgram
      ,[HealthInsuranceStatus] = @HealthInsuranceStatus
      ,[HealthInsurancePlan] = @HealthInsurancePlan
      ,[HealthInsurancePlanNumber] = @HealthInsurancePlanNumber
      ,[ContactPrefDays] = @ContactPrefDays
      ,[ContactPrefHours] = @ContactPrefHours
      ,[ContactPrefHoursOther] = @ContactPrefHoursOther
      ,[ContactPrefType] = @ContactPrefType
      ,[SRIndentifierCodeGenerated] = @SRIndentifierCodeGenerated
      ,[FK_NavigatorID] = @FK_NavigatorID
      ,[LastUpdated] = getdate()
      ,[UpdatedBy] = 'User Update'
 WHERE NavigationCycleID = @NavigationCycleID
END


/****** Object:  StoredProcedure [dbo].[usp_SelectNavigationCycle]    Script Date: 8/10/2016 2:46:46 PM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigator]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNavigator]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateNavigator] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/05/2016
-- Description:	Update Navigator record
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateNavigator] 
	-- Add the parameters for the stored procedure here
	@NavigatorID int
	,@DomainName varchar(20)
	,@Region smallint
    ,@Type int
    ,@LastName varchar(30)
    ,@FirstName varchar(20)
    ,@Address1 varchar(30)
    ,@Address2 varchar(30)
    ,@City varchar(25)
    ,@State char(2)
    ,@Zip char(5)
    ,@BusinessTelephone char(10)
	,@MobileTelephone char(10)
    ,@Email varchar(45)
    
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	UPDATE [dbo].[tNavigator]
	   SET [DomainName] = @DomainName
		  ,[Region] = @Region
		  ,[Type] = @Type
		  ,[LastName] = @LastName
		  ,[FirstName] = @FirstName
		  ,[Address1] = @Address1
		  ,[Address2] = @Address2
		  ,[City] = @City
		  ,[State] = @State
		  ,[Zip] = @Zip
		  ,[BusinessTelephone] = @BusinessTelephone
		  ,[MobileTelephone] = @MobileTelephone
		  ,[Email] = @Email
		  ,[LastUpdated] = GETDATE()
		  ,[UpdatedBy] = 'User Input'
	 WHERE NavigatorID = @NavigatorID
	


END



SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigatorLanguage]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNavigatorLanguage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateNavigatorLanguage] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake	
-- Create date: 12/12/2016
-- Description:	Update Navigator Language record
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateNavigatorLanguage] 
	-- Add the parameters for the stored procedure here
	@NavigatorLanguageID int,
	@FK_NavigatorID int,
	@LanguageCode int,
	@SpeakingScore int,
	@ListeningScore int,
	@ReadingScore int,
	@WritingScore int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	UPDATE [dbo].[tNavigatorLanguages]
	   SET [FK_NavigatorID] = @FK_NavigatorID
		  ,[LanguageCode] = @LanguageCode
		  ,[SpeakingScore] = @SpeakingScore
		  ,[ListeningScore] = @ListeningScore
		  ,[ReadingScore] = @ReadingScore
		  ,[WritingScore] = @WritingScore
		  ,[LastUpdated] = GETDATE()
		  ,[UpdatedBy] = 'User Update'
	 WHERE NavigatorLanguageID = @NavigatorLanguageID

END



SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigatorTraining]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNavigatorTraining]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateNavigatorTraining] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 11/17/2016
-- Description:	Update Navigator Training record
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateNavigatorTraining]
	-- Add the parameters for the stored procedure here
	@TrainingID int,
	@FK_NavigatorID int,
	@CourseName varchar(100),
	@Organization varchar(100),
	@CompletionDt smalldatetime,
	@Certificate bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


	UPDATE [dbo].[tNavigatorTraining]
   SET [FK_NavigatorID] = @FK_NavigatorID
      ,[CourseName] = @CourseName
      ,[Organization] = @Organization
      ,[CompletionDt] = @CompletionDt
      ,[Certificate] = @Certificate
      ,[LastUpdated] = GETDATE()
      ,[UpdatedBy] = 'User Update'
	WHERE TrainingID = @TrainingID


END



SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNSProvider]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNSProvider]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateNSProvider] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 04/28/2016
-- Description:	Stored Procedure for updating PCPs
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateNSProvider] 
	-- Add the parameters for the stored procedure here
	@NSProviderID int,
	@PCPName varchar(100),
	@EWCPCP bit,
    @PCP_NPI varchar(10),
    @PCP_Owner varchar(2),
    @PCP_Location varchar(3),
    @PCPAddress1 varchar(30),
    @PCPAddress2 varchar(30),
    @PCPCity varchar(24),
    @PCPState char(2),
    @PCPZip char(5),
    @PCPContactFName varchar(50),
    @PCPContactLName varchar(50),
    @PCPContactTitle varchar(50),
    @PCPContactTelephone char(10),
    @PCPContactEmail varchar(45),
    @Medical bit,
    @MedicalSpecialty int,
    @MedicalSpecialtyOther varchar(100),
    @ManagedCarePlan int,
    @ManagedCarePlanOther varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [EWC_NSAA].[dbo].[tNSProvider]
   SET [PCPName] = @PCPName
	  ,[EWCPCP] = @EWCPCP
      ,[PCP_NPI] = @PCP_NPI
      ,[PCP_Owner] = @PCP_Owner
      ,[PCP_Location] = @PCP_Location
      ,[PCPAddress1] = @PCPAddress1
      ,[PCPAddress2] = @PCPAddress2
      ,[PCPCity] = @PCPCity
      ,[PCPState] = @PCPState
      ,[PCPZip] = @PCPZip
      ,[PCPContactFName] = @PCPContactFName
      ,[PCPContactLName] = @PCPContactLName
      ,[PCPContactTitle] = @PCPContactTitle
      ,[PCPContactTelephone] = @PCPContactTelephone
      ,[PCPContactEmail] = @PCPContactEmail
      ,[Medical] = @Medical
      ,[MedicalSpecialty] = @MedicalSpecialty
      ,[MedicalSpecialtyOther] = @MedicalSpecialtyOther
      ,[ManagedCarePlan] = @ManagedCarePlan
      ,[ManagedCarePlanOther] = @ManagedCarePlanOther
      ,[xtag] = NULL
      ,[LastUpdated] = getdate()
      ,[UpdatedBy] = 'User Update'
 WHERE NSProviderID = @NSProviderID


END


/****** Object:  StoredProcedure [dbo].[usp_InsertNSProvider]    Script Date: 7/15/2016 10:41:01 AM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdatePartnersCollaborator]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdatePartnersCollaborator]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdatePartnersCollaborator] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_UpdatePartnersCollaborator]
@PartnersCollaboratorsId int,
@NameofOrg1	nvarchar(200),
@NameOfOrg2	nvarchar(200),
@Abbreviation	nvarchar(100),
@Address1	nvarchar(100),
@Address2	nvarchar(100),
@City	nvarchar(100),
@ZipCode int,
@Phone	varchar(15),
@Email	varchar(100),
@WebAddress	varchar	(250),

@FirstName	nvarchar(100),
@LastName	nvarchar(100),
@Title	nvarchar(20),
@Contact_Phone	varchar(15),
@Ext	int,
@Contact_Email	varchar(100),
@DateOfLastContact	datetime,

@OrgType1	int	,
@OrgType2	int	,
@ServiceCategory1	int	,
@ServiceCaregory2	int	,
@ServicePopulation1	int	,
@ServicePopulation2	int	,
@Language1	int	,
@Language2	int,
@Area	varchar	(50),
@Region	int	,
@Country1	int	,
@Country2	int	

AS
BEGIN

IF ( @PartnersCollaboratorsId<>null)
BEGIN

UPDATE
tPartnersCollaborators
SET NameofOrg1=@NameofOrg1,
NameOfOrg2=@NameOfOrg2,
Abbreviation=@Abbreviation,
Address1=@Address1,
Address2=@Address2,
City=@City,
ZipCode=@ZipCode,
Phone=@Phone,
Email=@Email,
WebAddress= @WebAddress
WHERE PartnersCollaboratorsId = @PartnersCollaboratorsId


UPDATE tPartnersCollaboratorsContact
sET FirstName=@FirstName,
LastName=@LastName,
Title=@Title,
Phone=@Contact_Phone,
Ext=@Ext,
Email=@Contact_Email,
DateOfLastContact=@DateOfLastContact
WHERE PartnersCollaboratorsId = @PartnersCollaboratorsId


UPDATE tPartnersCollaboratorsService
SET OrgType1=@OrgType1,
OrgType2=@OrgType2,
ServiceCategory1=@ServiceCategory1,
ServiceCaregory2=@ServiceCaregory2,
ServicePopulation1=@ServicePopulation1,
ServicePopulation2=@ServicePopulation2,
Language1=@Language1,
Language2=@Language2,
Area=@Area,
Region=@Region,
Country1=@Country1,
Country2=@Country2

WHERE PartnersCollaboratorsId = @PartnersCollaboratorsId
END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateScreeningNav]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateScreeningNav]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateScreeningNav] AS' 
END
GO


-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 07/31/2017
-- Description:	Update Screening Nav record
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateScreeningNav] 
	-- Add the parameters for the stored procedure here
			@SN_ID int
		   ,@FK_NavigatorID int
		   ,@SN_CODE1 varchar(11)
           ,@SN_CODE2 smallint
           ,@LastName varchar(30)
           ,@FirstName varchar(20)
           ,@DOB smalldatetime
           ,@Address1 varchar(30)
           ,@Address2 varchar(30)
           ,@City varchar(25)
           ,@State char(2)
           ,@Zip char(5)
           ,@HomeTelephone char(10)
           ,@Cellphone char(10)
           ,@Email varchar(45)
           ,@Computer bit
           ,@TextMessage char(1)
           ,@DateOfContact1 smalldatetime
           ,@ApptScreen1 char(1)
           ,@SvcDate1 smalldatetime
           ,@SvcType1 char(2)
           ,@Response1 char(2)
           ,@DateOfContact2 smalldatetime
           ,@ApptScreen2 char(1)
           ,@SvcDate2 smalldatetime
           ,@SvcType2 char(2)
           ,@Response2 char(2)
           ,@NSEnrollment char(1)
           ,@EnrollmentDate smalldatetime
    
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	UPDATE [dbo].[tScreeningNav]
   SET [FK_NavigatorID] = @FK_NavigatorID 
      ,[SN_CODE1] = @SN_CODE1
      ,[SN_CODE2] = @SN_CODE2
      ,[LastName] = @LastName
      ,[FirstName] = @FirstName
      ,[DOB] = @DOB
      ,[Address1] = @Address1
      ,[Address2] = @Address2
      ,[City] = @City
      ,[State] = @State
      ,[Zip] = @Zip
      ,[HomeTelephone] = @HomeTelephone
      ,[Cellphone] = @Cellphone
      ,[Email] = @Email
      ,[Computer] = @Computer
      ,[TextMessage] = @TextMessage
      ,[DateOfContact1] = @DateOfContact1
      ,[ApptScreen1] = @ApptScreen1
      ,[SvcDate1] = @SvcDate1
      ,[SvcType1] = @SvcType1
      ,[Response1] = @Response1
      ,[DateOfContact2] = @DateOfContact2
      ,[ApptScreen2] = @ApptScreen2
      ,[SvcDate2] = @SvcDate2
      ,[SvcType2] = @SvcType2
      ,[Response2] = @Response2
      ,[NSEnrollment] = @NSEnrollment
      ,[EnrollmentDate] = @EnrollmentDate
      ,[LastUpdated] = GETDATE()
      ,[UpdatedBy] = 'User Input'
 WHERE SN_ID = @SN_ID
	


END





GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateServiceRecipient]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateServiceRecipient]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateServiceRecipient] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 03/21/2016
-- Description:	Update a Recipient Enrollment record
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateServiceRecipient] 
	-- Add the parameters for the stored procedure here
		   @NSRecipientID int,
		   @FK_NavigatorID int,
		   @FK_RecipID char(14),
           @LastName varchar(30),
           @FirstName varchar(20),
           @MiddleInitial varchar(1),
           @DOB smalldatetime,
           @AddressNotAvailable bit,
           @Address1 varchar(30),
           @Address2 varchar(30),
           @City varchar(25),
           @State char(2),
           @Zip char(5),
           @Phone1 char(10),
           @P1PhoneType smallint,
           @P1PersonalMessage bit,
           @Phone2 char(10),
           @P2PhoneType smallint,
           @P2PersonalMessage bit,
           @MotherMaidenName varchar(20),
           @Email varchar(45),
           @SSN char(10),
           @ImmigrantStatus bit,
           @CountryOfOrigin smallint,
           @Gender char(1),
           @Ethnicity bit,
           @PrimaryLanguage smallint,
           @AgreeToSpeakEnglish bit,
           @CaregiverName varchar(50),
           @Relationship smallint,
           @RelationshipOther varchar(30),
           @CaregiverPhone char(10),
           @CaregiverPhoneType smallint,
           @CaregiverPhonePersonalMessage bit,
           @CaregiverEmail varchar(45),
           @CaregiverContactPreference int,
           @Comments varchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [EWC_NSAA].[dbo].[tServiceRecipient]
   SET [FK_RecipID] = @FK_RecipID
	  ,[FK_NavigatorID] = @FK_NavigatorID
      ,[LastName] = @LastName
      ,[FirstName] = @FirstName
      ,[MiddleInitial] = @MiddleInitial
      ,[DOB] = @DOB
      ,[AddressNotAvailable] = @AddressNotAvailable
      ,[Address1] = @Address1
      ,[Address2] = @Address2
      ,[City] = @City
      ,[State] = @State
      ,[Zip] = @Zip
      ,[Phone1] = @Phone1
      ,[P1PhoneType] = @P1PhoneType
      ,[P1PersonalMessage] = @P1PersonalMessage
      ,[Phone2] = @Phone2
      ,[P2PhoneType] = @P2PhoneType
      ,[P2PersonalMessage] = @P2PersonalMessage
      ,[MotherMaidenName] = @MotherMaidenName
      ,[Email] = @Email
      ,[SSN] = @SSN
      ,[ImmigrantStatus] = @ImmigrantStatus
      ,[CountryOfOrigin] = @CountryOfOrigin
      ,[Gender] = @Gender
      ,[Ethnicity] = @Ethnicity
      ,[PrimaryLanguage] = @PrimaryLanguage
      ,[AgreeToSpeakEnglish] = @AgreeToSpeakEnglish
      ,[CaregiverName] = @CaregiverName
      ,[Relationship] = @Relationship
      ,[RelationshipOther] = @RelationshipOther
      ,[CaregiverPhone] = @CaregiverPhone
      ,[CaregiverPhoneType] = @CaregiverPhoneType
      ,[CaregiverPhonePersonalMessage] = @CaregiverPhonePersonalMessage
      ,[CaregiverEmail] = @CaregiverEmail
      ,[CaregiverContactPreference] = @CaregiverContactPreference
      ,[Comments] = @Comments
      ,[LastUpdated] = getdate()
      ,[UpdatedBy] = 'User update'
 WHERE NSRecipientID = @NSRecipientID


END




GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateSolution]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateSolution]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateSolution] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Update Solution
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateSolution]
	-- Add the parameters for the stored procedure here
	@SolutionID int
	,@FK_BarrierID int
    ,@SolutionOffered int
    ,@SolutionImplementationStatus int
	,@FollowUpDt smalldatetime
	,@Solution varchar(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	UPDATE [dbo].[tSolution]
	   SET [FK_BarrierID] = @FK_BarrierID
		  ,[SolutionOffered] = @SolutionOffered
		  ,[SolutionImplementationStatus] = @SolutionImplementationStatus
		  ,[FollowUpDt] = @FollowUpDt
		  ,[LastUpdated] = GETDATE()
		  ,[UpdatedBy] = 'User update'
		  ,[Solution] = @Solution
	 WHERE SolutionID = @SolutionID
	


END



SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateTransferStatus]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateTransferStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateTransferStatus] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 09/30/2016
-- Description:	Upate Navigator Transfer Status
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateTransferStatus]
	@TransferID int,
	@TransferStatus int,
	@TransferRefusedReason varchar(200),
	@TransferRefusedDt smalldatetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    --Update Interim transfer
	UPDATE [dbo].[tNavigatorTransfer]
			SET [TransferStatus] = @TransferStatus
				,[TransferRefusedReason] = @TransferRefusedReason
		  		,[TransferRefusedDt] = @TransferRefusedDt
				,[LastUpdated] = getdate()
				,[UpdatedBy] = 'User Update'
	 WHERE TransferID = @TransferID

	 


END


/****** Object:  StoredProcedure [dbo].[usp_GetNavigationCycle]    Script Date: 10/6/2016 2:10:15 PM ******/
SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_usp_GetSystemMessageDetails]    Script Date: 5/10/2018 12:06:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_usp_GetSystemMessageDetails]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_usp_GetSystemMessageDetails] AS' 
END
GO

ALTER procedure [dbo].[usp_usp_GetSystemMessageDetails]
@messageId int
As
Begin

Select * from tSystemMessage where Id = @messageId


End
GO
