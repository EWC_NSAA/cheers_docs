/****** Object:  StoredProcedure [dbo].[usp_UpdateTransferStatus]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateTransferStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateTransferStatus]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateSolution]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateSolution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateSolution]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateServiceRecipient]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateServiceRecipient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateServiceRecipient]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateScreeningNav]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateScreeningNav]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateScreeningNav]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNSProvider]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNSProvider]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateNSProvider]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigatorTraining]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNavigatorTraining]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateNavigatorTraining]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigatorLanguage]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNavigatorLanguage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateNavigatorLanguage]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigator]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNavigator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateNavigator]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigationCycle]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNavigationCycle]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateNavigationCycle]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateHECActivity]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateHECActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateHECActivity]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateEncounter]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateEncounter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateEncounter]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateBarrier]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateBarrier]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateBarrier]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateAssistanceResolution]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateAssistanceResolution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateAssistanceResolution]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateAssistanceIssues]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateAssistanceIssues]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateAssistanceIssues]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateAssistance]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateAssistance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateAssistance]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectTransferReasons]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectTransferReasons]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectTransferReasons]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectSolutionsList]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectSolutionsList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectSolutionsList]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectServiceRecipient]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectServiceRecipient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectServiceRecipient]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectScreeningNav]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectScreeningNav]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectScreeningNav]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectResults]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectResults]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectResults]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectRacePacIslander]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectRacePacIslander]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectRacePacIslander]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectRaceAsian]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectRaceAsian]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectRaceAsian]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectRace]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectRace]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectRace]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectNSProvider]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectNSProvider]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectNSProvider]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectNSProblem]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectNSProblem]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectNSProblem]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectNavigator]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectNavigator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectNavigator]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectNavigationCycle]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectNavigationCycle]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectNavigationCycle]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectMyRole]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectMyRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectMyRole]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectLanguages]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectLanguages]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectLanguages]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectHealthProgram]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectHealthProgram]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectHealthProgram]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectHealthInsurance]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectHealthInsurance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectHealthInsurance]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectEncounter]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectEncounter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectEncounter]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectDiscussionTopics]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectDiscussionTopics]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectDiscussionTopics]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCountries]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectCountries]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectCountries]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCollaborators]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectCollaborators]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectCollaborators]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCollaboratorContribution]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectCollaboratorContribution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectCollaboratorContribution]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCaregiverApproval]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectCaregiverApproval]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectCaregiverApproval]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCancerSite]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectCancerSite]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectCancerSite]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectBarrierType]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectBarrierType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectBarrierType]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectBarrierSolution]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectBarrierSolution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectBarrierSolution]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectBarrierList]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectBarrierList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectBarrierList]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectBarrier]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectBarrier]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectBarrier]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceResolutionList]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceResolutionList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceResolutionList]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceResolution]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceResolution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceResolution]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceRequestor]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceRequestor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceRequestor]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceReferralProvider]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceReferralProvider]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceReferralProvider]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceList]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceList]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceIssuesList]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceIssuesList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceIssuesList]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceEWCRecipient]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceEWCRecipient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceEWCRecipient]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceEWCPCP]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceEWCPCP]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceEWCPCP]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceEWCEnrollee]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceEWCEnrollee]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceEWCEnrollee]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceCommunication]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceCommunication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectAssistanceCommunication]
GO
/****** Object:  StoredProcedure [dbo].[usp_SelectActivityType]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectActivityType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SelectActivityType]
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchRecipients]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SearchRecipients]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SearchRecipients]
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchAssistanceIncident]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SearchAssistanceIncident]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SearchAssistanceIncident]
GO
/****** Object:  StoredProcedure [dbo].[usp_SaveRace]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SaveRace]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SaveRace]
GO
/****** Object:  StoredProcedure [dbo].[usp_SaveCaregiverApproval]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SaveCaregiverApproval]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SaveCaregiverApproval]
GO
/****** Object:  StoredProcedure [dbo].[usp_RecipientReport]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RecipientReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RecipientReport]
GO
/****** Object:  StoredProcedure [dbo].[usp_NavigatorTransfer]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_NavigatorTransfer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_NavigatorTransfer]
GO
/****** Object:  StoredProcedure [dbo].[usp_MarkSystemMessageAsRead]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MarkSystemMessageAsRead]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MarkSystemMessageAsRead]
GO
/****** Object:  StoredProcedure [dbo].[usp_MarkMessageAsRead]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MarkMessageAsRead]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MarkMessageAsRead]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertUserAccess]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertUserAccess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertUserAccess]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertSolution]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertSolution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertSolution]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertServiceRecipient]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertServiceRecipient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertServiceRecipient]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertScreeningNav]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertScreeningNav]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertScreeningNav]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNSProvider]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNSProvider]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertNSProvider]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNavigatorTraining]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNavigatorTraining]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertNavigatorTraining]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNavigatorLanguage]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNavigatorLanguage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertNavigatorLanguage]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNavigator]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNavigator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertNavigator]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNavigationCycle]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNavigationCycle]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertNavigationCycle]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertInstantMessage]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertInstantMessage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertInstantMessage]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertHECActivity]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertHECActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertHECActivity]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertEncounter]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertEncounter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertEncounter]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertBarrier]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertBarrier]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertBarrier]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertAssistanceResolution]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertAssistanceResolution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertAssistanceResolution]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertAssistanceIssues]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertAssistanceIssues]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertAssistanceIssues]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertAssistance]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertAssistance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertAssistance]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserSystemInstanceMessages]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetUserSystemInstanceMessages]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetUserSystemInstanceMessages]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserSendInstanceMessages]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetUserSendInstanceMessages]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetUserSendInstanceMessages]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserInstanceMessages]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetUserInstanceMessages]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetUserInstanceMessages]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetTrainingsList]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetTrainingsList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetTrainingsList]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetSystemMessageDetails]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetSystemMessageDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetSystemMessageDetails]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetScreeningNav]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetScreeningNav]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetScreeningNav]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetRecipient]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetRecipient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetRecipient]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetRace]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetRace]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetRace]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetNavigationCycle]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetNavigationCycle]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetNavigationCycle]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetLanguagesList]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetLanguagesList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetLanguagesList]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetInstanceMessageDetails]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetInstanceMessageDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetInstanceMessageDetails]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetHECActivity]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetHECActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetHECActivity]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetEncounter]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetEncounter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetEncounter]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBarrier]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetBarrier]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetBarrier]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAssistanceResolution]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAssistanceResolution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetAssistanceResolution]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAssistanceIssues]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAssistanceIssues]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetAssistanceIssues]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAssistanceIncidentNo]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAssistanceIncidentNo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetAssistanceIncidentNo]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAssistance]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAssistance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetAssistance]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAllHECActivities]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAllHECActivities]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetAllHECActivities]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteSolutions]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DeleteSolutions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_DeleteSolutions]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteHECActivity]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DeleteHECActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_DeleteHECActivity]
GO
/****** Object:  StoredProcedure [dbo].[GetCaregiverApproval]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCaregiverApproval]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetCaregiverApproval]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tSystemMessage_tNavigator_rcvdBy]') AND parent_object_id = OBJECT_ID(N'[dbo].[tSystemMessage]'))
ALTER TABLE [dbo].[tSystemMessage] DROP CONSTRAINT [FK_tSystemMessage_tNavigator_rcvdBy]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tSolution_tBarrier]') AND parent_object_id = OBJECT_ID(N'[dbo].[tSolution]'))
ALTER TABLE [dbo].[tSolution] DROP CONSTRAINT [FK_tSolution_tBarrier]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigatorTraining_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigatorTraining]'))
ALTER TABLE [dbo].[tNavigatorTraining] DROP CONSTRAINT [FK_tNavigatorTraining_tNavigator]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle] DROP CONSTRAINT [FK_tNavigationCycle_tServiceRecipient]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNSProvider]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle] DROP CONSTRAINT [FK_tNavigationCycle_tNSProvider]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle] DROP CONSTRAINT [FK_tNavigationCycle_tNavigator]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNavigationCycle]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle] DROP CONSTRAINT [FK_tNavigationCycle_tNavigationCycle]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tInstantMessage_tNavigator_RcvdFrom]') AND parent_object_id = OBJECT_ID(N'[dbo].[tInstantMessage]'))
ALTER TABLE [dbo].[tInstantMessage] DROP CONSTRAINT [FK_tInstantMessage_tNavigator_RcvdFrom]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tInstantMessage_tNavigator_rcvdBy]') AND parent_object_id = OBJECT_ID(N'[dbo].[tInstantMessage]'))
ALTER TABLE [dbo].[tInstantMessage] DROP CONSTRAINT [FK_tInstantMessage_tNavigator_rcvdBy]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecScreeningEvent_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecScreeningEvent]'))
ALTER TABLE [dbo].[tHecScreeningEvent] DROP CONSTRAINT [FK_tHecScreeningEvent_tHecActivities]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecActivityParticipantInfo_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecActivityParticipantInfo]'))
ALTER TABLE [dbo].[tHecActivityParticipantInfo] DROP CONSTRAINT [FK_tHecActivityParticipantInfo_tHecActivities]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tEncounter_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tEncounter]'))
ALTER TABLE [dbo].[tEncounter] DROP CONSTRAINT [FK_tEncounter_tServiceRecipient]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tEncounter_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tEncounter]'))
ALTER TABLE [dbo].[tEncounter] DROP CONSTRAINT [FK_tEncounter_tNavigator]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tCaregiverApprovals_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tCaregiverApprovals]'))
ALTER TABLE [dbo].[tCaregiverApprovals] DROP CONSTRAINT [FK_tCaregiverApprovals_tServiceRecipient]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tBarrier_tEncounter]') AND parent_object_id = OBJECT_ID(N'[dbo].[tBarrier]'))
ALTER TABLE [dbo].[tBarrier] DROP CONSTRAINT [FK_tBarrier_tEncounter]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF_tAssistance]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tAssistance] DROP CONSTRAINT [DF_tAssistance]
END
GO
/****** Object:  Table [dbo].[tUserAccess]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tUserAccess]') AND type in (N'U'))
DROP TABLE [dbo].[tUserAccess]
GO
/****** Object:  Table [dbo].[tSystemMessage]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tSystemMessage]') AND type in (N'U'))
DROP TABLE [dbo].[tSystemMessage]
GO
/****** Object:  Table [dbo].[tSolution]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tSolution]') AND type in (N'U'))
DROP TABLE [dbo].[tSolution]
GO
/****** Object:  Table [dbo].[tServiceRecipient]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tServiceRecipient]') AND type in (N'U'))
DROP TABLE [dbo].[tServiceRecipient]
GO
/****** Object:  Table [dbo].[tScreeningNav]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tScreeningNav]') AND type in (N'U'))
DROP TABLE [dbo].[tScreeningNav]
GO
/****** Object:  Table [dbo].[trSolutionImplementationStatus]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trSolutionImplementationStatus]') AND type in (N'U'))
DROP TABLE [dbo].[trSolutionImplementationStatus]
GO
/****** Object:  Table [dbo].[trServiceTerminatedReason]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trServiceTerminatedReason]') AND type in (N'U'))
DROP TABLE [dbo].[trServiceTerminatedReason]
GO
/****** Object:  Table [dbo].[trRacePacIslander]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trRacePacIslander]') AND type in (N'U'))
DROP TABLE [dbo].[trRacePacIslander]
GO
/****** Object:  Table [dbo].[trRaceAsian]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trRaceAsian]') AND type in (N'U'))
DROP TABLE [dbo].[trRaceAsian]
GO
/****** Object:  Table [dbo].[trRace]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trRace]') AND type in (N'U'))
DROP TABLE [dbo].[trRace]
GO
/****** Object:  Table [dbo].[trProvLanguages]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trProvLanguages]') AND type in (N'U'))
DROP TABLE [dbo].[trProvLanguages]
GO
/****** Object:  Table [dbo].[trOutcomeStatusReason]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trOutcomeStatusReason]') AND type in (N'U'))
DROP TABLE [dbo].[trOutcomeStatusReason]
GO
/****** Object:  Table [dbo].[trOutcomeStatus]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trOutcomeStatus]') AND type in (N'U'))
DROP TABLE [dbo].[trOutcomeStatus]
GO
/****** Object:  Table [dbo].[trNSProblem]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trNSProblem]') AND type in (N'U'))
DROP TABLE [dbo].[trNSProblem]
GO
/****** Object:  Table [dbo].[trNavigatorType]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trNavigatorType]') AND type in (N'U'))
DROP TABLE [dbo].[trNavigatorType]
GO
/****** Object:  Table [dbo].[trNavigatorTransferReason]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trNavigatorTransferReason]') AND type in (N'U'))
DROP TABLE [dbo].[trNavigatorTransferReason]
GO
/****** Object:  Table [dbo].[trHealthProgram]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trHealthProgram]') AND type in (N'U'))
DROP TABLE [dbo].[trHealthProgram]
GO
/****** Object:  Table [dbo].[trHealthInsuranceStatus]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trHealthInsuranceStatus]') AND type in (N'U'))
DROP TABLE [dbo].[trHealthInsuranceStatus]
GO
/****** Object:  Table [dbo].[trHealthInsurance]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trHealthInsurance]') AND type in (N'U'))
DROP TABLE [dbo].[trHealthInsurance]
GO
/****** Object:  Table [dbo].[tResults]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tResults]') AND type in (N'U'))
DROP TABLE [dbo].[tResults]
GO
/****** Object:  Table [dbo].[trEncounterType]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trEncounterType]') AND type in (N'U'))
DROP TABLE [dbo].[trEncounterType]
GO
/****** Object:  Table [dbo].[trEncounterPurpose]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trEncounterPurpose]') AND type in (N'U'))
DROP TABLE [dbo].[trEncounterPurpose]
GO
/****** Object:  Table [dbo].[trCountries]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trCountries]') AND type in (N'U'))
DROP TABLE [dbo].[trCountries]
GO
/****** Object:  Table [dbo].[trCaregiverApproval]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trCaregiverApproval]') AND type in (N'U'))
DROP TABLE [dbo].[trCaregiverApproval]
GO
/****** Object:  Table [dbo].[trCancerSite]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trCancerSite]') AND type in (N'U'))
DROP TABLE [dbo].[trCancerSite]
GO
/****** Object:  Table [dbo].[trBarrierType]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trBarrierType]') AND type in (N'U'))
DROP TABLE [dbo].[trBarrierType]
GO
/****** Object:  Table [dbo].[trBarrierSolution]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trBarrierSolution]') AND type in (N'U'))
DROP TABLE [dbo].[trBarrierSolution]
GO
/****** Object:  Table [dbo].[trBarrier]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trBarrier]') AND type in (N'U'))
DROP TABLE [dbo].[trBarrier]
GO
/****** Object:  Table [dbo].[trAssistanceResolution]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceResolution]') AND type in (N'U'))
DROP TABLE [dbo].[trAssistanceResolution]
GO
/****** Object:  Table [dbo].[trAssistanceRequestor]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceRequestor]') AND type in (N'U'))
DROP TABLE [dbo].[trAssistanceRequestor]
GO
/****** Object:  Table [dbo].[trAssistanceReferralProvider]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceReferralProvider]') AND type in (N'U'))
DROP TABLE [dbo].[trAssistanceReferralProvider]
GO
/****** Object:  Table [dbo].[trAssistanceEWCRecipient]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceEWCRecipient]') AND type in (N'U'))
DROP TABLE [dbo].[trAssistanceEWCRecipient]
GO
/****** Object:  Table [dbo].[trAssistanceEWCPCP]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceEWCPCP]') AND type in (N'U'))
DROP TABLE [dbo].[trAssistanceEWCPCP]
GO
/****** Object:  Table [dbo].[trAssistanceEWCEnrollee]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceEWCEnrollee]') AND type in (N'U'))
DROP TABLE [dbo].[trAssistanceEWCEnrollee]
GO
/****** Object:  Table [dbo].[trAssistanceCommunication]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceCommunication]') AND type in (N'U'))
DROP TABLE [dbo].[trAssistanceCommunication]
GO
/****** Object:  Table [dbo].[tRace]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tRace]') AND type in (N'U'))
DROP TABLE [dbo].[tRace]
GO
/****** Object:  Table [dbo].[tPartnersCollaborators]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tPartnersCollaborators]') AND type in (N'U'))
DROP TABLE [dbo].[tPartnersCollaborators]
GO
/****** Object:  Table [dbo].[tNSProvider]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNSProvider]') AND type in (N'U'))
DROP TABLE [dbo].[tNSProvider]
GO
/****** Object:  Table [dbo].[tNavigatorTransfer]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigatorTransfer]') AND type in (N'U'))
DROP TABLE [dbo].[tNavigatorTransfer]
GO
/****** Object:  Table [dbo].[tNavigatorTraining]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigatorTraining]') AND type in (N'U'))
DROP TABLE [dbo].[tNavigatorTraining]
GO
/****** Object:  Table [dbo].[tNavigatorLanguages]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigatorLanguages]') AND type in (N'U'))
DROP TABLE [dbo].[tNavigatorLanguages]
GO
/****** Object:  Table [dbo].[tNavigator]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigator]') AND type in (N'U'))
DROP TABLE [dbo].[tNavigator]
GO
/****** Object:  Table [dbo].[tNavigationCycle]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]') AND type in (N'U'))
DROP TABLE [dbo].[tNavigationCycle]
GO
/****** Object:  Table [dbo].[tMyRole]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tMyRole]') AND type in (N'U'))
DROP TABLE [dbo].[tMyRole]
GO
/****** Object:  Table [dbo].[tInstantMessage]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tInstantMessage]') AND type in (N'U'))
DROP TABLE [dbo].[tInstantMessage]
GO
/****** Object:  Table [dbo].[tHecScreeningEvent]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecScreeningEvent]') AND type in (N'U'))
DROP TABLE [dbo].[tHecScreeningEvent]
GO
/****** Object:  Table [dbo].[tHecActivityParticipantInfo]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecActivityParticipantInfo]') AND type in (N'U'))
DROP TABLE [dbo].[tHecActivityParticipantInfo]
GO
/****** Object:  Table [dbo].[tHecActivities]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecActivities]') AND type in (N'U'))
DROP TABLE [dbo].[tHecActivities]
GO
/****** Object:  Table [dbo].[tEncounter]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tEncounter]') AND type in (N'U'))
DROP TABLE [dbo].[tEncounter]
GO
/****** Object:  Table [dbo].[tDiscussionTopic]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tDiscussionTopic]') AND type in (N'U'))
DROP TABLE [dbo].[tDiscussionTopic]
GO
/****** Object:  Table [dbo].[tCollaboratorContribution]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tCollaboratorContribution]') AND type in (N'U'))
DROP TABLE [dbo].[tCollaboratorContribution]
GO
/****** Object:  Table [dbo].[tCaregiverApprovals]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tCaregiverApprovals]') AND type in (N'U'))
DROP TABLE [dbo].[tCaregiverApprovals]
GO
/****** Object:  Table [dbo].[tBarrier]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tBarrier]') AND type in (N'U'))
DROP TABLE [dbo].[tBarrier]
GO
/****** Object:  Table [dbo].[tAssistanceResolution]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tAssistanceResolution]') AND type in (N'U'))
DROP TABLE [dbo].[tAssistanceResolution]
GO
/****** Object:  Table [dbo].[tAssistanceIssues]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tAssistanceIssues]') AND type in (N'U'))
DROP TABLE [dbo].[tAssistanceIssues]
GO
/****** Object:  Table [dbo].[tAssistance]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tAssistance]') AND type in (N'U'))
DROP TABLE [dbo].[tAssistance]
GO
/****** Object:  Table [dbo].[tActivityType]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tActivityType]') AND type in (N'U'))
DROP TABLE [dbo].[tActivityType]
GO
/****** Object:  UserDefinedFunction [dbo].[GetSolutionsById]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSolutionsById]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetSolutionsById]
GO
/****** Object:  UserDefinedFunction [dbo].[GetImplementationsById]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetImplementationsById]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetImplementationsById]
GO

DECLARE @RoleName sysname
set @RoleName = N'db_executor'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
    DECLARE @RoleMemberName sysname
    DECLARE Member_Cursor CURSOR FOR
    select [name]
    from sys.database_principals 
    where principal_id in ( 
        select member_principal_id
        from sys.database_role_members
        where role_principal_id in (
            select principal_id
            FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

    OPEN Member_Cursor;

    FETCH NEXT FROM Member_Cursor
    into @RoleMemberName
    
    DECLARE @SQL NVARCHAR(4000)

    WHILE @@FETCH_STATUS = 0
    BEGIN
        
        SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
        EXEC(@SQL)
        
        FETCH NEXT FROM Member_Cursor
        into @RoleMemberName
    END;

    CLOSE Member_Cursor;
    DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [db_executor]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'db_executor' AND type = 'R')
DROP ROLE [db_executor]
GO
/****** Object:  Database [EWC_NSAA]    Script Date: 5/26/2018 7:05:39 AM ******/
IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'EWC_NSAA')
DROP DATABASE [EWC_NSAA]
GO
/****** Object:  Database [EWC_NSAA]    Script Date: 5/26/2018 7:05:39 AM ******/
IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = N'EWC_NSAA')
BEGIN
CREATE DATABASE [EWC_NSAA]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'EWC_NSAA', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\EWC_NSAA_Phase2.mdf' , SIZE = 6144KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'EWC_NSAA_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\EWC_NSAA_Phase2_log.ldf' , SIZE = 8384KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
END
GO
ALTER DATABASE [EWC_NSAA] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EWC_NSAA].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EWC_NSAA] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EWC_NSAA] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EWC_NSAA] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EWC_NSAA] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EWC_NSAA] SET ARITHABORT OFF 
GO
ALTER DATABASE [EWC_NSAA] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [EWC_NSAA] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EWC_NSAA] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EWC_NSAA] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EWC_NSAA] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EWC_NSAA] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EWC_NSAA] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EWC_NSAA] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EWC_NSAA] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EWC_NSAA] SET  DISABLE_BROKER 
GO
ALTER DATABASE [EWC_NSAA] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EWC_NSAA] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EWC_NSAA] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EWC_NSAA] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [EWC_NSAA] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EWC_NSAA] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [EWC_NSAA] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EWC_NSAA] SET RECOVERY FULL 
GO
ALTER DATABASE [EWC_NSAA] SET  MULTI_USER 
GO
ALTER DATABASE [EWC_NSAA] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EWC_NSAA] SET DB_CHAINING OFF 
GO
ALTER DATABASE [EWC_NSAA] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [EWC_NSAA] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [EWC_NSAA] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'EWC_NSAA', N'ON'
GO
/****** Object:  DatabaseRole [db_executor]    Script Date: 5/26/2018 7:05:40 AM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'db_executor' AND type = 'R')
CREATE ROLE [db_executor]
GO
/****** Object:  UserDefinedFunction [dbo].[GetImplementationsById]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetImplementationsById]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[GetImplementationsById]
(
    @BarrierID int
)
RETURNS varchar(max)
AS
BEGIN

	declare @CLRF char(2)
	set @CLRF = CHAR(13) + CHAR(10)

    declare @output varchar(max)
    select @output = COALESCE(@output + ''; '', '''') + CASE SolutionImplementationStatus WHEN 1 THEN ''Implemented'' WHEN 2 THEN ''Not Implemented'' END
    from tSolution
    where FK_BarrierID = @BarrierID

    return @output
END;


' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetSolutionsById]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSolutionsById]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE FUNCTION [dbo].[GetSolutionsById]
(
    @BarrierID int
)
RETURNS varchar(max)
AS
BEGIN
    declare @output varchar(max)
    select @output = COALESCE(@output + '', '', '''') + BarrierSolutionDescription 
    from tSolution
	JOIN trBarrierSolution
	ON SolutionOffered = BarrierSolutionCode
    where FK_BarrierID = @BarrierID

    return @output
END;


' 
END
GO
/****** Object:  Table [dbo].[tActivityType]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tActivityType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tActivityType](
	[ActivityTypeID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityType] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tActivityType] PRIMARY KEY CLUSTERED 
(
	[ActivityTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tAssistance]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tAssistance]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tAssistance](
	[AssistanceID] [int] IDENTITY(1,1) NOT NULL,
	[IncidentNumber] [char](10) NOT NULL,
	[IncidentDt] [smalldatetime] NOT NULL,
	[FK_CommunicationCode] [smallint] NULL,
	[FirstName] [varchar](20) NOT NULL,
	[Lastname] [varchar](30) NOT NULL,
	[Telephone] [char](10) NOT NULL,
	[Email] [varchar](45) NULL,
	[RecipID] [char](14) NULL,
	[FK_RequestorCode] [smallint] NOT NULL,
	[NPI] [varchar](10) NULL,
	[NPIOwner] [varchar](2) NULL,
	[NPISvcLoc] [varchar](3) NULL,
	[BusinessName] [varchar](100) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[FK_NavigatorID] [int] NOT NULL,
	[ZipCode] [varchar](10) NULL,
	[Other] [varchar](500) NULL,
 CONSTRAINT [PK_tAssistance] PRIMARY KEY CLUSTERED 
(
	[AssistanceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tAssistanceIssues]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tAssistanceIssues]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tAssistanceIssues](
	[IssueID] [int] IDENTITY(1,1) NOT NULL,
	[FK_AssistanceID] [int] NOT NULL,
	[IncidentNumber] [char](10) NOT NULL,
	[FK_EWCRecipientCode] [smallint] NULL,
	[FK_EWCPCPCode] [smallint] NULL,
	[FK_ReferralProviderCode] [smallint] NULL,
	[FK_EWCEnrolleeCode] [smallint] NULL,
	[OtherIssues] [varchar](500) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[ProblemOtherIssue] [varchar](500) NULL,
 CONSTRAINT [PK_tAssistanceIssues] PRIMARY KEY CLUSTERED 
(
	[IssueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tAssistanceResolution]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tAssistanceResolution]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tAssistanceResolution](
	[ResolutionID] [int] IDENTITY(1,1) NOT NULL,
	[FK_AssistanceID] [int] NOT NULL,
	[IncidentNumber] [char](10) NOT NULL,
	[ResolutionInitiationDt] [smalldatetime] NULL,
	[FK_ResolutionCode] [smallint] NULL,
	[ResolutionEndDt] [smalldatetime] NULL,
	[OtherResolution] [varchar](500) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[UnabletoResolve] [varchar](500) NULL,
 CONSTRAINT [PK_tAssistanceResolution] PRIMARY KEY CLUSTERED 
(
	[ResolutionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tBarrier]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tBarrier]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tBarrier](
	[BarrierID] [int] IDENTITY(1,1) NOT NULL,
	[FK_EncounterID] [int] NOT NULL,
	[BarrierType] [int] NOT NULL,
	[BarrierAssessed] [int] NOT NULL,
	[FollowUpDt] [smalldatetime] NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[Other] [varchar](2000) NULL,
 CONSTRAINT [PK_tBarrier] PRIMARY KEY CLUSTERED 
(
	[BarrierID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tCaregiverApprovals]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tCaregiverApprovals]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tCaregiverApprovals](
	[ApprovalID] [int] IDENTITY(1,1) NOT NULL,
	[FK_NSRecipientID] [int] NOT NULL,
	[CaregiverApproval] [int] NOT NULL,
	[CaregiverApprovalOther] [varchar](20) NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
 CONSTRAINT [PK_tCaregiverApprovals] PRIMARY KEY CLUSTERED 
(
	[ApprovalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tCollaboratorContribution]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tCollaboratorContribution]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tCollaboratorContribution](
	[CollabContributionID] [int] IDENTITY(1,1) NOT NULL,
	[CollabContribution] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tCollaboratorContribution] PRIMARY KEY CLUSTERED 
(
	[CollabContributionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tDiscussionTopic]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tDiscussionTopic]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tDiscussionTopic](
	[DiscussionID] [int] IDENTITY(1,1) NOT NULL,
	[Discussion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tDiscussionTopic] PRIMARY KEY CLUSTERED 
(
	[DiscussionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tEncounter]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tEncounter]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tEncounter](
	[EncounterID] [int] IDENTITY(1,1) NOT NULL,
	[FK_NavigatorID] [int] NOT NULL,
	[FK_RecipientID] [int] NOT NULL,
	[EncounterNumber] [int] NOT NULL,
	[EncounterDt] [smalldatetime] NULL,
	[EncounterStartTime] [varchar](20) NULL,
	[EncounterEndTime] [varchar](20) NULL,
	[EncounterType] [int] NOT NULL,
	[EncounterReason] [int] NOT NULL,
	[MissedAppointment] [bit] NULL,
	[SvcOutcomeStatus] [int] NULL,
	[SvcOutcomeStatusReason] [varchar](200) NULL,
	[NextAppointmentDt] [smalldatetime] NULL,
	[NextAppointmentTime] [varchar](20) NULL,
	[NextAppointmentType] [int] NULL,
	[ServicesTerminated] [bit] NULL,
	[ServicesTerminatedDt] [smalldatetime] NULL,
	[ServicesTerminatedReason] [varchar](200) NULL,
	[Notes] [varchar](2000) NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[EncounterPurpose] [int] NULL,
 CONSTRAINT [PK_tEncounter] PRIMARY KEY CLUSTERED 
(
	[EncounterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tHecActivities]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecActivities]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tHecActivities](
	[ActivityId] [int] IDENTITY(1,1) NOT NULL,
	[ActivityCancelled] [bit] NULL,
	[ReasonForCancellation] [varchar](250) NULL,
	[ActivityDate] [datetime] NULL,
	[ActivityType] [smallint] NULL,
	[OtherActivityType] [varchar](300) NULL,
	[NameOrPurpose] [varchar](250) NULL,
	[CollaboratorId1] [int] NULL,
	[CollaboratorId2] [int] NULL,
	[CollaboratorContributionId1] [int] NULL,
	[CollaboratorContributionId2] [int] NULL,
	[Address1] [nvarchar](100) NULL,
	[Address2] [nvarchar](100) NULL,
	[City] [varchar](50) NULL,
	[Zip] [varchar](5) NULL,
	[CHW1] [int] NULL,
	[CHW2] [int] NULL,
	[OtherAttendee1] [nvarchar](100) NULL,
	[OtherAttendee2] [nvarchar](100) NULL,
	[Population] [int] NULL,
	[LanguageId] [int] NULL,
	[Discussed] [int] NULL,
	[OtherDiscussed] [varchar](300) NULL,
	[PrePostTest] [varchar](300) NULL,
	[Result] [int] NULL,
	[OtherResult] [varchar](300) NULL,
	[MyRole] [int] NULL,
	[OtherMyRole] [varchar](300) NULL,
	[Attendee] [bit] NULL,
	[ConfirmAttendance] [bit] NULL,
	[AnnoucementDoc] [varchar](300) NULL,
	[Travel] [bit] NULL,
	[Notes] [nvarchar](250) NULL,
 CONSTRAINT [PK_tHecActivities] PRIMARY KEY CLUSTERED 
(
	[ActivityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tHecActivityParticipantInfo]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecActivityParticipantInfo]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tHecActivityParticipantInfo](
	[ActivityId] [int] NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Month] [smallint] NULL,
	[Year] [int] NULL,
	[RaceOrEthinicity] [int] NULL,
	[LanguageId] [int] NULL,
	[Address1] [nvarchar](100) NULL,
	[Address2] [nvarchar](100) NULL,
	[City] [varchar](50) NULL,
	[Zip] [varchar](5) NULL,
	[Telephone] [varchar](50) NULL,
	[Type] [bit] NULL,
	[Email] [varchar](150) NULL,
	[MAM] [bit] NULL,
	[PAP] [bit] NULL,
	[HI] [bit] NULL,
	[OkToContact] [bit] NULL,
	[LastMAM] [int] NULL,
	[LastPAP] [int] NULL,
	[EWCEligible] [bit] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tHecScreeningEvent]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecScreeningEvent]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tHecScreeningEvent](
	[ActivityId] [int] NOT NULL,
	[BusinessName] [nvarchar](100) NULL,
	[NPI] [varchar](50) NULL,
	[ProviderFirstName] [nvarchar](50) NULL,
	[ProviderLastName] [nvarchar](50) NULL,
	[EwcProvider] [smallint] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tInstantMessage]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tInstantMessage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tInstantMessage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [nvarchar](250) NULL,
	[Message] [nvarchar](max) NULL,
	[ReceivedOn] [datetime] NOT NULL,
	[ReceivedFrom] [int] NOT NULL,
	[ReceivedBy] [int] NOT NULL,
	[IsRead] [bit] NOT NULL,
	[ParentId] [int] NULL,
 CONSTRAINT [PK_tInstantMessage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tMyRole]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tMyRole]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tMyRole](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[Role] [varchar](50) NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tNavigationCycle]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tNavigationCycle](
	[NavigationCycleID] [int] IDENTITY(1,1) NOT NULL,
	[FK_RecipientID] [int] NOT NULL,
	[FK_ProviderID] [int] NULL,
	[FK_ReferralID] [int] NULL,
	[CancerSite] [int] NOT NULL,
	[NSProblem] [int] NOT NULL,
	[HealthProgram] [int] NOT NULL,
	[HealthInsuranceStatus] [int] NOT NULL,
	[HealthInsurancePlan] [int] NULL,
	[HealthInsurancePlanNumber] [varchar](15) NULL,
	[ContactPrefDays] [varchar](15) NULL,
	[ContactPrefHours] [varchar](15) NULL,
	[ContactPrefHoursOther] [varchar](20) NULL,
	[ContactPrefType] [int] NULL,
	[SRIndentifierCodeGenerated] [bit] NULL,
	[FK_NavigatorID] [int] NOT NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
 CONSTRAINT [PK_tNavigationCycle] PRIMARY KEY CLUSTERED 
(
	[NavigationCycleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tNavigator]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigator]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tNavigator](
	[NavigatorID] [int] IDENTITY(5000,1) NOT NULL,
	[DomainName] [varchar](20) NOT NULL,
	[Type] [int] NOT NULL,
	[LastName] [varchar](30) NOT NULL,
	[FirstName] [varchar](20) NOT NULL,
	[Address1] [varchar](30) NOT NULL,
	[Address2] [varchar](30) NULL,
	[City] [varchar](25) NOT NULL,
	[State] [char](2) NOT NULL,
	[Zip] [char](5) NOT NULL,
	[BusinessTelephone] [char](10) NOT NULL,
	[MobileTelephone] [char](10) NULL,
	[Email] [varchar](45) NOT NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[Region] [smallint] NULL,
 CONSTRAINT [PK_tNavigator] PRIMARY KEY CLUSTERED 
(
	[NavigatorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tNavigatorLanguages]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigatorLanguages]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tNavigatorLanguages](
	[NavigatorLanguageID] [int] IDENTITY(1,1) NOT NULL,
	[FK_NavigatorID] [int] NOT NULL,
	[LanguageCode] [int] NOT NULL,
	[SpeakingScore] [int] NULL,
	[ListeningScore] [int] NULL,
	[ReadingScore] [int] NULL,
	[WritingScore] [int] NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
 CONSTRAINT [PK_tNavigatorLanguages] PRIMARY KEY CLUSTERED 
(
	[NavigatorLanguageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tNavigatorTraining]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigatorTraining]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tNavigatorTraining](
	[TrainingID] [int] IDENTITY(1,1) NOT NULL,
	[FK_NavigatorID] [int] NOT NULL,
	[CourseName] [varchar](100) NOT NULL,
	[Organization] [varchar](100) NOT NULL,
	[CompletionDt] [smalldatetime] NOT NULL,
	[Certificate] [bit] NOT NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[BeginDt] [smalldatetime] NULL,
	[CMEAwarded] [bit] NULL,
 CONSTRAINT [PK_tNavigatorTraining] PRIMARY KEY CLUSTERED 
(
	[TrainingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tNavigatorTransfer]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNavigatorTransfer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tNavigatorTransfer](
	[TransferID] [int] IDENTITY(1,1) NOT NULL,
	[FK_RecipientID] [int] NOT NULL,
	[CurrentNavigatorID] [int] NOT NULL,
	[NewNavigatorID] [int] NOT NULL,
	[NavigatorTransferReason] [varchar](200) NULL,
	[NavigatorTransferDt] [smalldatetime] NULL,
	[TransferStatus] [int] NULL,
	[TransferRefusedReason] [varchar](200) NULL,
	[TransferRefusedDt] [smalldatetime] NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
PRIMARY KEY CLUSTERED 
(
	[TransferID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tNSProvider]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNSProvider]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tNSProvider](
	[NSProviderID] [int] IDENTITY(1,1) NOT NULL,
	[PCPName] [varchar](100) NOT NULL,
	[PCP_NPI] [varchar](10) NULL,
	[PCP_Owner] [varchar](2) NULL,
	[PCP_Location] [varchar](3) NULL,
	[PCPAddress1] [varchar](30) NOT NULL,
	[PCPAddress2] [varchar](30) NULL,
	[PCPCity] [varchar](24) NOT NULL,
	[PCPState] [char](2) NOT NULL,
	[PCPZip] [char](5) NOT NULL,
	[PCPContactFName] [varchar](50) NULL,
	[PCPContactLName] [varchar](50) NULL,
	[PCPContactTitle] [varchar](50) NULL,
	[PCPContactTelephone] [char](10) NULL,
	[PCPContactEmail] [varchar](45) NULL,
	[Medical] [bit] NOT NULL,
	[MedicalSpecialty] [int] NULL,
	[MedicalSpecialtyOther] [varchar](100) NULL,
	[ManagedCarePlan] [int] NULL,
	[ManagedCarePlanOther] [varchar](100) NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[EWCPCP] [bit] NULL,
 CONSTRAINT [PK_tNSProvider] PRIMARY KEY CLUSTERED 
(
	[NSProviderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tPartnersCollaborators]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tPartnersCollaborators]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tPartnersCollaborators](
	[PartnersCollaboratorsId] [int] IDENTITY(1,1) NOT NULL,
	[NameofOrg1] [varchar](50) NULL,
	[NameOfOrg2] [varchar](50) NULL,
	[Abbreviation] [varchar](50) NULL,
 CONSTRAINT [PK_dbo]].[tPartnersCollaborators] PRIMARY KEY CLUSTERED 
(
	[PartnersCollaboratorsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tRace]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tRace]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tRace](
	[RaceID] [int] IDENTITY(1,1) NOT NULL,
	[FK_NSRecipientID] [int] NOT NULL,
	[RaceCode] [int] NOT NULL,
	[RaceOther] [varchar](20) NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
 CONSTRAINT [PK_tRace] PRIMARY KEY CLUSTERED 
(
	[RaceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trAssistanceCommunication]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceCommunication]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trAssistanceCommunication](
	[CommunicationCode] [smallint] NOT NULL,
	[CommunicationDescription] [varchar](200) NOT NULL,
 CONSTRAINT [PK_trAssistanceCommunication] PRIMARY KEY CLUSTERED 
(
	[CommunicationCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trAssistanceEWCEnrollee]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceEWCEnrollee]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trAssistanceEWCEnrollee](
	[EWCEnrolleeCode] [smallint] NOT NULL,
	[EWCEnrolleeDescription] [varchar](200) NOT NULL,
 CONSTRAINT [PK_trAssistanceEWCEnrollee] PRIMARY KEY CLUSTERED 
(
	[EWCEnrolleeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trAssistanceEWCPCP]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceEWCPCP]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trAssistanceEWCPCP](
	[EWCPCPCode] [smallint] NOT NULL,
	[EWCPCPDescription] [varchar](200) NOT NULL,
 CONSTRAINT [PK_trAssistanceEWCPCP] PRIMARY KEY CLUSTERED 
(
	[EWCPCPCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trAssistanceEWCRecipient]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceEWCRecipient]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trAssistanceEWCRecipient](
	[EWCRecipientCode] [smallint] NOT NULL,
	[EWCRecipientDescription] [varchar](200) NOT NULL,
 CONSTRAINT [PK_trAssistanceEWCRecipient] PRIMARY KEY CLUSTERED 
(
	[EWCRecipientCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trAssistanceReferralProvider]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceReferralProvider]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trAssistanceReferralProvider](
	[ReferralProviderCode] [smallint] NOT NULL,
	[ReferralProviderDescription] [varchar](200) NOT NULL,
 CONSTRAINT [PK_trAssistanceReferralProvider] PRIMARY KEY CLUSTERED 
(
	[ReferralProviderCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trAssistanceRequestor]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceRequestor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trAssistanceRequestor](
	[RequestorCode] [smallint] NOT NULL,
	[RequestorDescription] [varchar](200) NOT NULL,
 CONSTRAINT [PK_trAssistanceRequestor] PRIMARY KEY CLUSTERED 
(
	[RequestorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trAssistanceResolution]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trAssistanceResolution]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trAssistanceResolution](
	[ResolutionCode] [smallint] NOT NULL,
	[ResolutionDescription] [varchar](200) NOT NULL,
 CONSTRAINT [PK_trAssistanceResolution] PRIMARY KEY CLUSTERED 
(
	[ResolutionCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trBarrier]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trBarrier]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trBarrier](
	[BarrierCode] [int] NOT NULL,
	[BarrierDescription] [varchar](100) NOT NULL,
	[BarrierTypeCode] [int] NULL,
 CONSTRAINT [PK_trBarrier] PRIMARY KEY CLUSTERED 
(
	[BarrierCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trBarrierSolution]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trBarrierSolution]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trBarrierSolution](
	[BarrierSolutionCode] [int] NOT NULL,
	[BarrierSolutionDescription] [varchar](100) NOT NULL,
	[BarrierTypeCode] [int] NULL,
 CONSTRAINT [PK_trBarrierSolution] PRIMARY KEY CLUSTERED 
(
	[BarrierSolutionCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trBarrierType]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trBarrierType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trBarrierType](
	[BarrierTypeCode] [int] NOT NULL,
	[BarrierTypeDescription] [varchar](50) NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trCancerSite]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trCancerSite]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trCancerSite](
	[CancerSiteCode] [int] NOT NULL,
	[CancerSiteName] [varchar](20) NOT NULL,
 CONSTRAINT [PK_trCancerSite] PRIMARY KEY CLUSTERED 
(
	[CancerSiteCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trCaregiverApproval]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trCaregiverApproval]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trCaregiverApproval](
	[CaregiverApprovalCode] [int] NOT NULL,
	[CaregiverApprovalDescription] [varchar](100) NOT NULL,
 CONSTRAINT [PK_trCaregiverApproval] PRIMARY KEY CLUSTERED 
(
	[CaregiverApprovalCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trCountries]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trCountries]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trCountries](
	[CountryCode] [int] NOT NULL,
	[CountryName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_trCountries] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trEncounterPurpose]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trEncounterPurpose]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trEncounterPurpose](
	[EncounterPurposeCode] [int] NOT NULL,
	[EncounterPurposeDescription] [varchar](50) NOT NULL,
 CONSTRAINT [PK_trEncounterPurpose] PRIMARY KEY CLUSTERED 
(
	[EncounterPurposeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trEncounterType]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trEncounterType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trEncounterType](
	[EncounterTypeCode] [int] NOT NULL,
	[EncounterTypeDescription] [varchar](50) NOT NULL,
 CONSTRAINT [PK_trEncounterType] PRIMARY KEY CLUSTERED 
(
	[EncounterTypeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tResults]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tResults]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tResults](
	[ResultID] [int] IDENTITY(1,1) NOT NULL,
	[Result] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tResults] PRIMARY KEY CLUSTERED 
(
	[ResultID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trHealthInsurance]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trHealthInsurance]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trHealthInsurance](
	[HealthInsuranceCode] [int] NOT NULL,
	[HealthInsuranceName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_trHealthInsurance] PRIMARY KEY CLUSTERED 
(
	[HealthInsuranceCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trHealthInsuranceStatus]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trHealthInsuranceStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trHealthInsuranceStatus](
	[HealthInsuranceStatusCode] [int] NOT NULL,
	[HealthInsuranceStatusName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_trHealthInsuranceStatus] PRIMARY KEY CLUSTERED 
(
	[HealthInsuranceStatusCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trHealthProgram]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trHealthProgram]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trHealthProgram](
	[HealthProgramCode] [int] NOT NULL,
	[HealthProgramName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_trHealthProgram] PRIMARY KEY CLUSTERED 
(
	[HealthProgramCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trNavigatorTransferReason]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trNavigatorTransferReason]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trNavigatorTransferReason](
	[NavigatorTransferReasonCode] [int] NOT NULL,
	[NavigatorTransferReason] [varchar](200) NOT NULL,
 CONSTRAINT [PK_trNavigatorTransferReason] PRIMARY KEY CLUSTERED 
(
	[NavigatorTransferReasonCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trNavigatorType]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trNavigatorType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trNavigatorType](
	[NavigatorTypeCode] [int] NOT NULL,
	[NavigatorTypeDescription] [varchar](50) NOT NULL,
 CONSTRAINT [PK_trNavigatorType] PRIMARY KEY CLUSTERED 
(
	[NavigatorTypeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trNSProblem]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trNSProblem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trNSProblem](
	[NSProblemCode] [int] NOT NULL,
	[NSProblemDescription] [varchar](100) NOT NULL,
 CONSTRAINT [PK_trNSProblem] PRIMARY KEY CLUSTERED 
(
	[NSProblemCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trOutcomeStatus]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trOutcomeStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trOutcomeStatus](
	[OutcomeStatusCode] [int] NOT NULL,
	[OutcomeStatusDescription] [varchar](50) NOT NULL,
 CONSTRAINT [PK_trOutcomeStatus] PRIMARY KEY CLUSTERED 
(
	[OutcomeStatusCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trOutcomeStatusReason]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trOutcomeStatusReason]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trOutcomeStatusReason](
	[OutcomeStatusReasonCode] [int] NOT NULL,
	[OutcomeStatusReason] [varchar](50) NOT NULL,
 CONSTRAINT [PK_trOutcomeStatusReason] PRIMARY KEY CLUSTERED 
(
	[OutcomeStatusReasonCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trProvLanguages]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trProvLanguages]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trProvLanguages](
	[LanguageCode] [int] NOT NULL,
	[LanguageName] [varchar](30) NOT NULL,
 CONSTRAINT [PK_trProvLanguages] PRIMARY KEY CLUSTERED 
(
	[LanguageCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trRace]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trRace]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trRace](
	[RaceCode] [int] NOT NULL,
	[RaceName] [varchar](30) NOT NULL,
 CONSTRAINT [PK_trRace] PRIMARY KEY CLUSTERED 
(
	[RaceCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trRaceAsian]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trRaceAsian]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trRaceAsian](
	[RaceAsianCode] [int] NOT NULL,
	[RaceAsianName] [varchar](30) NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trRacePacIslander]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trRacePacIslander]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trRacePacIslander](
	[RacePacIslanderCode] [int] NOT NULL,
	[RacePacIslanderName] [varchar](30) NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trServiceTerminatedReason]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trServiceTerminatedReason]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trServiceTerminatedReason](
	[ServiceTerminatedReasonCode] [int] NOT NULL,
	[ServiceTerminatedReason] [varchar](50) NOT NULL,
 CONSTRAINT [PK_trServiceTerminatedReason] PRIMARY KEY CLUSTERED 
(
	[ServiceTerminatedReasonCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[trSolutionImplementationStatus]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trSolutionImplementationStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[trSolutionImplementationStatus](
	[SolutionImplementationStatusCode] [int] NOT NULL,
	[SolutionImplementationStatus] [varchar](100) NOT NULL,
 CONSTRAINT [PK_trSolutionImplementationStatus] PRIMARY KEY CLUSTERED 
(
	[SolutionImplementationStatusCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tScreeningNav]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tScreeningNav]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tScreeningNav](
	[SN_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_NavigatorID] [int] NOT NULL,
	[SN_CODE1] [varchar](11) NOT NULL,
	[SN_CODE2] [smallint] NOT NULL,
	[LastName] [varchar](30) NOT NULL,
	[FirstName] [varchar](20) NULL,
	[DOB] [smalldatetime] NULL,
	[Address1] [varchar](30) NULL,
	[Address2] [varchar](30) NULL,
	[City] [varchar](25) NULL,
	[State] [char](2) NULL,
	[Zip] [char](5) NULL,
	[HomeTelephone] [char](10) NULL,
	[Cellphone] [char](10) NULL,
	[Email] [varchar](45) NULL,
	[Computer] [bit] NULL,
	[TextMessage] [char](1) NULL,
	[DateOfContact1] [smalldatetime] NULL,
	[ApptScreen1] [char](1) NULL,
	[SvcDate1] [smalldatetime] NULL,
	[SvcType1] [char](2) NULL,
	[Response1] [char](2) NULL,
	[DateOfContact2] [smalldatetime] NULL,
	[ApptScreen2] [char](1) NULL,
	[SvcDate2] [smalldatetime] NULL,
	[SvcType2] [char](2) NULL,
	[Response2] [char](2) NULL,
	[NSEnrollment] [char](1) NULL,
	[EnrollmentDate] [smalldatetime] NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
PRIMARY KEY CLUSTERED 
(
	[SN_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tServiceRecipient]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tServiceRecipient]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tServiceRecipient](
	[NSRecipientID] [int] IDENTITY(1,1) NOT NULL,
	[FK_NavigatorID] [int] NOT NULL,
	[FK_RecipID] [char](14) NULL,
	[LastName] [varchar](30) NOT NULL,
	[FirstName] [varchar](20) NOT NULL,
	[MiddleInitial] [varchar](1) NULL,
	[DOB] [smalldatetime] NOT NULL,
	[AddressNotAvailable] [bit] NULL,
	[Address1] [varchar](30) NULL,
	[Address2] [varchar](30) NULL,
	[City] [varchar](25) NULL,
	[State] [char](2) NULL,
	[Zip] [char](5) NULL,
	[Phone1] [char](10) NOT NULL,
	[P1PhoneType] [smallint] NOT NULL,
	[P1PersonalMessage] [bit] NOT NULL,
	[Phone2] [char](10) NULL,
	[P2PhoneType] [smallint] NULL,
	[P2PhoneTypeOther] [varchar](20) NULL,
	[P2PersonalMessage] [bit] NULL,
	[MotherMaidenName] [varchar](20) NULL,
	[Email] [varchar](45) NULL,
	[SSN] [char](10) NULL,
	[ImmigrantStatus] [bit] NULL,
	[CountryOfOrigin] [smallint] NULL,
	[Gender] [char](1) NULL,
	[Ethnicity] [bit] NULL,
	[PrimaryLanguage] [smallint] NOT NULL,
	[AgreeToSpeakEnglish] [bit] NOT NULL,
	[CaregiverName] [varchar](50) NULL,
	[Relationship] [smallint] NULL,
	[RelationshipOther] [varchar](30) NULL,
	[CaregiverPhone] [char](10) NULL,
	[CaregiverPhoneType] [smallint] NULL,
	[CaregiverPhonePersonalMessage] [bit] NULL,
	[CaregiverEmail] [varchar](45) NULL,
	[CaregiverContactPreference] [int] NULL,
	[Comments] [varchar](500) NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[NavigatorTransferDt] [smalldatetime] NULL,
	[NavigatorTransferReason] [varchar](200) NULL,
	[OldNavigatorID] [int] NULL,
 CONSTRAINT [PK_tServiceRecipient] PRIMARY KEY CLUSTERED 
(
	[NSRecipientID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tSolution]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tSolution]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tSolution](
	[SolutionID] [int] IDENTITY(1,1) NOT NULL,
	[FK_BarrierID] [int] NOT NULL,
	[SolutionOffered] [int] NOT NULL,
	[SolutionImplementationStatus] [int] NULL,
	[xtag] [char](3) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[CreatedBy] [varchar](24) NOT NULL,
	[LastUpdated] [smalldatetime] NULL,
	[UpdatedBy] [varchar](24) NULL,
	[FollowUpDt] [smalldatetime] NULL,
	[Solution] [varchar](2000) NULL,
 CONSTRAINT [PK_tSolution] PRIMARY KEY CLUSTERED 
(
	[SolutionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tSystemMessage]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tSystemMessage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tSystemMessage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [nvarchar](250) NULL,
	[Message] [nvarchar](max) NULL,
	[ReceivedOn] [datetime] NOT NULL,
	[ReceivedBy] [int] NOT NULL,
	[IsRead] [bit] NOT NULL,
 CONSTRAINT [PK_tSystemMessage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tUserAccess]    Script Date: 5/26/2018 7:05:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tUserAccess]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tUserAccess](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DomainName] [varchar](20) NOT NULL,
	[LastAccess] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_tUserAccess] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[tActivityType] ON 

INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (1, N'Cultural')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (2, N'Faith-based')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (3, N'Festival')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (4, N'Health Fair')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (5, N'Job Fair')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (6, N'LGBT')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (7, N'Resource Fair')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (8, N'School')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (9, N'Screening')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (10, N'Other')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (11, N'Cultural')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (12, N'Faith-based')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (13, N'Festival')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (14, N'Health Fair')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (15, N'Job Fair')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (16, N'LGBT')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (17, N'Resource Fair')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (18, N'School')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (19, N'Screening')
INSERT [dbo].[tActivityType] ([ActivityTypeID], [ActivityType]) VALUES (20, N'Other')
SET IDENTITY_INSERT [dbo].[tActivityType] OFF
SET IDENTITY_INSERT [dbo].[tAssistance] ON 

INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (31, N'AST0000001', CAST(N'2018-05-20T18:20:00' AS SmallDateTime), 1, N'aa', N'ss', N'(222) 222-', NULL, NULL, 1, NULL, NULL, NULL, NULL, CAST(N'2018-05-20T18:21:00' AS SmallDateTime), N'User input', CAST(N'2018-05-20T18:21:00' AS SmallDateTime), NULL, 5002, NULL, NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (32, N'ST00000032', CAST(N'2018-05-20T21:03:00' AS SmallDateTime), 1, N'Retna ', N'Mani', N'(123) 345-', N'retnamani@gmail.com', N'123456        ', 1, NULL, NULL, NULL, NULL, CAST(N'2018-05-20T21:04:00' AS SmallDateTime), N'User input', CAST(N'2018-05-20T21:04:00' AS SmallDateTime), NULL, 5002, NULL, NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (33, N'ST00000033', CAST(N'2018-05-20T21:08:00' AS SmallDateTime), NULL, N'rithin', N'sajith', N'(555) 555-', NULL, NULL, 1, NULL, NULL, NULL, NULL, CAST(N'2018-05-20T21:09:00' AS SmallDateTime), N'User input', CAST(N'2018-05-20T21:09:00' AS SmallDateTime), NULL, 5002, NULL, NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (34, N'ST00000034', CAST(N'2018-05-20T21:22:00' AS SmallDateTime), NULL, N'rithin', N'sajith', N'(999) 999-', NULL, NULL, 1, NULL, NULL, NULL, NULL, CAST(N'2018-05-20T21:23:00' AS SmallDateTime), N'User input', CAST(N'2018-05-20T21:23:00' AS SmallDateTime), NULL, 5002, NULL, NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (35, N'ST00000035', CAST(N'2018-05-20T22:04:00' AS SmallDateTime), 2, N'Retna1', N'Mani1', N'(123) 456-', N'test1@gmail.com', N'              ', 4, NULL, NULL, NULL, N' Organization Name', CAST(N'2018-05-20T22:05:00' AS SmallDateTime), N'User input', CAST(N'2018-05-20T22:23:00' AS SmallDateTime), N'User update', 5002, NULL, NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (36, N'ST00000036', CAST(N'2018-05-21T09:15:00' AS SmallDateTime), 3, N'fff', N'lll', N'2222222222', N'asd@asd.cas', NULL, 1, NULL, NULL, NULL, NULL, CAST(N'2018-05-21T09:16:00' AS SmallDateTime), N'User input', CAST(N'2018-05-21T12:23:00' AS SmallDateTime), N'User update', 5002, N'12345', NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (37, N'ST00000037', CAST(N'2018-05-21T12:25:00' AS SmallDateTime), 3, N'qq', N'ww', N'3333333333', N'asd@as.asd', NULL, 2, N'123', N'Y', NULL, NULL, CAST(N'2018-05-21T12:27:00' AS SmallDateTime), N'User input', CAST(N'2018-05-21T12:27:00' AS SmallDateTime), NULL, 5002, N'12345', NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (38, N'ST00000038', CAST(N'2018-05-21T12:33:00' AS SmallDateTime), NULL, N'asd', N'asd', N'3333333333', NULL, NULL, 2, NULL, NULL, NULL, NULL, CAST(N'2018-05-21T12:37:00' AS SmallDateTime), N'User input', CAST(N'2018-05-21T12:37:00' AS SmallDateTime), NULL, 5002, NULL, NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (39, N'ST00000039', CAST(N'2018-05-21T21:39:00' AS SmallDateTime), 5, N'rithin', N'sajith', N'8129075371', N'rithin@wowiso.com', N'654654        ', 1, NULL, NULL, NULL, NULL, CAST(N'2018-05-21T21:45:00' AS SmallDateTime), N'User input', CAST(N'2018-05-21T21:45:00' AS SmallDateTime), NULL, 5002, N'670611', N'nothing')
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (40, N'ST00000040', CAST(N'2018-05-22T10:40:00' AS SmallDateTime), 2, N'q', N'w', N'2222222222', N'asd@sss.com', N'12345         ', 1, NULL, NULL, NULL, NULL, CAST(N'2018-05-22T10:44:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T10:44:00' AS SmallDateTime), NULL, 5002, N'12345', NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (41, N'ST00000041', CAST(N'2018-05-22T10:50:00' AS SmallDateTime), 3, N'w', N'w', N'3456789012', N'asd', NULL, 2, N'123456', N'Y', NULL, NULL, CAST(N'2018-05-22T10:51:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T10:51:00' AS SmallDateTime), NULL, 5002, N'123', NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (42, N'ST00000042', CAST(N'2018-05-22T11:04:00' AS SmallDateTime), 4, N'ee', N'ee', N'3333333333', NULL, NULL, 3, N'22222', N'A', NULL, N'asdfg', CAST(N'2018-05-22T11:05:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T11:32:00' AS SmallDateTime), N'User update', 5002, NULL, NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (43, N'ST00000043', CAST(N'2018-05-22T11:51:00' AS SmallDateTime), 4, N'rr', N'rr', N'5555555555', NULL, NULL, 2, N'666', N'Y', NULL, NULL, CAST(N'2018-05-22T11:53:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T11:53:00' AS SmallDateTime), NULL, 5002, NULL, NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (44, N'ST00000044', CAST(N'2018-05-22T11:55:00' AS SmallDateTime), 3, N'yy', N'yy', N'7777777777', NULL, NULL, 3, NULL, NULL, NULL, NULL, CAST(N'2018-05-22T11:56:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T11:56:00' AS SmallDateTime), NULL, 5002, NULL, NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (45, N'ST00000045', CAST(N'2018-05-22T11:56:00' AS SmallDateTime), 4, N'tt', N'tt', N'9999999999', NULL, NULL, 3, NULL, NULL, N'N', NULL, CAST(N'2018-05-22T11:57:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T11:57:00' AS SmallDateTime), NULL, 5002, NULL, NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (46, N'ST00000046', CAST(N'2018-05-22T12:01:00' AS SmallDateTime), 2, N'aa', N'ss', N'9999999999', NULL, NULL, 3, NULL, NULL, N'N', NULL, CAST(N'2018-05-22T12:10:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T12:10:00' AS SmallDateTime), NULL, 5002, NULL, NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (47, N'ST00000047', CAST(N'2018-05-22T12:10:00' AS SmallDateTime), 1, N'aa', N'aa', N'1234567890', NULL, NULL, 2, N'123', N'Y', NULL, NULL, CAST(N'2018-05-22T12:11:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T12:11:00' AS SmallDateTime), NULL, 5002, NULL, NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (48, N'ST00000048', CAST(N'2018-05-22T12:12:00' AS SmallDateTime), 1, N'ss', N'ss', N'3333333333', NULL, NULL, 2, N'222', N'Y', N'N', NULL, CAST(N'2018-05-22T12:13:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T12:13:00' AS SmallDateTime), NULL, 5002, NULL, NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (49, N'ST00000049', CAST(N'2018-05-22T12:14:00' AS SmallDateTime), 4, N'zz', N'zz', N'2323232323', NULL, NULL, 2, N'123', N'12', N'12', NULL, CAST(N'2018-05-22T12:33:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T12:33:00' AS SmallDateTime), NULL, 5002, NULL, NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (50, N'ST00000050', CAST(N'2018-05-22T12:50:00' AS SmallDateTime), 2, N'cc', N'cc', N'3333333333', NULL, NULL, 2, NULL, NULL, NULL, NULL, CAST(N'2018-05-22T12:50:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T12:50:00' AS SmallDateTime), NULL, 5002, NULL, NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (51, N'ST00000051', CAST(N'2018-05-23T09:22:00' AS SmallDateTime), 3, N'hi', N'hello', N'2323232323', N'sdkj', NULL, 3, N'12', N'Y', N'N', NULL, CAST(N'2018-05-23T09:24:00' AS SmallDateTime), N'User input', CAST(N'2018-05-23T09:24:00' AS SmallDateTime), NULL, 5002, N'12345', NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (52, N'ST00000052', CAST(N'2018-05-23T09:49:00' AS SmallDateTime), 1, N'asd', N'asd', N'4444444444', N'asd', N'123           ', 1, NULL, NULL, NULL, NULL, CAST(N'2018-05-23T09:49:00' AS SmallDateTime), N'User input', CAST(N'2018-05-23T09:49:00' AS SmallDateTime), NULL, 5002, N'12345', NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (53, N'ST00000053', CAST(N'2018-05-23T09:50:00' AS SmallDateTime), 4, N'q', N'q', N'2222222222', NULL, NULL, 3, N'12', N'Y', N'N', NULL, CAST(N'2018-05-23T09:52:00' AS SmallDateTime), N'User input', CAST(N'2018-05-23T09:52:00' AS SmallDateTime), NULL, 5002, NULL, NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (54, N'ST00000054', CAST(N'2018-05-23T09:54:00' AS SmallDateTime), 3, N'asd', N'asd', N'2222222222', N'asss', NULL, 3, N'123', N'Y', N'N', NULL, CAST(N'2018-05-23T09:54:00' AS SmallDateTime), N'User input', CAST(N'2018-05-23T09:54:00' AS SmallDateTime), NULL, 5002, N'123', NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (55, N'ST00000055', CAST(N'2018-05-23T11:20:00' AS SmallDateTime), 1, N'a', N'a', N'2222222222', N'a', NULL, 3, N'aa', NULL, NULL, NULL, CAST(N'2018-05-23T11:21:00' AS SmallDateTime), N'User input', CAST(N'2018-05-23T11:21:00' AS SmallDateTime), NULL, 5002, N'a', NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (56, N'ST00000056', CAST(N'2018-05-23T19:34:00' AS SmallDateTime), 1, N'Retna', N'Mani', N'9162209315', N'retnamani@gmail.com', N'R1235         ', 1, NULL, NULL, NULL, NULL, CAST(N'2018-05-23T19:35:00' AS SmallDateTime), N'User input', CAST(N'2018-05-23T19:35:00' AS SmallDateTime), NULL, 5002, N'95630', NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (57, N'ST00000057', CAST(N'2018-05-24T12:31:00' AS SmallDateTime), 2, N'hh', N'hh', N'1234567890', N'aaaa', N'12345         ', 2, NULL, NULL, NULL, NULL, CAST(N'2018-05-24T12:32:00' AS SmallDateTime), N'User input', CAST(N'2018-05-24T13:35:00' AS SmallDateTime), N'User update', 5002, N'12345', NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (58, N'ST00000058', CAST(N'2018-05-24T12:47:00' AS SmallDateTime), 1, N'aa', N'aa', N'2222222222', N'as', N'as            ', 1, NULL, NULL, NULL, NULL, CAST(N'2018-05-24T12:48:00' AS SmallDateTime), N'User input', CAST(N'2018-05-24T13:26:00' AS SmallDateTime), N'User update', 5002, N'as', NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (59, N'ST00000059', CAST(N'2018-05-24T13:47:00' AS SmallDateTime), NULL, N'sd', N'sd', N'1234567890', N'sd', NULL, 3, NULL, NULL, NULL, NULL, CAST(N'2018-05-24T13:47:00' AS SmallDateTime), N'User input', CAST(N'2018-05-24T13:47:00' AS SmallDateTime), NULL, 5002, N'sd', NULL)
INSERT [dbo].[tAssistance] ([AssistanceID], [IncidentNumber], [IncidentDt], [FK_CommunicationCode], [FirstName], [Lastname], [Telephone], [Email], [RecipID], [FK_RequestorCode], [NPI], [NPIOwner], [NPISvcLoc], [BusinessName], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FK_NavigatorID], [ZipCode], [Other]) VALUES (60, N'ST00000060', CAST(N'2018-05-24T22:44:00' AS SmallDateTime), 1, N'rithin', N'sajith', N'1234567890', N'rithin@wowiso.com', NULL, 3, N'sada', N'12', N'12', NULL, CAST(N'2018-05-24T22:45:00' AS SmallDateTime), N'User input', CAST(N'2018-05-24T22:45:00' AS SmallDateTime), NULL, 5002, N'123456', NULL)
SET IDENTITY_INSERT [dbo].[tAssistance] OFF
SET IDENTITY_INSERT [dbo].[tAssistanceIssues] ON 

INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (29, 31, N'AST0000001', 0, 0, 0, 0, NULL, CAST(N'2018-05-20T18:21:00' AS SmallDateTime), N'User input', CAST(N'2018-05-20T18:21:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (30, 32, N'ST00000032', 0, 0, 0, 0, NULL, CAST(N'2018-05-20T21:04:00' AS SmallDateTime), N'User input', CAST(N'2018-05-20T21:04:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (31, 33, N'ST00000033', 0, 0, 0, 0, NULL, CAST(N'2018-05-20T21:09:00' AS SmallDateTime), N'User input', CAST(N'2018-05-20T21:09:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (32, 34, N'ST00000034', 0, 0, 0, 0, NULL, CAST(N'2018-05-20T21:23:00' AS SmallDateTime), N'User input', CAST(N'2018-05-20T21:23:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (33, 35, N'ST00000035', 0, 0, 0, 0, NULL, CAST(N'2018-05-20T22:05:00' AS SmallDateTime), N'User input', CAST(N'2018-05-20T22:23:00' AS SmallDateTime), N'User update', NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (34, 36, N'ST00000036', 0, 0, 0, 0, NULL, CAST(N'2018-05-21T09:16:00' AS SmallDateTime), N'User input', CAST(N'2018-05-21T12:23:00' AS SmallDateTime), N'User update', NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (35, 37, N'ST00000037', 0, 0, 0, 0, NULL, CAST(N'2018-05-21T12:27:00' AS SmallDateTime), N'User input', CAST(N'2018-05-21T12:27:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (36, 38, N'ST00000038', 0, 0, 0, 0, NULL, CAST(N'2018-05-21T12:37:00' AS SmallDateTime), N'User input', CAST(N'2018-05-21T12:37:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (37, 39, N'ST00000039', 0, 0, 0, 0, NULL, CAST(N'2018-05-21T21:45:00' AS SmallDateTime), N'User input', CAST(N'2018-05-21T21:45:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (38, 40, N'ST00000040', 1, 0, 0, 0, N',,,,,,,,,,,,,,,,,,,,,,,,', CAST(N'2018-05-22T10:44:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T10:44:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (39, 41, N'ST00000041', 0, 2, 0, 0, NULL, CAST(N'2018-05-22T10:51:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T10:51:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (40, 42, N'ST00000042', 0, 0, 6, 5, N',,,,,,,,,,,,,,,,,,,,,,,,', CAST(N'2018-05-22T11:05:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T11:32:00' AS SmallDateTime), N'User update', NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (41, 43, N'ST00000043', 0, 2, 0, 0, NULL, CAST(N'2018-05-22T11:53:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T11:53:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (42, 44, N'ST00000044', 0, 0, 2, 0, NULL, CAST(N'2018-05-22T11:56:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T11:56:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (43, 45, N'ST00000045', 0, 0, 2, 0, NULL, CAST(N'2018-05-22T11:57:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T11:57:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (44, 46, N'ST00000046', 0, 0, 2, 0, NULL, CAST(N'2018-05-22T12:10:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T12:10:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (45, 47, N'ST00000047', 0, 2, 0, 0, NULL, CAST(N'2018-05-22T12:11:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T12:11:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (46, 48, N'ST00000048', 0, 3, 0, 0, NULL, CAST(N'2018-05-22T12:13:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T12:13:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (47, 49, N'ST00000049', 0, 2, 0, 0, NULL, CAST(N'2018-05-22T12:33:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T12:33:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (48, 50, N'ST00000050', 0, 7, 0, 0, NULL, CAST(N'2018-05-22T12:50:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T12:50:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (49, 52, N'ST00000052', 1, 0, 0, 0, NULL, CAST(N'2018-05-23T09:49:00' AS SmallDateTime), N'User input', CAST(N'2018-05-23T09:49:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (50, 53, N'ST00000053', 0, 0, 22, 0, NULL, CAST(N'2018-05-23T09:52:00' AS SmallDateTime), N'User input', CAST(N'2018-05-23T09:52:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (51, 54, N'ST00000054', 0, 0, 22, 0, NULL, CAST(N'2018-05-23T09:54:00' AS SmallDateTime), N'User input', CAST(N'2018-05-23T09:54:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (52, 55, N'ST00000055', 0, 0, 21, 0, NULL, CAST(N'2018-05-23T11:21:00' AS SmallDateTime), N'User input', CAST(N'2018-05-23T11:21:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (53, 56, N'ST00000056', 2, 0, 0, 0, NULL, CAST(N'2018-05-23T19:35:00' AS SmallDateTime), N'User input', CAST(N'2018-05-23T19:35:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (54, 57, N'ST00000057', 3, 7, 0, 0, N',,', CAST(N'2018-05-24T12:32:00' AS SmallDateTime), N'User input', CAST(N'2018-05-24T13:35:00' AS SmallDateTime), N'User update', NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (55, 58, N'ST00000058', 2, 0, 0, 0, N'asd,asd,asd,asd,asd,asd,asd,asd,asd,asd,asd,asd,asd,asd,asd,asd,asd,asd,asd', CAST(N'2018-05-24T12:48:00' AS SmallDateTime), N'User input', CAST(N'2018-05-24T13:26:00' AS SmallDateTime), N'User update', NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (56, 59, N'ST00000059', 0, 0, 1, 0, N',,', CAST(N'2018-05-24T13:47:00' AS SmallDateTime), N'User input', CAST(N'2018-05-24T13:47:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceIssues] ([IssueID], [FK_AssistanceID], [IncidentNumber], [FK_EWCRecipientCode], [FK_EWCPCPCode], [FK_ReferralProviderCode], [FK_EWCEnrolleeCode], [OtherIssues], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [ProblemOtherIssue]) VALUES (57, 60, N'ST00000060', 0, 0, 21, 0, NULL, CAST(N'2018-05-24T22:45:00' AS SmallDateTime), N'User input', CAST(N'2018-05-24T22:45:00' AS SmallDateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tAssistanceIssues] OFF
SET IDENTITY_INSERT [dbo].[tAssistanceResolution] ON 

INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (7, 31, N'AST0000001', CAST(N'2018-05-20T18:21:00' AS SmallDateTime), 0, CAST(N'2018-05-20T18:21:00' AS SmallDateTime), NULL, CAST(N'2018-05-20T18:21:00' AS SmallDateTime), N'User input', CAST(N'2018-05-20T18:21:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (8, 34, N'ST00000034', CAST(N'2018-05-20T21:23:00' AS SmallDateTime), 0, CAST(N'2018-05-20T21:23:00' AS SmallDateTime), NULL, CAST(N'2018-05-20T21:23:00' AS SmallDateTime), N'User input', CAST(N'2018-05-20T21:23:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (9, 35, N'ST00000035', NULL, 0, NULL, NULL, CAST(N'2018-05-20T22:05:00' AS SmallDateTime), N'User input', CAST(N'2018-05-20T22:23:00' AS SmallDateTime), N'User update', NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (10, 36, N'ST00000036', NULL, 0, NULL, NULL, CAST(N'2018-05-21T09:16:00' AS SmallDateTime), N'User input', CAST(N'2018-05-21T12:23:00' AS SmallDateTime), N'User update', NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (11, 37, N'ST00000037', CAST(N'2018-05-21T12:27:00' AS SmallDateTime), 0, CAST(N'2018-05-21T12:27:00' AS SmallDateTime), NULL, CAST(N'2018-05-21T12:27:00' AS SmallDateTime), N'User input', CAST(N'2018-05-21T12:27:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (12, 38, N'ST00000038', CAST(N'2018-05-21T12:37:00' AS SmallDateTime), 0, CAST(N'2018-05-21T12:37:00' AS SmallDateTime), NULL, CAST(N'2018-05-21T12:37:00' AS SmallDateTime), N'User input', CAST(N'2018-05-21T12:37:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (13, 39, N'ST00000039', CAST(N'2018-05-21T21:45:00' AS SmallDateTime), 0, CAST(N'2018-05-21T21:45:00' AS SmallDateTime), NULL, CAST(N'2018-05-21T21:45:00' AS SmallDateTime), N'User input', CAST(N'2018-05-21T21:45:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (14, 40, N'ST00000040', CAST(N'2018-05-22T10:44:00' AS SmallDateTime), 1, CAST(N'2018-05-22T10:44:00' AS SmallDateTime), NULL, CAST(N'2018-05-22T10:44:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T10:44:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (15, 41, N'ST00000041', CAST(N'2018-05-22T10:51:00' AS SmallDateTime), 2, CAST(N'2018-05-22T10:51:00' AS SmallDateTime), NULL, CAST(N'2018-05-22T10:51:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T10:51:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (16, 42, N'ST00000042', CAST(N'2018-05-22T11:32:00' AS SmallDateTime), 8, CAST(N'2018-05-22T11:32:00' AS SmallDateTime), N'asassa', CAST(N'2018-05-22T11:05:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T11:32:00' AS SmallDateTime), N'User update', NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (17, 43, N'ST00000043', CAST(N'2018-05-22T11:53:00' AS SmallDateTime), 3, CAST(N'2018-05-22T11:53:00' AS SmallDateTime), NULL, CAST(N'2018-05-22T11:53:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T11:53:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (18, 44, N'ST00000044', CAST(N'2018-05-22T11:56:00' AS SmallDateTime), 2, CAST(N'2018-05-22T11:56:00' AS SmallDateTime), NULL, CAST(N'2018-05-22T11:56:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T11:56:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (19, 45, N'ST00000045', CAST(N'2018-05-22T11:57:00' AS SmallDateTime), 10, CAST(N'2018-05-22T11:57:00' AS SmallDateTime), NULL, CAST(N'2018-05-22T11:57:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T11:57:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (20, 46, N'ST00000046', CAST(N'2018-05-22T12:10:00' AS SmallDateTime), 2, CAST(N'2018-05-22T12:10:00' AS SmallDateTime), NULL, CAST(N'2018-05-22T12:10:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T12:10:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (21, 47, N'ST00000047', CAST(N'2018-05-22T12:11:00' AS SmallDateTime), 2, CAST(N'2018-05-22T12:11:00' AS SmallDateTime), NULL, CAST(N'2018-05-22T12:11:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T12:11:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (22, 48, N'ST00000048', CAST(N'2018-05-22T12:13:00' AS SmallDateTime), 2, CAST(N'2018-05-22T12:13:00' AS SmallDateTime), NULL, CAST(N'2018-05-22T12:13:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T12:13:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (23, 49, N'ST00000049', CAST(N'2018-05-22T12:33:00' AS SmallDateTime), 0, CAST(N'2018-05-22T12:33:00' AS SmallDateTime), NULL, CAST(N'2018-05-22T12:33:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T12:33:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (24, 50, N'ST00000050', CAST(N'2018-05-22T12:50:00' AS SmallDateTime), 0, CAST(N'2018-05-22T12:50:00' AS SmallDateTime), NULL, CAST(N'2018-05-22T12:50:00' AS SmallDateTime), N'User input', CAST(N'2018-05-22T12:50:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (25, 52, N'ST00000052', CAST(N'2018-05-23T09:49:00' AS SmallDateTime), 1, CAST(N'2018-05-23T09:49:00' AS SmallDateTime), NULL, CAST(N'2018-05-23T09:49:00' AS SmallDateTime), N'User input', CAST(N'2018-05-23T09:49:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (26, 53, N'ST00000053', CAST(N'2018-05-23T09:52:00' AS SmallDateTime), 8, CAST(N'2018-05-23T09:52:00' AS SmallDateTime), N'asd', CAST(N'2018-05-23T09:52:00' AS SmallDateTime), N'User input', CAST(N'2018-05-23T09:52:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (27, 54, N'ST00000054', CAST(N'2018-05-23T09:54:00' AS SmallDateTime), 8, CAST(N'2018-05-23T09:54:00' AS SmallDateTime), N'asasas', CAST(N'2018-05-23T09:54:00' AS SmallDateTime), N'User input', CAST(N'2018-05-23T09:54:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (28, 55, N'ST00000055', CAST(N'2018-05-23T11:21:00' AS SmallDateTime), 0, CAST(N'2018-05-23T11:21:00' AS SmallDateTime), NULL, CAST(N'2018-05-23T11:21:00' AS SmallDateTime), N'User input', CAST(N'2018-05-23T11:21:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (29, 56, N'ST00000056', CAST(N'2018-05-23T19:35:00' AS SmallDateTime), 0, CAST(N'2018-05-23T19:35:00' AS SmallDateTime), NULL, CAST(N'2018-05-23T19:35:00' AS SmallDateTime), N'User input', CAST(N'2018-05-23T19:35:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (30, 57, N'ST00000057', CAST(N'2018-05-24T13:35:00' AS SmallDateTime), 4, CAST(N'2018-05-24T13:35:00' AS SmallDateTime), N'hii', CAST(N'2018-05-24T12:32:00' AS SmallDateTime), N'User input', CAST(N'2018-05-24T13:35:00' AS SmallDateTime), N'User update', NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (31, 58, N'ST00000058', CAST(N'2018-05-24T13:26:00' AS SmallDateTime), 8, CAST(N'2018-05-24T13:26:00' AS SmallDateTime), N'asd', CAST(N'2018-05-24T12:48:00' AS SmallDateTime), N'User input', CAST(N'2018-05-24T13:26:00' AS SmallDateTime), N'User update', NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (32, 59, N'ST00000059', CAST(N'2018-05-24T13:47:00' AS SmallDateTime), 8, CAST(N'2018-05-24T13:47:00' AS SmallDateTime), N'sd', CAST(N'2018-05-24T13:47:00' AS SmallDateTime), N'User input', CAST(N'2018-05-24T13:47:00' AS SmallDateTime), NULL, NULL)
INSERT [dbo].[tAssistanceResolution] ([ResolutionID], [FK_AssistanceID], [IncidentNumber], [ResolutionInitiationDt], [FK_ResolutionCode], [ResolutionEndDt], [OtherResolution], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [UnabletoResolve]) VALUES (33, 60, N'ST00000060', CAST(N'2018-05-24T22:45:00' AS SmallDateTime), 7, CAST(N'2018-05-24T22:45:00' AS SmallDateTime), NULL, CAST(N'2018-05-24T22:45:00' AS SmallDateTime), N'User input', CAST(N'2018-05-24T22:45:00' AS SmallDateTime), NULL, N'hai')
SET IDENTITY_INSERT [dbo].[tAssistanceResolution] OFF
SET IDENTITY_INSERT [dbo].[tBarrier] ON 

INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (1, 1, 3, 9, NULL, NULL, CAST(N'2017-10-16T14:47:00' AS SmallDateTime), N'User input', CAST(N'2017-10-16T14:48:00' AS SmallDateTime), N'User update', NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (6, 6, 5, 15, NULL, NULL, CAST(N'2018-02-26T14:32:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (7, 8, 7, 23, NULL, NULL, CAST(N'2018-02-27T16:37:00' AS SmallDateTime), N'User input', CAST(N'2018-03-05T20:14:00' AS SmallDateTime), N'User update', N'ioudkjckkcskcsk')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (8, 9, 7, 23, NULL, NULL, CAST(N'2018-02-27T16:54:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (9, 10, 7, 23, NULL, NULL, CAST(N'2018-02-27T17:02:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (10, 11, 3, 10, NULL, NULL, CAST(N'2018-02-28T12:32:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (11, 12, 7, 23, NULL, NULL, CAST(N'2018-02-28T13:03:00' AS SmallDateTime), N'User input', CAST(N'2018-02-28T14:02:00' AS SmallDateTime), N'User update', NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (12, 14, 7, 23, NULL, NULL, CAST(N'2018-02-28T14:43:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (13, 15, 7, 23, NULL, NULL, CAST(N'2018-02-28T14:49:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (14, 16, 7, 23, NULL, NULL, CAST(N'2018-03-01T09:19:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (15, 17, 7, 23, NULL, NULL, CAST(N'2018-03-01T09:29:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (16, 18, 7, 23, NULL, NULL, CAST(N'2018-03-01T09:37:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (17, 19, 7, 23, NULL, NULL, CAST(N'2018-03-01T09:52:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (18, 20, 7, 23, NULL, NULL, CAST(N'2018-03-01T10:02:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (19, 21, 7, 23, NULL, NULL, CAST(N'2018-03-01T10:03:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (20, 22, 7, 23, NULL, NULL, CAST(N'2018-03-01T10:09:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (21, 23, 7, 23, NULL, NULL, CAST(N'2018-03-01T12:37:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (22, 24, 7, 23, NULL, NULL, CAST(N'2018-03-01T13:09:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (23, 25, 7, 23, NULL, NULL, CAST(N'2018-03-02T19:59:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (24, 26, 7, 23, NULL, NULL, CAST(N'2018-03-02T20:04:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (25, 26, 7, 23, NULL, NULL, CAST(N'2018-03-02T20:05:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (26, 26, 7, 23, NULL, NULL, CAST(N'2018-03-02T20:07:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (27, 27, 7, 23, NULL, NULL, CAST(N'2018-03-03T11:48:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (28, 28, 5, 16, NULL, NULL, CAST(N'2018-03-03T13:23:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (29, 29, 6, 20, NULL, NULL, CAST(N'2018-03-03T14:11:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (30, 30, 7, 23, NULL, NULL, CAST(N'2018-03-03T14:23:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (31, 31, 2, 7, NULL, NULL, CAST(N'2018-03-03T15:09:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (32, 32, 7, 23, NULL, NULL, CAST(N'2018-03-03T15:11:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (33, 33, 7, 23, NULL, NULL, CAST(N'2018-03-03T15:28:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (34, 34, 7, 23, NULL, NULL, CAST(N'2018-03-03T15:28:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (35, 35, 7, 23, NULL, NULL, CAST(N'2018-03-03T15:35:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (36, 36, 7, 23, NULL, NULL, CAST(N'2018-03-03T15:42:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (37, 37, 7, 23, NULL, NULL, CAST(N'2018-03-03T15:46:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (38, 38, 7, 23, NULL, NULL, CAST(N'2018-03-03T15:53:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (39, 39, 7, 23, NULL, NULL, CAST(N'2018-03-03T16:04:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (40, 40, 7, 23, NULL, NULL, CAST(N'2018-03-03T16:10:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (41, 41, 7, 23, NULL, NULL, CAST(N'2018-03-03T16:20:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (42, 42, 7, 23, NULL, NULL, CAST(N'2018-03-03T16:34:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (43, 43, 7, 23, NULL, NULL, CAST(N'2018-03-03T16:42:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (44, 44, 7, 23, NULL, NULL, CAST(N'2018-03-03T16:54:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (45, 45, 7, 23, NULL, NULL, CAST(N'2018-03-03T17:03:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (46, 46, 7, 23, NULL, NULL, CAST(N'2018-03-03T17:12:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (47, 47, 7, 23, NULL, NULL, CAST(N'2018-03-04T09:36:00' AS SmallDateTime), N'User input', NULL, NULL, N'OOOOOOOOOOOOOOO')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (48, 48, 7, 23, NULL, NULL, CAST(N'2018-03-04T09:44:00' AS SmallDateTime), N'User input', NULL, NULL, N'3/4/18  9:39 Other')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (49, 49, 7, 23, NULL, NULL, CAST(N'2018-03-04T09:48:00' AS SmallDateTime), N'User input', NULL, NULL, N'3/4/18 9:46  Other')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (50, 50, 7, 23, NULL, NULL, CAST(N'2018-03-04T12:55:00' AS SmallDateTime), N'User input', NULL, NULL, N'3/4/18 12:53PM Other stuff')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (51, 50, 7, 23, NULL, NULL, CAST(N'2018-03-04T12:56:00' AS SmallDateTime), N'User input', NULL, NULL, N'3/4/18 12:55PM Other stuff')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (52, 50, 7, 23, NULL, NULL, CAST(N'2018-03-04T13:09:00' AS SmallDateTime), N'User input', NULL, NULL, N'3/4/18 1:08 PM Other')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (53, 50, 1, 2, NULL, NULL, CAST(N'2018-03-04T13:15:00' AS SmallDateTime), N'User input', NULL, NULL, NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (54, 50, 7, 23, NULL, NULL, CAST(N'2018-03-04T13:16:00' AS SmallDateTime), N'User input', CAST(N'2018-03-04T14:44:00' AS SmallDateTime), N'User update', N'TTTTTTTTTTTTTTTTTTTTTTTTTTT')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (55, 50, 7, 23, NULL, NULL, CAST(N'2018-03-04T13:30:00' AS SmallDateTime), N'User input', NULL, NULL, N'3/4/18 1:30 PM Other')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (56, 50, 7, 23, NULL, NULL, CAST(N'2018-03-04T13:31:00' AS SmallDateTime), N'User input', CAST(N'2018-03-04T14:41:00' AS SmallDateTime), N'User update', N'3/4/18 1:32 PM OtherRRR')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (57, 50, 7, 23, NULL, NULL, CAST(N'2018-03-04T13:32:00' AS SmallDateTime), N'User input', CAST(N'2018-03-04T14:42:00' AS SmallDateTime), N'User update', N'QQQQQQQQQQQQQQQQQQQQQQQQQQQQ')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (58, 50, 2, 7, NULL, NULL, CAST(N'2018-03-04T14:45:00' AS SmallDateTime), N'User input', CAST(N'2018-03-04T14:46:00' AS SmallDateTime), N'User update', NULL)
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (59, 51, 7, 23, NULL, NULL, CAST(N'2018-03-05T20:16:00' AS SmallDateTime), N'User input', CAST(N'2018-03-06T09:03:00' AS SmallDateTime), N'User update', N'Other than Other')
INSERT [dbo].[tBarrier] ([BarrierID], [FK_EncounterID], [BarrierType], [BarrierAssessed], [FollowUpDt], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Other]) VALUES (60, 52, 7, 23, NULL, NULL, CAST(N'2018-03-06T17:08:00' AS SmallDateTime), N'User input', CAST(N'2018-03-06T17:27:00' AS SmallDateTime), N'User update', N'kjfkjkjcskjsfd')
SET IDENTITY_INSERT [dbo].[tBarrier] OFF
SET IDENTITY_INSERT [dbo].[tCaregiverApprovals] ON 

INSERT [dbo].[tCaregiverApprovals] ([ApprovalID], [FK_NSRecipientID], [CaregiverApproval], [CaregiverApprovalOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (1, 2, 74, N' ', NULL, CAST(N'2017-10-10T08:26:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tCaregiverApprovals] ([ApprovalID], [FK_NSRecipientID], [CaregiverApproval], [CaregiverApprovalOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (26, 8, 72, N' ', NULL, CAST(N'2017-11-15T15:26:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tCaregiverApprovals] ([ApprovalID], [FK_NSRecipientID], [CaregiverApproval], [CaregiverApprovalOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (27, 8, 73, N' ', NULL, CAST(N'2017-11-15T15:26:00' AS SmallDateTime), N'User Input', NULL, NULL)
SET IDENTITY_INSERT [dbo].[tCaregiverApprovals] OFF
SET IDENTITY_INSERT [dbo].[tCollaboratorContribution] ON 

INSERT [dbo].[tCollaboratorContribution] ([CollabContributionID], [CollabContribution]) VALUES (1, N'Recruitment')
INSERT [dbo].[tCollaboratorContribution] ([CollabContributionID], [CollabContribution]) VALUES (2, N'Food')
INSERT [dbo].[tCollaboratorContribution] ([CollabContributionID], [CollabContribution]) VALUES (3, N'Incentives')
INSERT [dbo].[tCollaboratorContribution] ([CollabContributionID], [CollabContribution]) VALUES (4, N'Transportation')
INSERT [dbo].[tCollaboratorContribution] ([CollabContributionID], [CollabContribution]) VALUES (5, N'Childcare')
INSERT [dbo].[tCollaboratorContribution] ([CollabContributionID], [CollabContribution]) VALUES (6, N'Interpretation/Translation')
INSERT [dbo].[tCollaboratorContribution] ([CollabContributionID], [CollabContribution]) VALUES (7, N'Facility')
INSERT [dbo].[tCollaboratorContribution] ([CollabContributionID], [CollabContribution]) VALUES (8, N'Co-facilitated	')
INSERT [dbo].[tCollaboratorContribution] ([CollabContributionID], [CollabContribution]) VALUES (9, N'Other')
SET IDENTITY_INSERT [dbo].[tCollaboratorContribution] OFF
SET IDENTITY_INSERT [dbo].[tDiscussionTopic] ON 

INSERT [dbo].[tDiscussionTopic] ([DiscussionID], [Discussion]) VALUES (1, N'Full curriculum')
INSERT [dbo].[tDiscussionTopic] ([DiscussionID], [Discussion]) VALUES (2, N'Short curriculum')
INSERT [dbo].[tDiscussionTopic] ([DiscussionID], [Discussion]) VALUES (3, N'Tailored curriculum')
INSERT [dbo].[tDiscussionTopic] ([DiscussionID], [Discussion]) VALUES (4, N'Eligibility')
INSERT [dbo].[tDiscussionTopic] ([DiscussionID], [Discussion]) VALUES (5, N'Other')
SET IDENTITY_INSERT [dbo].[tDiscussionTopic] OFF
SET IDENTITY_INSERT [dbo].[tEncounter] ON 

INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (1, 5000, 1, 1, CAST(N'2017-10-16T00:00:00' AS SmallDateTime), N'2:46 PM', NULL, 2, 0, 0, NULL, NULL, CAST(N'2017-10-25T00:00:00' AS SmallDateTime), N'9.00 AM', 1, 0, NULL, NULL, NULL, NULL, CAST(N'2017-10-16T14:47:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (2, 5000, 8, 1, CAST(N'2017-11-09T00:00:00' AS SmallDateTime), N'1:56 PM', N'3.00 PM', 1, 0, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-11-09T13:57:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (3, 5000, 8, 2, CAST(N'2017-11-09T00:00:00' AS SmallDateTime), N'4:27 PM', N'5.00 PM', 2, 2, NULL, 1, N'Test', CAST(N'2017-11-15T00:00:00' AS SmallDateTime), N'9.00 AM', 1, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-11-09T16:29:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (4, 5000, 8, 3, CAST(N'2017-11-09T00:00:00' AS SmallDateTime), N'6:00 PM', N'7.00 PM', 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-11-09T18:01:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (5, 5000, 8, 4, CAST(N'2017-11-16T00:00:00' AS SmallDateTime), N'2:43 PM', NULL, 1, 1, NULL, NULL, NULL, CAST(N'2017-11-22T00:00:00' AS SmallDateTime), N'9.00 AM', 1, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-11-16T14:45:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (6, 5001, 11, 1, CAST(N'2018-02-26T00:00:00' AS SmallDateTime), N'2:30 PM', N'3:01 PM', 4, 0, 0, NULL, N'done', CAST(N'2018-03-02T00:00:00' AS SmallDateTime), N'3:00 PM', 4, 0, NULL, NULL, N'noted', NULL, CAST(N'2018-02-26T14:32:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (7, 5001, 11, 2, CAST(N'2018-02-27T00:00:00' AS SmallDateTime), N'4:29 PM', N'6:00 PM', 4, 0, 0, NULL, NULL, CAST(N'2018-03-05T00:00:00' AS SmallDateTime), N'10:00 AM', 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-02-27T16:31:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (8, 5001, 11, 3, CAST(N'2018-02-27T00:00:00' AS SmallDateTime), N'4:36 PM', N'6:30 PM', 4, 0, 0, NULL, NULL, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'9:00 AM', 4, 0, NULL, NULL, N'A check of the Note text', NULL, CAST(N'2018-02-27T16:37:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (9, 5001, 11, 4, CAST(N'2018-02-27T00:00:00' AS SmallDateTime), N'4:52 PM', N'6:00 PM', 4, 0, 0, NULL, NULL, CAST(N'2018-03-02T00:00:00' AS SmallDateTime), N'10:00 AM', 4, 0, NULL, NULL, N' A note test', NULL, CAST(N'2018-02-27T16:54:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (10, 5001, 11, 5, CAST(N'2018-02-27T00:00:00' AS SmallDateTime), N'5:01 PM', NULL, 4, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-02-27T17:02:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (11, 5001, 11, 6, CAST(N'2018-02-28T00:00:00' AS SmallDateTime), N'12:27 PM', N'1:00 PM', 4, 0, 0, NULL, NULL, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'2:00 PM', 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-02-28T12:30:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (12, 5001, 11, 7, CAST(N'2018-02-28T00:00:00' AS SmallDateTime), N'1:01 PM', NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-02-28T13:03:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (13, 5001, 11, 8, CAST(N'2018-02-28T00:00:00' AS SmallDateTime), N'2:30 PM', N'4:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-02T00:00:00' AS SmallDateTime), NULL, 2, 0, NULL, NULL, NULL, NULL, CAST(N'2018-02-28T14:31:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (14, 5001, 11, 9, CAST(N'2018-02-28T00:00:00' AS SmallDateTime), N'2:42 PM', NULL, 1, 0, 0, NULL, NULL, CAST(N'2018-03-02T00:00:00' AS SmallDateTime), NULL, 2, 0, NULL, NULL, N'motes', NULL, CAST(N'2018-02-28T14:43:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (15, 5001, 11, 8, CAST(N'2018-02-28T00:00:00' AS SmallDateTime), N'2:47 PM', N'4:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-05T00:00:00' AS SmallDateTime), N'3:00PM', 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-02-28T14:49:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (16, 5001, 12, 1, CAST(N'2018-03-01T00:00:00' AS SmallDateTime), N'9:17 AM', N'10:00 AM', 2, 0, 0, NULL, NULL, CAST(N'2018-03-02T00:00:00' AS SmallDateTime), N'9:00 AM', 1, 0, NULL, NULL, N'Noted Jack', NULL, CAST(N'2018-03-01T09:19:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (17, 5001, 12, 2, CAST(N'2018-03-01T00:00:00' AS SmallDateTime), N'9:26 AM', N'10:00 AM', 3, 0, 0, NULL, NULL, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'11:00 AM', 1, 0, NULL, NULL, N'enc 2 notes', NULL, CAST(N'2018-03-01T09:29:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (18, 5001, 12, 3, CAST(N'2018-03-01T00:00:00' AS SmallDateTime), N'9:35 AM', N'12: 00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-04T00:00:00' AS SmallDateTime), N'8:00 AM', 2, 0, NULL, NULL, N'enc 3 MORE NOTES ', NULL, CAST(N'2018-03-01T09:37:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (19, 5001, 12, 4, CAST(N'2018-03-01T00:00:00' AS SmallDateTime), N'9:50 AM', N'11:30 AM', 2, 0, 0, NULL, NULL, CAST(N'2018-03-05T00:00:00' AS SmallDateTime), N'9:00 AM', 1, 0, NULL, NULL, N'ENC 4', NULL, CAST(N'2018-03-01T09:52:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (20, 5001, 12, 5, CAST(N'2018-03-01T00:00:00' AS SmallDateTime), N'10:00 AM', N'11:00 AM', 3, 0, 0, NULL, NULL, CAST(N'2018-03-06T00:00:00' AS SmallDateTime), N'8:00 AM', 4, 0, NULL, NULL, N'ENC 5 notes', NULL, CAST(N'2018-03-01T10:02:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (21, 5001, 12, 6, CAST(N'2018-03-01T00:00:00' AS SmallDateTime), N'10:02 AM', N'11:00 AM', 4, 0, 0, NULL, NULL, CAST(N'2018-03-07T00:00:00' AS SmallDateTime), N'8:30 AM', 1, 0, NULL, NULL, N'ENC 6 Notes', NULL, CAST(N'2018-03-01T10:03:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (22, 5001, 12, 7, CAST(N'2018-03-01T00:00:00' AS SmallDateTime), N'10:07 AM', N'10:30 AM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-02T00:00:00' AS SmallDateTime), N'7:00 AM', 4, 0, NULL, NULL, N'ENC 7 note', NULL, CAST(N'2018-03-01T10:09:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (23, 5001, 11, 9, CAST(N'2018-03-01T00:00:00' AS SmallDateTime), N'12:36 PM', N'1:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-02T00:00:00' AS SmallDateTime), N'2:00 PM', 3, 0, NULL, NULL, N'ENC 10 Note', NULL, CAST(N'2018-03-01T12:37:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (24, 5001, 12, 8, CAST(N'2018-03-01T00:00:00' AS SmallDateTime), N'1:08 PM', N'2:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-04T00:00:00' AS SmallDateTime), N'9:00 AM', 1, 0, NULL, NULL, N'ENC 8 Notes', NULL, CAST(N'2018-03-01T13:09:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (25, 5001, 11, 2, CAST(N'2018-03-02T00:00:00' AS SmallDateTime), N'7:57 PM', N'9:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-05T00:00:00' AS SmallDateTime), N'10:00 AM', 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-02T19:59:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (26, 5001, 11, 2, CAST(N'2018-03-02T00:00:00' AS SmallDateTime), N'8:02 PM', N'9:00 PM', 3, 0, 0, NULL, NULL, CAST(N'2018-03-05T00:00:00' AS SmallDateTime), N'11:00 AM', 4, 0, NULL, NULL, N'Note 3/2/18 8:03 PM', NULL, CAST(N'2018-03-02T20:04:00' AS SmallDateTime), N'User Input', CAST(N'2018-03-02T20:07:00' AS SmallDateTime), N'User Update', NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (27, 5001, 11, 3, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'11:46 AM', N'1:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-07T00:00:00' AS SmallDateTime), N'9:00 AM', 4, 0, NULL, NULL, N'ENC 10 3/3/18 Note text', NULL, CAST(N'2018-03-03T11:48:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (28, 5001, 11, 3, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'1:20 PM', N'3:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-05T00:00:00' AS SmallDateTime), N'8:00 AM', 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T13:23:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (29, 5001, 11, 5, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'2:08 PM', N'3:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-09T00:00:00' AS SmallDateTime), N'2:00 PM', NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T14:11:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (30, 5001, 11, 8, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'2:18 PM', N'3:30 PM', 4, 0, 0, NULL, NULL, CAST(N'2018-03-07T00:00:00' AS SmallDateTime), N'8:30 AM', 1, 0, NULL, NULL, N'ENC 8 note 3/3 2:18PM', NULL, CAST(N'2018-03-03T14:21:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (31, 5001, 11, 3, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'3:07 PM', N'4:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-08T00:00:00' AS SmallDateTime), N'1:00 PM', 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T15:09:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (32, 5001, 11, 7, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'3:09 PM', N'5:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-07T00:00:00' AS SmallDateTime), N'9:00 AM', 3, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T15:11:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (33, 5001, 11, 3, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'3:26 PM', NULL, 2, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T15:28:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (34, 5001, 11, 2, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'3:27 PM', NULL, 2, 0, 0, NULL, NULL, CAST(N'2018-03-07T00:00:00' AS SmallDateTime), NULL, 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T15:28:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (35, 5001, 11, 2, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'3:34 PM', NULL, 1, 0, 0, NULL, NULL, CAST(N'2018-03-07T00:00:00' AS SmallDateTime), NULL, 3, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T15:35:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (36, 5001, 11, 2, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'3:40 PM', N'4:00 PM', 3, 0, 0, NULL, NULL, CAST(N'2018-03-05T00:00:00' AS SmallDateTime), NULL, 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T15:42:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (37, 5001, 11, 2, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'3:45 PM', N'4:00 PM', 4, 0, 0, NULL, NULL, CAST(N'2018-03-04T00:00:00' AS SmallDateTime), NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T15:46:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (38, 5001, 11, 4, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'3:52 PM', N'4:00 PM', 2, 0, 0, NULL, NULL, CAST(N'2018-03-08T00:00:00' AS SmallDateTime), NULL, 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T15:53:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (39, 5001, 11, 7, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'4:02 PM', N'5:00PM', 2, 0, 0, NULL, NULL, CAST(N'2018-03-07T00:00:00' AS SmallDateTime), NULL, 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T16:04:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (40, 5001, 11, 3, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'4:09 PM', NULL, 4, 0, 0, NULL, NULL, CAST(N'2018-03-07T00:00:00' AS SmallDateTime), NULL, 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T16:10:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (41, 5001, 11, 3, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'4:18 PM', NULL, 1, 0, 0, NULL, NULL, CAST(N'2018-03-08T00:00:00' AS SmallDateTime), NULL, 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T16:20:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (42, 5001, 12, 3, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'4:33 PM', NULL, 1, 0, 0, NULL, NULL, NULL, NULL, 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T16:34:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (43, 5001, 12, 9, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'4:40 PM', N'5:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-08T00:00:00' AS SmallDateTime), NULL, 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T16:42:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (44, 5001, 12, 2, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'4:53 PM', NULL, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T16:54:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (45, 5001, 12, 7, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'5:02 PM', NULL, 4, 0, 0, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T17:03:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (46, 5001, 12, 6, CAST(N'2018-03-03T00:00:00' AS SmallDateTime), N'5:11 PM', NULL, 1, 0, 0, NULL, NULL, NULL, NULL, 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-03T17:12:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (47, 5001, 11, 5, CAST(N'2018-03-04T00:00:00' AS SmallDateTime), N'9:34 AM', N'10:00 AM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-08T00:00:00' AS SmallDateTime), N'9:00 AM', 4, 0, NULL, NULL, N'NNNNNNNNNNNNNN', NULL, CAST(N'2018-03-04T09:36:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (48, 5001, 11, 7, CAST(N'2018-03-04T00:00:00' AS SmallDateTime), N'9:39 AM', N'11:00 AM', 2, 0, 0, NULL, NULL, CAST(N'2018-03-06T00:00:00' AS SmallDateTime), N'8:00 AM', 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04T09:42:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (49, 5001, 11, 3, CAST(N'2018-03-04T00:00:00' AS SmallDateTime), N'9:45 AM', N'10:30 AM', 4, 0, 0, NULL, NULL, CAST(N'2018-03-08T00:00:00' AS SmallDateTime), N'7:00 AM', 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04T09:48:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (50, 5001, 13, 1, CAST(N'2018-03-04T00:00:00' AS SmallDateTime), N'12:53 PM', N'2:00 PM', 1, 0, 0, NULL, NULL, CAST(N'2018-03-08T00:00:00' AS SmallDateTime), N'10:00 AM', 4, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04T12:55:00' AS SmallDateTime), N'User Input', CAST(N'2018-03-04T14:45:00' AS SmallDateTime), N'User Update', NULL)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (51, 5001, 18, 1, CAST(N'2018-03-05T00:00:00' AS SmallDateTime), N'8:14 PM', N'9:00 PM', 4, 0, 0, NULL, N'Done', CAST(N'2018-03-10T00:00:00' AS SmallDateTime), N'9:00 AM', 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-05T20:16:00' AS SmallDateTime), N'User Input', CAST(N'2018-03-05T20:27:00' AS SmallDateTime), N'User Update', 2)
INSERT [dbo].[tEncounter] ([EncounterID], [FK_NavigatorID], [FK_RecipientID], [EncounterNumber], [EncounterDt], [EncounterStartTime], [EncounterEndTime], [EncounterType], [EncounterReason], [MissedAppointment], [SvcOutcomeStatus], [SvcOutcomeStatusReason], [NextAppointmentDt], [NextAppointmentTime], [NextAppointmentType], [ServicesTerminated], [ServicesTerminatedDt], [ServicesTerminatedReason], [Notes], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EncounterPurpose]) VALUES (52, 5001, 13, 2, CAST(N'2018-03-06T00:00:00' AS SmallDateTime), N'5:06 PM', NULL, 1, 0, 0, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-06T17:08:00' AS SmallDateTime), N'User Input', NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[tEncounter] OFF
SET IDENTITY_INSERT [dbo].[tHecActivities] ON 

INSERT [dbo].[tHecActivities] ([ActivityId], [ActivityCancelled], [ReasonForCancellation], [ActivityDate], [ActivityType], [OtherActivityType], [NameOrPurpose], [CollaboratorId1], [CollaboratorId2], [CollaboratorContributionId1], [CollaboratorContributionId2], [Address1], [Address2], [City], [Zip], [CHW1], [CHW2], [OtherAttendee1], [OtherAttendee2], [Population], [LanguageId], [Discussed], [OtherDiscussed], [PrePostTest], [Result], [OtherResult], [MyRole], [OtherMyRole], [Attendee], [ConfirmAttendance], [AnnoucementDoc], [Travel], [Notes]) VALUES (1, 0, NULL, CAST(N'2018-05-01T00:00:00.000' AS DateTime), 0, NULL, NULL, 1, 0, 2, 0, N'house number 1', N'house number 1', N'kannur', N'63235', 1, 0, NULL, NULL, 1, 2, 1, NULL, N'1', 1, NULL, 1, NULL, 1, NULL, N'Announcement Doc', 0, NULL)
INSERT [dbo].[tHecActivities] ([ActivityId], [ActivityCancelled], [ReasonForCancellation], [ActivityDate], [ActivityType], [OtherActivityType], [NameOrPurpose], [CollaboratorId1], [CollaboratorId2], [CollaboratorContributionId1], [CollaboratorContributionId2], [Address1], [Address2], [City], [Zip], [CHW1], [CHW2], [OtherAttendee1], [OtherAttendee2], [Population], [LanguageId], [Discussed], [OtherDiscussed], [PrePostTest], [Result], [OtherResult], [MyRole], [OtherMyRole], [Attendee], [ConfirmAttendance], [AnnoucementDoc], [Travel], [Notes]) VALUES (2, 0, NULL, CAST(N'2018-05-01T00:00:00.000' AS DateTime), 0, NULL, NULL, 1, 0, 2, 0, N'house number 1', N'house number 1', N'kannur', N'63235', 1, 0, NULL, NULL, 1, 2, 1, NULL, N'1', 1, NULL, 1, NULL, 1, NULL, N'Announcement Doc', 0, NULL)
INSERT [dbo].[tHecActivities] ([ActivityId], [ActivityCancelled], [ReasonForCancellation], [ActivityDate], [ActivityType], [OtherActivityType], [NameOrPurpose], [CollaboratorId1], [CollaboratorId2], [CollaboratorContributionId1], [CollaboratorContributionId2], [Address1], [Address2], [City], [Zip], [CHW1], [CHW2], [OtherAttendee1], [OtherAttendee2], [Population], [LanguageId], [Discussed], [OtherDiscussed], [PrePostTest], [Result], [OtherResult], [MyRole], [OtherMyRole], [Attendee], [ConfirmAttendance], [AnnoucementDoc], [Travel], [Notes]) VALUES (3, 0, NULL, CAST(N'2018-05-01T00:00:00.000' AS DateTime), 0, NULL, NULL, 1, 0, 2, 0, N'house number 1', N'house number 1', N'ernakulam', N'21564', 1, 0, NULL, NULL, 1, 2, 1, NULL, N'1', 1, NULL, 1, NULL, 1, NULL, N'Announcement Doc', 0, NULL)
INSERT [dbo].[tHecActivities] ([ActivityId], [ActivityCancelled], [ReasonForCancellation], [ActivityDate], [ActivityType], [OtherActivityType], [NameOrPurpose], [CollaboratorId1], [CollaboratorId2], [CollaboratorContributionId1], [CollaboratorContributionId2], [Address1], [Address2], [City], [Zip], [CHW1], [CHW2], [OtherAttendee1], [OtherAttendee2], [Population], [LanguageId], [Discussed], [OtherDiscussed], [PrePostTest], [Result], [OtherResult], [MyRole], [OtherMyRole], [Attendee], [ConfirmAttendance], [AnnoucementDoc], [Travel], [Notes]) VALUES (4, 0, NULL, CAST(N'2018-05-24T06:55:16.000' AS DateTime), 0, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0, NULL, N'0', 0, NULL, 0, NULL, 1, NULL, N'Announcement Doc', 0, NULL)
SET IDENTITY_INSERT [dbo].[tHecActivities] OFF
INSERT [dbo].[tHecActivityParticipantInfo] ([ActivityId], [FirstName], [LastName], [Month], [Year], [RaceOrEthinicity], [LanguageId], [Address1], [Address2], [City], [Zip], [Telephone], [Type], [Email], [MAM], [PAP], [HI], [OkToContact], [LastMAM], [LastPAP], [EWCEligible]) VALUES (1, N'rithin', N'sajith', 11, 1992, 0, 2, N'housenumber1', NULL, N'kannur', N'67895', N'2547896541', NULL, N'a@a.com', 0, NULL, 1, NULL, 1, NULL, 0)
INSERT [dbo].[tHecActivityParticipantInfo] ([ActivityId], [FirstName], [LastName], [Month], [Year], [RaceOrEthinicity], [LanguageId], [Address1], [Address2], [City], [Zip], [Telephone], [Type], [Email], [MAM], [PAP], [HI], [OkToContact], [LastMAM], [LastPAP], [EWCEligible]) VALUES (2, N'rithin', N'sajith', 11, 1992, 0, 2, N'housenumber1', NULL, N'kannur', N'67895', N'2547896541', NULL, N'a@a.com', 0, NULL, 1, NULL, 1, NULL, 0)
INSERT [dbo].[tHecActivityParticipantInfo] ([ActivityId], [FirstName], [LastName], [Month], [Year], [RaceOrEthinicity], [LanguageId], [Address1], [Address2], [City], [Zip], [Telephone], [Type], [Email], [MAM], [PAP], [HI], [OkToContact], [LastMAM], [LastPAP], [EWCEligible]) VALUES (3, N'rithin', N'sajith', 11, 1992, 0, 2, N'housenumber1', NULL, N'kannur', N'67895', N'2547896541', NULL, N'a@a.com', 0, NULL, 1, NULL, 1, NULL, 0)
INSERT [dbo].[tHecActivityParticipantInfo] ([ActivityId], [FirstName], [LastName], [Month], [Year], [RaceOrEthinicity], [LanguageId], [Address1], [Address2], [City], [Zip], [Telephone], [Type], [Email], [MAM], [PAP], [HI], [OkToContact], [LastMAM], [LastPAP], [EWCEligible]) VALUES (4, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tHecScreeningEvent] ([ActivityId], [BusinessName], [NPI], [ProviderFirstName], [ProviderLastName], [EwcProvider]) VALUES (1, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[tHecScreeningEvent] ([ActivityId], [BusinessName], [NPI], [ProviderFirstName], [ProviderLastName], [EwcProvider]) VALUES (2, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[tHecScreeningEvent] ([ActivityId], [BusinessName], [NPI], [ProviderFirstName], [ProviderLastName], [EwcProvider]) VALUES (3, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[tHecScreeningEvent] ([ActivityId], [BusinessName], [NPI], [ProviderFirstName], [ProviderLastName], [EwcProvider]) VALUES (4, NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[tInstantMessage] ON 

INSERT [dbo].[tInstantMessage] ([Id], [Subject], [Message], [ReceivedOn], [ReceivedFrom], [ReceivedBy], [IsRead], [ParentId]) VALUES (1, N'test 1 messahes', N'asdasdsadas', CAST(N'2018-03-18T08:23:45.917' AS DateTime), 5002, 5000, 1, NULL)
INSERT [dbo].[tInstantMessage] ([Id], [Subject], [Message], [ReceivedOn], [ReceivedFrom], [ReceivedBy], [IsRead], [ParentId]) VALUES (2, N'Re:test 1 messahes', N'asdasdsadas', CAST(N'2018-03-18T08:24:02.817' AS DateTime), 5002, 5002, 1, 1)
INSERT [dbo].[tInstantMessage] ([Id], [Subject], [Message], [ReceivedOn], [ReceivedFrom], [ReceivedBy], [IsRead], [ParentId]) VALUES (3, N'test', N'test', CAST(N'2018-03-18T08:39:13.037' AS DateTime), 5002, 5002, 1, NULL)
INSERT [dbo].[tInstantMessage] ([Id], [Subject], [Message], [ReceivedOn], [ReceivedFrom], [ReceivedBy], [IsRead], [ParentId]) VALUES (4, N'test', N'test', CAST(N'2018-03-18T08:39:37.840' AS DateTime), 5002, 5000, 0, NULL)
INSERT [dbo].[tInstantMessage] ([Id], [Subject], [Message], [ReceivedOn], [ReceivedFrom], [ReceivedBy], [IsRead], [ParentId]) VALUES (5, N'test message', N'test message test message test message', CAST(N'2018-03-24T12:14:08.740' AS DateTime), 5002, 5002, 1, NULL)
INSERT [dbo].[tInstantMessage] ([Id], [Subject], [Message], [ReceivedOn], [ReceivedFrom], [ReceivedBy], [IsRead], [ParentId]) VALUES (6, N'Re:test message', N'test message test message test message


reply', CAST(N'2018-03-24T12:15:44.380' AS DateTime), 5002, 5002, 1, 5)
INSERT [dbo].[tInstantMessage] ([Id], [Subject], [Message], [ReceivedOn], [ReceivedFrom], [ReceivedBy], [IsRead], [ParentId]) VALUES (7, N'Sample Instant Message', N'It was first released in January 2002 with version 1.0 of the .NET Framework, and is the successor to Microsoft''s Active Server Pages (ASP) technology. ASP.NET is built on the Common Language Runtime (CLR), allowing programmers to write ASP.NET code using any supported .NET language. The ASP.NET SOAP extension framework allows ASP.NET components to process SOAP messages', CAST(N'2018-04-10T00:00:54.883' AS DateTime), 5002, 5002, 1, NULL)
SET IDENTITY_INSERT [dbo].[tInstantMessage] OFF
SET IDENTITY_INSERT [dbo].[tMyRole] ON 

INSERT [dbo].[tMyRole] ([RoleID], [Role]) VALUES (1, N'Participant/Observer')
INSERT [dbo].[tMyRole] ([RoleID], [Role]) VALUES (2, N'Presenter/Teacher')
INSERT [dbo].[tMyRole] ([RoleID], [Role]) VALUES (3, N'Organizer')
INSERT [dbo].[tMyRole] ([RoleID], [Role]) VALUES (4, N'Other	')
SET IDENTITY_INSERT [dbo].[tMyRole] OFF
SET IDENTITY_INSERT [dbo].[tNavigationCycle] ON 

INSERT [dbo].[tNavigationCycle] ([NavigationCycleID], [FK_RecipientID], [FK_ProviderID], [FK_ReferralID], [CancerSite], [NSProblem], [HealthProgram], [HealthInsuranceStatus], [HealthInsurancePlan], [HealthInsurancePlanNumber], [ContactPrefDays], [ContactPrefHours], [ContactPrefHoursOther], [ContactPrefType], [SRIndentifierCodeGenerated], [FK_NavigatorID], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (1, 2, 1, NULL, 1, 1, 1, 0, NULL, NULL, N' ', N' ', NULL, NULL, NULL, 5000, NULL, CAST(N'2017-10-10T08:26:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigationCycle] ([NavigationCycleID], [FK_RecipientID], [FK_ProviderID], [FK_ReferralID], [CancerSite], [NSProblem], [HealthProgram], [HealthInsuranceStatus], [HealthInsurancePlan], [HealthInsurancePlanNumber], [ContactPrefDays], [ContactPrefHours], [ContactPrefHoursOther], [ContactPrefType], [SRIndentifierCodeGenerated], [FK_NavigatorID], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (2, 4, NULL, NULL, 1, 1, 1, 0, NULL, NULL, N' ', N' ', NULL, NULL, NULL, 5000, NULL, CAST(N'2017-10-17T11:41:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigationCycle] ([NavigationCycleID], [FK_RecipientID], [FK_ProviderID], [FK_ReferralID], [CancerSite], [NSProblem], [HealthProgram], [HealthInsuranceStatus], [HealthInsurancePlan], [HealthInsurancePlanNumber], [ContactPrefDays], [ContactPrefHours], [ContactPrefHoursOther], [ContactPrefType], [SRIndentifierCodeGenerated], [FK_NavigatorID], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (5, 8, 2, NULL, 1, 1, 1, 0, 1, N'123456', N'2,4', N'4', NULL, 1, NULL, 5000, NULL, CAST(N'2017-10-25T14:07:00' AS SmallDateTime), N'User Input', CAST(N'2017-11-15T15:27:00' AS SmallDateTime), N'User Update')
INSERT [dbo].[tNavigationCycle] ([NavigationCycleID], [FK_RecipientID], [FK_ProviderID], [FK_ReferralID], [CancerSite], [NSProblem], [HealthProgram], [HealthInsuranceStatus], [HealthInsurancePlan], [HealthInsurancePlanNumber], [ContactPrefDays], [ContactPrefHours], [ContactPrefHoursOther], [ContactPrefType], [SRIndentifierCodeGenerated], [FK_NavigatorID], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (6, 9, NULL, NULL, 1, 1, 1, 0, NULL, NULL, N' ', N' ', NULL, NULL, NULL, 5000, NULL, CAST(N'2018-01-10T14:40:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigationCycle] ([NavigationCycleID], [FK_RecipientID], [FK_ProviderID], [FK_ReferralID], [CancerSite], [NSProblem], [HealthProgram], [HealthInsuranceStatus], [HealthInsurancePlan], [HealthInsurancePlanNumber], [ContactPrefDays], [ContactPrefHours], [ContactPrefHoursOther], [ContactPrefType], [SRIndentifierCodeGenerated], [FK_NavigatorID], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (7, 10, NULL, NULL, 1, 1, 1, 0, NULL, NULL, N' ', N' ', NULL, NULL, NULL, 5000, NULL, CAST(N'2018-01-10T14:55:00' AS SmallDateTime), N'User Input', NULL, NULL)
SET IDENTITY_INSERT [dbo].[tNavigationCycle] OFF
SET IDENTITY_INSERT [dbo].[tNavigator] ON 

INSERT [dbo].[tNavigator] ([NavigatorID], [DomainName], [Type], [LastName], [FirstName], [Address1], [Address2], [City], [State], [Zip], [BusinessTelephone], [MobileTelephone], [Email], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Region]) VALUES (5000, N'DHSINTRA\bbuchake', 1, N'Buchake', N'Bilwa', N'664 Cygnus Ln', NULL, N'Foster City', N'CA', N'94404', N'9167868942', NULL, N'bilwa.buchake@dhcs.ca.gov', NULL, CAST(N'2017-09-18T13:26:00' AS SmallDateTime), N'User Input', CAST(N'2017-10-11T17:01:00' AS SmallDateTime), N'User Input', 4)
INSERT [dbo].[tNavigator] ([NavigatorID], [DomainName], [Type], [LastName], [FirstName], [Address1], [Address2], [City], [State], [Zip], [BusinessTelephone], [MobileTelephone], [Email], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Region]) VALUES (5001, N'DHSINTRA\grichey', 1, N'richey', N'gordon', N'1501 Capitol Ave', NULL, N'sacramento', N'CA', N'95818', N'9163417342', NULL, N'grichey@dhcs.ca.gov', NULL, CAST(N'2018-02-22T14:59:00' AS SmallDateTime), N'User Input', NULL, NULL, 5)
INSERT [dbo].[tNavigator] ([NavigatorID], [DomainName], [Type], [LastName], [FirstName], [Address1], [Address2], [City], [State], [Zip], [BusinessTelephone], [MobileTelephone], [Email], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [Region]) VALUES (5002, N'USER1\User1', 1, N'Mani', N'Retna', N'1501 Capitol Ave', NULL, N'sacramento', N'CA', N'95818', N'9163417342', NULL, N'grichey@dhcs.ca.gov', NULL, CAST(N'2018-02-22T14:59:00' AS SmallDateTime), N'User Input', NULL, NULL, 5)
SET IDENTITY_INSERT [dbo].[tNavigator] OFF
SET IDENTITY_INSERT [dbo].[tNavigatorLanguages] ON 

INSERT [dbo].[tNavigatorLanguages] ([NavigatorLanguageID], [FK_NavigatorID], [LanguageCode], [SpeakingScore], [ListeningScore], [ReadingScore], [WritingScore], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (3, 5000, 28, 3, 3, 3, 3, NULL, CAST(N'2017-09-27T15:58:00' AS SmallDateTime), N'User Input', CAST(N'2017-10-10T16:39:00' AS SmallDateTime), N'User Update')
INSERT [dbo].[tNavigatorLanguages] ([NavigatorLanguageID], [FK_NavigatorID], [LanguageCode], [SpeakingScore], [ListeningScore], [ReadingScore], [WritingScore], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (4, 5000, 14, 3, 3, 4, 2, NULL, CAST(N'2017-10-09T16:07:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigatorLanguages] ([NavigatorLanguageID], [FK_NavigatorID], [LanguageCode], [SpeakingScore], [ListeningScore], [ReadingScore], [WritingScore], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (5, 5000, 13, 2, 3, 3, 2, NULL, CAST(N'2017-10-10T15:12:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigatorLanguages] ([NavigatorLanguageID], [FK_NavigatorID], [LanguageCode], [SpeakingScore], [ListeningScore], [ReadingScore], [WritingScore], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (6, 5000, 7, 2, 2, 2, 2, NULL, CAST(N'2017-10-10T16:40:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigatorLanguages] ([NavigatorLanguageID], [FK_NavigatorID], [LanguageCode], [SpeakingScore], [ListeningScore], [ReadingScore], [WritingScore], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (8, 5000, 20, 2, 2, 2, 2, NULL, CAST(N'2017-10-16T14:35:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigatorLanguages] ([NavigatorLanguageID], [FK_NavigatorID], [LanguageCode], [SpeakingScore], [ListeningScore], [ReadingScore], [WritingScore], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (9, 5000, 17, 2, 2, 2, 2, NULL, CAST(N'2017-10-16T14:38:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigatorLanguages] ([NavigatorLanguageID], [FK_NavigatorID], [LanguageCode], [SpeakingScore], [ListeningScore], [ReadingScore], [WritingScore], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (10, 0, 1, 5, 4, 4, 3, NULL, CAST(N'2018-03-29T06:41:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tNavigatorLanguages] ([NavigatorLanguageID], [FK_NavigatorID], [LanguageCode], [SpeakingScore], [ListeningScore], [ReadingScore], [WritingScore], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (11, 0, 3, 5, 5, 5, 3, NULL, CAST(N'2018-04-19T00:15:00' AS SmallDateTime), N'User Input', NULL, NULL)
SET IDENTITY_INSERT [dbo].[tNavigatorLanguages] OFF
SET IDENTITY_INSERT [dbo].[tNavigatorTraining] ON 

INSERT [dbo].[tNavigatorTraining] ([TrainingID], [FK_NavigatorID], [CourseName], [Organization], [CompletionDt], [Certificate], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [BeginDt], [CMEAwarded]) VALUES (1, 5000, N'ABC Course', N'ABC Org', CAST(N'2017-09-07T00:00:00' AS SmallDateTime), 1, NULL, CAST(N'2017-09-27T16:01:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tNavigatorTraining] ([TrainingID], [FK_NavigatorID], [CourseName], [Organization], [CompletionDt], [Certificate], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [BeginDt], [CMEAwarded]) VALUES (2, 5000, N'XYZ Course', N'XYZ Org', CAST(N'2017-10-01T00:00:00' AS SmallDateTime), 1, NULL, CAST(N'2017-10-10T15:13:00' AS SmallDateTime), N'User Input', CAST(N'2017-10-10T16:40:00' AS SmallDateTime), N'User Update', NULL, NULL)
INSERT [dbo].[tNavigatorTraining] ([TrainingID], [FK_NavigatorID], [CourseName], [Organization], [CompletionDt], [Certificate], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [BeginDt], [CMEAwarded]) VALUES (3, 5000, N'LMN Course', N'LMN Org', CAST(N'2017-10-02T00:00:00' AS SmallDateTime), 0, NULL, CAST(N'2017-10-10T16:41:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tNavigatorTraining] ([TrainingID], [FK_NavigatorID], [CourseName], [Organization], [CompletionDt], [Certificate], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [BeginDt], [CMEAwarded]) VALUES (4, 5000, N'ABC Course', N'ABC Org', CAST(N'2017-10-16T00:00:00' AS SmallDateTime), 0, NULL, CAST(N'2017-10-16T14:38:00' AS SmallDateTime), N'User Input', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tNavigatorTraining] OFF
SET IDENTITY_INSERT [dbo].[tNSProvider] ON 

INSERT [dbo].[tNSProvider] ([NSProviderID], [PCPName], [PCP_NPI], [PCP_Owner], [PCP_Location], [PCPAddress1], [PCPAddress2], [PCPCity], [PCPState], [PCPZip], [PCPContactFName], [PCPContactLName], [PCPContactTitle], [PCPContactTelephone], [PCPContactEmail], [Medical], [MedicalSpecialty], [MedicalSpecialtyOther], [ManagedCarePlan], [ManagedCarePlanOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EWCPCP]) VALUES (1, N'EWC', N'0099211928', N'01', N'001', N'1501 Capitol Ave', NULL, N'Sacramento', N'CA', N'95814', N'Bilwa', N'Buchake', N'SPA', N'(916) 786-', N'bilwa.buchake@dhcs.ca.gov', 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-10-10T08:26:00' AS SmallDateTime), N'User Input', NULL, NULL, 0)
INSERT [dbo].[tNSProvider] ([NSProviderID], [PCPName], [PCP_NPI], [PCP_Owner], [PCP_Location], [PCPAddress1], [PCPAddress2], [PCPCity], [PCPState], [PCPZip], [PCPContactFName], [PCPContactLName], [PCPContactTitle], [PCPContactTelephone], [PCPContactEmail], [Medical], [MedicalSpecialty], [MedicalSpecialtyOther], [ManagedCarePlan], [ManagedCarePlanOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [EWCPCP]) VALUES (2, N'EWCPCP', N'0099211928', N'01', N'001', N'1500 Capitol Ave', N'1111', N'Sacramento', N'CA', N'95814', N'TestContact', N'TestLContact', N'ABC', N'3456789087', N'contacttest@dhcs.ca.gov', 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-10-30T13:45:00' AS SmallDateTime), N'User Input', CAST(N'2017-11-15T15:26:00' AS SmallDateTime), N'User Update', 1)
SET IDENTITY_INSERT [dbo].[tNSProvider] OFF
SET IDENTITY_INSERT [dbo].[tPartnersCollaborators] ON 

INSERT [dbo].[tPartnersCollaborators] ([PartnersCollaboratorsId], [NameofOrg1], [NameOfOrg2], [Abbreviation]) VALUES (1, N'Avenxo', N'Avenxo labs', N'Ave')
INSERT [dbo].[tPartnersCollaborators] ([PartnersCollaboratorsId], [NameofOrg1], [NameOfOrg2], [Abbreviation]) VALUES (2, N'Google', N'Google', N'Goo')
SET IDENTITY_INSERT [dbo].[tPartnersCollaborators] OFF
SET IDENTITY_INSERT [dbo].[tRace] ON 

INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (2, 2, 0, N' ', NULL, CAST(N'2017-10-10T08:26:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (3, 3, 0, N' ', NULL, CAST(N'2017-10-17T11:20:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (4, 4, 0, N' ', NULL, CAST(N'2017-10-17T11:41:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (5, 5, 1, N' ', NULL, CAST(N'2017-10-19T16:05:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (6, 6, 0, N' ', NULL, CAST(N'2017-10-24T10:02:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (7, 7, 0, N' ', NULL, CAST(N'2017-10-24T10:11:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (36, 8, 3, N' ', NULL, CAST(N'2017-11-15T15:26:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (37, 9, 1, N' ', NULL, CAST(N'2018-01-10T14:40:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (38, 10, 1, N' ', NULL, CAST(N'2018-01-10T14:54:00' AS SmallDateTime), N'User Input', NULL, NULL)
INSERT [dbo].[tRace] ([RaceID], [FK_NSRecipientID], [RaceCode], [RaceOther], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (39, 14, 6, N' ', NULL, CAST(N'2018-03-04T15:18:00' AS SmallDateTime), N'User Input', NULL, NULL)
SET IDENTITY_INSERT [dbo].[tRace] OFF
INSERT [dbo].[trAssistanceCommunication] ([CommunicationCode], [CommunicationDescription]) VALUES (1, N'Phone Call')
INSERT [dbo].[trAssistanceCommunication] ([CommunicationCode], [CommunicationDescription]) VALUES (2, N'Face-to-Face')
INSERT [dbo].[trAssistanceCommunication] ([CommunicationCode], [CommunicationDescription]) VALUES (3, N'Translate')
INSERT [dbo].[trAssistanceCommunication] ([CommunicationCode], [CommunicationDescription]) VALUES (4, N'Email')
INSERT [dbo].[trAssistanceCommunication] ([CommunicationCode], [CommunicationDescription]) VALUES (5, N'Other')
INSERT [dbo].[trAssistanceEWCEnrollee] ([EWCEnrolleeCode], [EWCEnrolleeDescription]) VALUES (1, N'EWC Program Information')
INSERT [dbo].[trAssistanceEWCEnrollee] ([EWCEnrolleeCode], [EWCEnrolleeDescription]) VALUES (2, N'Courtesy Enrollment procedure question')
INSERT [dbo].[trAssistanceEWCEnrollee] ([EWCEnrolleeCode], [EWCEnrolleeDescription]) VALUES (3, N'Request Navigation (e.g., BCCTP or other)')
INSERT [dbo].[trAssistanceEWCEnrollee] ([EWCEnrolleeCode], [EWCEnrolleeDescription]) VALUES (4, N'Invitation to meeting or conference')
INSERT [dbo].[trAssistanceEWCEnrollee] ([EWCEnrolleeCode], [EWCEnrolleeDescription]) VALUES (5, N'Other or Notes for Selected Problem/Issue')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (1, N'Recipient Issue')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (2, N'Billing Issue')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (3, N'DETEC or data entry issue')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (4, N'Clarification of Program or Clinical Issue')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (5, N'Request for Case Management Navigation')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (6, N'PCP Enrollment/Disenrollment, Addition/Removal of unregistered or Satellite Facility')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (7, N'Other or Notes for Selected Problem/Issue')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (21, N'How to bill')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (22, N'Claims denial')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (31, N'Training request, DETEC')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (32, N'Error Remediation')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (41, N'Covered benefits')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (42, N'Program policy')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (43, N'Training Request, CPPI')
INSERT [dbo].[trAssistanceEWCPCP] ([EWCPCPCode], [EWCPCPDescription]) VALUES (44, N'Training Request, Clinical Algorithm')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (1, N'Contacting an EWC provider, (i,e., phone number or wrong address; no longer in EWC)')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (2, N'Obtaining an appointment with an EWC provider (i,e., provider not taking new clients or not taking EWC clients)')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (3, N'Billing issue i,e., recipient has been billed for services')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (4, N'Access to a specialist or getting a follow-up appointment with a specialist')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (5, N'Provider or staff behavior (i,e., inappropriate provider and staff behavior)')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (6, N'Program eligibility/enrollment question')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (7, N'Request for Navigation')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (8, N'Other or Notes for Selected Problem/Issue')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (61, N'EWC')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (62, N'BCCTP')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (63, N'Medi Cal')
INSERT [dbo].[trAssistanceEWCRecipient] ([EWCRecipientCode], [EWCRecipientDescription]) VALUES (64, N'Other')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (1, N'Recipient Issue')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (2, N'Billing Issue')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (3, N'Clarification of program or clinical issue')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (4, N'Request for Navigation')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (5, N'Other or Notes for Selected Problem/Issue')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (21, N'How to bill')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (22, N'Claims denial')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (31, N'Covered benefits')
INSERT [dbo].[trAssistanceReferralProvider] ([ReferralProviderCode], [ReferralProviderDescription]) VALUES (32, N'Program policy')
INSERT [dbo].[trAssistanceRequestor] ([RequestorCode], [RequestorDescription]) VALUES (1, N'EWC Recipient or designated representative')
INSERT [dbo].[trAssistanceRequestor] ([RequestorCode], [RequestorDescription]) VALUES (2, N'EWC PCP')
INSERT [dbo].[trAssistanceRequestor] ([RequestorCode], [RequestorDescription]) VALUES (3, N'Referral Provider')
INSERT [dbo].[trAssistanceRequestor] ([RequestorCode], [RequestorDescription]) VALUES (4, N'Program or Community Partner, Public, Potential EWC Enrollees')
INSERT [dbo].[trAssistanceRequestor] ([RequestorCode], [RequestorDescription]) VALUES (5, N'Other')
INSERT [dbo].[trAssistanceResolution] ([ResolutionCode], [ResolutionDescription]) VALUES (1, N'Answered question')
INSERT [dbo].[trAssistanceResolution] ([ResolutionCode], [ResolutionDescription]) VALUES (2, N'Accepted Navigation referral')
INSERT [dbo].[trAssistanceResolution] ([ResolutionCode], [ResolutionDescription]) VALUES (3, N'Sent informtion/made referral, specify')
INSERT [dbo].[trAssistanceResolution] ([ResolutionCode], [ResolutionDescription]) VALUES (4, N'PCP/Referral provider TA')
INSERT [dbo].[trAssistanceResolution] ([ResolutionCode], [ResolutionDescription]) VALUES (5, N'PCP/Referral provider referred to QAP online or Live Training')
INSERT [dbo].[trAssistanceResolution] ([ResolutionCode], [ResolutionDescription]) VALUES (6, N'PCP Training')
INSERT [dbo].[trAssistanceResolution] ([ResolutionCode], [ResolutionDescription]) VALUES (7, N'Unable to resolve, why')
INSERT [dbo].[trAssistanceResolution] ([ResolutionCode], [ResolutionDescription]) VALUES (8, N'Other')
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (1, N'No car/access to or can’t afford public transportation', 1)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (2, N'Conflict between PCP and work schedules; can’t take time off work', 1)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (3, N'Lack of coordination between services or service locations', 1)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (4, N'Needs child or elder-care', 1)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (5, N'Disabled', 1)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (6, N'Difficulty speaking or reading English', 2)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (7, N'Cultural beliefs inconsistent with exam, test, procedure or treatment', 2)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (8, N'Difficulty communicating needs or desires to PCP or staff', 2)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (9, N'No health insurance', 3)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (10, N'Can’t afford co-pay /share-of-cost/ deductible', 3)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (11, N'Health not a priority', 4)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (12, N'Confused/doesn’t understand', 4)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (13, N'Difficulty completing forms; gathering additional information', 4)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (14, N'Problem contacting PCP (i.e., phone number or address incorrect)', 5)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (15, N'Problem making appointment (i.e., not accepting new/program clients)', 5)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (16, N'Billing issue (i.e., client billed for services)', 5)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (17, N'Problem, access or follow-up appointment with specialist', 5)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (18, N'Problem, PCP or staff behavior', 5)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (19, N'Program eligibility question', 5)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (20, N'Avoidance or refusal of care due to fear or stress', 6)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (21, N'Lack of family or social support', 6)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (22, N'Unstable living situation / homeless', 6)
INSERT [dbo].[trBarrier] ([BarrierCode], [BarrierDescription], [BarrierTypeCode]) VALUES (23, N'Other', 7)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (1, N'Find alternative PCP', 1)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (2, N'Discuss/identity public/personal transportation options', 1)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (3, N'Discuss options for child/elder care; or identify PCP with appropriate service', 1)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (4, N'Assist coordinating appointments/travel; or arrival of records/information', 1)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (5, N'Coach on how to or communicate with PCP about special needs', 1)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (6, N'Refer to culturally competent/linguistically appropriate PCP or with translation service', 2)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (7, N'Coach on how to or communicate with PCP about special needs', 2)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (8, N'If ineligible for Covered California, identify appropriate safety net program', 3)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (9, N'Educate - cancer, healthcare system, test, procedures, diagnoses and/or treatments', 3)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (10, N'Refer to culturally competent/linguistically appropriate PCP or with translation service', 4)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (11, N'Assist in completing forms; explain additional information needed', 4)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (12, N'Find correct phone number/address or PCP accepting clients, then report to EWC, PSU', 4)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (13, N'Investigate, billing or behavior issue, then report to EWC, CSU', 5)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (14, N'Assess eligibility', 5)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (15, N'Educate - cancer, healthcare system, test, procedures, diagnoses and/or treatments', 5)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (16, N'Refer to support groups or counseling services; or appropriate authority', 6)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (17, N'Other', 6)
INSERT [dbo].[trBarrierSolution] ([BarrierSolutionCode], [BarrierSolutionDescription], [BarrierTypeCode]) VALUES (18, N'Other Solution', 7)
INSERT [dbo].[trBarrierType] ([BarrierTypeCode], [BarrierTypeDescription]) VALUES (1, N'Transportation and Logistics')
INSERT [dbo].[trBarrierType] ([BarrierTypeCode], [BarrierTypeDescription]) VALUES (2, N'Language, Culture and Literacy')
INSERT [dbo].[trBarrierType] ([BarrierTypeCode], [BarrierTypeDescription]) VALUES (3, N'Financial')
INSERT [dbo].[trBarrierType] ([BarrierTypeCode], [BarrierTypeDescription]) VALUES (4, N'Health Literacy or Education')
INSERT [dbo].[trBarrierType] ([BarrierTypeCode], [BarrierTypeDescription]) VALUES (5, N'PCP or Referral PCP')
INSERT [dbo].[trBarrierType] ([BarrierTypeCode], [BarrierTypeDescription]) VALUES (6, N'Social Support or Distress')
INSERT [dbo].[trBarrierType] ([BarrierTypeCode], [BarrierTypeDescription]) VALUES (7, N'Other')
INSERT [dbo].[trCancerSite] ([CancerSiteCode], [CancerSiteName]) VALUES (1, N'Breast Cancer')
INSERT [dbo].[trCancerSite] ([CancerSiteCode], [CancerSiteName]) VALUES (2, N'Cervical Cancer')
INSERT [dbo].[trCancerSite] ([CancerSiteCode], [CancerSiteName]) VALUES (3, N'Breast and Cervical')
INSERT [dbo].[trCancerSite] ([CancerSiteCode], [CancerSiteName]) VALUES (4, N'Other')
INSERT [dbo].[trCaregiverApproval] ([CaregiverApprovalCode], [CaregiverApprovalDescription]) VALUES (71, N'Translate')
INSERT [dbo].[trCaregiverApproval] ([CaregiverApprovalCode], [CaregiverApprovalDescription]) VALUES (72, N'Make appointments')
INSERT [dbo].[trCaregiverApproval] ([CaregiverApprovalCode], [CaregiverApprovalDescription]) VALUES (73, N'Take messages')
INSERT [dbo].[trCaregiverApproval] ([CaregiverApprovalCode], [CaregiverApprovalDescription]) VALUES (74, N'Co-decision maker')
INSERT [dbo].[trCaregiverApproval] ([CaregiverApprovalCode], [CaregiverApprovalDescription]) VALUES (75, N'Other')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (1, N'Afghanistan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (2, N'Aland Islands (Finland)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (3, N'Albania')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (4, N'Algeria')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (5, N'American Samoa (USA)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (6, N'Andorra')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (7, N'Angola')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (8, N'Anguilla (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (9, N'Antigua and Barbuda')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (10, N'Argentina')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (11, N'Armenia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (12, N'Aruba (Netherlands)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (13, N'Australia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (14, N'Austria')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (15, N'Azerbaijan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (16, N'Bahamas')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (17, N'Bahrain')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (18, N'Bangladesh')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (19, N'Barbados')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (20, N'Belarus')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (21, N'Belgium')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (22, N'Belize')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (23, N'Benin')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (24, N'Bermuda (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (25, N'Bhutan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (26, N'Bolivia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (27, N'Bosnia and Herzegovina')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (28, N'Botswana')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (29, N'Brazil')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (30, N'British Virgin Islands (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (31, N'Brunei')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (32, N'Bulgaria')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (33, N'Burkina Faso')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (34, N'Burma')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (35, N'Burundi')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (36, N'Cambodia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (37, N'Cameroon')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (38, N'Canada')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (39, N'Cape Verde')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (40, N'Caribbean Netherlands (Netherlands)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (41, N'Cayman Islands (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (42, N'Central African Republic')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (43, N'Chad')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (44, N'Chile')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (45, N'China')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (46, N'Christmas Island (Australia)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (47, N'Cocos (Keeling) Islands (Australia)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (48, N'Colombia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (49, N'Comoros')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (50, N'Cook Islands (NZ)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (51, N'Costa Rica')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (52, N'Croatia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (53, N'Cuba')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (54, N'Curacao (Netherlands)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (55, N'Cyprus')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (56, N'Czech Republic')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (57, N'Democratic Republic of the Congo')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (58, N'Denmark')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (59, N'Djibouti')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (60, N'Dominica')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (61, N'Dominican Republic')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (62, N'Ecuador')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (63, N'Egypt')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (64, N'El Salvador')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (65, N'Equatorial Guinea')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (66, N'Eritrea')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (67, N'Estonia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (68, N'Ethiopia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (69, N'Falkland Islands (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (70, N'Faroe Islands (Denmark)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (71, N'Federated States of Micronesia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (72, N'Fiji')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (73, N'Finland')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (74, N'France')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (75, N'French Guiana (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (76, N'French Polynesia (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (77, N'Gabon')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (78, N'Gambia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (79, N'Georgia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (80, N'Germany')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (81, N'Ghana')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (82, N'Gibraltar (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (83, N'Greece')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (84, N'Greenland (Denmark)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (85, N'Grenada')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (86, N'Guadeloupe (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (87, N'Guam (USA)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (88, N'Guatemala')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (89, N'Guernsey (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (90, N'Guinea')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (91, N'Guinea-Bissau')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (92, N'Guyana')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (93, N'Haiti')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (94, N'Honduras')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (95, N'Hong Kong (China)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (96, N'Hungary')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (97, N'Iceland')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (98, N'India')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (99, N'Indonesia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (100, N'Iran')
GO
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (101, N'Iraq')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (102, N'Ireland')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (103, N'Isle of Man (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (104, N'Israel')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (105, N'Italy')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (106, N'Ivory Coast')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (107, N'Jamaica')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (108, N'Japan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (109, N'Jersey (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (110, N'Jordan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (111, N'Kazakhstan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (112, N'Kenya')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (113, N'Kiribati')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (114, N'Kosovo')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (115, N'Kuwait')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (116, N'Kyrgyzstan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (117, N'Laos')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (118, N'Latvia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (119, N'Lebanon')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (120, N'Lesotho')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (121, N'Liberia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (122, N'Libya')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (123, N'Liechtenstein')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (124, N'Lithuania')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (125, N'Luxembourg')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (126, N'Macau (China)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (127, N'Macedonia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (128, N'Madagascar')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (129, N'Malawi')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (130, N'Malaysia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (131, N'Maldives')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (132, N'Mali')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (133, N'Malta')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (134, N'Marshall Islands')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (135, N'Martinique (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (136, N'Mauritania')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (137, N'Mauritius')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (138, N'Mayotte (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (139, N'Mexico')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (140, N'Moldov')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (141, N'Monaco')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (142, N'Mongolia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (143, N'Montenegro')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (144, N'Montserrat (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (145, N'Morocco')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (146, N'Mozambique')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (147, N'Namibia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (148, N'Nauru')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (149, N'Nepal')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (150, N'Netherlands')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (151, N'New Caledonia (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (152, N'New Zealand')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (153, N'Nicaragua')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (154, N'Niger')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (155, N'Nigeria')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (156, N'Niue (NZ)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (157, N'Norfolk Island (Australia)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (158, N'North Korea')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (159, N'Northern Mariana Islands (USA)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (160, N'Norway')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (161, N'Oman')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (162, N'Pakistan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (163, N'Palau')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (164, N'Palestine')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (165, N'Panama')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (166, N'Papua New Guinea')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (167, N'Paraguay')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (168, N'Peru')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (169, N'Philippines')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (170, N'Pitcairn Islands (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (171, N'Poland')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (172, N'Portugal')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (173, N'Puerto Rico')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (174, N'Qatar')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (175, N'Republic of the Congo')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (176, N'Reunion (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (177, N'Romania')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (178, N'Russia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (179, N'Rwanda')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (180, N'Saint Barthelemy (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (181, N'Saint Helena, Ascension and Tristan da Cunha (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (182, N'Saint Kitts and Nevis')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (183, N'Saint Lucia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (184, N'Saint Martin (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (185, N'Saint Pierre and Miquelon (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (186, N'Saint Vincent and the Grenadines')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (187, N'Samoa')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (188, N'San Marino')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (189, N'Sao Tom and Principe')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (190, N'Saudi Arabia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (191, N'Senegal')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (192, N'Serbia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (193, N'Seychelles')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (194, N'Sierra Leone')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (195, N'Singapore')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (196, N'Sint Maarten (Netherlands)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (197, N'Slovakia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (198, N'Slovenia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (199, N'Solomon Islands')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (200, N'Somalia')
GO
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (201, N'South Africa')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (202, N'South Korea')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (203, N'South Sudan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (204, N'Spain')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (205, N'Sri Lanka')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (206, N'Sudan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (207, N'Suriname')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (208, N'Svalbard and Jan Mayen (Norway)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (209, N'Swaziland')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (210, N'Sweden')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (211, N'Switzerland')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (212, N'Syria')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (213, N'Taiwan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (214, N'Tajikistan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (215, N'Tanzania')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (216, N'Thailand')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (217, N'Timor-Leste')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (218, N'Togo')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (219, N'Tokelau (NZ)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (220, N'Tonga')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (221, N'Trinidad and Tobago')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (222, N'Tunisia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (223, N'Turkey')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (224, N'Turkmenistan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (225, N'Turks and Caicos Islands (UK)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (226, N'Tuvalu')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (227, N'Uganda')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (228, N'Ukraine')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (229, N'United Arab Emirates')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (230, N'United Kingdom')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (231, N'United States')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (232, N'United States Virgin Islands (USA)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (233, N'Uruguay')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (234, N'Uzbekistan')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (235, N'Vanuatu')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (236, N'Vatican City')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (237, N'Venezuela')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (238, N'Vietnam')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (239, N'Wallis and Futuna (France)')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (240, N'Western Sahara')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (241, N'Yemen')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (242, N'Zambia')
INSERT [dbo].[trCountries] ([CountryCode], [CountryName]) VALUES (243, N'Zimbabwe')
INSERT [dbo].[trEncounterPurpose] ([EncounterPurposeCode], [EncounterPurposeDescription]) VALUES (1, N'Assessment')
INSERT [dbo].[trEncounterPurpose] ([EncounterPurposeCode], [EncounterPurposeDescription]) VALUES (2, N'Check-in')
SET IDENTITY_INSERT [dbo].[tResults] ON 

INSERT [dbo].[tResults] ([ResultID], [Result]) VALUES (1, N'Materials distributed')
INSERT [dbo].[tResults] ([ResultID], [Result]) VALUES (2, N'Referral')
INSERT [dbo].[tResults] ([ResultID], [Result]) VALUES (3, N'Future activity planned')
INSERT [dbo].[tResults] ([ResultID], [Result]) VALUES (4, N'Other')
SET IDENTITY_INSERT [dbo].[tResults] OFF
INSERT [dbo].[trHealthInsurance] ([HealthInsuranceCode], [HealthInsuranceName]) VALUES (1, N'Medi-Cal')
INSERT [dbo].[trHealthInsurance] ([HealthInsuranceCode], [HealthInsuranceName]) VALUES (2, N'Adventist Health Plan')
INSERT [dbo].[trHealthInsurance] ([HealthInsuranceCode], [HealthInsuranceName]) VALUES (3, N'Aetna Health of California, Inc.')
INSERT [dbo].[trHealthInsurance] ([HealthInsuranceCode], [HealthInsuranceName]) VALUES (4, N'Kaiser Permanente Hospitals of California - San luis Obispo Town Center Medical Clinic')
INSERT [dbo].[trHealthProgram] ([HealthProgramCode], [HealthProgramName]) VALUES (1, N'EWC')
INSERT [dbo].[trHealthProgram] ([HealthProgramCode], [HealthProgramName]) VALUES (2, N'BCCTP')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (11, N'Recipient has specific or complex medical issue, including previous diagnosis of breast and/or cervical cancer')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (12, N'Recipient has complex insurance/EWC billing issues')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (13, N'Recipient expressed difficulty with PCP/staff behavior')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (14, N'CC is a more appropriate language/social/cultural match')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (21, N'Recipient requires only basic health, breast and/or cervical cancer education for navigation')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (22, N'HE is a more appropriate language/social/cultural match')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (31, N'Recipient moves to other region and wishes to change Navigator')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (32, N'Other region has more appropriate or specific resources')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (33, N'HE/CC is a more appropriate language/social/cultural match')
INSERT [dbo].[trNavigatorTransferReason] ([NavigatorTransferReasonCode], [NavigatorTransferReason]) VALUES (34, N'Region has reached caseload maximum or has strained staffing')
INSERT [dbo].[trNSProblem] ([NSProblemCode], [NSProblemDescription]) VALUES (1, N'Screening')
INSERT [dbo].[trNSProblem] ([NSProblemCode], [NSProblemDescription]) VALUES (2, N'Case Management')
INSERT [dbo].[trNSProblem] ([NSProblemCode], [NSProblemDescription]) VALUES (3, N'Treatment Referral')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (1, N'Asian Languages')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (2, N'Cambodian                     ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (3, N'Cantonese                     ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (5, N'Farsi                         ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (7, N'French                        ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (8, N'German                        ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (9, N'Hmong                         ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (10, N'Italian                       ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (11, N'Korean                        ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (13, N'Punjabi                       ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (14, N'Spanish                       ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (15, N'Tagalog                       ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (16, N'Vietnamese                    ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (17, N'Yiddish                       ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (18, N'Hebrew                        ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (20, N'Russian                       ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (21, N'Japanese                      ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (22, N'Mandarin                      ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (24, N'American Sign                 ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (25, N'Arabic                        ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (26, N'Armenian                      ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (28, N'Hindi                         ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (31, N'Portuguese                    ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (32, N'Thai                          ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (33, N'Urdu                          ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (34, N'Greek                         ')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (35, N'Afrikaans')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (36, N'Albanian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (37, N'Aymara')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (38, N'Bengali')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (39, N'Bulgarian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (40, N'Cakchiquel')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (41, N'Catalan')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (42, N'Cebuano')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (43, N'Croatian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (44, N'Czech')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (45, N'Danish')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (46, N'Dutch')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (47, N'Egyptian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (48, N'Esperanto')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (49, N'Finnish')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (50, N'Galego')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (51, N'Haitian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (52, N'Hausa')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (53, N'Hungarian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (54, N'Icelandic')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (55, N'Indonesian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (56, N'Irish')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (57, N'Khmer')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (58, N'Lao')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (59, N'Latin')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (60, N'Latvian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (61, N'Malay')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (62, N'Maori')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (63, N'Marathi')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (64, N'Mixteco')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (65, N'Navajo')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (66, N'Nepali')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (67, N'Norwegian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (68, N'Polish')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (69, N'Rarotongan')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (70, N'Romanian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (71, N'Samoan')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (72, N'Serbo-Croatian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (73, N'Slovak')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (74, N'Somali')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (75, N'Swahili')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (76, N'Swedish')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (77, N'Tahitian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (78, N'Tamil')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (79, N'Tibetan')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (80, N'Tongan')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (81, N'Turkish')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (82, N'Ukrainian')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (83, N'Welsh')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (84, N'Translation Service')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (85, N'Burmese')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (86, N'Kanjobal')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (87, N'Lau')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (88, N'Mien')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (89, N'Sinhalese')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (90, N'Sudanese')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (91, N'Taiwanese')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (92, N'Teo-Chow')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (93, N'Triki')
INSERT [dbo].[trProvLanguages] ([LanguageCode], [LanguageName]) VALUES (99, N'English')
INSERT [dbo].[trRace] ([RaceCode], [RaceName]) VALUES (1, N'White')
INSERT [dbo].[trRace] ([RaceCode], [RaceName]) VALUES (2, N'African American')
INSERT [dbo].[trRace] ([RaceCode], [RaceName]) VALUES (3, N'Asian')
INSERT [dbo].[trRace] ([RaceCode], [RaceName]) VALUES (4, N'Pacific Islander')
INSERT [dbo].[trRace] ([RaceCode], [RaceName]) VALUES (5, N'American Indian/Alaskan Native')
INSERT [dbo].[trRace] ([RaceCode], [RaceName]) VALUES (6, N'Other')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (11, N'Asian Indian')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (12, N'Cambodian')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (13, N'Chinese')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (14, N'Filipino')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (15, N'Hmong')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (16, N'Japanese')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (17, N'Korean')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (18, N'Laotian')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (19, N'Vietnamese')
INSERT [dbo].[trRaceAsian] ([RaceAsianCode], [RaceAsianName]) VALUES (20, N'Other Asian')
INSERT [dbo].[trRacePacIslander] ([RacePacIslanderCode], [RacePacIslanderName]) VALUES (21, N'Native Hawaiian')
INSERT [dbo].[trRacePacIslander] ([RacePacIslanderCode], [RacePacIslanderName]) VALUES (22, N'Guamanian or Chamorro')
INSERT [dbo].[trRacePacIslander] ([RacePacIslanderCode], [RacePacIslanderName]) VALUES (23, N'Samoan')
INSERT [dbo].[trRacePacIslander] ([RacePacIslanderCode], [RacePacIslanderName]) VALUES (24, N'Other Pacific Islander')
SET IDENTITY_INSERT [dbo].[tScreeningNav] ON 

INSERT [dbo].[tScreeningNav] ([SN_ID], [FK_NavigatorID], [SN_CODE1], [SN_CODE2], [LastName], [FirstName], [DOB], [Address1], [Address2], [City], [State], [Zip], [HomeTelephone], [Cellphone], [Email], [Computer], [TextMessage], [DateOfContact1], [ApptScreen1], [SvcDate1], [SvcType1], [Response1], [DateOfContact2], [ApptScreen2], [SvcDate2], [SvcType2], [Response2], [NSEnrollment], [EnrollmentDate], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (2, 5000, N'04BB111317A', 1, N'Kopti', N'Mary', CAST(N'1960-09-01T00:00:00' AS SmallDateTime), NULL, NULL, NULL, NULL, NULL, NULL, N'1234567890', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-11-13T13:50:00' AS SmallDateTime), N'User Input', CAST(N'2017-11-13T16:32:00' AS SmallDateTime), N'User Input')
INSERT [dbo].[tScreeningNav] ([SN_ID], [FK_NavigatorID], [SN_CODE1], [SN_CODE2], [LastName], [FirstName], [DOB], [Address1], [Address2], [City], [State], [Zip], [HomeTelephone], [Cellphone], [Email], [Computer], [TextMessage], [DateOfContact1], [ApptScreen1], [SvcDate1], [SvcType1], [Response1], [DateOfContact2], [ApptScreen2], [SvcDate2], [SvcType2], [Response2], [NSEnrollment], [EnrollmentDate], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy]) VALUES (3, 5000, N'04BB111317B', 1, N'Herg', N'Jessica', CAST(N'1956-10-01T00:00:00' AS SmallDateTime), N'123 First Street', N'Suite B', N'Sacramento', N'CA', N'95814', N'0987654321', N'1234567890', N'jess@gmail.com', 1, N'U', CAST(N'2017-10-10T00:00:00' AS SmallDateTime), N'A', CAST(N'2017-10-15T00:00:00' AS SmallDateTime), N'C ', N'NO', CAST(N'2017-10-12T00:00:00' AS SmallDateTime), N'S', CAST(N'2017-10-15T00:00:00' AS SmallDateTime), N'C ', N'NR', N'Y', CAST(N'2017-10-01T00:00:00' AS SmallDateTime), NULL, CAST(N'2017-11-13T21:03:00' AS SmallDateTime), N'User Input', NULL, NULL)
SET IDENTITY_INSERT [dbo].[tScreeningNav] OFF
SET IDENTITY_INSERT [dbo].[tServiceRecipient] ON 

INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (1, 5000, NULL, N'Conway', N'Alexandra', NULL, CAST(N'1950-06-01T00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, NULL, NULL, N'1234567890', 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, NULL, NULL, 99, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, CAST(N'2017-10-09T14:44:00' AS SmallDateTime), N'User input', CAST(N'2017-10-10T16:41:00' AS SmallDateTime), N'User update', NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (2, 5000, NULL, N'Conway', N'Alexandra', NULL, CAST(N'1960-06-01T00:00:00' AS SmallDateTime), NULL, NULL, NULL, NULL, NULL, NULL, N'1234567890', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 99, 0, N'Bill Conway', NULL, NULL, N'3456789876', 2, NULL, N'bill.conway@gmail.com', NULL, NULL, NULL, CAST(N'2017-10-10T08:26:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (3, 5000, NULL, N'TestLast', N'Test', NULL, CAST(N'1960-10-11T00:00:00' AS SmallDateTime), NULL, NULL, NULL, NULL, NULL, NULL, N'1234567890', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 99, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-10-17T11:20:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (4, 5000, NULL, N'TestLast', N'Test', NULL, CAST(N'1960-10-11T00:00:00' AS SmallDateTime), NULL, NULL, NULL, NULL, NULL, NULL, N'1234567890', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 99, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-10-17T11:41:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (5, 5000, NULL, N'TestLast', N'TestA', NULL, CAST(N'1960-10-17T00:00:00' AS SmallDateTime), NULL, NULL, NULL, NULL, NULL, NULL, N'1234567890', 1, 1, N'          ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 99, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-10-19T16:05:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (8, 5000, N'129A1234567890', N'Necessary', N'Minimum', N'R', CAST(N'1960-10-10T00:00:00' AS SmallDateTime), NULL, N'123 First St', N'B', N'San Mateo', N'CA', N'94404', N'1234567890', 2, 1, N'5678905678', 1, NULL, 1, NULL, N'min.necessary@gmail.com', NULL, 1, 98, NULL, 0, 99, 1, N'Contact Name', 1, NULL, N'1234567890', 2, 1, N'contact@gmail.com', 4, N'</br>This is a new comment.
This is a new comment and it is a long one.
This is one more new comment


', NULL, CAST(N'2017-10-25T14:07:00' AS SmallDateTime), N'User input', CAST(N'2017-11-15T15:26:00' AS SmallDateTime), N'User update', NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (9, 5000, NULL, N'Necessary', N'Minimum', NULL, CAST(N'1960-01-01T00:00:00' AS SmallDateTime), 0, N'123 First Street', NULL, N'Sacramento', N'CA', N'95814', N'1234567890', 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 11, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-01-10T14:40:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (10, 5000, NULL, N'ABC', N'XYZ', NULL, CAST(N'1960-01-01T00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'1234567890', 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 7, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-01-10T14:54:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (11, 5001, N'6777777       ', N'doe', N'jane', NULL, CAST(N'1977-07-07T00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'4156969788', 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 14, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-02-22T15:06:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (12, 5001, NULL, N'doe', N'jack', NULL, CAST(N'1999-12-12T00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'6555829797', 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 14, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-02-22T15:10:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (13, 5001, N'9912399       ', N'turner', N'Ted', NULL, CAST(N'1992-09-05T00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'9163586585', 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 99, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04T12:51:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (14, 5001, NULL, N'Stone', N'John', NULL, CAST(N'1983-03-05T00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'4159697865', 4, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 231, NULL, NULL, 99, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04T15:18:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (15, 5001, NULL, N'Edwards', N'Don', NULL, CAST(N'1990-06-05T00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'9167894563', 4, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 99, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04T15:27:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (16, 5001, NULL, N'Roberts', N'Bob', NULL, CAST(N'1976-08-02T00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'5667988222', 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 99, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04T15:30:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (17, 5001, NULL, N'Shane', N'Sam', NULL, CAST(N'1993-03-03T00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'4445556666', 4, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 8, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04T15:36:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (18, 5001, NULL, N'James', N'Carl', NULL, CAST(N'1966-06-06T00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'9161234568', 4, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 22, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, CAST(N'2018-03-04T15:49:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tServiceRecipient] ([NSRecipientID], [FK_NavigatorID], [FK_RecipID], [LastName], [FirstName], [MiddleInitial], [DOB], [AddressNotAvailable], [Address1], [Address2], [City], [State], [Zip], [Phone1], [P1PhoneType], [P1PersonalMessage], [Phone2], [P2PhoneType], [P2PhoneTypeOther], [P2PersonalMessage], [MotherMaidenName], [Email], [SSN], [ImmigrantStatus], [CountryOfOrigin], [Gender], [Ethnicity], [PrimaryLanguage], [AgreeToSpeakEnglish], [CaregiverName], [Relationship], [RelationshipOther], [CaregiverPhone], [CaregiverPhoneType], [CaregiverPhonePersonalMessage], [CaregiverEmail], [CaregiverContactPreference], [Comments], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [NavigatorTransferDt], [NavigatorTransferReason], [OldNavigatorID]) VALUES (19, 5001, NULL, N'Bowman', N'Chad', NULL, CAST(N'1978-07-05T00:00:00' AS SmallDateTime), 0, NULL, NULL, NULL, N'CA', NULL, N'9166397452', 4, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, CAST(N'2018-03-05T20:29:00' AS SmallDateTime), N'User input', CAST(N'2018-03-05T20:35:00' AS SmallDateTime), N'User update', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tServiceRecipient] OFF
SET IDENTITY_INSERT [dbo].[tSolution] ON 

INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (2, 1, 8, 1, NULL, CAST(N'2017-10-16T14:48:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2017-10-20T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (3, 1, 9, 1, NULL, CAST(N'2017-10-16T14:48:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2017-10-28T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (4, 6, 13, 1, NULL, CAST(N'2018-02-26T14:32:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-01T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (5, 12, 0, NULL, NULL, CAST(N'2018-02-28T14:43:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (6, 13, 0, NULL, NULL, CAST(N'2018-02-28T14:49:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (7, 14, 0, NULL, NULL, CAST(N'2018-03-01T09:19:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (8, 15, 0, NULL, NULL, CAST(N'2018-03-01T09:29:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (9, 16, 0, NULL, NULL, CAST(N'2018-03-01T09:37:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (10, 17, 0, NULL, NULL, CAST(N'2018-03-01T09:52:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (11, 18, 0, NULL, NULL, CAST(N'2018-03-01T10:02:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (12, 19, 0, NULL, NULL, CAST(N'2018-03-01T10:03:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (13, 20, 0, NULL, NULL, CAST(N'2018-03-01T10:09:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (14, 21, 0, NULL, NULL, CAST(N'2018-03-01T12:37:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (15, 22, 0, NULL, NULL, CAST(N'2018-03-01T13:09:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (16, 23, 0, NULL, NULL, CAST(N'2018-03-02T19:59:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (17, 24, 0, NULL, NULL, CAST(N'2018-03-02T20:04:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (18, 27, 0, NULL, NULL, CAST(N'2018-03-03T11:48:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (19, 28, 0, NULL, NULL, CAST(N'2018-03-03T13:23:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (20, 29, 0, NULL, NULL, CAST(N'2018-03-03T14:12:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (21, 30, 18, 1, NULL, CAST(N'2018-03-03T14:24:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-06T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (22, 31, 6, 1, NULL, CAST(N'2018-03-03T15:09:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-07T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (23, 32, 18, 1, NULL, CAST(N'2018-03-03T15:11:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-05T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (24, 36, 18, 1, NULL, CAST(N'2018-03-03T15:42:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-05T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (25, 37, 18, 1, NULL, CAST(N'2018-03-03T15:46:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-08T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (26, 38, 18, 2, NULL, CAST(N'2018-03-03T15:53:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-18T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (27, 39, 18, 2, NULL, CAST(N'2018-03-03T16:04:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-18T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (28, 40, 18, 2, NULL, CAST(N'2018-03-03T16:10:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-05T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (29, 41, 18, 2, NULL, CAST(N'2018-03-03T16:20:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-06T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (30, 42, 18, 2, NULL, CAST(N'2018-03-03T16:34:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-08T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (31, 43, 18, 1, NULL, CAST(N'2018-03-03T16:42:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-10T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (32, 44, 18, 2, NULL, CAST(N'2018-03-03T16:54:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-09T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (33, 45, 18, 1, NULL, CAST(N'2018-03-03T17:03:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-12T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (34, 46, 18, 2, NULL, CAST(N'2018-03-03T17:12:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-08T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (35, 47, 18, 1, NULL, CAST(N'2018-03-04T09:36:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-10T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (36, 48, 18, 2, NULL, CAST(N'2018-03-04T09:44:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-09T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (37, 49, 18, 1, NULL, CAST(N'2018-03-04T09:48:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-07T00:00:00' AS SmallDateTime), N'3/4/18 9:46  Solution')
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (38, 50, 18, 2, NULL, CAST(N'2018-03-04T12:55:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-05T00:00:00' AS SmallDateTime), N'3/4/18 12:53PM Solution stuff')
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (39, 53, 2, NULL, NULL, CAST(N'2018-03-04T13:15:00' AS SmallDateTime), N'User input', NULL, NULL, NULL, NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (41, 55, 18, 1, NULL, CAST(N'2018-03-04T13:30:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-09T00:00:00' AS SmallDateTime), N'3/4/18 1:30 PM Solution')
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (48, 56, 18, 2, NULL, CAST(N'2018-03-04T14:41:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-08T00:00:00' AS SmallDateTime), N'3/4/18 1:32 PM SolutioNNN')
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (49, 57, 18, 1, NULL, CAST(N'2018-03-04T14:42:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-07T00:00:00' AS SmallDateTime), N'WWWWWWWWWWWWWWWWWWWWWWWWW')
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (51, 54, 18, 2, NULL, CAST(N'2018-03-04T14:44:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-10T00:00:00' AS SmallDateTime), N'3/4/18 1:15 PM Solution stuff')
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (53, 58, 6, 2, NULL, CAST(N'2018-03-04T14:46:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-17T00:00:00' AS SmallDateTime), NULL)
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (54, 7, 18, 1, NULL, CAST(N'2018-03-05T20:14:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-08T00:00:00' AS SmallDateTime), N'plifcsljofcsll;ad vllad')
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (61, 59, 18, 1, NULL, CAST(N'2018-03-06T09:03:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-09T00:00:00' AS SmallDateTime), N'Solution to Solutionz')
INSERT [dbo].[tSolution] ([SolutionID], [FK_BarrierID], [SolutionOffered], [SolutionImplementationStatus], [xtag], [DateCreated], [CreatedBy], [LastUpdated], [UpdatedBy], [FollowUpDt], [Solution]) VALUES (64, 60, 18, 1, NULL, CAST(N'2018-03-06T17:27:00' AS SmallDateTime), N'User input', NULL, NULL, CAST(N'2018-03-05T00:00:00' AS SmallDateTime), N'po[weoiiouwre')
SET IDENTITY_INSERT [dbo].[tSolution] OFF
SET IDENTITY_INSERT [dbo].[tSystemMessage] ON 

INSERT [dbo].[tSystemMessage] ([Id], [Subject], [Message], [ReceivedOn], [ReceivedBy], [IsRead]) VALUES (1, N'Test Sub', N'Meeting Req', CAST(N'2018-03-03T00:00:00.000' AS DateTime), 5002, 1)
SET IDENTITY_INSERT [dbo].[tSystemMessage] OFF
SET IDENTITY_INSERT [dbo].[tUserAccess] ON 

INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1, N'DHSINTRA\bbuchake', CAST(N'2017-09-18T13:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2, N'DHSINTRA\bbuchake', CAST(N'2017-09-18T13:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3, N'DHSINTRA\bbuchake', CAST(N'2017-09-19T10:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4, N'DHSINTRA\bbuchake', CAST(N'2017-09-19T10:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (5, N'DHSINTRA\bbuchake', CAST(N'2017-09-26T10:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (6, N'DHSINTRA\bbuchake', CAST(N'2017-09-27T08:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (7, N'DHSINTRA\bbuchake', CAST(N'2017-09-27T09:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (8, N'DHSINTRA\bbuchake', CAST(N'2017-09-27T15:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (9, N'DHSINTRA\bbuchake', CAST(N'2017-09-27T15:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (10, N'DHSINTRA\bbuchake', CAST(N'2017-09-27T15:54:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (11, N'DHSINTRA\bbuchake', CAST(N'2017-09-27T15:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (12, N'DHSINTRA\bbuchake', CAST(N'2017-09-27T16:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (13, N'DHSINTRA\bbuchake', CAST(N'2017-09-28T17:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (14, N'DHSINTRA\bbuchake', CAST(N'2017-10-02T13:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (15, N'DHSINTRA\bbuchake', CAST(N'2017-10-03T08:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (16, N'DHSINTRA\bbuchake', CAST(N'2017-10-03T09:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (17, N'DHSINTRA\bbuchake', CAST(N'2017-10-03T13:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (18, N'DHSINTRA\bbuchake', CAST(N'2017-10-03T15:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (19, N'DHSINTRA\pchinnap', CAST(N'2017-10-06T10:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (20, N'DHSINTRA\pchinnap', CAST(N'2017-10-06T10:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (21, N'DHSINTRA\bbuchake', CAST(N'2017-10-06T10:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (22, N'DHSINTRA\pchinnap', CAST(N'2017-10-06T10:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (23, N'DHSINTRA\pchinnap', CAST(N'2017-10-06T10:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (24, N'DHSINTRA\bbuchake', CAST(N'2017-10-06T10:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (25, N'DHSINTRA\pchinnap', CAST(N'2017-10-06T11:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (26, N'DHSINTRA\bbuchake', CAST(N'2017-10-06T13:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (27, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T11:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (28, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T14:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (29, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T15:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (30, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T15:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (31, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T15:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (32, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T15:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (33, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T15:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (34, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T15:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (35, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T15:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (36, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T15:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (37, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T15:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (38, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T15:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (39, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T15:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (40, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T15:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (41, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T15:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (42, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T16:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (43, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T16:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (44, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T16:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (45, N'DHSINTRA\bbuchake', CAST(N'2017-10-09T16:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (46, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T08:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (47, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (48, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (49, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (50, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (51, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (52, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (53, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (54, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (55, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (56, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (57, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (58, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (59, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (60, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (61, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (62, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (63, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (64, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (65, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (66, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (67, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (68, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (69, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (70, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T09:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (71, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T10:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (72, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T10:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (73, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T10:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (74, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T10:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (75, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T10:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (76, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T10:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (77, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T10:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (78, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T10:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (79, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T10:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (80, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T10:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (81, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T10:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (82, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T10:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (83, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T10:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (84, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T10:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (85, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T10:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (86, N'DHSINTRA\pchinnap', CAST(N'2017-10-10T11:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (87, N'DHSINTRA\pchinnap', CAST(N'2017-10-10T11:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (88, N'DHSINTRA\pchinnap', CAST(N'2017-10-10T11:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (89, N'DHSINTRA\pchinnap', CAST(N'2017-10-10T11:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (90, N'DHSINTRA\pchinnap', CAST(N'2017-10-10T11:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (91, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T14:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (92, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T14:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (93, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T14:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (94, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T14:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (95, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T14:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (96, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T14:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (97, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T14:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (98, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T14:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (99, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T14:56:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (100, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (101, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (102, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (103, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (104, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (105, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (106, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (107, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (108, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (109, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (110, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (111, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (112, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (113, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (114, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (115, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (116, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (117, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (118, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (119, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (120, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (121, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (122, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (123, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (124, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (125, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (126, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (127, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (128, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (129, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (130, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (131, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (132, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (133, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T15:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (134, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T16:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (135, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T16:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (136, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T16:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (137, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T16:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (138, N'DHSINTRA\bbuchake', CAST(N'2017-10-10T16:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (139, N'DHSINTRA\bbuchake', CAST(N'2017-10-11T14:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (140, N'DHSINTRA\bbuchake', CAST(N'2017-10-11T15:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (141, N'DHSINTRA\bbuchake', CAST(N'2017-10-11T15:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (142, N'DHSINTRA\bbuchake', CAST(N'2017-10-11T16:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (143, N'DHSINTRA\bbuchake', CAST(N'2017-10-11T16:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (144, N'DHSINTRA\bbuchake', CAST(N'2017-10-11T16:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (145, N'DHSINTRA\bbuchake', CAST(N'2017-10-11T16:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (146, N'DHSINTRA\bbuchake', CAST(N'2017-10-11T16:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (147, N'DHSINTRA\bbuchake', CAST(N'2017-10-11T17:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (148, N'DHSINTRA\bbuchake', CAST(N'2017-10-12T10:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (149, N'DHSINTRA\bbuchake', CAST(N'2017-10-12T10:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (150, N'DHSINTRA\bbuchake', CAST(N'2017-10-12T11:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (151, N'DHSINTRA\bbuchake', CAST(N'2017-10-12T15:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (152, N'DHSINTRA\pchinnap', CAST(N'2017-10-13T11:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (153, N'DHSINTRA\pchinnap', CAST(N'2017-10-13T11:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (154, N'DHSINTRA\pchinnap', CAST(N'2017-10-16T11:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (155, N'DHSINTRA\pchinnap', CAST(N'2017-10-16T11:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (156, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (157, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (158, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (159, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (160, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (161, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (162, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (163, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (164, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (165, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (166, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (167, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (168, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (169, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (170, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (171, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (172, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (173, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (174, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (175, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (176, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (177, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (178, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (179, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (180, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (181, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (182, N'DHSINTRA\pchinnap', CAST(N'2017-10-16T14:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (183, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (184, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (185, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (186, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (187, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (188, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (189, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (190, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (191, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (192, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (193, N'DHSINTRA\bbuchake', CAST(N'2017-10-16T14:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (194, N'DHSINTRA\pchinnap', CAST(N'2017-10-17T05:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (195, N'DHSINTRA\pchinnap', CAST(N'2017-10-17T05:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (196, N'DHSINTRA\pchinnap', CAST(N'2017-10-17T05:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (197, N'DHSINTRA\pchinnap', CAST(N'2017-10-17T05:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (198, N'DHSINTRA\pchinnap', CAST(N'2017-10-17T05:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (199, N'DHSINTRA\pchinnap', CAST(N'2017-10-17T05:41:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (200, N'DHSINTRA\pchinnap', CAST(N'2017-10-17T07:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (201, N'DHSINTRA\bbuchake', CAST(N'2017-10-17T10:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (202, N'DHSINTRA\bbuchake', CAST(N'2017-10-17T11:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (203, N'DHSINTRA\bbuchake', CAST(N'2017-10-17T11:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (204, N'DHSINTRA\bbuchake', CAST(N'2017-10-17T11:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (205, N'DHSINTRA\bbuchake', CAST(N'2017-10-17T11:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (206, N'DHSINTRA\bbuchake', CAST(N'2017-10-17T12:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (207, N'DHSINTRA\bbuchake', CAST(N'2017-10-17T13:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (208, N'DHSINTRA\bbuchake', CAST(N'2017-10-17T14:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (209, N'DHSINTRA\bbuchake', CAST(N'2017-10-17T14:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (210, N'DHSINTRA\pchinnap', CAST(N'2017-10-17T14:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (211, N'DHSINTRA\bbuchake', CAST(N'2017-10-18T10:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (212, N'DHSINTRA\bbuchake', CAST(N'2017-10-18T10:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (213, N'DHSINTRA\bbuchake', CAST(N'2017-10-19T14:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (214, N'DHSINTRA\bbuchake', CAST(N'2017-10-19T14:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (215, N'DHSINTRA\bbuchake', CAST(N'2017-10-19T14:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (216, N'DHSINTRA\bbuchake', CAST(N'2017-10-19T15:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (217, N'DHSINTRA\bbuchake', CAST(N'2017-10-19T15:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (218, N'DHSINTRA\bbuchake', CAST(N'2017-10-19T16:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (219, N'DHSINTRA\bbuchake', CAST(N'2017-10-19T16:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (220, N'DHSINTRA\bbuchake', CAST(N'2017-10-20T11:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (221, N'DHSINTRA\bbuchake', CAST(N'2017-10-20T14:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (222, N'DHSINTRA\bbuchake', CAST(N'2017-10-20T15:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (223, N'DHSINTRA\bbuchake', CAST(N'2017-10-24T09:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (224, N'DHSINTRA\bbuchake', CAST(N'2017-10-24T10:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (225, N'DHSINTRA\bbuchake', CAST(N'2017-10-24T11:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (226, N'DHSINTRA\bbuchake', CAST(N'2017-10-24T11:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (227, N'DHSINTRA\bbuchake', CAST(N'2017-10-24T11:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (228, N'DHSINTRA\bbuchake', CAST(N'2017-10-24T11:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (229, N'DHSINTRA\pchinnap', CAST(N'2017-10-24T12:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (230, N'DHSINTRA\bbuchake', CAST(N'2017-10-25T11:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (231, N'DHSINTRA\bbuchake', CAST(N'2017-10-25T14:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (232, N'DHSINTRA\bbuchake', CAST(N'2017-10-25T14:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (233, N'DHSINTRA\bbuchake', CAST(N'2017-10-25T14:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (234, N'DHSINTRA\bbuchake', CAST(N'2017-10-25T15:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (235, N'DHSINTRA\bbuchake', CAST(N'2017-10-25T15:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (236, N'DHSINTRA\bbuchake', CAST(N'2017-10-25T15:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (237, N'DHSINTRA\bbuchake', CAST(N'2017-10-25T15:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (238, N'DHSINTRA\bbuchake', CAST(N'2017-10-25T15:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (239, N'DHSINTRA\bbuchake', CAST(N'2017-10-25T15:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (240, N'DHSINTRA\bbuchake', CAST(N'2017-10-25T15:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (241, N'DHSINTRA\bbuchake', CAST(N'2017-10-25T15:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (242, N'DHSINTRA\bbuchake', CAST(N'2017-10-25T16:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (243, N'DHSINTRA\bbuchake', CAST(N'2017-10-26T15:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (244, N'DHSINTRA\bbuchake', CAST(N'2017-10-26T15:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (245, N'DHSINTRA\bbuchake', CAST(N'2017-10-26T16:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (246, N'DHSINTRA\bbuchake', CAST(N'2017-10-26T16:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (247, N'DHSINTRA\bbuchake', CAST(N'2017-10-26T16:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (248, N'DHSINTRA\bbuchake', CAST(N'2017-10-26T16:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (249, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T08:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (250, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T09:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (251, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T09:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (252, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T09:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (253, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T09:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (254, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T09:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (255, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T09:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (256, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T09:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (257, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T09:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (258, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T09:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (259, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T09:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (260, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T09:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (261, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T09:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (262, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T09:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (263, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T10:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (264, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T10:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (265, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T10:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (266, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T10:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (267, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T10:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (268, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T10:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (269, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T10:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (270, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T11:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (271, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T11:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (272, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T11:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (273, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T13:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (274, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T13:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (275, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T13:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (276, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T13:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (277, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T13:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (278, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T13:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (279, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T14:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (280, N'DHSINTRA\bbuchake', CAST(N'2017-10-27T14:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (281, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T11:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (282, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T12:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (283, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T12:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (284, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T12:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (285, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T13:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (286, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T13:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (287, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T13:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (288, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T13:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (289, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T13:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (290, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T13:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (291, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T13:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (292, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T14:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (293, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T14:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (294, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T15:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (295, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T15:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (296, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T15:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (297, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T15:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (298, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T15:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (299, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T15:51:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (300, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T15:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (301, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T15:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (302, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T16:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (303, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T16:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (304, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T16:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (305, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T16:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (306, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T16:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (307, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T16:54:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (308, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T16:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (309, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T17:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (310, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T17:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (311, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T17:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (312, N'DHSINTRA\bbuchake', CAST(N'2017-10-30T17:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (313, N'DHSINTRA\bbuchake', CAST(N'2017-11-02T09:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (314, N'DHSINTRA\bbuchake', CAST(N'2017-11-02T10:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (315, N'DHSINTRA\bbuchake', CAST(N'2017-11-02T13:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (316, N'DHSINTRA\bbuchake', CAST(N'2017-11-02T13:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (317, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T12:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (318, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T13:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (319, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T13:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (320, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T13:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (321, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T13:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (322, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T13:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (323, N'DHSINTRA\pchinnap', CAST(N'2017-11-03T14:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (324, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (325, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (326, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (327, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (328, N'DHSINTRA\pchinnap', CAST(N'2017-11-03T14:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (329, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T15:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (330, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T15:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (331, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T16:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (332, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T16:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (333, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T16:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (334, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T16:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (335, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T16:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (336, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T18:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (337, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T18:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (338, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T18:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (339, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T18:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (340, N'DHSINTRA\bbuchake', CAST(N'2017-11-03T18:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (341, N'DHSINTRA\pchinnap', CAST(N'2017-11-07T10:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (342, N'DHSINTRA\pchinnap', CAST(N'2017-11-07T13:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (343, N'DHSINTRA\pchinnap', CAST(N'2017-11-07T13:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (344, N'DHSINTRA\pchinnap', CAST(N'2017-11-07T13:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (345, N'DHSINTRA\pchinnap', CAST(N'2017-11-08T09:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (346, N'DHSINTRA\pchinnap', CAST(N'2017-11-08T10:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (347, N'DHSINTRA\pchinnap', CAST(N'2017-11-08T10:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (348, N'DHSINTRA\pchinnap', CAST(N'2017-11-08T10:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (349, N'DHSINTRA\bbuchake', CAST(N'2017-11-08T13:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (350, N'DHSINTRA\pchinnap', CAST(N'2017-11-08T13:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (351, N'DHSINTRA\bbuchake', CAST(N'2017-11-08T15:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (352, N'DHSINTRA\bbuchake', CAST(N'2017-11-08T15:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (353, N'DHSINTRA\bbuchake', CAST(N'2017-11-08T15:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (354, N'DHSINTRA\bbuchake', CAST(N'2017-11-08T15:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (355, N'DHSINTRA\bbuchake', CAST(N'2017-11-08T15:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (356, N'DHSINTRA\bbuchake', CAST(N'2017-11-08T15:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (357, N'DHSINTRA\bbuchake', CAST(N'2017-11-08T16:06:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (358, N'DHSINTRA\bbuchake', CAST(N'2017-11-08T16:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (359, N'DHSINTRA\bbuchake', CAST(N'2017-11-08T16:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (360, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T10:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (361, N'DHSINTRA\pchinnap', CAST(N'2017-11-09T11:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (362, N'DHSINTRA\pchinnap', CAST(N'2017-11-09T11:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (363, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T11:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (364, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T11:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (365, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T11:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (366, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T11:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (367, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T11:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (368, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T11:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (369, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T11:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (370, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T12:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (371, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T12:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (372, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T12:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (373, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T12:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (374, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T12:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (375, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T13:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (376, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T13:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (377, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T13:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (378, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T13:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (379, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T13:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (380, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T13:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (381, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T13:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (382, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T13:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (383, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T13:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (384, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T13:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (385, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T13:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (386, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T13:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (387, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T13:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (388, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T13:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (389, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T13:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (390, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T13:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (391, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T13:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (392, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T14:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (393, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T14:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (394, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T14:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (395, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T15:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (396, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T15:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (397, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T15:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (398, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T15:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (399, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T15:55:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (400, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T15:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (401, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T16:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (402, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T16:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (403, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T16:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (404, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T16:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (405, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T16:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (406, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T16:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (407, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T16:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (408, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T16:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (409, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T16:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (410, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T16:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (411, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T17:06:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (412, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T17:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (413, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T18:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (414, N'DHSINTRA\bbuchake', CAST(N'2017-11-09T18:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (415, N'DHSINTRA\bbuchake', CAST(N'2017-11-10T10:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (416, N'DHSINTRA\bbuchake', CAST(N'2017-11-10T11:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (417, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T12:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (418, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T13:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (419, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T13:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (420, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T13:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (421, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T13:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (422, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T13:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (423, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T13:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (424, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T13:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (425, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T13:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (426, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T13:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (427, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T13:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (428, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T13:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (429, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T13:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (430, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T13:54:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (431, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T14:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (432, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T14:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (433, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T14:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (434, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T14:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (435, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T15:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (436, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T15:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (437, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T15:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (438, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T16:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (439, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T16:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (440, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T16:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (441, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T16:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (442, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T16:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (443, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T16:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (444, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T16:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (445, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T17:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (446, N'DHSINTRA\bbuchake', CAST(N'2017-11-13T21:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (447, N'DHSINTRA\bbuchake', CAST(N'2017-11-14T10:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (448, N'DHSINTRA\bbuchake', CAST(N'2017-11-14T11:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (449, N'DHSINTRA\bbuchake', CAST(N'2017-11-14T13:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (450, N'DHSINTRA\bbuchake', CAST(N'2017-11-14T13:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (451, N'DHSINTRA\bbuchake', CAST(N'2017-11-14T13:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (452, N'DHSINTRA\bbuchake', CAST(N'2017-11-14T14:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (453, N'DHSINTRA\bbuchake', CAST(N'2017-11-14T15:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (454, N'DHSINTRA\bbuchake', CAST(N'2017-11-14T15:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (455, N'DHSINTRA\bbuchake', CAST(N'2017-11-14T15:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (456, N'DHSINTRA\bbuchake', CAST(N'2017-11-14T15:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (457, N'DHSINTRA\bbuchake', CAST(N'2017-11-14T15:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (458, N'DHSINTRA\bbuchake', CAST(N'2017-11-14T16:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (459, N'DHSINTRA\bbuchake', CAST(N'2017-11-14T16:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (460, N'DHSINTRA\bbuchake', CAST(N'2017-11-14T16:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (461, N'DHSINTRA\bbuchake', CAST(N'2017-11-14T17:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (462, N'DHSINTRA\bbuchake', CAST(N'2017-11-15T09:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (463, N'DHSINTRA\bbuchake', CAST(N'2017-11-15T09:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (464, N'DHSINTRA\bbuchake', CAST(N'2017-11-15T14:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (465, N'DHSINTRA\bbuchake', CAST(N'2017-11-15T14:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (466, N'DHSINTRA\bbuchake', CAST(N'2017-11-15T14:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (467, N'DHSINTRA\bbuchake', CAST(N'2017-11-15T15:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (468, N'DHSINTRA\bbuchake', CAST(N'2017-11-15T15:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (469, N'DHSINTRA\bbuchake', CAST(N'2017-11-15T15:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (470, N'DHSINTRA\bbuchake', CAST(N'2017-11-15T15:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (471, N'DHSINTRA\bbuchake', CAST(N'2017-11-15T15:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (472, N'DHSINTRA\bbuchake', CAST(N'2017-11-15T15:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (473, N'DHSINTRA\bbuchake', CAST(N'2017-11-15T16:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (474, N'DHSINTRA\bbuchake', CAST(N'2017-11-15T16:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (475, N'DHSINTRA\bbuchake', CAST(N'2017-11-15T16:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (476, N'DHSINTRA\bbuchake', CAST(N'2017-11-15T16:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (477, N'DHSINTRA\bbuchake', CAST(N'2017-11-16T10:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (478, N'DHSINTRA\bbuchake', CAST(N'2017-11-16T10:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (479, N'DHSINTRA\bbuchake', CAST(N'2017-11-16T14:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (480, N'DHSINTRA\bbuchake', CAST(N'2017-11-16T14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (481, N'DHSINTRA\bbuchake', CAST(N'2017-11-16T14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (482, N'DHSINTRA\bbuchake', CAST(N'2017-11-16T14:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (483, N'DHSINTRA\bbuchake', CAST(N'2017-11-16T16:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (484, N'DHSINTRA\bbuchake', CAST(N'2017-11-16T16:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (485, N'DHSINTRA\bbuchake', CAST(N'2017-11-16T16:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (486, N'DHSINTRA\bbuchake', CAST(N'2017-11-16T16:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (487, N'DHSINTRA\bbuchake', CAST(N'2017-11-16T16:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (488, N'DHSINTRA\bbuchake', CAST(N'2017-11-16T16:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (489, N'DHSINTRA\bbuchake', CAST(N'2017-11-16T16:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (490, N'DHSINTRA\bbuchake', CAST(N'2017-11-17T08:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (491, N'DHSINTRA\bbuchake', CAST(N'2017-11-17T16:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (492, N'DHSINTRA\bbuchake', CAST(N'2017-11-17T16:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (493, N'DHSINTRA\bbuchake', CAST(N'2017-11-20T09:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (494, N'DHSINTRA\bbuchake', CAST(N'2017-11-20T09:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (495, N'DHSINTRA\bbuchake', CAST(N'2017-11-20T10:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (496, N'DHSINTRA\bbuchake', CAST(N'2017-11-20T14:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (497, N'DHSINTRA\bbuchake', CAST(N'2017-11-20T14:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (498, N'DHSINTRA\bbuchake', CAST(N'2017-11-21T14:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (499, N'DHSINTRA\bbuchake', CAST(N'2017-11-21T14:06:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (500, N'DHSINTRA\bbuchake', CAST(N'2017-11-21T14:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (501, N'DHSINTRA\bbuchake', CAST(N'2017-11-21T15:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (502, N'DHSINTRA\bbuchake', CAST(N'2017-11-21T15:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (503, N'DHSINTRA\bbuchake', CAST(N'2017-11-21T15:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (504, N'DHSINTRA\bbuchake', CAST(N'2017-11-28T12:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (505, N'DHSINTRA\bbuchake', CAST(N'2017-12-08T10:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (506, N'DHSINTRA\bbuchake', CAST(N'2017-12-08T10:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (507, N'DHSINTRA\bbuchake', CAST(N'2017-12-08T11:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (508, N'DHSINTRA\bbuchake', CAST(N'2017-12-08T13:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (509, N'DHSINTRA\bbuchake', CAST(N'2017-12-08T13:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (510, N'DHSINTRA\bbuchake', CAST(N'2017-12-08T13:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (511, N'DHSINTRA\bbuchake', CAST(N'2017-12-08T13:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (512, N'DHSINTRA\bbuchake', CAST(N'2017-12-08T13:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (513, N'DHSINTRA\bbuchake', CAST(N'2017-12-08T13:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (514, N'DHSINTRA\bbuchake', CAST(N'2017-12-08T14:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (515, N'DHSINTRA\bbuchake', CAST(N'2017-12-08T14:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (516, N'DHSINTRA\bbuchake', CAST(N'2017-12-08T14:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (517, N'DHSINTRA\bbuchake', CAST(N'2017-12-11T12:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (518, N'DHSINTRA\bbuchake', CAST(N'2017-12-11T15:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (519, N'DHSINTRA\bbuchake', CAST(N'2017-12-11T15:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (520, N'DHSINTRA\bbuchake', CAST(N'2017-12-12T14:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (521, N'DHSINTRA\bbuchake', CAST(N'2018-01-03T08:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (522, N'DHSINTRA\bbuchake', CAST(N'2018-01-03T13:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (523, N'DHSINTRA\bbuchake', CAST(N'2018-01-03T13:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (524, N'DHSINTRA\bbuchake', CAST(N'2018-01-03T13:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (525, N'DHSINTRA\bbuchake', CAST(N'2018-01-03T13:06:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (526, N'DHSINTRA\bbuchake', CAST(N'2018-01-03T13:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (527, N'DHSINTRA\bbuchake', CAST(N'2018-01-08T14:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (528, N'DHSINTRA\bbuchake', CAST(N'2018-01-08T15:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (529, N'DHSINTRA\bbuchake', CAST(N'2018-01-08T15:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (530, N'DHSINTRA\bbuchake', CAST(N'2018-01-08T15:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (531, N'DHSINTRA\bbuchake', CAST(N'2018-01-08T15:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (532, N'DHSINTRA\bbuchake', CAST(N'2018-01-09T08:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (533, N'DHSINTRA\bbuchake', CAST(N'2018-01-09T08:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (534, N'DHSINTRA\bbuchake', CAST(N'2018-01-09T08:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (535, N'DHSINTRA\bbuchake', CAST(N'2018-01-09T09:06:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (536, N'DHSINTRA\bbuchake', CAST(N'2018-01-09T10:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (537, N'DHSINTRA\bbuchake', CAST(N'2018-01-09T10:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (538, N'DHSINTRA\bbuchake', CAST(N'2018-01-10T14:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (539, N'DHSINTRA\bbuchake', CAST(N'2018-01-10T14:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (540, N'DHSINTRA\bbuchake', CAST(N'2018-01-10T14:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (541, N'DHSINTRA\bbuchake', CAST(N'2018-01-10T14:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (542, N'DHSINTRA\bbuchake', CAST(N'2018-01-10T14:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (543, N'DHSINTRA\bbuchake', CAST(N'2018-01-10T14:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (544, N'DHSINTRA\bbuchake', CAST(N'2018-01-10T14:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (545, N'DHSINTRA\bbuchake', CAST(N'2018-01-10T14:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (546, N'DHSINTRA\bbuchake', CAST(N'2018-01-10T14:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (547, N'DHSINTRA\bbuchake', CAST(N'2018-01-10T14:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (548, N'DHSINTRA\bbuchake', CAST(N'2018-01-10T14:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (549, N'DHSINTRA\bbuchake', CAST(N'2018-01-10T14:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (550, N'DHSINTRA\bbuchake', CAST(N'2018-01-11T14:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (551, N'DHSINTRA\bbuchake', CAST(N'2018-01-11T14:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (552, N'DHSINTRA\bbuchake', CAST(N'2018-01-11T14:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (553, N'DHSINTRA\bbuchake', CAST(N'2018-01-11T14:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (554, N'DHSINTRA\pchinnap', CAST(N'2018-01-11T15:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (555, N'DHSINTRA\bbuchake', CAST(N'2018-01-12T09:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (556, N'DHSINTRA\pchinnap', CAST(N'2018-01-25T13:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (557, N'DHSINTRA\pchinnap', CAST(N'2018-01-25T14:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (558, N'DHSINTRA\vgrenz', CAST(N'2018-01-29T11:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (559, N'DHSINTRA\vgrenz', CAST(N'2018-01-29T11:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (560, N'DHSINTRA\vgrenz', CAST(N'2018-01-29T11:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (561, N'DHSINTRA\vgrenz', CAST(N'2018-01-29T13:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (562, N'DHSINTRA\vgrenz', CAST(N'2018-01-29T14:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (563, N'DHSINTRA\vgrenz', CAST(N'2018-01-30T11:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (564, N'DHSINTRA\vgrenz', CAST(N'2018-01-30T11:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (565, N'DHSINTRA\vgrenz', CAST(N'2018-01-30T11:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (566, N'DHSINTRA\vgrenz', CAST(N'2018-01-30T12:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (567, N'DHSINTRA\grichey', CAST(N'2018-02-20T12:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (568, N'DHSINTRA\grichey', CAST(N'2018-02-20T12:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (569, N'DHSINTRA\grichey', CAST(N'2018-02-20T12:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (570, N'DHSINTRA\grichey', CAST(N'2018-02-20T12:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (571, N'DHSINTRA\grichey', CAST(N'2018-02-20T12:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (572, N'DHSINTRA\grichey', CAST(N'2018-02-20T12:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (573, N'DHSINTRA\grichey', CAST(N'2018-02-20T12:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (574, N'DHSINTRA\grichey', CAST(N'2018-02-20T13:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (575, N'DHSINTRA\grichey', CAST(N'2018-02-20T13:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (576, N'DHSINTRA\grichey', CAST(N'2018-02-20T14:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (577, N'DHSINTRA\grichey', CAST(N'2018-02-20T14:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (578, N'DHSINTRA\grichey', CAST(N'2018-02-20T15:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (579, N'DHSINTRA\grichey', CAST(N'2018-02-20T15:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (580, N'DHSINTRA\grichey', CAST(N'2018-02-20T15:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (581, N'DHSINTRA\grichey', CAST(N'2018-02-20T15:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (582, N'DHSINTRA\pchinnap', CAST(N'2018-02-21T08:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (583, N'DHSINTRA\grichey', CAST(N'2018-02-21T08:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (584, N'DHSINTRA\grichey', CAST(N'2018-02-21T08:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (585, N'DHSINTRA\grichey', CAST(N'2018-02-21T08:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (586, N'DHSINTRA\grichey', CAST(N'2018-02-21T09:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (587, N'DHSINTRA\grichey', CAST(N'2018-02-21T09:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (588, N'DHSINTRA\grichey', CAST(N'2018-02-21T09:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (589, N'DHSINTRA\grichey', CAST(N'2018-02-21T09:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (590, N'DHSINTRA\grichey', CAST(N'2018-02-21T09:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (591, N'DHSINTRA\bbuchake', CAST(N'2018-02-21T09:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (592, N'DHSINTRA\grichey', CAST(N'2018-02-21T09:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (593, N'DHSINTRA\grichey', CAST(N'2018-02-21T09:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (594, N'DHSINTRA\grichey', CAST(N'2018-02-21T09:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (595, N'DHSINTRA\grichey', CAST(N'2018-02-21T09:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (596, N'DHSINTRA\grichey', CAST(N'2018-02-21T10:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (597, N'DHSINTRA\grichey', CAST(N'2018-02-21T10:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (598, N'DHSINTRA\grichey', CAST(N'2018-02-21T10:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (599, N'DHSINTRA\grichey', CAST(N'2018-02-21T10:29:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (600, N'DHSINTRA\grichey', CAST(N'2018-02-21T10:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (601, N'DHSINTRA\grichey', CAST(N'2018-02-21T10:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (602, N'DHSINTRA\grichey', CAST(N'2018-02-21T12:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (603, N'DHSINTRA\grichey', CAST(N'2018-02-21T12:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (604, N'DHSINTRA\grichey', CAST(N'2018-02-21T12:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (605, N'DHSINTRA\grichey', CAST(N'2018-02-21T12:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (606, N'DHSINTRA\grichey', CAST(N'2018-02-21T12:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (607, N'DHSINTRA\grichey', CAST(N'2018-02-21T12:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (608, N'DHSINTRA\grichey', CAST(N'2018-02-21T13:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (609, N'DHSINTRA\grichey', CAST(N'2018-02-21T13:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (610, N'DHSINTRA\grichey', CAST(N'2018-02-21T13:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (611, N'DHSINTRA\grichey', CAST(N'2018-02-21T14:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (612, N'DHSINTRA\grichey', CAST(N'2018-02-21T14:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (613, N'DHSINTRA\grichey', CAST(N'2018-02-22T08:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (614, N'DHSINTRA\GRichey', CAST(N'2018-02-22T09:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (615, N'DHSINTRA\grichey', CAST(N'2018-02-22T09:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (616, N'DHSINTRA\grichey', CAST(N'2018-02-22T09:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (617, N'DHSINTRA\grichey', CAST(N'2018-02-22T09:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (618, N'DHSINTRA\GRichey', CAST(N'2018-02-22T09:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (619, N'DHSINTRA\grichey', CAST(N'2018-02-22T09:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (620, N'DHSINTRA\grichey', CAST(N'2018-02-22T10:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (621, N'DHSINTRA\grichey', CAST(N'2018-02-22T10:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (622, N'DHSINTRA\grichey', CAST(N'2018-02-22T10:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (623, N'NT AUTHORITY\IUSR', CAST(N'2018-02-22T10:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (624, N'DHSINTRA\GRichey', CAST(N'2018-02-22T10:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (625, N'DHSINTRA\grichey', CAST(N'2018-02-22T12:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (626, N'DHSINTRA\grichey', CAST(N'2018-02-22T12:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (627, N'DHSINTRA\grichey', CAST(N'2018-02-22T12:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (628, N'DHSINTRA\grichey', CAST(N'2018-02-22T12:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (629, N'DHSINTRA\grichey', CAST(N'2018-02-22T12:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (630, N'DHSINTRA\grichey', CAST(N'2018-02-22T12:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (631, N'DHSINTRA\grichey', CAST(N'2018-02-22T12:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (632, N'DHSINTRA\grichey', CAST(N'2018-02-22T13:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (633, N'DHSINTRA\grichey', CAST(N'2018-02-22T13:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (634, N'DHSINTRA\grichey', CAST(N'2018-02-22T13:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (635, N'DHSINTRA\grichey', CAST(N'2018-02-22T13:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (636, N'DHSINTRA\grichey', CAST(N'2018-02-22T13:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (637, N'DHSINTRA\grichey', CAST(N'2018-02-22T14:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (638, N'IIS APPPOOL\NSAA', CAST(N'2018-02-22T14:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (639, N'IIS APPPOOL\NSAA', CAST(N'2018-02-22T14:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (640, N'IIS APPPOOL\NSAA', CAST(N'2018-02-22T14:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (641, N'IIS APPPOOL\NSAA', CAST(N'2018-02-22T14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (642, N'IIS APPPOOL\NSAA', CAST(N'2018-02-22T14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (643, N'IIS APPPOOL\NSAA', CAST(N'2018-02-22T14:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (644, N'NT AUTHORITY\NETWORK', CAST(N'2018-02-22T14:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (645, N'DHSINTRA\grichey', CAST(N'2018-02-22T14:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (646, N'DHSINTRA\grichey', CAST(N'2018-02-22T14:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (647, N'DHSINTRA\grichey', CAST(N'2018-02-22T15:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (648, N'DHSINTRA\grichey', CAST(N'2018-02-22T15:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (649, N'DHSINTRA\grichey', CAST(N'2018-02-23T08:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (650, N'DHSINTRA\grichey', CAST(N'2018-02-23T08:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (651, N'DHSINTRA\grichey', CAST(N'2018-02-23T09:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (652, N'DHSINTRA\grichey', CAST(N'2018-02-23T12:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (653, N'DHSINTRA\grichey', CAST(N'2018-02-23T12:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (654, N'DHSINTRA\grichey', CAST(N'2018-02-23T12:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (655, N'DHSINTRA\grichey', CAST(N'2018-02-23T12:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (656, N'DHSINTRA\grichey', CAST(N'2018-02-23T12:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (657, N'DHSINTRA\grichey', CAST(N'2018-02-23T12:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (658, N'DHSINTRA\grichey', CAST(N'2018-02-23T13:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (659, N'DHSINTRA\grichey', CAST(N'2018-02-23T13:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (660, N'DHSINTRA\grichey', CAST(N'2018-02-23T13:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (661, N'DHSINTRA\grichey', CAST(N'2018-02-23T13:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (662, N'DHSINTRA\grichey', CAST(N'2018-02-23T14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (663, N'DHSINTRA\grichey', CAST(N'2018-02-23T15:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (664, N'DHSINTRA\grichey', CAST(N'2018-02-23T15:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (665, N'DHSINTRA\grichey', CAST(N'2018-02-25T09:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (666, N'DHSINTRA\grichey', CAST(N'2018-02-25T10:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (667, N'DHSINTRA\grichey', CAST(N'2018-02-25T11:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (668, N'DHSINTRA\grichey', CAST(N'2018-02-25T13:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (669, N'DHSINTRA\grichey', CAST(N'2018-02-25T13:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (670, N'DHSINTRA\grichey', CAST(N'2018-02-25T13:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (671, N'DHSINTRA\grichey', CAST(N'2018-02-25T14:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (672, N'DHSINTRA\grichey', CAST(N'2018-02-26T14:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (673, N'DHSINTRA\grichey', CAST(N'2018-02-27T07:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (674, N'DHSINTRA\grichey', CAST(N'2018-02-27T12:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (675, N'DHSINTRA\vgrenz', CAST(N'2018-02-27T12:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (676, N'DHSINTRA\vgrenz', CAST(N'2018-02-27T13:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (677, N'DHSINTRA\vgrenz', CAST(N'2018-02-27T13:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (678, N'DHSINTRA\vgrenz', CAST(N'2018-02-27T13:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (679, N'DHSINTRA\vgrenz', CAST(N'2018-02-27T13:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (680, N'DHSINTRA\vgrenz', CAST(N'2018-02-27T13:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (681, N'DHSINTRA\vgrenz', CAST(N'2018-02-27T13:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (682, N'DHSINTRA\vgrenz', CAST(N'2018-02-27T14:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (683, N'DHSINTRA\vgrenz', CAST(N'2018-02-27T14:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (684, N'DHSINTRA\vgrenz', CAST(N'2018-02-27T14:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (685, N'DHSINTRA\grichey', CAST(N'2018-02-27T14:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (686, N'DHSINTRA\grichey', CAST(N'2018-02-27T15:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (687, N'DHSINTRA\RPerumal', CAST(N'2018-02-27T16:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (688, N'DHSINTRA\grichey', CAST(N'2018-02-27T16:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (689, N'DHSINTRA\grichey', CAST(N'2018-02-27T16:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (690, N'DHSINTRA\RPerumal', CAST(N'2018-02-27T19:06:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (691, N'DHSINTRA\RPerumal', CAST(N'2018-02-27T19:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (692, N'DHSINTRA\RPerumal', CAST(N'2018-02-27T19:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (694, N'DHSINTRA\RPerumal', CAST(N'2018-02-27T00:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (695, N'DHSINTRA\RPerumal', CAST(N'2018-02-27T19:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (696, N'DHSINTRA\RPerumal', CAST(N'2018-02-27T19:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (697, N'DHSINTRA\RPerumal', CAST(N'2018-02-27T19:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (698, N'DHSINTRA\RPerumal', CAST(N'2018-02-27T20:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (699, N'DHSINTRA\RPerumal', CAST(N'2018-02-27T20:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (700, N'DHSINTRA\grichey', CAST(N'2018-02-28T08:11:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (701, N'DHSINTRA\RPerumal', CAST(N'2018-02-28T08:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (702, N'DHSINTRA\grichey', CAST(N'2018-02-28T08:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (703, N'DHSINTRA\grichey', CAST(N'2018-02-28T09:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (704, N'DHSINTRA\grichey', CAST(N'2018-02-28T09:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (705, N'DHSINTRA\grichey', CAST(N'2018-02-28T09:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (706, N'DHSINTRA\grichey', CAST(N'2018-02-28T10:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (707, N'DHSINTRA\RPerumal', CAST(N'2018-02-28T10:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (708, N'DHSINTRA\RPerumal', CAST(N'2018-02-28T10:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (709, N'DHSINTRA\RPerumal', CAST(N'2018-02-28T10:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (710, N'DHSINTRA\RPerumal', CAST(N'2018-02-28T10:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (711, N'DHSINTRA\RPerumal', CAST(N'2018-02-28T10:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (712, N'DHSINTRA\RPerumal', CAST(N'2018-02-28T10:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (713, N'DHSINTRA\RPerumal', CAST(N'2018-02-28T10:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (714, N'DHSINTRA\RPerumal', CAST(N'2018-02-28T11:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (716, N'DHSINTRA\RPerumal', CAST(N'2018-02-28T11:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (718, N'DHSINTRA\RPerumal', CAST(N'2018-02-28T11:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (719, N'DHSINTRA\RPerumal', CAST(N'2018-02-28T11:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (720, N'DHSINTRA\RPerumal', CAST(N'2018-02-28T11:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (721, N'DHSINTRA\RPerumal', CAST(N'2018-02-28T11:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (722, N'DHSINTRA\grichey', CAST(N'2018-02-28T12:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (723, N'DHSINTRA\grichey', CAST(N'2018-02-28T12:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (724, N'DHSINTRA\grichey', CAST(N'2018-02-28T13:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (725, N'DHSINTRA\RPerumal', CAST(N'2018-02-28T13:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (726, N'DHSINTRA\grichey', CAST(N'2018-02-28T13:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (727, N'DHSINTRA\grichey', CAST(N'2018-02-28T14:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (728, N'DHSINTRA\grichey', CAST(N'2018-02-28T14:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (729, N'DHSINTRA\grichey', CAST(N'2018-02-28T14:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (730, N'DHSINTRA\RPerumal', CAST(N'2018-02-28T16:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (731, N'DHSINTRA\RPerumal', CAST(N'2018-03-01T08:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (732, N'DHSINTRA\RPerumal', CAST(N'2018-03-01T08:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (733, N'DHSINTRA\RPerumal', CAST(N'2018-03-01T08:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (734, N'DHSINTRA\grichey', CAST(N'2018-03-01T09:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (735, N'DHSINTRA\grichey', CAST(N'2018-03-01T09:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (736, N'DHSINTRA\grichey', CAST(N'2018-03-01T10:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (737, N'DHSINTRA\grichey', CAST(N'2018-03-01T10:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (738, N'DHSINTRA\grichey', CAST(N'2018-03-01T10:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (739, N'DHSINTRA\RPerumal', CAST(N'2018-03-01T10:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (740, N'DHSINTRA\grichey', CAST(N'2018-03-01T12:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (741, N'DHSINTRA\grichey', CAST(N'2018-03-01T13:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (742, N'DHSINTRA\grichey', CAST(N'2018-03-01T13:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (743, N'DHSINTRA\grichey', CAST(N'2018-03-01T14:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (744, N'DHSINTRA\grichey', CAST(N'2018-03-01T15:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (745, N'DHSINTRA\grichey', CAST(N'2018-03-02T08:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (746, N'DHSINTRA\grichey', CAST(N'2018-03-02T11:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (747, N'DHSINTRA\grichey', CAST(N'2018-03-02T11:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (748, N'DHSINTRA\grichey', CAST(N'2018-03-02T12:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (749, N'DHSINTRA\grichey', CAST(N'2018-03-02T13:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (750, N'DHSINTRA\grichey', CAST(N'2018-03-02T14:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (751, N'DHSINTRA\grichey', CAST(N'2018-03-02T15:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (752, N'DHSINTRA\grichey', CAST(N'2018-03-02T17:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (753, N'DHSINTRA\grichey', CAST(N'2018-03-02T17:54:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (754, N'DHSINTRA\grichey', CAST(N'2018-03-02T18:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (755, N'DHSINTRA\grichey', CAST(N'2018-03-02T19:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (756, N'DHSINTRA\grichey', CAST(N'2018-03-03T10:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (757, N'DHSINTRA\grichey', CAST(N'2018-03-03T11:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (758, N'DHSINTRA\grichey', CAST(N'2018-03-03T12:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (759, N'DHSINTRA\grichey', CAST(N'2018-03-03T12:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (760, N'DHSINTRA\grichey', CAST(N'2018-03-03T13:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (761, N'DHSINTRA\grichey', CAST(N'2018-03-03T14:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (762, N'DHSINTRA\grichey', CAST(N'2018-03-03T14:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (763, N'DHSINTRA\grichey', CAST(N'2018-03-03T14:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (764, N'DHSINTRA\grichey', CAST(N'2018-03-03T16:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (765, N'DHSINTRA\grichey', CAST(N'2018-03-04T08:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (766, N'DHSINTRA\grichey', CAST(N'2018-03-04T09:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (767, N'DHSINTRA\grichey', CAST(N'2018-03-04T09:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (768, N'DHSINTRA\grichey', CAST(N'2018-03-04T10:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (769, N'DHSINTRA\grichey', CAST(N'2018-03-04T10:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (770, N'DHSINTRA\grichey', CAST(N'2018-03-04T10:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (771, N'DHSINTRA\grichey', CAST(N'2018-03-04T10:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (772, N'DHSINTRA\grichey', CAST(N'2018-03-04T11:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (773, N'DHSINTRA\grichey', CAST(N'2018-03-04T12:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (774, N'DHSINTRA\grichey', CAST(N'2018-03-04T12:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (775, N'DHSINTRA\grichey', CAST(N'2018-03-04T12:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (776, N'DHSINTRA\grichey', CAST(N'2018-03-04T13:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (777, N'DHSINTRA\grichey', CAST(N'2018-03-04T13:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (778, N'DHSINTRA\grichey', CAST(N'2018-03-04T13:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (779, N'DHSINTRA\grichey', CAST(N'2018-03-04T13:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (780, N'DHSINTRA\grichey', CAST(N'2018-03-04T14:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (781, N'DHSINTRA\grichey', CAST(N'2018-03-04T14:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (782, N'DHSINTRA\grichey', CAST(N'2018-03-04T14:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (783, N'DHSINTRA\grichey', CAST(N'2018-03-04T15:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (784, N'DHSINTRA\grichey', CAST(N'2018-03-04T15:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (785, N'DHSINTRA\grichey', CAST(N'2018-03-04T17:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (786, N'DHSINTRA\grichey', CAST(N'2018-03-05T10:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (787, N'DHSINTRA\grichey', CAST(N'2018-03-05T14:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (788, N'DHSINTRA\grichey', CAST(N'2018-03-05T15:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (789, N'DHSINTRA\vgrenz', CAST(N'2018-03-05T15:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (790, N'DHSINTRA\vgrenz', CAST(N'2018-03-05T15:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (791, N'DHSINTRA\grichey', CAST(N'2018-03-05T16:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (792, N'DHSINTRA\vgrenz', CAST(N'2018-03-05T16:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (793, N'DHSINTRA\grichey', CAST(N'2018-03-05T16:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (794, N'DHSINTRA\vgrenz', CAST(N'2018-03-05T17:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (795, N'DHSINTRA\grichey', CAST(N'2018-03-05T18:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (796, N'DHSINTRA\grichey', CAST(N'2018-03-05T18:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (797, N'DHSINTRA\grichey', CAST(N'2018-03-05T19:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (798, N'DHSINTRA\grichey', CAST(N'2018-03-05T20:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (799, N'DHSINTRA\grichey', CAST(N'2018-03-05T20:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (800, N'DHSINTRA\grichey', CAST(N'2018-03-06T08:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (801, N'DHSINTRA\grichey', CAST(N'2018-03-06T09:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (802, N'DHSINTRA\grichey', CAST(N'2018-03-06T12:24:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (803, N'DHSINTRA\grichey', CAST(N'2018-03-06T12:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (804, N'DHSINTRA\grichey', CAST(N'2018-03-06T13:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (805, N'DHSINTRA\grichey', CAST(N'2018-03-06T14:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (806, N'DHSINTRA\grichey', CAST(N'2018-03-06T14:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (807, N'DHSINTRA\grichey', CAST(N'2018-03-06T14:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (808, N'DHSINTRA\grichey', CAST(N'2018-03-06T14:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (809, N'DHSINTRA\grichey', CAST(N'2018-03-06T14:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (810, N'DHSINTRA\grichey', CAST(N'2018-03-06T15:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (811, N'DHSINTRA\grichey', CAST(N'2018-03-06T15:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (812, N'DHSINTRA\grichey', CAST(N'2018-03-06T15:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (813, N'DHSINTRA\grichey', CAST(N'2018-03-06T15:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (814, N'DHSINTRA\grichey', CAST(N'2018-03-06T16:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (815, N'DHSINTRA\grichey', CAST(N'2018-03-06T16:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (816, N'dell\user', CAST(N'2018-03-10T00:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (817, N'USER1\User1', CAST(N'2018-03-09T12:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (818, N'USER1\User1', CAST(N'2018-03-10T20:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (819, N'USER1\User1', CAST(N'2018-03-11T13:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (820, N'USER1\User1', CAST(N'2018-03-11T15:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (821, N'USER1\User1', CAST(N'2018-03-11T16:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (822, N'USER1\User1', CAST(N'2018-03-11T17:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (823, N'USER1\User1', CAST(N'2018-03-11T18:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (824, N'USER1\User1', CAST(N'2018-03-11T18:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1817, N'USER1\User1', CAST(N'2018-03-18T08:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1818, N'USER1\User1', CAST(N'2018-03-18T08:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1819, N'USER1\User1', CAST(N'2018-03-18T10:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1820, N'USER1\User1', CAST(N'2018-03-19T09:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1821, N'USER1\User1', CAST(N'2018-03-19T09:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1822, N'USER1\User1', CAST(N'2018-03-19T09:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1823, N'USER1\User1', CAST(N'2018-03-22T03:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1824, N'USER1\User1', CAST(N'2018-03-22T03:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1825, N'USER1\User1', CAST(N'2018-03-22T04:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1826, N'USER1\User1', CAST(N'2018-03-22T04:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1827, N'USER1\User1', CAST(N'2018-03-22T04:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1828, N'USER1\User1', CAST(N'2018-03-22T05:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1829, N'USER1\User1', CAST(N'2018-03-22T05:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1830, N'USER1\User1', CAST(N'2018-03-22T05:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1831, N'USER1\User1', CAST(N'2018-03-22T06:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1832, N'USER1\User1', CAST(N'2018-03-22T06:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1833, N'USER1\User1', CAST(N'2018-03-22T06:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1834, N'USER1\User1', CAST(N'2018-03-22T08:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1835, N'USER1\User1', CAST(N'2018-03-22T09:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1836, N'USER1\User1', CAST(N'2018-03-24T02:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1837, N'USER1\User1', CAST(N'2018-03-24T02:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1838, N'USER1\User1', CAST(N'2018-03-24T02:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1839, N'USER1\User1', CAST(N'2018-03-24T02:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1840, N'USER1\User1', CAST(N'2018-03-24T03:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1841, N'USER1\User1', CAST(N'2018-03-24T03:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1842, N'USER1\User1', CAST(N'2018-03-24T03:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1843, N'USER1\User1', CAST(N'2018-03-24T03:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1844, N'USER1\User1', CAST(N'2018-03-24T03:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1845, N'USER1\User1', CAST(N'2018-03-24T03:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1846, N'USER1\User1', CAST(N'2018-03-24T03:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1847, N'USER1\User1', CAST(N'2018-03-24T04:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1848, N'USER1\User1', CAST(N'2018-03-24T04:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1849, N'USER1\User1', CAST(N'2018-03-24T04:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1850, N'USER1\User1', CAST(N'2018-03-24T07:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1851, N'USER1\User1', CAST(N'2018-03-24T07:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1852, N'USER1\User1', CAST(N'2018-03-24T10:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1853, N'USER1\User1', CAST(N'2018-03-24T10:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1854, N'USER1\User1', CAST(N'2018-03-24T10:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1855, N'USER1\User1', CAST(N'2018-03-24T12:06:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1856, N'USER1\User1', CAST(N'2018-03-28T20:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1857, N'USER1\User1', CAST(N'2018-03-28T21:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1858, N'USER1\User1', CAST(N'2018-03-28T21:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1859, N'USER1\User1', CAST(N'2018-03-29T06:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1860, N'USER1\User1', CAST(N'2018-03-30T21:54:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1861, N'USER1\User1', CAST(N'2018-03-31T00:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1862, N'USER1\User1', CAST(N'2018-03-31T23:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1863, N'USER1\User1', CAST(N'2018-04-09T11:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1864, N'USER1\User1', CAST(N'2018-04-09T11:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1865, N'USER1\User1', CAST(N'2018-04-09T18:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1866, N'USER1\User1', CAST(N'2018-04-09T23:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1867, N'USER1\User1', CAST(N'2018-04-09T23:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1868, N'USER1\User1', CAST(N'2018-04-09T23:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1869, N'USER1\User1', CAST(N'2018-04-09T23:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1870, N'USER1\User1', CAST(N'2018-04-09T23:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1871, N'USER1\User1', CAST(N'2018-04-13T19:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1872, N'USER1\User1', CAST(N'2018-04-14T22:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1873, N'USER1\User1', CAST(N'2018-04-15T15:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1874, N'USER1\User1', CAST(N'2018-04-15T21:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1875, N'USER1\User1', CAST(N'2018-04-16T15:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1876, N'USER1\User1', CAST(N'2018-04-16T19:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1877, N'USER1\User1', CAST(N'2018-04-16T21:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1878, N'USER1\User1', CAST(N'2018-04-16T23:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1879, N'USER1\User1', CAST(N'2018-04-16T23:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1880, N'USER1\User1', CAST(N'2018-04-16T23:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1881, N'USER1\User1', CAST(N'2018-04-17T12:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1882, N'USER1\User1', CAST(N'2018-04-17T15:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1883, N'USER1\User1', CAST(N'2018-04-17T17:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1884, N'USER1\User1', CAST(N'2018-04-17T17:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1885, N'USER1\User1', CAST(N'2018-04-17T18:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1886, N'USER1\User1', CAST(N'2018-04-17T19:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1887, N'USER1\User1', CAST(N'2018-04-17T19:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1888, N'USER1\User1', CAST(N'2018-04-17T19:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1889, N'USER1\User1', CAST(N'2018-04-17T20:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1890, N'USER1\User1', CAST(N'2018-04-17T21:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1891, N'USER1\User1', CAST(N'2018-04-17T21:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1892, N'USER1\User1', CAST(N'2018-04-17T21:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1893, N'USER1\User1', CAST(N'2018-04-17T22:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1894, N'USER1\User1', CAST(N'2018-04-17T22:29:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1895, N'USER1\User1', CAST(N'2018-04-17T22:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1896, N'USER1\User1', CAST(N'2018-04-17T23:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (1897, N'USER1\User1', CAST(N'2018-04-17T23:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2877, N'USER1\User1', CAST(N'2018-04-18T23:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2878, N'USER1\User1', CAST(N'2018-04-18T23:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2879, N'USER1\User1', CAST(N'2018-04-18T23:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2880, N'USER1\User1', CAST(N'2018-04-18T23:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2881, N'USER1\User1', CAST(N'2018-04-18T23:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2882, N'USER1\User1', CAST(N'2018-04-18T23:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2883, N'USER1\User1', CAST(N'2018-04-18T23:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2884, N'USER1\User1', CAST(N'2018-04-19T00:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2885, N'USER1\User1', CAST(N'2018-04-19T07:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2886, N'USER1\User1', CAST(N'2018-04-19T09:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2887, N'USER1\User1', CAST(N'2018-04-19T09:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2888, N'USER1\User1', CAST(N'2018-04-19T11:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2889, N'USER1\User1', CAST(N'2018-04-19T13:54:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2890, N'USER1\User1', CAST(N'2018-04-19T14:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2891, N'USER1\User1', CAST(N'2018-04-19T17:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2892, N'USER1\User1', CAST(N'2018-04-21T23:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2893, N'USER1\User1', CAST(N'2018-04-22T08:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2894, N'USER1\User1', CAST(N'2018-04-22T09:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2895, N'USER1\User1', CAST(N'2018-04-22T12:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2896, N'USER1\User1', CAST(N'2018-04-22T13:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2897, N'USER1\User1', CAST(N'2018-04-22T16:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2898, N'USER1\User1', CAST(N'2018-04-22T16:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2899, N'USER1\User1', CAST(N'2018-04-22T16:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2900, N'USER1\User1', CAST(N'2018-04-22T16:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2901, N'USER1\User1', CAST(N'2018-04-22T17:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2902, N'USER1\User1', CAST(N'2018-04-22T17:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2903, N'USER1\User1', CAST(N'2018-04-22T17:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2904, N'USER1\User1', CAST(N'2018-04-22T17:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2905, N'USER1\User1', CAST(N'2018-04-22T18:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2906, N'USER1\User1', CAST(N'2018-04-22T19:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2907, N'USER1\User1', CAST(N'2018-04-22T21:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2908, N'USER1\User1', CAST(N'2018-04-22T21:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2909, N'USER1\User1', CAST(N'2018-04-22T21:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2910, N'USER1\User1', CAST(N'2018-04-22T21:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2911, N'USER1\User1', CAST(N'2018-04-22T22:06:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2912, N'USER1\User1', CAST(N'2018-04-22T22:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2913, N'USER1\User1', CAST(N'2018-04-22T23:06:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2914, N'USER1\User1', CAST(N'2018-04-22T23:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2915, N'USER1\User1', CAST(N'2018-04-22T23:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2916, N'USER1\User1', CAST(N'2018-04-23T21:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2917, N'USER1\User1', CAST(N'2018-04-23T21:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2918, N'USER1\User1', CAST(N'2018-04-23T21:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2919, N'USER1\User1', CAST(N'2018-04-23T22:54:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2920, N'USER1\User1', CAST(N'2018-04-23T23:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2921, N'USER1\User1', CAST(N'2018-04-24T10:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2922, N'USER1\User1', CAST(N'2018-04-24T10:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2923, N'USER1\User1', CAST(N'2018-04-24T14:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2924, N'USER1\User1', CAST(N'2018-04-24T14:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (2925, N'USER1\User1', CAST(N'2018-04-24T18:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3921, N'USER1\User1', CAST(N'2018-04-26T19:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3922, N'USER1\User1', CAST(N'2018-04-26T20:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3923, N'USER1\User1', CAST(N'2018-04-26T20:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3924, N'USER1\User1', CAST(N'2018-04-26T20:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3925, N'USER1\User1', CAST(N'2018-04-26T20:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3926, N'USER1\User1', CAST(N'2018-04-26T21:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3927, N'USER1\User1', CAST(N'2018-04-26T22:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3928, N'USER1\User1', CAST(N'2018-04-27T20:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3929, N'USER1\User1', CAST(N'2018-04-27T22:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3930, N'USER1\User1', CAST(N'2018-04-28T10:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3931, N'USER1\User1', CAST(N'2018-04-28T15:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3932, N'USER1\User1', CAST(N'2018-04-29T11:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3933, N'USER1\User1', CAST(N'2018-04-29T15:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3934, N'USER1\User1', CAST(N'2018-04-29T17:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3935, N'USER1\User1', CAST(N'2018-04-29T17:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3936, N'USER1\User1', CAST(N'2018-04-29T19:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3937, N'USER1\User1', CAST(N'2018-04-29T20:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3938, N'USER1\User1', CAST(N'2018-04-29T20:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3939, N'USER1\User1', CAST(N'2018-04-29T21:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3940, N'USER1\User1', CAST(N'2018-04-29T21:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3941, N'USER1\User1', CAST(N'2018-04-29T22:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3942, N'USER1\User1', CAST(N'2018-04-29T22:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3943, N'USER1\User1', CAST(N'2018-04-29T23:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3944, N'USER1\User1', CAST(N'2018-05-02T22:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3945, N'USER1\User1', CAST(N'2018-05-02T23:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3946, N'USER1\User1', CAST(N'2018-05-03T20:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3947, N'USER1\User1', CAST(N'2018-05-03T21:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3948, N'USER1\User1', CAST(N'2018-05-03T22:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3949, N'USER1\User1', CAST(N'2018-05-03T22:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3950, N'USER1\User1', CAST(N'2018-05-03T23:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3951, N'USER1\User1', CAST(N'2018-05-03T23:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3952, N'USER1\User1', CAST(N'2018-05-06T11:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3953, N'USER1\User1', CAST(N'2018-05-06T13:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3954, N'USER1\User1', CAST(N'2018-05-06T14:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3955, N'USER1\User1', CAST(N'2018-05-06T14:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3956, N'USER1\User1', CAST(N'2018-05-06T14:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3957, N'USER1\User1', CAST(N'2018-05-06T16:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3958, N'USER1\User1', CAST(N'2018-05-06T17:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3959, N'USER1\User1', CAST(N'2018-05-06T17:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3960, N'USER1\User1', CAST(N'2018-05-06T17:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3961, N'USER1\User1', CAST(N'2018-05-06T18:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3962, N'USER1\User1', CAST(N'2018-05-06T20:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3963, N'USER1\User1', CAST(N'2018-05-06T20:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3964, N'USER1\User1', CAST(N'2018-05-06T20:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3965, N'USER1\User1', CAST(N'2018-05-06T20:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3966, N'USER1\User1', CAST(N'2018-05-06T21:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3967, N'USER1\User1', CAST(N'2018-05-06T21:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3968, N'USER1\User1', CAST(N'2018-05-06T22:19:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3969, N'USER1\User1', CAST(N'2018-05-06T22:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3970, N'USER1\User1', CAST(N'2018-05-06T23:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3971, N'USER1\User1', CAST(N'2018-05-06T23:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3972, N'USER1\User1', CAST(N'2018-05-07T01:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3973, N'USER1\User1', CAST(N'2018-05-07T02:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3974, N'USER1\User1', CAST(N'2018-05-07T07:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3975, N'USER1\User1', CAST(N'2018-05-07T07:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3976, N'USER1\User1', CAST(N'2018-05-07T08:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3977, N'USER1\User1', CAST(N'2018-05-07T10:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3978, N'USER1\User1', CAST(N'2018-05-07T10:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3979, N'USER1\User1', CAST(N'2018-05-07T11:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3980, N'USER1\User1', CAST(N'2018-05-07T12:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3981, N'USER1\User1', CAST(N'2018-05-07T12:54:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3982, N'USER1\User1', CAST(N'2018-05-07T13:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3983, N'USER1\User1', CAST(N'2018-05-07T13:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3984, N'USER1\User1', CAST(N'2018-05-07T14:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3985, N'USER1\User1', CAST(N'2018-05-07T14:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3986, N'USER1\User1', CAST(N'2018-05-07T15:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3987, N'USER1\User1', CAST(N'2018-05-07T16:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3988, N'USER1\User1', CAST(N'2018-05-07T16:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3989, N'USER1\User1', CAST(N'2018-05-07T19:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3990, N'USER1\User1', CAST(N'2018-05-07T20:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3991, N'USER1\User1', CAST(N'2018-05-07T20:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3992, N'USER1\User1', CAST(N'2018-05-07T21:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3993, N'USER1\User1', CAST(N'2018-05-07T22:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3994, N'USER1\User1', CAST(N'2018-05-07T23:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3995, N'USER1\User1', CAST(N'2018-05-07T23:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3996, N'USER1\User1', CAST(N'2018-05-08T00:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3997, N'USER1\User1', CAST(N'2018-05-08T00:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3998, N'USER1\User1', CAST(N'2018-05-08T00:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (3999, N'USER1\User1', CAST(N'2018-05-08T00:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4000, N'USER1\User1', CAST(N'2018-05-08T00:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4001, N'USER1\User1', CAST(N'2018-05-08T00:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4002, N'USER1\User1', CAST(N'2018-05-08T00:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4003, N'USER1\User1', CAST(N'2018-05-08T19:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4004, N'USER1\User1', CAST(N'2018-05-08T20:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4005, N'USER1\User1', CAST(N'2018-05-09T21:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4006, N'USER1\User1', CAST(N'2018-05-09T22:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4007, N'USER1\User1', CAST(N'2018-05-09T22:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4008, N'USER1\User1', CAST(N'2018-05-09T22:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4009, N'USER1\User1', CAST(N'2018-05-09T23:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4010, N'USER1\User1', CAST(N'2018-05-09T23:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4011, N'USER1\User1', CAST(N'2018-05-09T23:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4012, N'USER1\User1', CAST(N'2018-05-10T05:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4013, N'USER1\User1', CAST(N'2018-05-10T21:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4014, N'USER1\User1', CAST(N'2018-05-10T23:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4015, N'USER1\User1', CAST(N'2018-05-11T19:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4016, N'USER1\User1', CAST(N'2018-05-11T22:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4017, N'USER1\User1', CAST(N'2018-05-12T08:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4018, N'USER1\User1', CAST(N'2018-05-12T10:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4019, N'USER1\User1', CAST(N'2018-05-12T10:39:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4020, N'USER1\User1', CAST(N'2018-05-12T11:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4021, N'USER1\User1', CAST(N'2018-05-12T15:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4022, N'USER1\User1', CAST(N'2018-05-12T18:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4023, N'USER1\User1', CAST(N'2018-05-12T21:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4024, N'USER1\User1', CAST(N'2018-05-12T22:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4025, N'USER1\User1', CAST(N'2018-05-12T23:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4026, N'USER1\User1', CAST(N'2018-05-12T23:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4027, N'USER1\User1', CAST(N'2018-05-13T00:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4028, N'USER1\User1', CAST(N'2018-05-13T00:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4029, N'USER1\User1', CAST(N'2018-05-13T00:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4030, N'USER1\User1', CAST(N'2018-05-13T00:54:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4031, N'USER1\User1', CAST(N'2018-05-13T08:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4032, N'USER1\User1', CAST(N'2018-05-13T15:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4033, N'USER1\User1', CAST(N'2018-05-13T15:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4034, N'USER1\User1', CAST(N'2018-05-13T15:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4035, N'USER1\User1', CAST(N'2018-05-13T15:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4036, N'USER1\User1', CAST(N'2018-05-13T16:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4037, N'USER1\User1', CAST(N'2018-05-13T22:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4038, N'USER1\User1', CAST(N'2018-05-13T22:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4039, N'USER1\User1', CAST(N'2018-05-13T23:35:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4040, N'USER1\User1', CAST(N'2018-05-13T23:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4041, N'USER1\User1', CAST(N'2018-05-15T14:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4042, N'USER1\User1', CAST(N'2018-05-15T14:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4043, N'USER1\User1', CAST(N'2018-05-15T14:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4044, N'USER1\User1', CAST(N'2018-05-16T08:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4045, N'USER1\User1', CAST(N'2018-05-16T10:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4046, N'USER1\User1', CAST(N'2018-05-16T10:44:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4047, N'USER1\User1', CAST(N'2018-05-16T11:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4048, N'USER1\User1', CAST(N'2018-05-16T11:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4049, N'USER1\User1', CAST(N'2018-05-16T11:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4050, N'USER1\User1', CAST(N'2018-05-17T07:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4051, N'USER1\User1', CAST(N'2018-05-17T08:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4052, N'USER1\User1', CAST(N'2018-05-17T08:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4053, N'USER1\User1', CAST(N'2018-05-17T08:10:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4054, N'USER1\User1', CAST(N'2018-05-17T08:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4055, N'USER1\User1', CAST(N'2018-05-17T08:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4056, N'USER1\User1', CAST(N'2018-05-17T08:23:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4057, N'USER1\User1', CAST(N'2018-05-17T08:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4058, N'USER1\User1', CAST(N'2018-05-17T08:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4059, N'USER1\User1', CAST(N'2018-05-17T08:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4060, N'USER1\User1', CAST(N'2018-05-17T08:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4061, N'USER1\User1', CAST(N'2018-05-17T08:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4062, N'USER1\User1', CAST(N'2018-05-17T08:42:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4063, N'USER1\User1', CAST(N'2018-05-17T08:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4064, N'USER1\User1', CAST(N'2018-05-17T08:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4065, N'USER1\User1', CAST(N'2018-05-17T10:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4066, N'USER1\User1', CAST(N'2018-05-17T10:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4067, N'USER1\User1', CAST(N'2018-05-17T10:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4068, N'USER1\User1', CAST(N'2018-05-17T10:26:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4069, N'USER1\User1', CAST(N'2018-05-17T10:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4070, N'USER1\User1', CAST(N'2018-05-17T10:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4071, N'USER1\User1', CAST(N'2018-05-17T11:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4072, N'USER1\User1', CAST(N'2018-05-17T11:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4073, N'USER1\User1', CAST(N'2018-05-17T12:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4074, N'USER1\User1', CAST(N'2018-05-17T12:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4075, N'USER1\User1', CAST(N'2018-05-17T12:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4076, N'USER1\User1', CAST(N'2018-05-17T12:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4077, N'USER1\User1', CAST(N'2018-05-17T12:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4078, N'USER1\User1', CAST(N'2018-05-17T12:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4079, N'USER1\User1', CAST(N'2018-05-17T12:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4080, N'USER1\User1', CAST(N'2018-05-17T12:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4081, N'USER1\User1', CAST(N'2018-05-17T13:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4082, N'USER1\User1', CAST(N'2018-05-17T13:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4083, N'USER1\User1', CAST(N'2018-05-17T15:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4084, N'USER1\User1', CAST(N'2018-05-17T15:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4085, N'USER1\User1', CAST(N'2018-05-17T16:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4086, N'USER1\User1', CAST(N'2018-05-18T06:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4087, N'USER1\User1', CAST(N'2018-05-18T06:58:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4088, N'USER1\User1', CAST(N'2018-05-18T19:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4089, N'USER1\User1', CAST(N'2018-05-18T22:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4090, N'USER1\User1', CAST(N'2018-05-18T22:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4091, N'USER1\User1', CAST(N'2018-05-18T23:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4092, N'USER1\User1', CAST(N'2018-05-18T23:49:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4093, N'USER1\User1', CAST(N'2018-05-19T00:04:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4094, N'USER1\User1', CAST(N'2018-05-19T00:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4095, N'USER1\User1', CAST(N'2018-05-19T00:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4096, N'USER1\User1', CAST(N'2018-05-19T00:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4097, N'USER1\User1', CAST(N'2018-05-19T01:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4098, N'USER1\User1', CAST(N'2018-05-19T01:20:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4099, N'USER1\User1', CAST(N'2018-05-19T01:27:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4100, N'USER1\User1', CAST(N'2018-05-19T01:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4101, N'USER1\User1', CAST(N'2018-05-19T10:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4102, N'USER1\User1', CAST(N'2018-05-19T10:54:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4103, N'USER1\User1', CAST(N'2018-05-19T11:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4104, N'USER1\User1', CAST(N'2018-05-19T14:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4105, N'USER1\User1', CAST(N'2018-05-19T17:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4106, N'USER1\User1', CAST(N'2018-05-19T20:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4107, N'USER1\User1', CAST(N'2018-05-19T21:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4108, N'USER1\User1', CAST(N'2018-05-19T21:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4109, N'USER1\User1', CAST(N'2018-05-19T21:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4110, N'USER1\User1', CAST(N'2018-05-19T21:54:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4111, N'USER1\User1', CAST(N'2018-05-19T21:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4112, N'USER1\User1', CAST(N'2018-05-19T22:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4113, N'USER1\User1', CAST(N'2018-05-19T22:31:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4114, N'USER1\User1', CAST(N'2018-05-19T22:53:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4115, N'USER1\User1', CAST(N'2018-05-19T23:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4116, N'USER1\User1', CAST(N'2018-05-20T07:25:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4117, N'USER1\User1', CAST(N'2018-05-20T08:29:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4118, N'USER1\User1', CAST(N'2018-05-20T08:32:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4119, N'USER1\User1', CAST(N'2018-05-20T08:46:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4120, N'USER1\User1', CAST(N'2018-05-20T09:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4121, N'USER1\User1', CAST(N'2018-05-20T09:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4122, N'USER1\User1', CAST(N'2018-05-20T09:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4123, N'USER1\User1', CAST(N'2018-05-20T09:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4124, N'USER1\User1', CAST(N'2018-05-20T09:52:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4125, N'USER1\User1', CAST(N'2018-05-20T10:02:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4126, N'USER1\User1', CAST(N'2018-05-20T10:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4127, N'USER1\User1', CAST(N'2018-05-20T10:50:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4128, N'USER1\User1', CAST(N'2018-05-20T11:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4129, N'USER1\User1', CAST(N'2018-05-20T11:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4130, N'USER1\User1', CAST(N'2018-05-20T12:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4131, N'USER1\User1', CAST(N'2018-05-20T12:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4132, N'USER1\User1', CAST(N'2018-05-20T12:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4133, N'USER1\User1', CAST(N'2018-05-20T13:15:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4134, N'USER1\User1', CAST(N'2018-05-20T13:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4135, N'USER1\User1', CAST(N'2018-05-20T13:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4136, N'USER1\User1', CAST(N'2018-05-20T14:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4137, N'USER1\User1', CAST(N'2018-05-20T14:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4138, N'USER1\User1', CAST(N'2018-05-20T14:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4139, N'USER1\User1', CAST(N'2018-05-20T15:47:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4140, N'USER1\User1', CAST(N'2018-05-20T16:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4141, N'USER1\User1', CAST(N'2018-05-20T17:24:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4142, N'USER1\User1', CAST(N'2018-05-20T18:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4143, N'USER1\User1', CAST(N'2018-05-20T19:05:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4144, N'USER1\User1', CAST(N'2018-05-20T20:00:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4145, N'USER1\User1', CAST(N'2018-05-20T20:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4146, N'USER1\User1', CAST(N'2018-05-20T20:26:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4147, N'USER1\User1', CAST(N'2018-05-20T20:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4148, N'USER1\User1', CAST(N'2018-05-20T20:45:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4149, N'USER1\User1', CAST(N'2018-05-20T21:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4150, N'USER1\User1', CAST(N'2018-05-20T21:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4151, N'USER1\User1', CAST(N'2018-05-20T21:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4152, N'USER1\User1', CAST(N'2018-05-20T23:13:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4153, N'USER1\User1', CAST(N'2018-05-20T23:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4154, N'USER1\User1', CAST(N'2018-05-20T23:57:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4155, N'USER1\User1', CAST(N'2018-05-21T09:14:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4156, N'USER1\User1', CAST(N'2018-05-21T10:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4157, N'USER1\User1', CAST(N'2018-05-21T10:40:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4158, N'USER1\User1', CAST(N'2018-05-21T10:55:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4159, N'USER1\User1', CAST(N'2018-05-21T11:56:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4160, N'USER1\User1', CAST(N'2018-05-21T12:08:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4161, N'USER1\User1', CAST(N'2018-05-21T21:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4162, N'USER1\User1', CAST(N'2018-05-22T10:03:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4163, N'USER1\User1', CAST(N'2018-05-22T10:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4164, N'USER1\User1', CAST(N'2018-05-22T11:48:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4165, N'USER1\User1', CAST(N'2018-05-23T09:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4166, N'USER1\User1', CAST(N'2018-05-23T09:21:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4167, N'USER1\User1', CAST(N'2018-05-23T09:30:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4168, N'USER1\User1', CAST(N'2018-05-23T11:19:00' AS SmallDateTime))
GO
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4169, N'USER1\User1', CAST(N'2018-05-23T12:16:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4170, N'USER1\User1', CAST(N'2018-05-23T15:22:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4171, N'USER1\User1', CAST(N'2018-05-23T18:33:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4172, N'USER1\User1', CAST(N'2018-05-23T19:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4173, N'USER1\User1', CAST(N'2018-05-23T21:12:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4174, N'USER1\User1', CAST(N'2018-05-23T21:59:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4175, N'USER1\User1', CAST(N'2018-05-24T00:18:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4176, N'USER1\User1', CAST(N'2018-05-24T02:41:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4177, N'USER1\User1', CAST(N'2018-05-24T04:07:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4178, N'USER1\User1', CAST(N'2018-05-24T04:19:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4179, N'USER1\User1', CAST(N'2018-05-24T06:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4180, N'USER1\User1', CAST(N'2018-05-24T09:17:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4181, N'USER1\User1', CAST(N'2018-05-24T09:54:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4182, N'USER1\User1', CAST(N'2018-05-24T10:34:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4183, N'USER1\User1', CAST(N'2018-05-24T10:38:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4184, N'USER1\User1', CAST(N'2018-05-24T10:43:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4185, N'USER1\User1', CAST(N'2018-05-24T11:36:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4186, N'USER1\User1', CAST(N'2018-05-24T12:28:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4187, N'USER1\User1', CAST(N'2018-05-24T12:37:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4188, N'USER1\User1', CAST(N'2018-05-24T13:51:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4189, N'USER1\User1', CAST(N'2018-05-24T22:01:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4190, N'USER1\User1', CAST(N'2018-05-24T22:06:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4191, N'USER1\User1', CAST(N'2018-05-24T22:09:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4192, N'USER1\User1', CAST(N'2018-05-24T22:11:00' AS SmallDateTime))
INSERT [dbo].[tUserAccess] ([ID], [DomainName], [LastAccess]) VALUES (4193, N'USER1\User1', CAST(N'2018-05-24T22:34:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[tUserAccess] OFF
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF_tAssistance]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tAssistance] ADD  CONSTRAINT [DF_tAssistance]  DEFAULT (getdate()) FOR [LastUpdated]
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tBarrier_tEncounter]') AND parent_object_id = OBJECT_ID(N'[dbo].[tBarrier]'))
ALTER TABLE [dbo].[tBarrier]  WITH CHECK ADD  CONSTRAINT [FK_tBarrier_tEncounter] FOREIGN KEY([FK_EncounterID])
REFERENCES [dbo].[tEncounter] ([EncounterID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tBarrier_tEncounter]') AND parent_object_id = OBJECT_ID(N'[dbo].[tBarrier]'))
ALTER TABLE [dbo].[tBarrier] CHECK CONSTRAINT [FK_tBarrier_tEncounter]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tCaregiverApprovals_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tCaregiverApprovals]'))
ALTER TABLE [dbo].[tCaregiverApprovals]  WITH CHECK ADD  CONSTRAINT [FK_tCaregiverApprovals_tServiceRecipient] FOREIGN KEY([FK_NSRecipientID])
REFERENCES [dbo].[tServiceRecipient] ([NSRecipientID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tCaregiverApprovals_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tCaregiverApprovals]'))
ALTER TABLE [dbo].[tCaregiverApprovals] CHECK CONSTRAINT [FK_tCaregiverApprovals_tServiceRecipient]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tEncounter_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tEncounter]'))
ALTER TABLE [dbo].[tEncounter]  WITH CHECK ADD  CONSTRAINT [FK_tEncounter_tNavigator] FOREIGN KEY([FK_NavigatorID])
REFERENCES [dbo].[tNavigator] ([NavigatorID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tEncounter_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tEncounter]'))
ALTER TABLE [dbo].[tEncounter] CHECK CONSTRAINT [FK_tEncounter_tNavigator]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tEncounter_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tEncounter]'))
ALTER TABLE [dbo].[tEncounter]  WITH CHECK ADD  CONSTRAINT [FK_tEncounter_tServiceRecipient] FOREIGN KEY([FK_RecipientID])
REFERENCES [dbo].[tServiceRecipient] ([NSRecipientID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tEncounter_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tEncounter]'))
ALTER TABLE [dbo].[tEncounter] CHECK CONSTRAINT [FK_tEncounter_tServiceRecipient]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecActivityParticipantInfo_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecActivityParticipantInfo]'))
ALTER TABLE [dbo].[tHecActivityParticipantInfo]  WITH CHECK ADD  CONSTRAINT [FK_tHecActivityParticipantInfo_tHecActivities] FOREIGN KEY([ActivityId])
REFERENCES [dbo].[tHecActivities] ([ActivityId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecActivityParticipantInfo_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecActivityParticipantInfo]'))
ALTER TABLE [dbo].[tHecActivityParticipantInfo] CHECK CONSTRAINT [FK_tHecActivityParticipantInfo_tHecActivities]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecScreeningEvent_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecScreeningEvent]'))
ALTER TABLE [dbo].[tHecScreeningEvent]  WITH CHECK ADD  CONSTRAINT [FK_tHecScreeningEvent_tHecActivities] FOREIGN KEY([ActivityId])
REFERENCES [dbo].[tHecActivities] ([ActivityId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecScreeningEvent_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecScreeningEvent]'))
ALTER TABLE [dbo].[tHecScreeningEvent] CHECK CONSTRAINT [FK_tHecScreeningEvent_tHecActivities]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tInstantMessage_tNavigator_rcvdBy]') AND parent_object_id = OBJECT_ID(N'[dbo].[tInstantMessage]'))
ALTER TABLE [dbo].[tInstantMessage]  WITH CHECK ADD  CONSTRAINT [FK_tInstantMessage_tNavigator_rcvdBy] FOREIGN KEY([ReceivedBy])
REFERENCES [dbo].[tNavigator] ([NavigatorID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tInstantMessage_tNavigator_rcvdBy]') AND parent_object_id = OBJECT_ID(N'[dbo].[tInstantMessage]'))
ALTER TABLE [dbo].[tInstantMessage] CHECK CONSTRAINT [FK_tInstantMessage_tNavigator_rcvdBy]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tInstantMessage_tNavigator_RcvdFrom]') AND parent_object_id = OBJECT_ID(N'[dbo].[tInstantMessage]'))
ALTER TABLE [dbo].[tInstantMessage]  WITH CHECK ADD  CONSTRAINT [FK_tInstantMessage_tNavigator_RcvdFrom] FOREIGN KEY([ReceivedFrom])
REFERENCES [dbo].[tNavigator] ([NavigatorID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tInstantMessage_tNavigator_RcvdFrom]') AND parent_object_id = OBJECT_ID(N'[dbo].[tInstantMessage]'))
ALTER TABLE [dbo].[tInstantMessage] CHECK CONSTRAINT [FK_tInstantMessage_tNavigator_RcvdFrom]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNavigationCycle]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle]  WITH CHECK ADD  CONSTRAINT [FK_tNavigationCycle_tNavigationCycle] FOREIGN KEY([NavigationCycleID])
REFERENCES [dbo].[tNavigationCycle] ([NavigationCycleID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNavigationCycle]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle] CHECK CONSTRAINT [FK_tNavigationCycle_tNavigationCycle]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle]  WITH CHECK ADD  CONSTRAINT [FK_tNavigationCycle_tNavigator] FOREIGN KEY([FK_NavigatorID])
REFERENCES [dbo].[tNavigator] ([NavigatorID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle] CHECK CONSTRAINT [FK_tNavigationCycle_tNavigator]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNSProvider]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle]  WITH CHECK ADD  CONSTRAINT [FK_tNavigationCycle_tNSProvider] FOREIGN KEY([FK_ProviderID])
REFERENCES [dbo].[tNSProvider] ([NSProviderID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tNSProvider]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle] CHECK CONSTRAINT [FK_tNavigationCycle_tNSProvider]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle]  WITH CHECK ADD  CONSTRAINT [FK_tNavigationCycle_tServiceRecipient] FOREIGN KEY([FK_RecipientID])
REFERENCES [dbo].[tServiceRecipient] ([NSRecipientID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigationCycle_tServiceRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigationCycle]'))
ALTER TABLE [dbo].[tNavigationCycle] CHECK CONSTRAINT [FK_tNavigationCycle_tServiceRecipient]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigatorTraining_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigatorTraining]'))
ALTER TABLE [dbo].[tNavigatorTraining]  WITH CHECK ADD  CONSTRAINT [FK_tNavigatorTraining_tNavigator] FOREIGN KEY([FK_NavigatorID])
REFERENCES [dbo].[tNavigator] ([NavigatorID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tNavigatorTraining_tNavigator]') AND parent_object_id = OBJECT_ID(N'[dbo].[tNavigatorTraining]'))
ALTER TABLE [dbo].[tNavigatorTraining] CHECK CONSTRAINT [FK_tNavigatorTraining_tNavigator]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tSolution_tBarrier]') AND parent_object_id = OBJECT_ID(N'[dbo].[tSolution]'))
ALTER TABLE [dbo].[tSolution]  WITH CHECK ADD  CONSTRAINT [FK_tSolution_tBarrier] FOREIGN KEY([FK_BarrierID])
REFERENCES [dbo].[tBarrier] ([BarrierID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tSolution_tBarrier]') AND parent_object_id = OBJECT_ID(N'[dbo].[tSolution]'))
ALTER TABLE [dbo].[tSolution] CHECK CONSTRAINT [FK_tSolution_tBarrier]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tSystemMessage_tNavigator_rcvdBy]') AND parent_object_id = OBJECT_ID(N'[dbo].[tSystemMessage]'))
ALTER TABLE [dbo].[tSystemMessage]  WITH CHECK ADD  CONSTRAINT [FK_tSystemMessage_tNavigator_rcvdBy] FOREIGN KEY([ReceivedBy])
REFERENCES [dbo].[tNavigator] ([NavigatorID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tSystemMessage_tNavigator_rcvdBy]') AND parent_object_id = OBJECT_ID(N'[dbo].[tSystemMessage]'))
ALTER TABLE [dbo].[tSystemMessage] CHECK CONSTRAINT [FK_tSystemMessage_tNavigator_rcvdBy]
GO
/****** Object:  StoredProcedure [dbo].[GetCaregiverApproval]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCaregiverApproval]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetCaregiverApproval] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 07/13/2016
-- Description:	Get caregiver approval values for a NS Recipient
-- =============================================
ALTER PROCEDURE [dbo].[GetCaregiverApproval] 
	@NSRecipientID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT A.[ApprovalID]
		  ,A.[FK_NSRecipientID]
		  ,A.[CaregiverApproval]
		  ,CA.CaregiverApprovalDescription
		  ,A.[CaregiverApprovalOther]
		  ,A.[xtag]
		  ,A.[DateCreated]
		  ,A.[CreatedBy]
		  ,A.[LastUpdated]
		  ,A.[UpdatedBy]
	  FROM [dbo].[tCaregiverApprovals] A
	  LEFT JOIN trCaregiverApproval CA
	  ON A.CaregiverApproval = CA.CaregiverApprovalCode
	  WHERE FK_NSRecipientID = @NSRecipientID



    
END

/****** Object:  StoredProcedure [dbo].[usp_SelectNSProvider]    Script Date: 7/15/2016 10:38:55 AM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteHECActivity]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DeleteHECActivity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_DeleteHECActivity] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_DeleteHECActivity]
@ActivityId int
AS
BEGIN
 IF(@ActivityId<>Null)
 Begin
	DELETE fROM tHecScreeningEvent WHERE ActivityId = @ActivityId
	DELETE FROM tHecActivityParticipantInfo WHERE ActivityId = @ActivityId
	DELETE FROM tHecActivities WHERE ActivityId = @ActivityId
 End
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteSolutions]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DeleteSolutions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_DeleteSolutions] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 08/17/2016
-- Description:	Delete all Solutions for a Barrier
-- =============================================
ALTER PROCEDURE [dbo].[usp_DeleteSolutions] 
	@FK_BarrierID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM tSolution WHERE FK_BarrierID = @FK_BarrierID

END


/****** Object:  StoredProcedure [dbo].[usp_InsertEncounter]    Script Date: 8/19/2016 11:51:21 AM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_GetAllHECActivities]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAllHECActivities]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetAllHECActivities] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_GetAllHECActivities]
AS
BEGIN

SELECT hecmaster.Address1 as HECAddress1,hecmaster.Address2 as HECAddress2,hecmaster.Zip as activityZip, hecmaster.*, hecpartInfo.Address1 ParticipantAddress1,hecpartInfo.Address2 ParticipantAddress2,
hecpartInfo.Zip as partZip, hecpartInfo.*,hecscrnevent.* FROM tHecActivities hecmaster
left outer join tHecActivityParticipantInfo hecpartInfo on hecmaster.ActivityId = hecpartInfo.ActivityId
left outer join tHecScreeningEvent hecscrnevent on hecmaster.ActivityId = hecscrnevent.ActivityId


END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAssistance]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAssistance]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetAssistance] AS' 
END
GO

-- =====================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to get Assistance using AssistanceID
-- Edited by Bilwa Buchake: Added FK_NavigatorID column
-- =====================================================================
ALTER PROCEDURE [dbo].[usp_GetAssistance] 
	-- Add the parameters for the stored procedure here
	@AssistanceID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [AssistanceID] ,
	[FK_NavigatorID] ,
	[IncidentNumber] ,
	[IncidentDt] ,
	[FK_CommunicationCode] ,
	[FirstName] ,
	[Lastname] ,
	[Telephone] ,
	[Email] ,
	[RecipID] ,
	[FK_RequestorCode] ,
	[NPI] ,
	[NPIOwner] ,
	[NPISvcLoc] ,
	[BusinessName] ,
	[DateCreated] ,
	[CreatedBy] ,
	[LastUpdated] ,
	[UpdatedBy],
	[ZipCode],
	[Other] 
FROM [dbo].[tAssistance]
	  WHERE [AssistanceID] = @AssistanceID

END


SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_GetAssistanceIncidentNo]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAssistanceIncidentNo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetAssistanceIncidentNo] AS' 
END
GO
ALTER Procedure [dbo].[usp_GetAssistanceIncidentNo]
As
Declare @AssistanceIncidentNumber varchar(10)
Begin
 IF NOT EXISTS(SELECT 1 FROM tAssistance)
	set @AssistanceIncidentNumber='AST0000001'
 ELSE
 Begin
	SELECT @AssistanceIncidentNumber= RIGHT('AST000000'+CAST(max(AssistanceID)+ 1 AS VARCHAR(10)),10) from tAssistance
 End
 Select @AssistanceIncidentNumber AssistanceIncidentNumber

End



GO
/****** Object:  StoredProcedure [dbo].[usp_GetAssistanceIssues]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAssistanceIssues]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetAssistanceIssues] AS' 
END
GO


-- =====================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to get Assistance using IncidentNumber
-- =====================================================================
ALTER PROCEDURE [dbo].[usp_GetAssistanceIssues] 
	-- Add the parameters for the stored procedure here
	@AssistanceID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [IssueID] ,
	[FK_AssistanceID] ,
	[IncidentNumber] ,
	[FK_EWCRecipientCode] ,
	[FK_EWCPCPCode] ,
	[FK_ReferralProviderCode] ,
	[FK_EWCEnrolleeCode] ,
	[OtherIssues] ,
	[DateCreated] ,
	[CreatedBy] ,
	[LastUpdated] ,
	[UpdatedBy],
	[ProblemOtherIssue]	 
FROM [dbo].[tAssistanceIssues]
	  WHERE FK_AssistanceID = @AssistanceID

END





GO
/****** Object:  StoredProcedure [dbo].[usp_GetAssistanceResolution]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAssistanceResolution]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetAssistanceResolution] AS' 
END
GO



-- ==============================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to get Assistance resolution using [ResolutionID]
-- ==============================================================================
ALTER PROCEDURE [dbo].[usp_GetAssistanceResolution] 
	-- Add the parameters for the stored procedure here
	@AssistanceID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [ResolutionID] ,
	[FK_AssistanceID] ,
	[IncidentNumber] ,
	[ResolutionInitiationDt] ,
	[FK_ResolutionCode] ,
	[ResolutionEndDt] ,
	[OtherResolution] ,
	[DateCreated] ,
	[CreatedBy] ,
	[LastUpdated] ,
	[UpdatedBy],
	[UnabletoResolve] 
FROM [dbo].[tAssistanceResolution]
	  WHERE FK_AssistanceID =@AssistanceID 

END






GO
/****** Object:  StoredProcedure [dbo].[usp_GetBarrier]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetBarrier]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetBarrier] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 06/20/2016
-- Description:	Stored Procedure to get Encounter using EncounterID
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetBarrier] 
	-- Add the parameters for the stored procedure here
	@BarrierID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [BarrierID]
		  ,[FK_EncounterID]
		  ,[BarrierType]
		  ,[BarrierAssessed]
		  ,[FollowUpDt]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
		  ,[Other]
	  FROM [dbo].[tBarrier]
	  WHERE BarrierID = @BarrierID

END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetEncounter]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetEncounter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetEncounter] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 06/20/2016
-- Description:	Stored Procedure to get Encounter using EncounterID
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetEncounter] 
	-- Add the parameters for the stored procedure here
	@EncounterID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [EncounterID]
		  ,[FK_NavigatorID]
		  ,[FK_RecipientID]
		  ,[EncounterNumber]
		  ,[EncounterDt]
		  ,[EncounterStartTime]
		  ,[EncounterEndTime]
		  ,[EncounterType]
		  ,[EncounterReason]
		  ,[MissedAppointment]
		  ,[SvcOutcomeStatus]
		  ,[SvcOutcomeStatusReason]
		  ,[NextAppointmentDt]
		  ,[NextAppointmentTime]
		  ,[NextAppointmentType]
		  ,[ServicesTerminated]
		  ,[ServicesTerminatedDt]
		  ,[ServicesTerminatedReason]
		  ,[Notes]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
		  ,[EncounterPurpose]
	  FROM [dbo].[tEncounter]
	  WHERE EncounterID = @EncounterID

END


/****** Object:  StoredProcedure [dbo].[usp_InsertEncounter]    Script Date: 9/14/2016 9:30:04 AM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_GetHECActivity]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetHECActivity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetHECActivity] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_GetHECActivity]
@ActivityId int
AS
BEGIN

IF(@ActivityId>0)
Begin
SELECT hecmaster.City as HECCity,hecmaster.ActivityId as HECActivityId, hecmaster.Address1 as HECAddress1,hecmaster.Address2 as HECAddress2,hecmaster.Zip as activityZip,
 hecmaster.*, hecpartInfo.Address1 ParticipantAddress1,hecpartInfo.Address2 ParticipantAddress2, hecpartInfo.*,
 hecpartInfo.Zip as partZip,hecpartInfo.city as participantCity,hecscrnevent.* FROM tHecActivities hecmaster
left outer join tHecActivityParticipantInfo hecpartInfo on hecmaster.ActivityId = hecpartInfo.ActivityId
left outer join tHecScreeningEvent hecscrnevent on hecmaster.ActivityId = hecscrnevent.ActivityId
where hecmaster.ActivityId = @ActivityId
END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetInstanceMessageDetails]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetInstanceMessageDetails]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetInstanceMessageDetails] AS' 
END
GO
ALTER procedure [dbo].[usp_GetInstanceMessageDetails]
@messageId int
As
Begin

 Select Id,Subject,Message,ReceivedOn,ReceivedFrom,ReceivedBy,IsRead,
	nav1.LastName+','+nav1.FirstName  as SenderName,
	nav2.LastName+','+nav2.FirstName  as ReceiverName
	 from 
	tInstantMessage im
	inner join tNavigator nav1 on nav1.NavigatorID = im.ReceivedFrom
	inner join tNavigator nav2 on nav2.NavigatorID = im.ReceivedBy
	 where Id = @messageId


End
GO
/****** Object:  StoredProcedure [dbo].[usp_GetLanguagesList]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetLanguagesList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetLanguagesList] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 12/01/2016
-- Description:	Get a list of languages from the NavigatorLanguages table
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetLanguagesList]
	@FK_NavigatorID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- Insert statements for procedure here

	SELECT [NavigatorLanguageID]
      ,[FK_NavigatorID]
      ,L.[LanguageCode]
	  ,L.[LanguageName]
      ,[SpeakingScore]
      ,[ListeningScore]
      ,[ReadingScore]
      ,[WritingScore]
      ,[xtag]
      ,[DateCreated]
      ,[CreatedBy]
      ,[LastUpdated]
      ,[UpdatedBy]
	FROM [EWC_NSAA].[dbo].[tNavigatorLanguages] N
	JOIN trProvLanguages L
	ON N.LanguageCode = L.LanguageCode
	  WHERE FK_NavigatorID = @FK_NavigatorID
	  
END


SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_GetNavigationCycle]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetNavigationCycle]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetNavigationCycle] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 06/21/2016
-- Description:	Stored Procedure to get Navigation Cycle using NavigationCycleID
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetNavigationCycle]
	@mode int,
	@NavigationCycleID int,
	@NSRecipientID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if @mode = 0
	begin
		SELECT [NavigationCycleID]
			  ,[FK_RecipientID]
			  ,[FK_ProviderID]
			  ,[FK_ReferralID]
			  ,[CancerSite]
			  ,[NSProblem]
			  ,[HealthProgram]
			  ,[HealthInsuranceStatus]
			  ,[HealthInsurancePlan]
			  ,[HealthInsurancePlanNumber]
			  ,[ContactPrefDays]
			  ,[ContactPrefHours]
			  ,[ContactPrefHoursOther]
			  ,[ContactPrefType]
			  ,[SRIndentifierCodeGenerated]
			  ,[FK_NavigatorID]
			  ,[xtag]
			  ,[DateCreated]
			  ,[CreatedBy]
			  ,[LastUpdated]
			  ,[UpdatedBy]
		  FROM [dbo].[tNavigationCycle]
		  WHERE NavigationCycleID = @NavigationCycleID
	  end

	  if @mode = 1
	begin
		SELECT [NavigationCycleID]
			  ,[FK_RecipientID]
			  ,[FK_ProviderID]
			  ,[FK_ReferralID]
			  ,[CancerSite]
			  ,[NSProblem]
			  ,[HealthProgram]
			  ,[HealthInsuranceStatus]
			  ,[HealthInsurancePlan]
			  ,[HealthInsurancePlanNumber]
			  ,[ContactPrefDays]
			  ,[ContactPrefHours]
			  ,[ContactPrefHoursOther]
			  ,[ContactPrefType]
			  ,[SRIndentifierCodeGenerated]
			  ,[FK_NavigatorID]
			  ,[xtag]
			  ,[DateCreated]
			  ,[CreatedBy]
			  ,[LastUpdated]
			  ,[UpdatedBy]
		  FROM [dbo].[tNavigationCycle]
		  WHERE FK_RecipientID = @NSRecipientID
	  end

END


/****** Object:  StoredProcedure [dbo].[usp_RecipientReport]    Script Date: 11/2/2016 3:01:55 PM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_GetRace]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetRace]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetRace] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 03/28/2016
-- Description:	Get race values for a NS Recipient
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetRace] 
	@NSRecipientID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT R.[RaceID]
      ,R.[FK_NSRecipientID]
      ,R.[RaceCode]
      ,RR.[RaceName]
      ,R.[RaceOther]
      ,R.[xtag]
      ,R.[DateCreated]
      ,R.[CreatedBy]
      ,R.[LastUpdated]
      ,R.[UpdatedBy]
  FROM [EWC_NSAA].[dbo].[tRace] R
  LEFT JOIN trRace RR
  ON R.RaceCode = RR.RaceCode
  WHERE FK_NSRecipientID = @NSRecipientID

    
END


/****** Object:  StoredProcedure [dbo].[usp_SelectCaregiverApproval]    Script Date: 7/12/2016 1:57:58 PM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_GetRecipient]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetRecipient]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetRecipient] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 06/20/2016
-- Description:	Stored Procedure to get Service Recipient using NSID
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetRecipient]
	@NSRecipientID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [NSRecipientID]
		  ,[FK_NavigatorID]
		  ,[FK_RecipID]
		  ,[LastName]
		  ,[FirstName]
		  ,[MiddleInitial]
		  ,[DOB]
		  ,[AddressNotAvailable]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[Zip]
		  ,[Phone1]
		  ,[P1PhoneType]
		  ,[P1PersonalMessage]
		  ,[Phone2]
		  ,[P2PhoneType]
		  ,[P2PhoneTypeOther]
		  ,[P2PersonalMessage]
		  ,[MotherMaidenName]
		  ,[Email]
		  ,[SSN]
		  ,[ImmigrantStatus]
		  ,[CountryOfOrigin]
		  ,[Gender]
		  ,[Ethnicity]
		  ,[PrimaryLanguage]
		  ,[AgreeToSpeakEnglish]
		  ,[CaregiverName]
		  ,[Relationship]
		  ,[RelationshipOther]
		  ,[CaregiverPhone]
		  ,[CaregiverPhoneType]
		  ,[CaregiverPhonePersonalMessage]
		  ,[CaregiverEmail]
		  ,[CaregiverContactPreference]
		  ,[Comments]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
	  FROM [dbo].[tServiceRecipient]
	  WHERE NSRecipientID = @NSRecipientID

END


GO
/****** Object:  StoredProcedure [dbo].[usp_GetScreeningNav]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetScreeningNav]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetScreeningNav] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 08/01/2017
-- Description:	Stored Procedure to get list of Screening Nav using FK_NavigatorID
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetScreeningNav]
	@FK_NavigatorID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT [SN_ID]
      ,[FK_NavigatorID]
      ,[SN_CODE1]
      ,[SN_CODE2]
      ,[LastName]
      ,[FirstName]
      ,[DOB]
      ,[Address1]
      ,[Address2]
      ,[City]
      ,[State]
      ,[Zip]
      ,[HomeTelephone]
      ,[Cellphone]
      ,[Email]
      ,[Computer]
      ,[TextMessage]
      ,[DateOfContact1]
      ,[ApptScreen1]
      ,[SvcDate1]
      ,[SvcType1]
      ,[Response1]
      ,[DateOfContact2]
      ,[ApptScreen2]
      ,[SvcDate2]
      ,[SvcType2]
      ,[Response2]
      ,[NSEnrollment]
      ,[EnrollmentDate]
      ,[xtag]
      ,[DateCreated]
      ,[CreatedBy]
      ,[LastUpdated]
      ,[UpdatedBy]
  FROM [dbo].[tScreeningNav]
  WHERE FK_NavigatorID = @FK_NavigatorID
  ORDER BY SN_ID DESC



END



GO
/****** Object:  StoredProcedure [dbo].[usp_GetSystemMessageDetails]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetSystemMessageDetails]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetSystemMessageDetails] AS' 
END
GO
ALTER procedure [dbo].[usp_GetSystemMessageDetails]
@messageId int
As
Begin

Select * from tSystemMessage where Id = @messageId


End
GO
/****** Object:  StoredProcedure [dbo].[usp_GetTrainingsList]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetTrainingsList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetTrainingsList] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 11/15/2016
-- Description:	get list of Navigator Trainings
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetTrainingsList]
	@FK_NavigatorID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [TrainingID]
		  ,[FK_NavigatorID]
		  ,[CourseName]
		  ,[Organization]
		  ,[CompletionDt]
		  ,[Certificate]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
	  FROM [dbo].[tNavigatorTraining]
	  WHERE FK_NavigatorID = @FK_NavigatorID


 
END

/****** Object:  StoredProcedure [dbo].[usp_InsertEncounter]    Script Date: 11/17/2016 1:19:25 PM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserInstanceMessages]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetUserInstanceMessages]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetUserInstanceMessages] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_GetUserInstanceMessages] 
	-- Add the parameters for the stored procedure here
	@NavigatorId int
AS
BEGIN
	
	SET NOCOUNT ON;

	Select Id,Subject,Message,ReceivedOn,ReceivedFrom,ReceivedBy,IsRead,
	nav1.LastName+','+nav1.FirstName  as SenderName,
	nav2.LastName+','+nav2.FirstName  as ReceiverName
	 from 
	tInstantMessage im
	inner join tNavigator nav1 on nav1.NavigatorID = im.ReceivedFrom
	inner join tNavigator nav2 on nav2.NavigatorID = im.ReceivedBy
	where ReceivedBy=@NavigatorId
	order by im.ReceivedOn desc
	
	

END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserSendInstanceMessages]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetUserSendInstanceMessages]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetUserSendInstanceMessages] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_GetUserSendInstanceMessages] 
	-- Add the parameters for the stored procedure here
	@NavigatorId int
AS
BEGIN
	
	SET NOCOUNT ON;

	Select Id,Subject,Message,ReceivedOn,ReceivedFrom,ReceivedBy,IsRead,
	nav1.LastName+','+nav1.FirstName as SenderName,
	nav2.LastName+','+nav2.FirstName as ReceiverName
	 from 
	tInstantMessage im
	inner join tNavigator nav1 on nav1.NavigatorID = im.ReceivedFrom
	inner join tNavigator nav2 on nav2.NavigatorID = im.ReceivedBy
	where ReceivedFrom=@NavigatorId  
	order by im.ReceivedOn desc

END


GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserSystemInstanceMessages]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetUserSystemInstanceMessages]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetUserSystemInstanceMessages] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_GetUserSystemInstanceMessages] 
	-- Add the parameters for the stored procedure here
	@NavigatorId varchar(150)
AS
BEGIN
	
	SET NOCOUNT ON;

	Select Id,Subject,Message,ReceivedOn,ReceivedBy,IsRead from 
	tSystemMessage where ReceivedBy=@NavigatorId  

	

END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertAssistance]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertAssistance]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertAssistance] AS' 
END
GO

-- ======================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to insert Assistance 
-- Edited by Bilwa Buchake: Added FK_NavigatorID column
-- ======================================================
ALTER PROCEDURE [dbo].[usp_InsertAssistance] 
	-- Add the parameters for the stored procedure here
	@AssistanceID int OUTPUT,
	@FK_NavigatorID int,
	@IncidentNumber char(10),
	@IncidentDt smalldatetime ,
	@FK_CommunicationCode smallint ,
	@FirstName varchar(20) ,
	@Lastname varchar(30) ,
	@Telephone char(10) ,
	@Email varchar(45) ,
	@ZipCode varchar(10),
	@RecipID char(14) ,
	@FK_RequestorCode smallint ,
	@NPI varchar(10) ,
	@NPIOwner varchar(2) ,
	@NPISvcLoc varchar(3) ,
	@BusinessName varchar(100),
	@Other varchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [EWC_NSAA].[dbo].[tAssistance] (
	[FK_NavigatorID],
	[IncidentNumber] ,
	[IncidentDt] ,
	[FK_CommunicationCode] ,
	[FirstName] ,
	[Lastname] ,
	[Telephone] ,
	[Email] ,
	[ZipCode],
	[RecipID] ,
	[FK_RequestorCode] ,
	[NPI] ,
	[NPIOwner] ,
	[NPISvcLoc] ,
	[BusinessName] ,
	[Other],
	[DateCreated],
	[CreatedBy],
	[LastUpdated],
	[UpdatedBy]
	)

VALUES
  (
	@FK_NavigatorID , 
  	@IncidentNumber ,
	@IncidentDt  ,
	@FK_CommunicationCode  ,
	@FirstName  ,
	@Lastname  ,
	@Telephone  ,
	@Email  ,
	@ZipCode,
	@RecipID  ,
	@FK_RequestorCode  ,
	@NPI ,
	@NPIOwner  ,
	@NPISvcLoc  ,
	@BusinessName,
	@Other,
	getdate() ,
	'User input' ,
	getdate() ,
	NULL) 

	SELECT @AssistanceID = SCOPE_IDENTITY()

END

SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertAssistanceIssues]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertAssistanceIssues]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertAssistanceIssues] AS' 
END
GO


-- ======================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to insert Assistance 
-- ======================================================
ALTER PROCEDURE [dbo].[usp_InsertAssistanceIssues] 
	-- Add the parameters for the stored procedure here
	@IssueID int OUTPUT,
	@FK_AssistanceID int,
	@IncidentNumber char(10),
	@FK_EWCRecipientCode smallint ,
	@FK_EWCPCPCode smallint ,
	@FK_ReferralProviderCode smallint ,
	@FK_EWCEnrolleeCode smallint,
	@OtherIssues varchar(500),
	@ProblemOtherIssue varchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [EWC_NSAA].[dbo].[tAssistanceIssues] (
	[FK_AssistanceID] ,
	[IncidentNumber] ,
	[FK_EWCRecipientCode] ,
	[FK_EWCPCPCode] ,
	[FK_ReferralProviderCode] ,
	[FK_EWCEnrolleeCode] ,
	[OtherIssues] ,
	[DateCreated] ,
	[CreatedBy] ,
	[LastUpdated] ,
	[UpdatedBy],
	[ProblemOtherIssue])

VALUES
  (
  	@FK_AssistanceID ,
	@IncidentNumber  ,
	@FK_EWCRecipientCode  ,
	@FK_EWCPCPCode  ,
	@FK_ReferralProviderCode  ,
	@FK_EWCEnrolleeCode  ,
	@OtherIssues,
	getdate() ,
	'User input' ,
	getdate() ,
	NULL,
	@ProblemOtherIssue) 

	SELECT @IssueID = SCOPE_IDENTITY()

END




GO
/****** Object:  StoredProcedure [dbo].[usp_InsertAssistanceResolution]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertAssistanceResolution]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertAssistanceResolution] AS' 
END
GO



-- ======================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to insert Assistance 
-- ======================================================
ALTER PROCEDURE [dbo].[usp_InsertAssistanceResolution] 
	-- Add the parameters for the stored procedure here
	@ResolutionID int OUTPUT,
	@FK_AssistanceID int ,
	@IncidentNumber char(10),
	@ResolutionInitiationDt smalldatetime,
	@FK_ResolutionCode smallint,
	@ResolutionEndDt smalldatetime,
	@OtherResolution varchar(500),
	@UnabletoResolve varchar(500) 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [EWC_NSAA].[dbo].[tAssistanceResolution] (
	[FK_AssistanceID] ,
	[IncidentNumber] ,
	[ResolutionInitiationDt] ,
	[FK_ResolutionCode] ,
	[ResolutionEndDt] ,
	[OtherResolution] ,
	[DateCreated] ,
	[CreatedBy] ,
	[LastUpdated] ,
	[UpdatedBy],
	[UnabletoResolve] )

VALUES
  (
  	@FK_AssistanceID ,
	@IncidentNumber ,
	@ResolutionInitiationDt  ,
	@FK_ResolutionCode  ,
	@ResolutionEndDt  ,
	@OtherResolution,
	getdate() ,
	'User input' ,
	getdate() ,
	NULL,
	@UnabletoResolve) 

	SELECT @ResolutionID = SCOPE_IDENTITY()

END


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertBarrier]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertBarrier]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertBarrier] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Insert new Barrier
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertBarrier]
	-- Add the parameters for the stored procedure here
	@BarrierID int OUTPUT,
	@FK_EncounterID int
    ,@BarrierType int
    ,@BarrierAssessed int
    ,@FollowUpDt smalldatetime
	,@Other varchar(2000)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	INSERT INTO [dbo].[tBarrier]
           ([FK_EncounterID]
           ,[BarrierType]
           ,[BarrierAssessed]
           ,[FollowUpDt]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy]
		   ,[Other])
     VALUES
           (@FK_EncounterID
           ,@BarrierType
           ,@BarrierAssessed
           ,@FollowUpDt
           ,NULL
           ,GETDATE()
           ,'User input'
           ,NULL
           ,NULL
		   ,@Other)

		   Select @BarrierID = SCOPE_IDENTITY()

END



GO
/****** Object:  StoredProcedure [dbo].[usp_InsertEncounter]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertEncounter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertEncounter] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/17/2016
-- Description:	Insert new Encounter record
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertEncounter]
	-- Add the parameters for the stored procedure here
			@EncounterID int OUTPUT,
			@FK_NavigatorID int
           ,@FK_RecipientID int
           ,@EncounterNumber int
           ,@EncounterDt smalldatetime
           ,@EncounterStartTime varchar(20)
           ,@EncounterEndTime varchar(20)
           ,@EncounterType int
		   ,@EncounterReason int
           ,@MissedAppointment bit
           ,@SvcOutcomeStatus int
           ,@SvcOutcomeStatusReason varchar(200)
           ,@NextAppointmentDt smalldatetime
           ,@NextAppointmentTime varchar(20)
           ,@NextAppointmentType int
           ,@ServicesTerminated bit
           ,@ServicesTerminatedDt smalldatetime
           ,@ServicesTerminatedReason varchar(200)
           ,@Notes varchar(2000)
		   ,@EncounterPurpose int
           
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	INSERT INTO [dbo].[tEncounter]
           ([FK_NavigatorID]
           ,[FK_RecipientID]
           ,[EncounterNumber]
           ,[EncounterDt]
           ,[EncounterStartTime]
           ,[EncounterEndTime]
           ,[EncounterType]
		   ,[EncounterReason]
           ,[MissedAppointment]
           ,[SvcOutcomeStatus]
           ,[SvcOutcomeStatusReason]
           ,[NextAppointmentDt]
           ,[NextAppointmentTime]
           ,[NextAppointmentType]
           ,[ServicesTerminated]
           ,[ServicesTerminatedDt]
           ,[ServicesTerminatedReason]
           ,[Notes]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy]
		   ,[EncounterPurpose])
     VALUES
           (@FK_NavigatorID
           ,@FK_RecipientID
           ,@EncounterNumber
           ,@EncounterDt
           ,@EncounterStartTime
           ,@EncounterEndTime
           ,@EncounterType
		   ,@EncounterReason
           ,@MissedAppointment
           ,@SvcOutcomeStatus
           ,@SvcOutcomeStatusReason
           ,@NextAppointmentDt
           ,@NextAppointmentTime
           ,@NextAppointmentType
           ,@ServicesTerminated
           ,@ServicesTerminatedDt
           ,@ServicesTerminatedReason
           ,@Notes
           ,NULL
           ,GETDATE()
           ,'User Input'
           ,NULL
           ,NULL
		   ,@EncounterPurpose)

		   Select @EncounterID = SCOPE_IDENTITY()

END



/****** Object:  StoredProcedure [dbo].[usp_UpdateEncounter]    Script Date: 9/14/2016 9:31:04 AM ******/
SET ANSI_NULLS ON


/****** Object:  StoredProcedure [dbo].[usp_UpdateEncounter]    Script Date: 11/17/2016 1:20:54 PM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertHECActivity]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertHECActivity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertHECActivity] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_InsertHECActivity]

@ActivityDate	datetime,	
@ActivityType	smallint,
@Cancelled bit,
@ReasonForCancellation varchar(300),
@OtherActivityType	varchar(300),	
@NameOrPurpose	varchar(250),	
@CollaboratorId1	int,	
@CollaboratorId2	int,	
@CollaboratorContributionId1	int,	
@CollaboratorContributionId2	int,	
@Address1	nvarchar(100),
@Address2	nvarchar(100),	
@City	VARCHAR(50),	
@Zip	varchar(5),	
@CHW1	int,
@CHW2	int,
@OtherAttendee1	nvarchar(100),	
@OtherAttendee2	nvarchar(100),	
@Population	varchar(50),	
@LanguageId	int,	
@Discussed	int,	
@OtherDiscussed	varchar(300),	
@PrePostTest	int,	
@Result	int,	
@OtherResult	varchar(300),	
@MyRole	int,
@OtherRole	varchar(300),	
@Attendee	bit,	
@AnnoucementDoc	varchar(300),	
@Travel	bit,	
@Notes	nvarchar(250),

@FirstName	nvarchar(50),	
@LastName	nvarchar(50),	
@Month	smallint,	
@Year	int,	
@RaceOrEthinicity	int,	
@Part_Info_Address1	nvarchar(100),	
@Part_Info_Address2	nvarchar(100),	
@Part_Info_City	varchar(50),
@Part_Info_Zip	varchar(5),	
@Telephone	varchar(50),	
@Email	varchar(150),	
@Part_Info_LanguageId	int,	
@MAM bit,
@PAP bit,
@HI bit,
@OkToContact bit,
@LastPAP int,
@LastMAM int,
@EWCEligible bit,

@BusinessName	nvarchar(100),	
@NPI	varchar(50),	
@ProviderFirstName	nvarchar(50),	
@ProviderLastName	nvarchar(50),	
@EwcProvider	smallint

AS
Begin
DEClARE @ActivityId int = null
INSERT INTO 
			tHecActivities
			( ActivityDate, 
			ActivityCancelled ,
			ReasonForCancellation,
			ActivityType, 
			NameOrPurpose,  
			CollaboratorId1,  
			CollaboratorId2,  
			CollaboratorContributionId1,  
			CollaboratorContributionId2,  
			Address1,  
			Address2,  
			City,  
			Zip,  
			CHW1,  
			CHW2,  
			OtherAttendee1,
			OtherAttendee2,
			Population,  
			LanguageId,  
			Discussed,  
			OtherDiscussed,  
			PrePostTest,  
			Result, 
			OtherResult, 
			MyRole,
			OtherMyRole,  
			Attendee,  
			AnnoucementDoc,  
			Travel,  
			Notes)
    VALUES (@ActivityDate,
			@Cancelled,
			@ReasonForCancellation,
			@ActivityType,
			@NameOrPurpose,
			@CollaboratorId1,
			@CollaboratorId2,
			@CollaboratorContributionId1	,
			@CollaboratorContributionId2	,
			@Address1,
			@Address2,
			@City,	
			@Zip,
			@CHW1,
			@CHW2,
			@OtherAttendee1	,	
			@OtherAttendee2	,	
			@Population	,	
			@LanguageId	,
			@Discussed	,
			@OtherDiscussed,
			@PrePostTest	,	
			@Result	,
			@OtherResult,
			@MyRole	,
			@OtherRole,
			@Attendee	,	
			@AnnoucementDoc	,	
			@Travel	,	
			@Notes)

set @ActivityId = SCOPE_IDENTITY()



IF(@ActivityId>0)
Begin
INSERT INTO tHecActivityParticipantInfo(
			ActivityId,
			FirstName,
			LastName,
			Month,
			Year,
			RaceOrEthinicity,
			Address1,
			Address2,
			City,
			Zip,
			Telephone,
			Email,
			LanguageId,
			MAM,
			PAP,
			HI,
			OkToContact,
			LastPAP,
			LastMAM,
			EWCEligible)
	VALUES (@ActivityId,
			@FirstName,
			@LastName,
			@Month	,
			@Year	,
			@RaceOrEthinicity	,	
			@Part_Info_Address1	,
			@Part_Info_Address2,	
			@Part_Info_City	,
			@Part_Info_Zip,	
			@Telephone,
			@Email	,
			@Part_Info_LanguageId,
			@MAM ,
			@PAP ,
			@HI ,
			@OkToContact ,
			@LastPAP ,
			@LastMAM ,
			@EWCEligible)

INSERT INTO tHecScreeningEvent(
			ActivityId,
			BusinessName,
			NPI,
			ProviderFirstName,
			ProviderLastName,
			EwcProvider)
	VALUES (@ActivityId,
			@BusinessName,
			@NPI,
			@ProviderFirstName,
			@ProviderLastName,
			@EwcProvider)


END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertInstantMessage]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertInstantMessage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertInstantMessage] AS' 
END
GO
ALTER Procedure [dbo].[usp_InsertInstantMessage]
@subject nvarchar(150),
@message nvarchar(max),
@receivedfrom int,
@receivedby int,
@ParentId int
As
Begin


insert into tInstantMessage(Subject,
Message,
ReceivedOn,
ReceivedFrom,
ReceivedBy,
IsRead,ParentId)
values ( @subject,@message,GETDATE(),@receivedfrom,@receivedby,0,IIF(@ParentId=0,null,@ParentId))


End


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNavigationCycle]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNavigationCycle]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertNavigationCycle] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/12/2016
-- Description:	Insert a new Navigation Cycle
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertNavigationCycle] 
			@NavigationCycleID int OUTPUT,
			@FK_RecipientID int
           ,@FK_ProviderID int
           ,@FK_ReferralID int
           ,@CancerSite int
           ,@NSProblem int
           ,@HealthProgram int
           ,@HealthInsuranceStatus int
           ,@HealthInsurancePlan int
           ,@HealthInsurancePlanNumber varchar(15)
           ,@ContactPrefDays varchar(15)
           ,@ContactPrefHours varchar(15)
           ,@ContactPrefHoursOther varchar(20)
           ,@ContactPrefType int
           ,@SRIndentifierCodeGenerated bit
           ,@FK_NavigatorID int
           
           
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [EWC_NSAA].[dbo].[tNavigationCycle]
           ([FK_RecipientID]
           ,[FK_ProviderID]
           ,[FK_ReferralID]
           ,[CancerSite]
           ,[NSProblem]
           ,[HealthProgram]
           ,[HealthInsuranceStatus]
           ,[HealthInsurancePlan]
           ,[HealthInsurancePlanNumber]
           ,[ContactPrefDays]
           ,[ContactPrefHours]
           ,[ContactPrefHoursOther]
           ,[ContactPrefType]
           ,[SRIndentifierCodeGenerated]
           ,[FK_NavigatorID]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy])
     VALUES
           (@FK_RecipientID
           ,@FK_ProviderID
           ,@FK_ReferralID
           ,@CancerSite
           ,@NSProblem
           ,@HealthProgram
           ,@HealthInsuranceStatus
           ,@HealthInsurancePlan
           ,@HealthInsurancePlanNumber
           ,@ContactPrefDays
           ,@ContactPrefHours
           ,@ContactPrefHoursOther
           ,@ContactPrefType
           ,@SRIndentifierCodeGenerated
           ,@FK_NavigatorID
           ,NULL
           ,getdate()
           ,'User Input'
           ,NULL
           ,NULL)

		   Select @NavigationCycleID = SCOPE_IDENTITY()
END

/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigationCycle]    Script Date: 8/10/2016 2:46:18 PM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNavigator]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNavigator]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertNavigator] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/05/2016	
-- Description:	Insert a new record in Navigator table
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertNavigator]
	-- Add the parameters for the stored procedure here
			@NavigatorID int OUTPUT
		   ,@DomainName varchar(20)
		   ,@Region smallint
           ,@Type int
           ,@LastName varchar(30)
           ,@FirstName varchar(20)
           ,@Address1 varchar(30)
           ,@Address2 varchar(30)
           ,@City varchar(25)
           ,@State char(2)
           ,@Zip char(5)
           ,@BusinessTelephone char(10)
		   ,@MobileTelephone char(10)
           ,@Email varchar(45)
           
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	INSERT INTO [dbo].[tNavigator]
           ([DomainName]
		   ,[Region]
           ,[Type]
           ,[LastName]
           ,[FirstName]
           ,[Address1]
           ,[Address2]
           ,[City]
           ,[State]
           ,[Zip]
           ,[BusinessTelephone]
		   ,[MobileTelephone]
           ,[Email]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy])
     VALUES
           (@DomainName
		   ,@Region
           ,@Type
           ,@LastName
           ,@FirstName
           ,@Address1
           ,@Address2
           ,@City
           ,@State
           ,@Zip
           ,@BusinessTelephone
		   ,@MobileTelephone
           ,@Email
           ,NULL
           ,getdate()
           ,'User Input'
           ,NULL
           ,NULL)

		   Select @NavigatorID = SCOPE_IDENTITY()
END


/****** Object:  StoredProcedure [dbo].[usp_SelectNavigator]    Script Date: 7/5/2016 2:16:15 PM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNavigatorLanguage]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNavigatorLanguage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertNavigatorLanguage] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 12/12/2016
-- Description:	Insert a record in the Navigator Languages table
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertNavigatorLanguage]
	-- Add the parameters for the stored procedure here

	@FK_NavigatorID int,
	@LanguageCode int,
	@SpeakingScore int,
	@ListeningScore int,
	@ReadingScore int,
	@WritingScore int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[tNavigatorLanguages]
			   ([FK_NavigatorID]
			   ,[LanguageCode]
			   ,[SpeakingScore]
			   ,[ListeningScore]
			   ,[ReadingScore]
			   ,[WritingScore]
			   ,[DateCreated]
			   ,[CreatedBy]
			   )
		 VALUES
			   (@FK_NavigatorID
			   ,@LanguageCode
			   ,@SpeakingScore
			   ,@ListeningScore
			   ,@ReadingScore
			   ,@WritingScore
			   ,GETDATE()
			   ,'User Input')

END


/****** Object:  StoredProcedure [dbo].[usp_SelectTransferReasons]    Script Date: 12/27/2016 11:01:57 AM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNavigatorTraining]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNavigatorTraining]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertNavigatorTraining] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 11/17/2016
-- Description:	Insert a new Navigator Training record
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertNavigatorTraining] 
	@FK_NavigatorID int,
	@CourseName varchar(100),
	@Organization varchar(100),
	@CompletionDt smalldatetime,
	@Certificate bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	INSERT INTO [dbo].[tNavigatorTraining]
           ([FK_NavigatorID]
           ,[CourseName]
           ,[Organization]
           ,[CompletionDt]
           ,[Certificate]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy])
     VALUES
           (@FK_NavigatorID
           ,@CourseName
           ,@Organization
           ,@CompletionDt
           ,@Certificate
           ,NULL
           ,GETDATE()
           ,'User Input'
           ,NULL
           ,NULL)



END


SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNSProvider]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertNSProvider]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertNSProvider] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 04/28/2016
-- Description:	Stored Procedure for inserting a new PCP record
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertNSProvider] 
	-- Add the parameters for the stored procedure here
	@NSProviderID int OUTPUT,
	@PCPName varchar(100),
	@EWCPCP bit,
    @PCP_NPI varchar(10),
    @PCP_Owner varchar(2),
    @PCP_Location varchar(3),
    @PCPAddress1 varchar(30),
    @PCPAddress2 varchar(30),
    @PCPCity varchar(24),
    @PCPState char(2),
    @PCPZip char(5),
    @PCPContactFName varchar(50),
    @PCPContactLName varchar(50),
    @PCPContactTitle varchar(50),
    @PCPContactTelephone char(10),
    @PCPContactEmail varchar(45),
    @Medical bit,
    @MedicalSpecialty int,
    @MedicalSpecialtyOther varchar(100),
    @ManagedCarePlan int,
    @ManagedCarePlanOther varchar(100)
    
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [EWC_NSAA].[dbo].[tNSProvider]
           ([PCPName]
		   ,[EWCPCP]
           ,[PCP_NPI]
           ,[PCP_Owner]
           ,[PCP_Location]
           ,[PCPAddress1]
           ,[PCPAddress2]
           ,[PCPCity]
           ,[PCPState]
           ,[PCPZip]
           ,[PCPContactFName]
           ,[PCPContactLName]
           ,[PCPContactTitle]
           ,[PCPContactTelephone]
           ,[PCPContactEmail]
           ,[Medical]
           ,[MedicalSpecialty]
           ,[MedicalSpecialtyOther]
           ,[ManagedCarePlan]
           ,[ManagedCarePlanOther]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy])
     VALUES
           (@PCPName
		   ,@EWCPCP
           ,@PCP_NPI
           ,@PCP_Owner
           ,@PCP_Location
           ,@PCPAddress1
           ,@PCPAddress2
           ,@PCPCity
           ,@PCPState
           ,@PCPZip
           ,@PCPContactFName
           ,@PCPContactLName
           ,@PCPContactTitle
           ,@PCPContactTelephone
           ,@PCPContactEmail
           ,@Medical
           ,@MedicalSpecialty
           ,@MedicalSpecialtyOther
           ,@ManagedCarePlan
           ,@ManagedCarePlanOther
           ,NULL
           ,getdate()
           ,'User Input'
           ,NULL
           ,NULL)

		   Select @NSProviderID = SCOPE_IDENTITY()
END






GO
/****** Object:  StoredProcedure [dbo].[usp_InsertScreeningNav]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertScreeningNav]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertScreeningNav] AS' 
END
GO


-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 07/31/2017	
-- Description:	Insert a new record in Screening Navigation table
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertScreeningNav]
	-- Add the parameters for the stored procedure here
		   @SN_ID int OUTPUT
		   ,@FK_NavigatorID int
		   ,@SN_CODE1 varchar(11)
           ,@SN_CODE2 smallint
           ,@LastName varchar(30)
           ,@FirstName varchar(20)
           ,@DOB smalldatetime
           ,@Address1 varchar(30)
           ,@Address2 varchar(30)
           ,@City varchar(25)
           ,@State char(2)
           ,@Zip char(5)
           ,@HomeTelephone char(10)
           ,@Cellphone char(10)
           ,@Email varchar(45)
           ,@Computer bit
           ,@TextMessage char(1)
           ,@DateOfContact1 smalldatetime
           ,@ApptScreen1 char(1)
           ,@SvcDate1 smalldatetime
           ,@SvcType1 char(2)
           ,@Response1 char(2)
           ,@DateOfContact2 smalldatetime
           ,@ApptScreen2 char(1)
           ,@SvcDate2 smalldatetime
           ,@SvcType2 char(2)
           ,@Response2 char(2)
           ,@NSEnrollment char(1)
           ,@EnrollmentDate smalldatetime
           
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SET @SN_CODE2 = (SELECT ISNULL(MAX(SN_Code2),0) FROM tScreeningNav WHERE SN_CODE1 = @SN_CODE1) + 1
	

	INSERT INTO [dbo].[tScreeningNav]
           (FK_NavigatorID
		   ,[SN_CODE1]
           ,[SN_CODE2]
           ,[LastName]
           ,[FirstName]
           ,[DOB]
           ,[Address1]
           ,[Address2]
           ,[City]
           ,[State]
           ,[Zip]
           ,[HomeTelephone]
           ,[Cellphone]
           ,[Email]
           ,[Computer]
           ,[TextMessage]
           ,[DateOfContact1]
           ,[ApptScreen1]
           ,[SvcDate1]
           ,[SvcType1]
           ,[Response1]
           ,[DateOfContact2]
           ,[ApptScreen2]
           ,[SvcDate2]
           ,[SvcType2]
           ,[Response2]
           ,[NSEnrollment]
           ,[EnrollmentDate]
		   ,[DateCreated]
		   ,[CreatedBy])
     VALUES
           (@FK_NavigatorID
		   ,@SN_CODE1
           ,@SN_CODE2
           ,@LastName
           ,@FirstName
           ,@DOB
           ,@Address1
           ,@Address2
           ,@City
           ,@State
           ,@Zip
           ,@HomeTelephone
           ,@Cellphone
           ,@Email
           ,@Computer
           ,@TextMessage
           ,@DateOfContact1
           ,@ApptScreen1
           ,@SvcDate1
           ,@SvcType1
           ,@Response1
           ,@DateOfContact2
           ,@ApptScreen2
           ,@SvcDate2
           ,@SvcType2
           ,@Response2
           ,@NSEnrollment
           ,@EnrollmentDate
		   ,GETDATE()
		   ,'User Input')


		   Select @SN_ID = SCOPE_IDENTITY()
END



SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertServiceRecipient]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertServiceRecipient]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertServiceRecipient] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 03/14/2016
-- Description:	Add a new Recipient Record
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertServiceRecipient] 
			@NSRecipientID int OUTPUT,
		   @FK_NavigatorID int,
		   @FK_RecipID char(14),
           @LastName varchar(30),
           @FirstName varchar(20),
           @MiddleInitial varchar(1),
           @DOB smalldatetime,
           @AddressNotAvailable bit,
           @Address1 varchar(30),
           @Address2 varchar(30),
           @City varchar(25),
           @State char(2),
           @Zip char(5),
           @Phone1 char(10),
           @P1PhoneType smallint,
           @P1PersonalMessage bit,
           @Phone2 char(10),
           @P2PhoneType smallint,
           @P2PersonalMessage bit,
           @MotherMaidenName varchar(20),
           @Email varchar(45),
           @SSN char(10),
           @ImmigrantStatus bit,
           @CountryOfOrigin smallint,
           @Gender char(1),
           @Ethnicity bit,
           @PrimaryLanguage smallint,
           @AgreeToSpeakEnglish bit,
           @CaregiverName varchar(50),
           @Relationship smallint,
           @RelationshipOther varchar(30),
           @CaregiverPhone char(10),
           @CaregiverPhoneType smallint,
           @CaregiverPhonePersonalMessage bit,
           @CaregiverEmail varchar(45),
           @CaregiverContactPreference int,
           @Comments varchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [EWC_NSAA].[dbo].[tServiceRecipient]
           ([FK_NavigatorID]
		   ,[FK_RecipID]
           ,[LastName]
           ,[FirstName]
           ,[MiddleInitial]
           ,[DOB]
           ,[AddressNotAvailable]
           ,[Address1]
           ,[Address2]
           ,[City]
           ,[State]
           ,[Zip]
           ,[Phone1]
           ,[P1PhoneType]
           ,[P1PersonalMessage]
           ,[Phone2]
           ,[P2PhoneType]
           ,[P2PersonalMessage]
           ,[MotherMaidenName]
           ,[Email]
           ,[SSN]
           ,[ImmigrantStatus]
           ,[CountryOfOrigin]
           ,[Gender]
           ,[Ethnicity]
           ,[PrimaryLanguage]
           ,[AgreeToSpeakEnglish]
           ,[CaregiverName]
           ,[Relationship]
           ,[RelationshipOther]
           ,[CaregiverPhone]
           ,[CaregiverPhoneType]
           ,[CaregiverPhonePersonalMessage]
           ,[CaregiverEmail]
           ,[CaregiverContactPreference]
           ,[Comments]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy])
     VALUES
           (@FK_NavigatorID,
		   @FK_RecipID,
           @LastName,
           @FirstName,
           @MiddleInitial,
           @DOB,
           @AddressNotAvailable,
           @Address1,
           @Address2,
           @City,
           @State,
           @Zip,
           @Phone1,
           @P1PhoneType,
           @P1PersonalMessage,
           @Phone2,
           @P2PhoneType,
           @P2PersonalMessage,
           @MotherMaidenName,
           @Email,
           @SSN,
           @ImmigrantStatus,
           @CountryOfOrigin,
           @Gender,
           @Ethnicity,
           @PrimaryLanguage,
           @AgreeToSpeakEnglish,
           @CaregiverName,
           @Relationship,
           @RelationshipOther,
           @CaregiverPhone,
           @CaregiverPhoneType,
           @CaregiverPhonePersonalMessage,
           @CaregiverEmail,
           @CaregiverContactPreference,
           @Comments,
           NULL,
           getdate(),
           'User input',
           NULL,
           NULL)

		   Select @NSRecipientID = SCOPE_IDENTITY()

END


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertSolution]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertSolution]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertSolution] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Insert New Solution 
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertSolution]
	-- Add the parameters for the stored procedure here
	@FK_BarrierID int
    ,@SolutionOffered int
    ,@SolutionImplementationStatus int
	,@FollowUpDt smalldatetime
	,@Solution varchar(2000)
    AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	INSERT INTO [dbo].[tSolution]
           ([FK_BarrierID]
           ,[SolutionOffered]
           ,[SolutionImplementationStatus]
		   ,[FollowUpDt]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy]
		   ,[Solution])
     VALUES
           (@FK_BarrierID
           ,@SolutionOffered
           ,@SolutionImplementationStatus
		   ,@FollowUpDt
           ,NULL
           ,GETDATE()
           ,'User input'
           ,NULL
           ,NULL
		   ,@Solution)

END


/****** Object:  StoredProcedure [dbo].[usp_SelectSolutionsList]    Script Date: 8/4/2016 8:18:07 AM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertUserAccess]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertUserAccess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertUserAccess] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 06/09/2016
-- Description:	Log user access
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertUserAccess] 
	@DomainName varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	INSERT INTO [dbo].[tUserAccess]
           ([DomainName]
           ,[LastAccess])
     VALUES
           (@DomainName,
			GETDATE())


END



GO
/****** Object:  StoredProcedure [dbo].[usp_MarkMessageAsRead]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MarkMessageAsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_MarkMessageAsRead] AS' 
END
GO
ALTER procedure [dbo].[usp_MarkMessageAsRead]
@messageId int
As
Begin
Update tInstantMessage set IsRead =1 where Id=@messageId
End
GO
/****** Object:  StoredProcedure [dbo].[usp_MarkSystemMessageAsRead]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MarkSystemMessageAsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_MarkSystemMessageAsRead] AS' 
END
GO

ALTER procedure [dbo].[usp_MarkSystemMessageAsRead]
@messageId int
As
Begin
Update tSystemMessage set IsRead =1 where Id=@messageId
End
GO
/****** Object:  StoredProcedure [dbo].[usp_NavigatorTransfer]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_NavigatorTransfer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_NavigatorTransfer] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 08/09/2016
-- Description:	Stored Procedure for transferring a Service Recipient between Navigators
-- =============================================
ALTER PROCEDURE [dbo].[usp_NavigatorTransfer] 
	@mode int,
	@NSRecipientID int,
	@OldNavigatorID int,
	@NewNavigatorID int,
	@NavigatorTransferDt smalldatetime,
	@NavigatorTransferReason varchar(200)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Select Old Navigator details
	if @mode = 1
	begin
		SELECT LastName, FirstName, Region, [Type] 
		FROM tNavigator 
		WHERE NavigatorID = (SELECT FK_NavigatorID FROM tServiceRecipient WHERE NSRecipientID = @NSRecipientID)
	end

	-- Update New Navigator ID
	if @mode = 2
	begin
		
		--Update tServiceRecipient
		UPDATE tServiceRecipient
		SET FK_NavigatorID = @NewNavigatorID,
		NavigatorTransferDt = @NavigatorTransferDt,
		NavigatorTransferReason = @NavigatorTransferReason,
		OldNavigatorID = @OldNavigatorID
		WHERE NSRecipientID = @NSRecipientID

		--Update tNavigationCycle
		UPDATE tNavigationCycle
		SET FK_NavigatorID = @NewNavigatorID
		WHERE FK_RecipientID = @NSRecipientID

		--Update tEncounter
		UPDATE tNavigationCycle
		SET FK_NavigatorID = @NewNavigatorID
		WHERE FK_RecipientID = @NSRecipientID


	end

	--Get list of Navigators to choose from
	if @mode = 3
	begin
		SELECT NavigatorID, FirstName, LastName FROM tNavigator
	end

	-- Select Recipient details
	if @mode = 4
	begin
		SELECT R.LastName, R.FirstName, L.LanguageName As PrimaryLanguage, 
		AgreeToSpeakEnglish = CASE AgreeToSpeakEnglish WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' END 
		FROM tServiceRecipient R
		JOIN trProvLanguages L
			ON R.PrimaryLanguage = L.LanguageCode
		WHERE NSRecipientID = @NSRecipientID

	end
	
	--Save Interim transfer
	if @mode = 5
	begin
		
		INSERT INTO [EWC_NSAA].[dbo].[tNavigatorTransfer]
           ([FK_RecipientID]
           ,[CurrentNavigatorID]
           ,[NewNavigatorID]
           ,[NavigatorTransferReason]
           ,[NavigatorTransferDt]
           ,[DateCreated]
           ,[CreatedBy])
     VALUES
           (@NSRecipientID
           ,@OldNavigatorID
           ,@NewNavigatorID
           ,@NavigatorTransferReason
           ,@NavigatorTransferDt
           ,getdate()
           ,'User Input')
		
				
	end

	--List Interim transfers
	if @mode = 6
	begin

		SELECT [TransferID]
			  ,[FK_RecipientID]
			  ,[CurrentNavigatorID]
			  ,[NewNavigatorID]
			  ,[NavigatorTransferReason]
			  ,[NavigatorTransferDt]
			  ,[TransferStatus]
			  ,[TransferRefusedReason]
			  ,[TransferRefusedDt]
			  ,[xtag]
			  ,[DateCreated]
			  ,[CreatedBy]
			  ,[LastUpdated]
			  ,[UpdatedBy]
		  FROM [dbo].[tNavigatorTransfer]
			WHERE CurrentNavigatorID = @OldNavigatorID
				
	end

	--List Interim transfers
	if @mode = 7
	begin

		SELECT [TransferID]
			  ,[FK_RecipientID]
			  ,[CurrentNavigatorID]
			  ,[NewNavigatorID]
			  ,[NavigatorTransferReason]
			  ,[NavigatorTransferDt]
			  ,[TransferStatus]
			  ,[TransferRefusedReason]
			  ,[TransferRefusedDt]
			  ,[xtag]
			  ,[DateCreated]
			  ,[CreatedBy]
			  ,[LastUpdated]
			  ,[UpdatedBy]
		  FROM [dbo].[tNavigatorTransfer]
			WHERE NewNavigatorID = @OldNavigatorID
				
	end

END


SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_RecipientReport]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RecipientReport]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_RecipientReport] AS' 
END
GO
-- =============================================
-- Author:		Bilwa BUchake
-- Create date: 10/05/2016
-- Description:	Stored Procedure for generating Recipient Report
-- =============================================
ALTER PROCEDURE [dbo].[usp_RecipientReport] 
	@mode int,
	@NSRecipientID int,
	@NSProviderID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if @mode = 1
	begin
	SELECT [NSRecipientID]
		  ,[FK_NavigatorID]
		  ,[FK_RecipID]
		  ,[LastName]
		  ,[FirstName]
		  ,[MiddleInitial]
		  ,[DOB]
		  ,[AddressNotAvailable]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[Zip]
		  ,[Phone1]
		  ,CASE P1PhoneType WHEN 1 THEN 'Home' WHEN 2 THEN 'Cell/Mobile' WHEN 3 THEN 'Message' WHEN 4 THEN 'Work' END AS P1PhoneType
		  ,CASE P1PersonalMessage WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' END AS P1PersonalMessage
		  ,[Phone2]
		  ,CASE P2PhoneType WHEN 1 THEN 'Home' WHEN 2 THEN 'Cell/Mobile' WHEN 3 THEN 'Message' WHEN 4 THEN 'Work' END AS P2PhoneType
		  ,[P2PhoneTypeOther]
		  ,CASE [P2PersonalMessage] WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' END AS [P2PersonalMessage]
		  ,[MotherMaidenName]
		  ,[Email]
		  ,[SSN]
		  ,CASE [ImmigrantStatus] WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' END AS [ImmigrantStatus]
		  ,[CountryName] As CountryOfOrigin
		  ,[Gender]
		  ,CASE [Ethnicity] WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' END AS [Ethnicity]
		  ,[LanguageName] AS PrimaryLanguage
		  ,CASE AgreeToSpeakEnglish WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' END AS AgreeToSpeakEnglish
		  ,[CaregiverName]
		  ,CASE [Relationship] WHEN 1 THEN 'Family Support' WHEN 2 THEN 'Community Support' WHEN 3 THEN 'Professional Caretaker' WHEN 4 THEN 'Shelter' WHEN 4 THEN 'Shelter' WHEN 5 THEN 'Other' END AS [Relationship]
		  ,[RelationshipOther]
		  ,[CaregiverPhone]
		  ,CASE [CaregiverPhoneType] WHEN 1 THEN 'Home' WHEN 2 THEN 'Cell/Mobile' WHEN 3 THEN 'Message' WHEN 4 THEN 'Work' END AS [CaregiverPhoneType]
		  ,CASE [CaregiverPhonePersonalMessage] WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' END AS [CaregiverPhonePersonalMessage]
		  ,[CaregiverEmail]
		  ,CASE [CaregiverContactPreference] WHEN 1 THEN 'Phone Call' WHEN 2 THEN 'Email' WHEN 3 THEN 'Text' WHEN 4 THEN 'Face-to-Face' END AS [CaregiverContactPreference]
		  ,[Comments]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
		  ,[NavigatorTransferDt]
		  ,[NavigatorTransferReason]
		  ,[OldNavigatorID]
	  FROM [dbo].[tServiceRecipient]
	  LEFT JOIN trProvLanguages 
			ON PrimaryLanguage = LanguageCode
	  LEFT JOIN trCountries
			ON CountryOfOrigin = CountryCode
	  WHERE NSRecipientID = @NSRecipientID
	end

	if @mode = 2
	begin

		SELECT [NSProviderID]
			  ,[PCPName]
			  ,[PCP_NPI]
			  ,[PCPAddress1]
			  ,[PCPAddress2]
			  ,[PCPCity]
			  ,[PCPState]
			  ,[PCPZip]
			  ,[PCPContactFName]
			  ,[PCPContactLName]
			  ,[PCPContactTitle]
			  ,[PCPContactTelephone]
			  ,[PCPContactEmail]
			  ,[xtag]
			  ,[DateCreated]
			  ,[CreatedBy]
			  ,[LastUpdated]
			  ,[UpdatedBy]
			  ,[EWCPCP]
			FROM [dbo].[tNSProvider]
			WHERE NSProviderID = @NSProviderID

		end

		if @mode = 3
		begin

		SELECT [NavigationCycleID]
			  ,[FK_RecipientID]
			  ,[FK_ProviderID]
			  ,[FK_ReferralID]
			  ,CancerSiteName AS [CancerSite] 
			  ,NSProblemDescription AS [NSProblem] 
			  ,HealthProgramName As HealthProgram
			  ,CASE [HealthInsuranceStatus] WHEN 0 THEN 'Uninsured' wHEN 1 THEN 'Insured' END AS [HealthInsuranceStatus]
			  ,[HealthInsurancePlan]
			  ,[HealthInsurancePlanNumber]
			  ,[ContactPrefDays]
			  ,[ContactPrefHours]
			  ,[ContactPrefHoursOther]
			  ,CASE [ContactPrefType] WHEN 1 THEN 'Phone Call' WHEN 2 THEN 'Email' WHEN 3 THEN 'Text' WHEN 4 THEN 'Face-to-Face' END AS [ContactPrefType]
			  ,[SRIndentifierCodeGenerated]
			  ,[FK_NavigatorID]
			  ,[xtag]
			  ,[DateCreated]
			  ,[CreatedBy]
			  ,[LastUpdated]
			  ,[UpdatedBy]
		  FROM [dbo].[tNavigationCycle]
		  LEFT JOIN trHealthProgram
			ON HealthProgram = HealthProgramCode
		  LEFT JOIN trCancerSite
			ON CancerSite = CancerSiteCode
		  LEFT JOIN trNSProblem
			 ON NSProblem = NSProblemCode
		  WHERE FK_RecipientID = @NSRecipientID


		end

		if @mode = 4
		begin

			SELECT B.[BarrierID]
				  ,B.[FK_EncounterID]
				  ,BarrierTypeDescription As BarrierType
				  ,BarrierDescription As BarrierAssessed
				  ,S.SolutionID
				  ,S.FK_BarrierID
				  ,dbo.GetSolutionsById(BarrierID) As SolutionOffered
				  ,dbo.GetImplementationsById(BarrierID) AS SolutionImplementationStatus
				  ,S.FollowUpDt
				  ,E.EncounterDt
			  FROM [dbo].[tBarrier] B
				  JOIN tSolution S
					ON BarrierID = FK_BarrierID
				  JOIN trBarrierType
					ON BarrierType = BarrierTypeCode
				  JOIN trBarrier
					ON BarrierAssessed = BarrierCode
				  JOIN trBarrierSolution
					ON SolutionOffered = BarrierSolutionCode
				  JOIN tEncounter E
					ON B.FK_EncounterID = E.EncounterID
				  JOIN tServiceRecipient SR
					ON E.FK_RecipientID = SR.NSRecipientID
			  WHERE FK_RecipientID = @NSRecipientID

		end

		if @mode = 5
		begin

			SELECT Count(DISTINCT EncounterID) As encounters from tEncounter
			WHERE FK_RecipientID = @NSRecipientID

		end

		if @mode = 6
		begin

			SELECT Count(MissedAppointment) As missedappts from tEncounter
			WHERE FK_RecipientID = @NSRecipientID
			and MissedAppointment = 1

		end

		if @mode = 7
		begin

			SELECT MAX(EncounterDt) As LastEncounter
			from tEncounter
			WHERE FK_RecipientID = @NSRecipientID
			
		end

		if @mode = 8
		begin

			SELECT MAX(NextAppointmentDt) As NextEncounter 
			from tEncounter
			WHERE FK_RecipientID = @NSRecipientID
			--AND NextAppointmentDt > getdate()
			
		end
		
		if @mode = 9
		begin

			SELECT SvcOutcomeStatus, LastUpdated, ServicesTerminated, ServicesTerminatedDt 
			from tEncounter
			WHERE FK_RecipientID = @NSRecipientID
			AND EncounterDt = (SELECT MAX(EncounterDt) FROM tEncounter WHERE FK_RecipientID = @NSRecipientID)
			
		end

END



SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_SaveCaregiverApproval]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SaveCaregiverApproval]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SaveCaregiverApproval] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 07/12/2016
-- Description:	Save Caregiver Approval values to database
-- =============================================
ALTER PROCEDURE [dbo].[usp_SaveCaregiverApproval]
	@FK_NSRecipientID int,
	@ApprovalCodeList varchar(20),
	@ApprovalOther varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    --Delete all Race values for FK_NSRecipientID
    DELETE FROM tCaregiverApprovals WHERE FK_NSRecipientID = @FK_NSRecipientID
    
    
	--Begin string splitting function
	
	DECLARE @Index      smallint,
                    @Start      smallint,
                    @DelSize    smallint,
					@Delimiter char(1),
					@DelimitedString varchar(10)

	declare @ApprovalCode int


	DECLARE @tblArray TABLE
		(
		   Element     varchar(2)               -- Array element contents
		)


	SET @DelimitedString = @ApprovalCodeList

	SET @Delimiter = ','

    SET @DelSize = LEN(@Delimiter)

    -- Loop through source string and add elements to destination table array
    -- ----------------------------------------------------------------------
    WHILE LEN(@DelimitedString) > 0
    BEGIN

        SET @Index = CHARINDEX(@Delimiter, @DelimitedString)

        IF @Index = 0
            BEGIN

                INSERT INTO
                    @tblArray 
                    (Element)
                VALUES
                    (LTRIM(RTRIM(@DelimitedString)))

                BREAK
            END
        ELSE
            BEGIN

                INSERT INTO
                    @tblArray 
                    (Element)
                VALUES
                    (LTRIM(RTRIM(SUBSTRING(@DelimitedString, 1,@Index - 1))))

                SET @Start = @Index + @DelSize
                SET @DelimitedString = SUBSTRING(@DelimitedString, @Start , LEN(@DelimitedString) - @Start + 1)

            END
    END


	--End string splitting function

    DECLARE csr_CaregiverApprovalInsert CURSOR FOR (SELECT * FROM @tblArray)

	OPEN csr_CaregiverApprovalInsert

	FETCH NEXT FROM csr_CaregiverApprovalInsert INTO @ApprovalCode

	WHILE @@FETCH_STATUS = 0  

	BEGIN
	--Insert Caregiver Approval values

	INSERT INTO [dbo].[tCaregiverApprovals]
			   ([FK_NSRecipientID]
			   ,[CaregiverApproval]
			   ,[CaregiverApprovalOther]
			   ,[xtag]
			   ,[DateCreated]
			   ,[CreatedBy]
			   ,[LastUpdated]
			   ,[UpdatedBy])
		 VALUES
			   (@FK_NSRecipientID
			   ,@ApprovalCode
			   ,@ApprovalOther
			   ,NULL
			   ,getdate()
			   ,'User Input'
			   ,NULL
			   ,NULL)


	FETCH NEXT FROM csr_CaregiverApprovalInsert INTO @ApprovalCode

	END

	CLOSE csr_CaregiverApprovalInsert
	DEALLOCATE csr_CaregiverApprovalInsert


END


/****** Object:  StoredProcedure [dbo].[GetCaregiverApproval]    Script Date: 7/14/2016 2:43:13 PM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_SaveRace]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SaveRace]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SaveRace] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 04/20/2016
-- Description:	Save Race values to database
-- =============================================
ALTER PROCEDURE [dbo].[usp_SaveRace]
	@FK_NSRecipientID int,
	@RaceCodeList varchar(10),
	@RaceOther varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    --Delete all Race values for FK_NSRecipientID
    DELETE FROM tRace WHERE FK_NSRecipientID = @FK_NSRecipientID
    
    
	--Begin string splitting function
	
	DECLARE @Index      smallint,
                    @Start      smallint,
                    @DelSize    smallint,
					@Delimiter char(1),
					@DelimitedString varchar(10)

	declare @RaceCode int


	DECLARE @tblArray TABLE
		(
		   Element     varchar(2)               -- Array element contents
		)


	SET @DelimitedString = @RaceCodeList

	SET @Delimiter = ','

    SET @DelSize = LEN(@Delimiter)

    -- Loop through source string and add elements to destination table array
    -- ----------------------------------------------------------------------
    WHILE LEN(@DelimitedString) > 0
    BEGIN

        SET @Index = CHARINDEX(@Delimiter, @DelimitedString)

        IF @Index = 0
            BEGIN

                INSERT INTO
                    @tblArray 
                    (Element)
                VALUES
                    (LTRIM(RTRIM(@DelimitedString)))

                BREAK
            END
        ELSE
            BEGIN

                INSERT INTO
                    @tblArray 
                    (Element)
                VALUES
                    (LTRIM(RTRIM(SUBSTRING(@DelimitedString, 1,@Index - 1))))

                SET @Start = @Index + @DelSize
                SET @DelimitedString = SUBSTRING(@DelimitedString, @Start , LEN(@DelimitedString) - @Start + 1)

            END
    END


	--End string splitting function

    DECLARE csr_RaceInsert CURSOR FOR (SELECT * FROM @tblArray)

	OPEN csr_RaceInsert

	FETCH NEXT FROM csr_RaceInsert INTO @RaceCode

	WHILE @@FETCH_STATUS = 0  

	BEGIN
		--Insert Race values
    INSERT INTO [EWC_NSAA].[dbo].[tRace]
           ([FK_NSRecipientID]
           ,[RaceCode]
           ,[RaceOther]
           ,[xtag]
           ,[DateCreated]
           ,[CreatedBy]
           ,[LastUpdated]
           ,[UpdatedBy])
     VALUES
           (@FK_NSRecipientID
           ,@RaceCode
           ,@RaceOther
           ,NULL
           ,getdate()
           ,'User Input'
           ,NULL
           ,NULL)

	FETCH NEXT FROM csr_RaceInsert INTO @RaceCode

	END

	CLOSE csr_RaceInsert
	DEALLOCATE csr_RaceInsert


END


/****** Object:  StoredProcedure [dbo].[usp_GetRace]    Script Date: 7/12/2016 10:41:42 AM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_SearchAssistanceIncident]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SearchAssistanceIncident]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SearchAssistanceIncident] AS' 
END
GO


-- =====================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to search incident using IncidentNumber
-- =====================================================================
ALTER PROCEDURE [dbo].[usp_SearchAssistanceIncident] 
	-- Add the parameters for the stored procedure here
	@AssistanceID int,
	@IncidentNumber char(10),
	@Lastname varchar(30),
	@IncidentDt smalldatetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [AssistanceID] ,
	[IncidentNumber] ,
	[IncidentDt] ,
	[FK_CommunicationCode] ,
	[FirstName] ,
	[Lastname] ,
	[Telephone] ,
	[Email] ,
	[RecipID] ,
	[FK_RequestorCode] ,
	[NPI] ,
	[NPIOwner] ,
	[NPISvcLoc] ,
	[BusinessName] ,
	[DateCreated] ,
	[CreatedBy] ,
	[LastUpdated] ,
	[UpdatedBy] 
FROM [dbo].[tAssistance]
WHERE ([AssistanceID] = @AssistanceID OR
IncidentNumber = @IncidentNumber
	  OR  Lastname = @Lastname OR [IncidentDt] = @IncidentDt)

END



GO
/****** Object:  StoredProcedure [dbo].[usp_SearchRecipients]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SearchRecipients]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SearchRecipients] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 10/27/2016
-- Description:	Search for Recipients
-- =============================================
ALTER PROCEDURE [dbo].[usp_SearchRecipients] 
	@NSRecipientID int,
	@LastName varchar(30),
	@DOB smalldatetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [NSRecipientID]
		  ,[FK_NavigatorID]
		  ,[FK_RecipID]
		  ,[LastName]
		  ,[FirstName]
		  ,[MiddleInitial]
		  ,[DOB]
		  ,[AddressNotAvailable]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[Zip]
		  ,[Phone1]
		  ,[P1PhoneType]
		  ,[P1PersonalMessage]
		  ,[Phone2]
		  ,[P2PhoneType]
		  ,[P2PhoneTypeOther]
		  ,[P2PersonalMessage]
		  ,[MotherMaidenName]
		  ,[Email]
		  ,[SSN]
		  ,[ImmigrantStatus]
		  ,[CountryOfOrigin]
		  ,[Gender]
		  ,[Ethnicity]
		  ,[PrimaryLanguage]
		  ,[AgreeToSpeakEnglish]
		  ,[CaregiverName]
		  ,[Relationship]
		  ,[RelationshipOther]
		  ,[CaregiverPhone]
		  ,[CaregiverPhoneType]
		  ,[CaregiverPhonePersonalMessage]
		  ,[CaregiverEmail]
		  ,[CaregiverContactPreference]
		  ,[Comments]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
	  FROM [dbo].[tServiceRecipient]
	  WHERE (NSRecipientID = @NSRecipientID)
	  OR (LastName = @LastName AND DOB = @DOB)
END

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectActivityType]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectActivityType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectActivityType] AS' 
END
GO

-- =============================================
-- Author:		Nidhin C	
-- Create date: 5/24/2018
-- Description:	Stored Procedure to get all Activity type from [tActivityType] table
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectActivityType] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [ActivityTypeID]
      ,[ActivityType]
  FROM [EWC_NSAA].[dbo].[tActivityType]


END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceCommunication]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceCommunication]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceCommunication] AS' 
END
GO

-- ===================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to select Assistance communication
-- ===================================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceCommunication] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [CommunicationCode]
      ,[CommunicationDescription]
FROM [dbo].[trAssistanceCommunication]
	 

END


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceEWCEnrollee]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceEWCEnrollee]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceEWCEnrollee] AS' 
END
GO

-- ===================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to select Assistance Enrollee
-- ===================================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceEWCEnrollee] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [EWCEnrolleeCode]
      ,[EWCEnrolleeDescription]
  FROM [EWC_NSAA].[dbo].[trAssistanceEWCEnrollee]
	 

END


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceEWCPCP]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceEWCPCP]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceEWCPCP] AS' 
END
GO

-- ==========================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to select Assistance PCP
-- ==========================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceEWCPCP] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [EWCPCPCode]
      ,[EWCPCPDescription]
  FROM [EWC_NSAA].[dbo].[trAssistanceEWCPCP]
	 

END


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceEWCRecipient]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceEWCRecipient]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceEWCRecipient] AS' 
END
GO

-- ==========================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to select Assistance PCP
-- ==========================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceEWCRecipient] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [EWCRecipientCode]
      ,[EWCRecipientDescription]
  FROM [EWC_NSAA].[dbo].[trAssistanceEWCRecipient]
	 

END


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceIssuesList]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceIssuesList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceIssuesList] AS' 
END
GO


-- ================================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/08/2017
-- Description:	Stored Procedure to select AssistanceIssues based on @FK_AssistanceID
-- ================================================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceIssuesList] 
	-- Add the parameters for the stored procedure here
	@FK_AssistanceID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT
	I.[IssueID] ,
	I.[FK_AssistanceID] ,
	I.[IncidentNumber] ,
	I.[FK_EWCRecipientCode] ,
	I.[FK_EWCPCPCode] ,
	I.[FK_ReferralProviderCode] ,
	I.[FK_EWCEnrolleeCode] ,
	I.[OtherIssues] ,
	I.[DateCreated] ,
	I.[CreatedBy] ,
	I.[LastUpdated] ,
	I.[UpdatedBy]	 
FROM [dbo].[tAssistanceIssues] I
JOIN tAssistance A
ON I.FK_AssistanceID = A.AssistanceID
WHERE I.FK_AssistanceID = @FK_AssistanceID
	  
END

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceList]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceList] AS' 
END
GO

-- =========================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/08/2017
-- Description:	Stored Procedure to get list of Assistance records using FK_NavigatorID 
-- Edited by Bilwa Buchake: Added FK_NavigatorID column and changed WHERE clause to use FK_NavigatorID and not IncidentNumber
-- =========================================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceList] 
	-- Add the parameters for the stored procedure here
	@FK_NavigatorID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT
	[AssistanceID] ,
	[FK_NavigatorID],
	[IncidentNumber] ,
	[IncidentDt] ,
	[FK_CommunicationCode] ,
	[FirstName] ,
	[Lastname] ,
	[Telephone] ,
	[Email] ,
	[RecipID] ,
	[FK_RequestorCode] ,
	[NPI] ,
	[NPIOwner] ,
	[NPISvcLoc] ,
	[BusinessName] ,
	[DateCreated] ,
	[CreatedBy] ,
	[LastUpdated] ,
	[UpdatedBy] 
	FROM [dbo].[tAssistance]
WHERE  [FK_NavigatorID] = @FK_NavigatorID

END

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceReferralProvider]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceReferralProvider]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceReferralProvider] AS' 
END
GO

-- ===================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to select Assistance ReferralProvider
-- ===================================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceReferralProvider] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [ReferralProviderCode]
      ,[ReferralProviderDescription]
  FROM [EWC_NSAA].[dbo].[trAssistanceReferralProvider]
	 

END


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceRequestor]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceRequestor]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceRequestor] AS' 
END
GO

-- ===================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to select Assistance Requestor
-- ===================================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceRequestor] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [RequestorCode]
      ,[RequestorDescription]
  FROM [EWC_NSAA].[dbo].[trAssistanceRequestor]
	 

END


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceResolution]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceResolution]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceResolution] AS' 
END
GO

-- ===================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/07/2017
-- Description:	Stored Procedure to select Assistance Resolution
-- ===================================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceResolution] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [ResolutionCode]
      ,[ResolutionDescription]
  FROM [EWC_NSAA].[dbo].[trAssistanceResolution]
	 

END


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectAssistanceResolutionList]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectAssistanceResolutionList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectAssistanceResolutionList] AS' 
END
GO



-- ======================================================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/08/2017
-- Description:	Stored Procedure to select AssistanceResolution based on @FK_AssistanceID
-- ======================================================================================
ALTER PROCEDURE [dbo].[usp_SelectAssistanceResolutionList] 
	-- Add the parameters for the stored procedure here
	@FK_AssistanceID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT
	R.[FK_AssistanceID] ,
	R.[IncidentNumber] ,
	R.[ResolutionInitiationDt] ,
	R.[FK_ResolutionCode] ,
	R.[ResolutionEndDt] ,
	R.[OtherResolution] ,
	R.[DateCreated] ,
	R.[CreatedBy] ,
	R.[LastUpdated] ,
	R.[UpdatedBy] 	 
FROM [dbo].[tAssistanceResolution] R
JOIN tAssistance A
ON R.FK_AssistanceID = A.AssistanceID
WHERE R.FK_AssistanceID = @FK_AssistanceID
	  

END


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectBarrier]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectBarrier]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectBarrier] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Select Barrier 
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectBarrier]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [BarrierCode]
		  ,[BarrierDescription]
		  ,[BarrierTypeCode]
	  FROM [dbo].[trBarrier]
	  --WHERE BarrierTypeCode = @BarrierTypeCode

END


/****** Object:  StoredProcedure [dbo].[usp_SelectBarrierSolution]    Script Date: 7/19/2016 10:10:06 AM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectBarrierList]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectBarrierList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectBarrierList] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 06/07/2016
-- Description:	Get list of Barriers for an Encounter
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectBarrierList] 
	@FK_EncounterID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @NSRecipientID int
	SET @NSRecipientID = (SELECT FK_RecipientID FROM tEncounter WHERE EncounterID = @FK_EncounterID)

    -- Insert statements for procedure here
	SELECT B.[BarrierID]
      ,B.[FK_EncounterID]
      ,B.[BarrierType]
      ,B.[BarrierAssessed]
      ,B.[FollowUpDt]
      ,B.[xtag]
      ,B.[DateCreated]
      ,B.[CreatedBy]
      ,B.[LastUpdated]
      ,B.[UpdatedBy]
	  ,B.[Other]
  FROM [EWC_NSAA].[dbo].[tBarrier] B
  JOIN tEncounter E
  ON B.FK_EncounterID = E.EncounterID
	JOIN tServiceRecipient SR
	ON E.FK_RecipientID = SR.NSRecipientID
  WHERE E.FK_RecipientID = @NSRecipientID


END




GO
/****** Object:  StoredProcedure [dbo].[usp_SelectBarrierSolution]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectBarrierSolution]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectBarrierSolution] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Select Barrier Solution
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectBarrierSolution]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [BarrierSolutionCode]
		  ,[BarrierSolutionDescription]
		  ,[BarrierTypeCode]
	  FROM [dbo].[trBarrierSolution]
	  --WHERE BarrierTypeCode = @BarrierTypeCode

END


/****** Object:  StoredProcedure [dbo].[usp_InsertSolution]    Script Date: 8/4/2016 8:16:52 AM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectBarrierType]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectBarrierType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectBarrierType] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Select Barrier Type
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectBarrierType]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [BarrierTypeCode]
		  ,[BarrierTypeDescription]
	  FROM [dbo].[trBarrierType]
	


END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCancerSite]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectCancerSite]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectCancerSite] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Select Cancer Site values
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectCancerSite] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [CancerSiteCode]
			,[CancerSiteName]
		FROM [dbo].[trCancerSite]



END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCaregiverApproval]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectCaregiverApproval]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectCaregiverApproval] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 03/28/2016
-- Description:	Get list of Caregiver codes and names from lookup table
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectCaregiverApproval]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [CaregiverApprovalCode]
		  ,[CaregiverApprovalDescription]
	  FROM [dbo].[trCaregiverApproval]


END

/****** Object:  StoredProcedure [dbo].[usp_SaveRace]    Script Date: 7/13/2016 2:50:01 PM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCollaboratorContribution]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectCollaboratorContribution]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectCollaboratorContribution] AS' 
END
GO

-- =============================================
-- Author:		Nidhin C	
-- Create date: 5/24/2018
-- Description:	Stored Procedure to get all Collaboration contribution from [tCollaboratorContribution] table
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectCollaboratorContribution] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT CollabContributionID
      ,CollabContribution
  FROM [EWC_NSAA].[dbo].[tCollaboratorContribution]


END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCollaborators]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectCollaborators]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectCollaborators] AS' 
END
GO
-- =============================================
-- Author:		Nidhin C
-- Create date: 06/20/2016
-- Description:	Stored Procedure to get Collaborators
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectCollaborators]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
			PartnersCollaboratorsId,
			NameofOrg1
	  FROM
			tPartnersCollaborators

END


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCountries]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectCountries]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectCountries] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 03/22/2016
-- Description:	Select Countries from lookup table
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectCountries] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [CountryCode]
      ,[CountryName]
	FROM [EWC_NSAA].[dbo].[trCountries]


END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectDiscussionTopics]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectDiscussionTopics]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectDiscussionTopics] AS' 
END
GO

-- =============================================
-- Author:		Nidhin C	
-- Create date: 5/24/2018
-- Description:	Stored Procedure to get all Discussion topics from [tDiscussionTopic] table
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectDiscussionTopics] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DiscussionID
      ,Discussion
  FROM [EWC_NSAA].[dbo].[tDiscussionTopic]


END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectEncounter]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectEncounter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectEncounter] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake	
-- Create date: 05/26/2016
-- Description:	Select Assessments for a Navigation Cycle
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectEncounter] 
	@FK_RecipientID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [EncounterID]
		  ,[FK_NavigatorID]
		  ,[FK_RecipientID]
		  ,[EncounterNumber]
		  ,[EncounterDt]
		  ,[EncounterStartTime]
		  ,[EncounterEndTime]
		  ,[EncounterType]
		  ,[EncounterReason]
		  ,[MissedAppointment]
		  ,[SvcOutcomeStatus]
		  ,[SvcOutcomeStatusReason]
		  ,[NextAppointmentDt]
		  ,[NextAppointmentTime]
		  ,[NextAppointmentType]
		  ,[ServicesTerminated]
		  ,[ServicesTerminatedDt]
		  ,[ServicesTerminatedReason]
		  ,[Notes]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
		  ,[EncounterPurpose]
	  FROM [dbo].[tEncounter]
	  WHERE FK_RecipientID = @FK_RecipientID

END


/****** Object:  StoredProcedure [dbo].[usp_GetEncounter]    Script Date: 9/14/2016 9:29:09 AM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectHealthInsurance]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectHealthInsurance]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectHealthInsurance] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Select Health Insurance Plan values
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectHealthInsurance] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [HealthInsuranceCode]
			,[HealthInsuranceName]
		FROM [dbo].[trHealthInsurance]



END













GO
/****** Object:  StoredProcedure [dbo].[usp_SelectHealthProgram]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectHealthProgram]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectHealthProgram] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Select Health Program values
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectHealthProgram] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [HealthProgramCode]
			,[HealthProgramName]
		FROM [dbo].[trHealthProgram]



END


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectLanguages]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectLanguages]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectLanguages] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake	
-- Create date: 4/20/2016
-- Description:	Stored Procedure to get all languages from trLanguages table
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectLanguages] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [LanguageCode]
      ,[LanguageName]
  FROM [EWC_NSAA].[dbo].[trProvLanguages]


END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectMyRole]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectMyRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectMyRole] AS' 
END
GO

-- =============================================
-- Author:		Nidhin C	
-- Create date: 5/24/2018
-- Description:	Stored Procedure to get all Roles from [tMyRole] table
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectMyRole] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT RoleID
      ,[Role]
  FROM [EWC_NSAA].[dbo].[tMyRole]


END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectNavigationCycle]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectNavigationCycle]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectNavigationCycle] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/26/2016
-- Description:	Select Navigation Cycles for a Recipient
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectNavigationCycle] 
	@FK_RecipientID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [NavigationCycleID]
		  ,[FK_RecipientID]
		  ,[FK_ProviderID]
		  ,[FK_ReferralID]
		  ,[CancerSite]
		  ,[NSProblem]
		  ,[HealthProgram]
		  ,[HealthInsuranceStatus]
		  ,[HealthInsurancePlan]
		  ,[HealthInsurancePlanNumber]
		  ,[ContactPrefDays]
		  ,[ContactPrefHours]
		  ,[ContactPrefHoursOther]
		  ,[ContactPrefType]
		  ,[SRIndentifierCodeGenerated]
		  ,[FK_NavigatorID]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
	  FROM [dbo].[tNavigationCycle] 
	  WHERE FK_RecipientID = @FK_RecipientID

END


/****** Object:  StoredProcedure [dbo].[usp_GetNavigationCycle]    Script Date: 8/11/2016 3:50:13 PM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectNavigator]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectNavigator]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectNavigator] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/05/2016
-- Description:	Select a Navigator record based on domain name
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectNavigator] 
	@DomainName varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [NavigatorID]
		  ,[DomainName]
		  ,[Region]
		  ,[Type]
		  ,[LastName]
		  ,[FirstName]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[Zip]
		  ,[BusinessTelephone]
		  ,[MobileTelephone]
		  ,[Email]
		  ,[xtag]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[LastUpdated]
		  ,[UpdatedBy]
	  FROM [dbo].[tNavigator]
	  WHERE DomainName = @DomainName



END


/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigator]    Script Date: 7/5/2016 2:18:08 PM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectNSProblem]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectNSProblem]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectNSProblem] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Select NSProblem values
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectNSProblem] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT [NSProblemCode]
		  ,[NSProblemDescription]
	  FROM [dbo].[trNSProblem]


END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectNSProvider]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectNSProvider]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectNSProvider] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake	
-- Create date: 04/29/2016
-- Description:	Stored Procedure for getting a Provider record
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectNSProvider] 
	@NSProvideriD int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [NSProviderID]
      ,[PCPName]
	  ,[EWCPCP]
      ,[PCP_NPI]
      ,[PCP_Owner]
      ,[PCP_Location]
      ,[PCPAddress1]
      ,[PCPAddress2]
      ,[PCPCity]
      ,[PCPState]
      ,[PCPZip]
      ,[PCPContactFName]
      ,[PCPContactLName]
      ,[PCPContactTitle]
      ,[PCPContactTelephone]
      ,[PCPContactEmail]
      ,[Medical]
      ,[MedicalSpecialty]
      ,[MedicalSpecialtyOther]
      ,[ManagedCarePlan]
      ,[ManagedCarePlanOther]
      ,[xtag]
      ,[DateCreated]
      ,[CreatedBy]
      ,[LastUpdated]
      ,[UpdatedBy]
  FROM [EWC_NSAA].[dbo].[tNSProvider]
  WHERE NSProviderID = @NSProviderID


END


/****** Object:  StoredProcedure [dbo].[usp_UpdateNSProvider]    Script Date: 7/15/2016 10:39:56 AM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectRace]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectRace]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectRace] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 03/28/2016
-- Description:	Get list of Race codes and names from lookup table
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectRace]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [RaceCode]
      ,[RaceName]
	FROM [EWC_NSAA].[dbo].[trRace]



END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectRaceAsian]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectRaceAsian]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectRaceAsian] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 07/07/2016
-- Description:	Get list of Race Asian
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectRaceAsian]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [RaceAsianCode]
		  ,[RaceAsianName]
	  FROM [dbo].[trRaceAsian]

END


SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectRacePacIslander]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectRacePacIslander]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectRacePacIslander] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 07/07/2016
-- Description:	Get list of Race Pac Islander
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectRacePacIslander]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [RacePacIslanderCode]
		  ,[RacePacIslanderName]
	  FROM [dbo].[trRacePacIslander]

END

/****** Object:  StoredProcedure [dbo].[usp_SaveRace]    Script Date: 7/11/2016 1:59:32 PM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectResults]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectResults]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectResults] AS' 
END
GO

-- =============================================
-- Author:		Nidhin C	
-- Create date: 5/24/2018
-- Description:	Stored Procedure to get all Results from [tDiscussionTopic] table
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectResults] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ResultID
      ,Result
  FROM [EWC_NSAA].[dbo].[tResults]


END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectScreeningNav]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectScreeningNav]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectScreeningNav] AS' 
END
GO


-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 07/31/2017
-- Description:	Select a Screening Nav record based on SN_ID
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectScreeningNav] 
	@SN_ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

SELECT [SN_ID]
	  ,[FK_NavigatorID]
      ,[SN_CODE1]
      ,[SN_CODE2]
      ,[LastName]
      ,[FirstName]
      ,[DOB]
      ,[Address1]
      ,[Address2]
      ,[City]
      ,[State]
      ,[Zip]
      ,[HomeTelephone]
      ,[Cellphone]
      ,[Email]
      ,[Computer]
      ,[TextMessage]
      ,[DateOfContact1]
      ,[ApptScreen1]
      ,[SvcDate1]
      ,[SvcType1]
      ,[Response1]
      ,[DateOfContact2]
      ,[ApptScreen2]
      ,[SvcDate2]
      ,[SvcType2]
      ,[Response2]
      ,[NSEnrollment]
      ,[EnrollmentDate]
      ,[xtag]
      ,[DateCreated]
      ,[CreatedBy]
      ,[LastUpdated]
      ,[UpdatedBy]
  FROM [dbo].[tScreeningNav]
  WHERE SN_ID = @SN_ID
END



SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectServiceRecipient]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectServiceRecipient]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectServiceRecipient] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 03/22/2016
-- Description:	Select Recipients from NS Recipients table
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectServiceRecipient]
	-- Add the parameters for the stored procedure here
	@FK_NavigatorID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [NSRecipientID]
	  ,[FK_NavigatorID]
      ,[FK_RecipID]
      ,[LastName]
      ,[FirstName]
      ,[MiddleInitial]
      ,[DOB]
      ,[AddressNotAvailable]
      ,[Address1]
      ,[Address2]
      ,[City]
      ,[State]
      ,[Zip]
      ,[Phone1]
      ,[P1PhoneType]
      ,[P1PersonalMessage]
      ,[Phone2]
      ,[P2PhoneType]
      ,[P2PersonalMessage]
      ,[MotherMaidenName]
      ,[Email]
      ,[SSN]
      ,[ImmigrantStatus]
      ,[CountryOfOrigin]
      ,[Gender]
      ,[Ethnicity]
      ,[PrimaryLanguage]
      ,[AgreeToSpeakEnglish]
      ,[CaregiverName]
      ,[Relationship]
      ,[RelationshipOther]
      ,[CaregiverPhone]
      ,[CaregiverPhoneType]
      ,[CaregiverPhonePersonalMessage]
      ,[CaregiverEmail]
      ,[CaregiverContactPreference]
      ,[Comments]
      ,[xtag]
      ,[DateCreated]
      ,[CreatedBy]
      ,[LastUpdated]
      ,[UpdatedBy]
  FROM [EWC_NSAA].[dbo].[tServiceRecipient]
  WHERE FK_NavigatorID = @FK_NavigatorID
  



END



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectSolutionsList]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectSolutionsList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectSolutionsList] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 06/10/2016
-- Description:	List of Solutions for a Barrier
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectSolutionsList] 
	-- Add the parameters for the stored procedure here
	@FK_BarrierID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [SolutionID]
      ,[FK_BarrierID]
      ,[SolutionOffered]
      ,[SolutionImplementationStatus]
	  ,[FollowUpDt]
      ,[xtag]
      ,[DateCreated]
      ,[CreatedBy]
      ,[LastUpdated]
      ,[UpdatedBy]
	  ,[Solution]
  FROM [EWC_NSAA].[dbo].[tSolution]
WHERE FK_BarrierID = @FK_BarrierID


END



/****** Object:  StoredProcedure [dbo].[usp_UpdateSolution]    Script Date: 8/4/2016 8:18:47 AM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectTransferReasons]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SelectTransferReasons]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_SelectTransferReasons] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 12/19/2016
-- Description:	Get a list of Navigator Transfer Reasons
-- =============================================
ALTER PROCEDURE [dbo].[usp_SelectTransferReasons]
	@NavigatorID1 int,
	@NavigatorID2 int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @NavigatorType1 int
	declare @NavigatorType2 int

	set @NavigatorType1 = (SELECT [Type] FROM tNavigator WHERE NavigatorID = @NavigatorID1)
	set @NavigatorType2 = (SELECT [Type] FROM tNavigator WHERE NavigatorID = @NavigatorID2)

    -- Insert statements for procedure here

	if @NavigatorType1 = 1 and @NavigatorType2 = 2 --HE to CC
	begin

	SELECT [NavigatorTransferReasonCode]
		  ,[NavigatorTransferReason]
	  FROM [dbo].[trNavigatorTransferReason]
	  WHERE NavigatorTransferReasonCode BETWEEN 10 and 20
	  
	end

	if @NavigatorType1 = 2 and @NavigatorType2 = 1 --CC to HE
	begin

	SELECT [NavigatorTransferReasonCode]
		  ,[NavigatorTransferReason]
	  FROM [dbo].[trNavigatorTransferReason]
	  WHERE NavigatorTransferReasonCode BETWEEN 20 and 30
	  
	end

	if @NavigatorType1 = 2 and @NavigatorType2 = 2 --CC to CC
	begin

	SELECT [NavigatorTransferReasonCode]
		  ,[NavigatorTransferReason]
	  FROM [dbo].[trNavigatorTransferReason]
	  WHERE NavigatorTransferReasonCode BETWEEN 30 and 40
	  
	end

	if @NavigatorType1 = 1 and @NavigatorType2 = 1 --HE to HE
	begin

	SELECT [NavigatorTransferReasonCode]
		  ,[NavigatorTransferReason]
	  FROM [dbo].[trNavigatorTransferReason]
	  
	end

	if (@NavigatorID1 = 0 and @NavigatorID2 = 0) or (@NavigatorType1 > 2) or (@NavigatorType2 > 2)
	begin

	SELECT [NavigatorTransferReasonCode]
		  ,[NavigatorTransferReason]
	  FROM [dbo].[trNavigatorTransferReason]
	  
	end

END


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateAssistance]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateAssistance]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateAssistance] AS' 
END
GO

-- ====================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/08/2017
-- Description:	Stored Procedure to update Assistance  
-- Edited by Bilwa Buchake: Added FK_NavigatorID column
-- =====================================================
ALTER PROCEDURE [dbo].[usp_UpdateAssistance] 
	-- Add the parameters for the stored procedure here
	@AssistanceID int,
	@FK_NavigatorID int,
	@IncidentNumber char(10),
	@IncidentDt smalldatetime ,
	@FK_CommunicationCode smallint ,
	@FirstName varchar(20) ,
	@Lastname varchar(30) ,
	@Telephone char(10) ,
	@Email varchar(45) ,
	@ZipCode varchar(10),
	@RecipID char(14) ,
	@FK_RequestorCode smallint ,
	@NPI varchar(10) ,
	@NPIOwner varchar(2) ,
	@NPISvcLoc varchar(3) ,
	@BusinessName varchar(100),
	@Other varchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Update statement
UPDATE [EWC_NSAA].[dbo].[tAssistance] 
	SET [FK_NavigatorID] = @FK_NavigatorID,
	[IncidentNumber] = @IncidentNumber,
	[IncidentDt] = @IncidentDt ,
	[FK_CommunicationCode] = @FK_CommunicationCode,
	[FirstName] = @FirstName,
	[Lastname] = @Lastname,
	[Telephone] = @Telephone ,
	[Email] = @Email,
	[ZiPCode]=@ZipCode,
	[RecipID] = @RecipID,
	[FK_RequestorCode] = @FK_RequestorCode,
	[NPI] = @NPI,
	[NPIOwner] = @NPIOwner,
	[NPISvcLoc] = @NPISvcLoc,
	[BusinessName] = @BusinessName,
	[Other]=@Other,
	[LastUpdated] = getdate(),
	[UpdatedBy] = 'User update'
WHERE AssistanceID = @AssistanceID

END

SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateAssistanceIssues]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateAssistanceIssues]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateAssistanceIssues] AS' 
END
GO


-- =========================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/08/2017
-- Description:	Stored Procedure to update AssistanceIssues  
-- ==========================================================
ALTER PROCEDURE [dbo].[usp_UpdateAssistanceIssues] 
	-- Add the parameters for the stored procedure here
	@IssueID int=null ,
	@FK_AssistanceID int,
	@IncidentNumber char(10),
	@FK_EWCRecipientCode smallint ,
	@FK_EWCPCPCode smallint ,
	@FK_ReferralProviderCode smallint ,
	@FK_EWCEnrolleeCode smallint,
	@OtherIssues varchar(500),
	@ProblemOtherIssue varchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Update statement
UPDATE [EWC_NSAA].[dbo].[tAssistanceIssues] 
	SET [FK_AssistanceID] = @FK_AssistanceID,
	[IncidentNumber] = @IncidentNumber,
	[FK_EWCRecipientCode] = @FK_EWCRecipientCode,
	[FK_EWCPCPCode] = @FK_EWCPCPCode,
	[FK_ReferralProviderCode] = @FK_ReferralProviderCode,
	[FK_EWCEnrolleeCode] = @FK_EWCEnrolleeCode,
	[OtherIssues] =@OtherIssues ,
	[ProblemOtherIssue]=@ProblemOtherIssue,
	[LastUpdated] = getdate(),
    [UpdatedBy] = 'User update'

WHERE FK_AssistanceID = @FK_AssistanceID

END



GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateAssistanceResolution]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateAssistanceResolution]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateAssistanceResolution] AS' 
END
GO



-- ==============================================================
-- Author:		Prasanna Chinnappagari
-- Create date: 11/08/2017
-- Description:	Stored Procedure to update AssistanceResolution 
-- ==============================================================
ALTER PROCEDURE [dbo].[usp_UpdateAssistanceResolution] 
	-- Add the parameters for the stored procedure here
	@ResolutionID int=null,
	@FK_AssistanceID int ,
	@IncidentNumber char(10),
	@ResolutionInitiationDt smalldatetime,
	@FK_ResolutionCode smallint,
	@ResolutionEndDt smalldatetime,
	@OtherResolution varchar(500),
	@UnabletoResolve varchar(500) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Update statement
UPDATE [EWC_NSAA].[dbo].[tAssistanceResolution] 
	SET [FK_AssistanceID] = @FK_AssistanceID ,
	[IncidentNumber] = @IncidentNumber ,
	[ResolutionInitiationDt] = @ResolutionInitiationDt ,
	[FK_ResolutionCode] = @FK_ResolutionCode,
	[ResolutionEndDt] = @ResolutionEndDt,
	[OtherResolution] = @OtherResolution ,
	[UnabletoResolve]=@UnabletoResolve,
	[LastUpdated] = getdate(),
    [UpdatedBy] = 'User update'

WHERE FK_AssistanceID = @FK_AssistanceID

END




GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateBarrier]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateBarrier]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateBarrier] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description: Update a Barrier record
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateBarrier]
	-- Add the parameters for the stored procedure here
	@BarrierID int
	,@FK_EncounterID int
    ,@BarrierType int
    ,@BarrierAssessed int
    ,@FollowUpDt smalldatetime
	,@Other varchar(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	UPDATE [dbo].[tBarrier]
	   SET [FK_EncounterID] = @FK_EncounterID
		  ,[BarrierType] = @BarrierType
		  ,[BarrierAssessed] = @BarrierAssessed
		  ,[FollowUpDt] = @FollowUpDt
		  ,[LastUpdated] = GETDATE()
		  ,[UpdatedBy] = 'User update'
		  ,[Other] = @Other
	 WHERE BarrierID = @BarrierID



END


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateEncounter]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateEncounter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateEncounter] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/17/2016
-- Description:	Update an Encounter
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateEncounter] 
	-- Add the parameters for the stored procedure here
	@EncounterID int
	,@FK_NavigatorID int
    ,@FK_RecipientID int
    ,@EncounterNumber int
    ,@EncounterDt smalldatetime
    ,@EncounterStartTime varchar(20)
    ,@EncounterEndTime varchar(20)
    ,@EncounterType int
	,@EncounterReason int
    ,@MissedAppointment bit
    ,@SvcOutcomeStatus int
    ,@SvcOutcomeStatusReason varchar(200)
    ,@NextAppointmentDt smalldatetime
    ,@NextAppointmentTime varchar(20)
    ,@NextAppointmentType int
    ,@ServicesTerminated bit
    ,@ServicesTerminatedDt smalldatetime
    ,@ServicesTerminatedReason varchar(200)
    ,@Notes varchar(2000)
    ,@EncounterPurpose int
           
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


	UPDATE [dbo].[tEncounter]
	   SET [FK_NavigatorID] = @FK_NavigatorID
		  ,[FK_RecipientID] = @FK_RecipientID
		  ,[EncounterNumber] = @EncounterNumber
		  ,[EncounterDt] = @EncounterDt
		  ,[EncounterStartTime] = @EncounterStartTime
		  ,[EncounterEndTime] = @EncounterEndTime
		  ,[EncounterType] = @EncounterType
		  ,[EncounterReason] = @EncounterReason
		  ,[MissedAppointment] = @MissedAppointment
		  ,[SvcOutcomeStatus] = @SvcOutcomeStatus
		  ,[SvcOutcomeStatusReason] = @SvcOutcomeStatusReason
		  ,[NextAppointmentDt] = @NextAppointmentDt
		  ,[NextAppointmentTime] = @NextAppointmentTime
		  ,[NextAppointmentType] = @NextAppointmentType
		  ,[ServicesTerminated] = @ServicesTerminated
		  ,[ServicesTerminatedDt] = @ServicesTerminatedDt
		  ,[ServicesTerminatedReason] = @ServicesTerminatedReason
		  ,[Notes] = @Notes
		  ,[LastUpdated] = GETDATE()
		  ,[UpdatedBy] = 'User Update'
		  ,[EncounterPurpose] = @EncounterPurpose
	 WHERE EncounterID = @EncounterID



END



SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateHECActivity]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateHECActivity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateHECActivity] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_UpdateHECActivity]
@ActivityId int = null,
@ActivityDate	datetime,	
@Cancelled bit,
@ReasonForCancellation varchar(250),
@ActivityType	smallint,
@OtherActivityType	varchar(300),	
@NameOrPurpose	varchar(250),	
@CollaboratorId1	int,	
@CollaboratorId2	int,	
@CollaboratorContributionId1	int,	
@CollaboratorContributionId2	int,	
@Address1	nvarchar(100),
@Address2	nvarchar(100),	
@City	VARCHAR(50),	
@Zip	varchar(5),	
@CHW1	int,
@CHW2	int,
@OtherAttendee1	nvarchar(100),	
@OtherAttendee2	nvarchar(100),	
@Population	varchar(50),	
@LanguageId	int,	
@Discussed	int,	
@OtherDiscussed	varchar(300),	
@PrePostTest	int,	
@Result	int,	
@OtherResult	varchar(300),	
@MyRole	int,
@OtherRole	varchar(300),	
@Attendee	bit,	
@AnnoucementDoc	varchar(300),	
@Travel	bit,	
@Notes	nvarchar(250),

@FirstName	nvarchar(50),	
@LastName	nvarchar(50),	
@Month	smallint,	
@Year	int,	
@RaceOrEthinicity	int,	
@Part_Info_Address1	nvarchar(100),	
@Part_Info_Address2	nvarchar(100),	
@Part_Info_City	varchar(50),
@Part_Info_Zip	varchar(5),	
@Telephone	varchar(50),	
@Email	varchar(150),	
@Part_Info_LanguageId	int,
@MAM bit,
@PAP bit,
@HI bit,
@OkToContact bit,
@LastPAP int,
@LastMAM int,
@EWCEligible bit,

@BusinessName	nvarchar(100),	
@NPI	varchar(50),	
@ProviderFirstName	nvarchar(50),	
@ProviderLastName	nvarchar(50),	
@EwcProvider	smallint

AS
Begin

IF(@ActivityId>0)
Begin

UPDATE tHecActivities
    SET ActivityDate = @ActivityDate,
		ActivityCancelled = @Cancelled,
		ReasonForCancellation = @ReasonForCancellation,
	    ActivityType = @ActivityType,
		NameOrPurpose = @NameOrPurpose,
		CollaboratorId1 = @CollaboratorId1,  
		CollaboratorId2 = @CollaboratorId2,  
		CollaboratorContributionId1 = @CollaboratorContributionId1,
		CollaboratorContributionId2 = @CollaboratorContributionId2,
		Address1 = @Address1, 
		Address2 = @Address2,  
		City = @City,  
		Zip = @Zip,  
		CHW1 = @CHW1,  
		CHW2 = @CHW2,  
		OtherAttendee1 = @OtherAttendee1,
		OtherAttendee2 = @OtherAttendee2,
		Population = @Population,  
		LanguageId = @LanguageId,  
		Discussed = @Discussed,  
		OtherDiscussed = @OtherDiscussed,  
		PrePostTest = @PrePostTest,  
		Result = @Result,  
		OtherResult = @OtherResult,  
		MyRole = @MyRole,  
		OtherMyRole = @OtherRole,  
		Attendee = @Attendee,  
		AnnoucementDoc = @AnnoucementDoc,  
		Travel = @Travel,  
		Notes = @Notes
  WHERE ActivityId = @ActivityId

  UPDATE tHecActivityParticipantInfo
     SET FirstName=@FirstName,
	     LastName = @LastName,
		 [Month] = @Month,[Year] = @Year,
		 RaceOrEthinicity = @RaceOrEthinicity,
		 Address1 = @Part_Info_Address1,
		 Address2 = @Part_Info_Address2,
		 City = @Part_Info_City,
		 Zip = @Part_Info_Zip,
		 Telephone = @Telephone,
		 Email = @Email,
		 LanguageId= @LanguageId,
		 MAM = @MAM,
		 PAP= @PAP,
		 HI= @HI,
		 OkToContact= @OkToContact,
		 LastPAP= @LastPAP,
		 LastMAM= @LastMAM,
		 EWCEligible= @EWCEligible
   WHERE @ActivityId = @ActivityId

   UPDATE tHecScreeningEvent 
      SET BusinessName = @BusinessName,
		  NPI = @NPI,
		  ProviderFirstName = @ProviderFirstName,
		  ProviderLastName = @ProviderLastName,
		  EwcProvider = @EwcProvider
	WHERE ActivityId = @ActivityId
  



END



END
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigationCycle]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNavigationCycle]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateNavigationCycle] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/12/2016
-- Description:	Update a Navigation Cycle
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateNavigationCycle] 
	-- Add the parameters for the stored procedure here
			@NavigationCycleID int
		   ,@FK_RecipientID int
           ,@FK_ProviderID int
           ,@FK_ReferralID int
           ,@CancerSite int
           ,@NSProblem int
           ,@HealthProgram int
           ,@HealthInsuranceStatus int
           ,@HealthInsurancePlan int
           ,@HealthInsurancePlanNumber varchar(15)
           ,@ContactPrefDays varchar(15)
           ,@ContactPrefHours varchar(15)
           ,@ContactPrefHoursOther varchar(20)
           ,@ContactPrefType int
           ,@SRIndentifierCodeGenerated bit
           ,@FK_NavigatorID int
           
           
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [EWC_NSAA].[dbo].[tNavigationCycle]
   SET [FK_RecipientID] = @FK_RecipientID
      ,[FK_ProviderID] = @FK_ProviderID
      ,[FK_ReferralID] = @FK_ReferralID
      ,[CancerSite] = @CancerSite
      ,[NSProblem] = @NSProblem
      ,[HealthProgram] = @HealthProgram
      ,[HealthInsuranceStatus] = @HealthInsuranceStatus
      ,[HealthInsurancePlan] = @HealthInsurancePlan
      ,[HealthInsurancePlanNumber] = @HealthInsurancePlanNumber
      ,[ContactPrefDays] = @ContactPrefDays
      ,[ContactPrefHours] = @ContactPrefHours
      ,[ContactPrefHoursOther] = @ContactPrefHoursOther
      ,[ContactPrefType] = @ContactPrefType
      ,[SRIndentifierCodeGenerated] = @SRIndentifierCodeGenerated
      ,[FK_NavigatorID] = @FK_NavigatorID
      ,[LastUpdated] = getdate()
      ,[UpdatedBy] = 'User Update'
 WHERE NavigationCycleID = @NavigationCycleID
END


/****** Object:  StoredProcedure [dbo].[usp_SelectNavigationCycle]    Script Date: 8/10/2016 2:46:46 PM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigator]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNavigator]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateNavigator] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/05/2016
-- Description:	Update Navigator record
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateNavigator] 
	-- Add the parameters for the stored procedure here
	@NavigatorID int
	,@DomainName varchar(20)
	,@Region smallint
    ,@Type int
    ,@LastName varchar(30)
    ,@FirstName varchar(20)
    ,@Address1 varchar(30)
    ,@Address2 varchar(30)
    ,@City varchar(25)
    ,@State char(2)
    ,@Zip char(5)
    ,@BusinessTelephone char(10)
	,@MobileTelephone char(10)
    ,@Email varchar(45)
    
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	UPDATE [dbo].[tNavigator]
	   SET [DomainName] = @DomainName
		  ,[Region] = @Region
		  ,[Type] = @Type
		  ,[LastName] = @LastName
		  ,[FirstName] = @FirstName
		  ,[Address1] = @Address1
		  ,[Address2] = @Address2
		  ,[City] = @City
		  ,[State] = @State
		  ,[Zip] = @Zip
		  ,[BusinessTelephone] = @BusinessTelephone
		  ,[MobileTelephone] = @MobileTelephone
		  ,[Email] = @Email
		  ,[LastUpdated] = GETDATE()
		  ,[UpdatedBy] = 'User Input'
	 WHERE NavigatorID = @NavigatorID
	


END



SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigatorLanguage]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNavigatorLanguage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateNavigatorLanguage] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake	
-- Create date: 12/12/2016
-- Description:	Update Navigator Language record
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateNavigatorLanguage] 
	-- Add the parameters for the stored procedure here
	@NavigatorLanguageID int,
	@FK_NavigatorID int,
	@LanguageCode int,
	@SpeakingScore int,
	@ListeningScore int,
	@ReadingScore int,
	@WritingScore int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	UPDATE [dbo].[tNavigatorLanguages]
	   SET [FK_NavigatorID] = @FK_NavigatorID
		  ,[LanguageCode] = @LanguageCode
		  ,[SpeakingScore] = @SpeakingScore
		  ,[ListeningScore] = @ListeningScore
		  ,[ReadingScore] = @ReadingScore
		  ,[WritingScore] = @WritingScore
		  ,[LastUpdated] = GETDATE()
		  ,[UpdatedBy] = 'User Update'
	 WHERE NavigatorLanguageID = @NavigatorLanguageID

END



SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNavigatorTraining]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNavigatorTraining]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateNavigatorTraining] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 11/17/2016
-- Description:	Update Navigator Training record
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateNavigatorTraining]
	-- Add the parameters for the stored procedure here
	@TrainingID int,
	@FK_NavigatorID int,
	@CourseName varchar(100),
	@Organization varchar(100),
	@CompletionDt smalldatetime,
	@Certificate bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


	UPDATE [dbo].[tNavigatorTraining]
   SET [FK_NavigatorID] = @FK_NavigatorID
      ,[CourseName] = @CourseName
      ,[Organization] = @Organization
      ,[CompletionDt] = @CompletionDt
      ,[Certificate] = @Certificate
      ,[LastUpdated] = GETDATE()
      ,[UpdatedBy] = 'User Update'
	WHERE TrainingID = @TrainingID


END



SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateNSProvider]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateNSProvider]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateNSProvider] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 04/28/2016
-- Description:	Stored Procedure for updating PCPs
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateNSProvider] 
	-- Add the parameters for the stored procedure here
	@NSProviderID int,
	@PCPName varchar(100),
	@EWCPCP bit,
    @PCP_NPI varchar(10),
    @PCP_Owner varchar(2),
    @PCP_Location varchar(3),
    @PCPAddress1 varchar(30),
    @PCPAddress2 varchar(30),
    @PCPCity varchar(24),
    @PCPState char(2),
    @PCPZip char(5),
    @PCPContactFName varchar(50),
    @PCPContactLName varchar(50),
    @PCPContactTitle varchar(50),
    @PCPContactTelephone char(10),
    @PCPContactEmail varchar(45),
    @Medical bit,
    @MedicalSpecialty int,
    @MedicalSpecialtyOther varchar(100),
    @ManagedCarePlan int,
    @ManagedCarePlanOther varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [EWC_NSAA].[dbo].[tNSProvider]
   SET [PCPName] = @PCPName
	  ,[EWCPCP] = @EWCPCP
      ,[PCP_NPI] = @PCP_NPI
      ,[PCP_Owner] = @PCP_Owner
      ,[PCP_Location] = @PCP_Location
      ,[PCPAddress1] = @PCPAddress1
      ,[PCPAddress2] = @PCPAddress2
      ,[PCPCity] = @PCPCity
      ,[PCPState] = @PCPState
      ,[PCPZip] = @PCPZip
      ,[PCPContactFName] = @PCPContactFName
      ,[PCPContactLName] = @PCPContactLName
      ,[PCPContactTitle] = @PCPContactTitle
      ,[PCPContactTelephone] = @PCPContactTelephone
      ,[PCPContactEmail] = @PCPContactEmail
      ,[Medical] = @Medical
      ,[MedicalSpecialty] = @MedicalSpecialty
      ,[MedicalSpecialtyOther] = @MedicalSpecialtyOther
      ,[ManagedCarePlan] = @ManagedCarePlan
      ,[ManagedCarePlanOther] = @ManagedCarePlanOther
      ,[xtag] = NULL
      ,[LastUpdated] = getdate()
      ,[UpdatedBy] = 'User Update'
 WHERE NSProviderID = @NSProviderID


END


/****** Object:  StoredProcedure [dbo].[usp_InsertNSProvider]    Script Date: 7/15/2016 10:41:01 AM ******/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateScreeningNav]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateScreeningNav]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateScreeningNav] AS' 
END
GO


-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 07/31/2017
-- Description:	Update Screening Nav record
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateScreeningNav] 
	-- Add the parameters for the stored procedure here
			@SN_ID int
		   ,@FK_NavigatorID int
		   ,@SN_CODE1 varchar(11)
           ,@SN_CODE2 smallint
           ,@LastName varchar(30)
           ,@FirstName varchar(20)
           ,@DOB smalldatetime
           ,@Address1 varchar(30)
           ,@Address2 varchar(30)
           ,@City varchar(25)
           ,@State char(2)
           ,@Zip char(5)
           ,@HomeTelephone char(10)
           ,@Cellphone char(10)
           ,@Email varchar(45)
           ,@Computer bit
           ,@TextMessage char(1)
           ,@DateOfContact1 smalldatetime
           ,@ApptScreen1 char(1)
           ,@SvcDate1 smalldatetime
           ,@SvcType1 char(2)
           ,@Response1 char(2)
           ,@DateOfContact2 smalldatetime
           ,@ApptScreen2 char(1)
           ,@SvcDate2 smalldatetime
           ,@SvcType2 char(2)
           ,@Response2 char(2)
           ,@NSEnrollment char(1)
           ,@EnrollmentDate smalldatetime
    
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	UPDATE [dbo].[tScreeningNav]
   SET [FK_NavigatorID] = @FK_NavigatorID 
      ,[SN_CODE1] = @SN_CODE1
      ,[SN_CODE2] = @SN_CODE2
      ,[LastName] = @LastName
      ,[FirstName] = @FirstName
      ,[DOB] = @DOB
      ,[Address1] = @Address1
      ,[Address2] = @Address2
      ,[City] = @City
      ,[State] = @State
      ,[Zip] = @Zip
      ,[HomeTelephone] = @HomeTelephone
      ,[Cellphone] = @Cellphone
      ,[Email] = @Email
      ,[Computer] = @Computer
      ,[TextMessage] = @TextMessage
      ,[DateOfContact1] = @DateOfContact1
      ,[ApptScreen1] = @ApptScreen1
      ,[SvcDate1] = @SvcDate1
      ,[SvcType1] = @SvcType1
      ,[Response1] = @Response1
      ,[DateOfContact2] = @DateOfContact2
      ,[ApptScreen2] = @ApptScreen2
      ,[SvcDate2] = @SvcDate2
      ,[SvcType2] = @SvcType2
      ,[Response2] = @Response2
      ,[NSEnrollment] = @NSEnrollment
      ,[EnrollmentDate] = @EnrollmentDate
      ,[LastUpdated] = GETDATE()
      ,[UpdatedBy] = 'User Input'
 WHERE SN_ID = @SN_ID
	


END




GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateServiceRecipient]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateServiceRecipient]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateServiceRecipient] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 03/21/2016
-- Description:	Update a Recipient Enrollment record
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateServiceRecipient] 
	-- Add the parameters for the stored procedure here
		   @NSRecipientID int,
		   @FK_NavigatorID int,
		   @FK_RecipID char(14),
           @LastName varchar(30),
           @FirstName varchar(20),
           @MiddleInitial varchar(1),
           @DOB smalldatetime,
           @AddressNotAvailable bit,
           @Address1 varchar(30),
           @Address2 varchar(30),
           @City varchar(25),
           @State char(2),
           @Zip char(5),
           @Phone1 char(10),
           @P1PhoneType smallint,
           @P1PersonalMessage bit,
           @Phone2 char(10),
           @P2PhoneType smallint,
           @P2PersonalMessage bit,
           @MotherMaidenName varchar(20),
           @Email varchar(45),
           @SSN char(10),
           @ImmigrantStatus bit,
           @CountryOfOrigin smallint,
           @Gender char(1),
           @Ethnicity bit,
           @PrimaryLanguage smallint,
           @AgreeToSpeakEnglish bit,
           @CaregiverName varchar(50),
           @Relationship smallint,
           @RelationshipOther varchar(30),
           @CaregiverPhone char(10),
           @CaregiverPhoneType smallint,
           @CaregiverPhonePersonalMessage bit,
           @CaregiverEmail varchar(45),
           @CaregiverContactPreference int,
           @Comments varchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [EWC_NSAA].[dbo].[tServiceRecipient]
   SET [FK_RecipID] = @FK_RecipID
	  ,[FK_NavigatorID] = @FK_NavigatorID
      ,[LastName] = @LastName
      ,[FirstName] = @FirstName
      ,[MiddleInitial] = @MiddleInitial
      ,[DOB] = @DOB
      ,[AddressNotAvailable] = @AddressNotAvailable
      ,[Address1] = @Address1
      ,[Address2] = @Address2
      ,[City] = @City
      ,[State] = @State
      ,[Zip] = @Zip
      ,[Phone1] = @Phone1
      ,[P1PhoneType] = @P1PhoneType
      ,[P1PersonalMessage] = @P1PersonalMessage
      ,[Phone2] = @Phone2
      ,[P2PhoneType] = @P2PhoneType
      ,[P2PersonalMessage] = @P2PersonalMessage
      ,[MotherMaidenName] = @MotherMaidenName
      ,[Email] = @Email
      ,[SSN] = @SSN
      ,[ImmigrantStatus] = @ImmigrantStatus
      ,[CountryOfOrigin] = @CountryOfOrigin
      ,[Gender] = @Gender
      ,[Ethnicity] = @Ethnicity
      ,[PrimaryLanguage] = @PrimaryLanguage
      ,[AgreeToSpeakEnglish] = @AgreeToSpeakEnglish
      ,[CaregiverName] = @CaregiverName
      ,[Relationship] = @Relationship
      ,[RelationshipOther] = @RelationshipOther
      ,[CaregiverPhone] = @CaregiverPhone
      ,[CaregiverPhoneType] = @CaregiverPhoneType
      ,[CaregiverPhonePersonalMessage] = @CaregiverPhonePersonalMessage
      ,[CaregiverEmail] = @CaregiverEmail
      ,[CaregiverContactPreference] = @CaregiverContactPreference
      ,[Comments] = @Comments
      ,[LastUpdated] = getdate()
      ,[UpdatedBy] = 'User update'
 WHERE NSRecipientID = @NSRecipientID


END



GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateSolution]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateSolution]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateSolution] AS' 
END
GO

-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 05/19/2016
-- Description:	Update Solution
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateSolution]
	-- Add the parameters for the stored procedure here
	@SolutionID int
	,@FK_BarrierID int
    ,@SolutionOffered int
    ,@SolutionImplementationStatus int
	,@FollowUpDt smalldatetime
	,@Solution varchar(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	UPDATE [dbo].[tSolution]
	   SET [FK_BarrierID] = @FK_BarrierID
		  ,[SolutionOffered] = @SolutionOffered
		  ,[SolutionImplementationStatus] = @SolutionImplementationStatus
		  ,[FollowUpDt] = @FollowUpDt
		  ,[LastUpdated] = GETDATE()
		  ,[UpdatedBy] = 'User update'
		  ,[Solution] = @Solution
	 WHERE SolutionID = @SolutionID
	


END



SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateTransferStatus]    Script Date: 5/26/2018 7:05:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateTransferStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateTransferStatus] AS' 
END
GO
-- =============================================
-- Author:		Bilwa Buchake
-- Create date: 09/30/2016
-- Description:	Upate Navigator Transfer Status
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateTransferStatus]
	@TransferID int,
	@TransferStatus int,
	@TransferRefusedReason varchar(200),
	@TransferRefusedDt smalldatetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    --Update Interim transfer
	UPDATE [dbo].[tNavigatorTransfer]
			SET [TransferStatus] = @TransferStatus
				,[TransferRefusedReason] = @TransferRefusedReason
		  		,[TransferRefusedDt] = @TransferRefusedDt
				,[LastUpdated] = getdate()
				,[UpdatedBy] = 'User Update'
	 WHERE TransferID = @TransferID

	 


END


/****** Object:  StoredProcedure [dbo].[usp_GetNavigationCycle]    Script Date: 10/6/2016 2:10:15 PM ******/
SET ANSI_NULLS ON

GO
ALTER DATABASE [EWC_NSAA] SET  READ_WRITE 
GO
