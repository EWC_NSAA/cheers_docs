﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EWC_NSAA.Models;
using EWC_NSAA.ViewModels;
using EWC_NSAA.Common;
using System.Data;

namespace EWC_NSAA.Controllers
{
    //[Authorize(Roles = @"EWC_NSAA_Contributors, EWC_NSAA_A")]
    public class HomeController : BaseController
    {
        public ActionResult CreateModifyNetworking()
        { 
            return View();
        }

        public ActionResult SearchProvider()
        {
            return View();
        }

        public ActionResult SearchActivity()
        {
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            return View();
        }

        public ActionResult HECDashboard()
        {
            return View();
            //return RedirectToActionPermanent("Index", "HECActivities");
        }

        //New method to check for language already exist or not
        public ActionResult IsLanguageExists(int navigatorId, int selectedId)
        {
            List<NavigatorLanguages> languages = GetLanguagesFromDataSet(navigatorId);
            var isValid = languages.Where(x => x.LanguageCode == selectedId).Count() > 0;
            return Json(isValid, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RegionalResources()
        {
            return View();
        }


        // GET: 

        //Home/Search
        public ActionResult Search()
        {
            List<trProvLanguages> lsLanguages = new List<trProvLanguages>();
            List<tServiceRecipient> lsRecipients = new List<tServiceRecipient>();

            lsLanguages = new EnrollmentController().GetLanguagesFromDataSet();
            SearchViewModel SearchVM = new SearchViewModel();

            SearchVM.Languages = lsLanguages;
            SearchVM.Recipients = lsRecipients;

            return View(SearchVM);
        }

        // POST: Home/Search
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Search(SearchViewModel SearchVM)
        {
            List<tServiceRecipient> lsRecipients = new List<tServiceRecipient>();
            List<trProvLanguages> lsLanguages = new List<trProvLanguages>();

            lsRecipients = GetSearchRecipients(SearchVM.NSRecipientID, SearchVM.LastName, SearchVM.DOB);
            lsLanguages = new EnrollmentController().GetLanguagesFromDataSet();

            SearchVM.Recipients = lsRecipients;
            SearchVM.Languages = lsLanguages;

            return View(SearchVM);
        }

        // GET: Home/ProfileCreate
        public ActionResult ProfileCreate()
        {
            NavigatorViewModel NavigatorVM = new NavigatorViewModel();
            tNavigator tnavigator = new tNavigator();
            List<NavigatorViewModel> NavigatorsList = new List<NavigatorViewModel>();
            List<NavigatorTraining> Trainings = new List<NavigatorTraining>();
            List<NavigatorLanguages> Languages = new List<NavigatorLanguages>();

            try
            {
                NavigatorsList = GetNavigatorFromDataSet(User.Identity.Name);                

                if (NavigatorsList.Count > 0)
                {
                    Trainings = GetTrainingsFromDataSet(NavigatorsList[0].NavigatorID);
                    Languages = GetLanguagesFromDataSet(NavigatorsList[0].NavigatorID);

                    NavigatorVM = NavigatorsList[0];
                    TempData["FeedbackMessage"] = "Edit your profile.";
                }
                else
                {
                    //NavigatorVM.tNavigator = tnavigator;
                    TempData["FeedbackMessage"] = "Create a user profile.";
                }

                NavigatorVM.DomainName = User.Identity.Name;
                NavigatorVM.Trainings = Trainings;
                NavigatorVM.Languages = Languages;
            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

            return View(NavigatorVM);
        }

        // POST: Navigators/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProfileCreate(NavigatorViewModel NavigatorVM) //[Bind(Include = "tNavigator.NavigatorID,tNavigator.Type,tNavigator.LastName,tNavigator.FirstName,tNavigator.Address1,tNavigator.Address2,tNavigator.City,tNavigator.State,tNavigator.Zip,tNavigator.Telephone,tNavigator.Email")] NavigatorViewModel NavigatorVM)
        {   
            NavigatorVM.Trainings = GetTrainingsFromDataSet(NavigatorVM.NavigatorID);
            NavigatorVM.Languages = GetLanguagesFromDataSet(NavigatorVM.NavigatorID);
            if (ModelState.IsValid)
            {
                try
                {
                    var businessTel = "";
                    if (!string.IsNullOrWhiteSpace(NavigatorVM.BusinessTelephone))
                    {
                        businessTel = NavigatorVM.BusinessTelephone.Replace("(", "").Replace(")", "").Replace("-", "").Trim();
                    }

                    if (IfNavigatorExists())
                    {

                        int retValue = getProxy().UpdateNavigator(NavigatorVM.NavigatorID, User.Identity.Name,
                            NavigatorVM.Region, NavigatorVM.Type, NavigatorVM.LastName, NavigatorVM.FirstName,
                            NavigatorVM.Address1, NavigatorVM.Address2, NavigatorVM.City,
                            NavigatorVM.State, NavigatorVM.Zip, businessTel,
                            NavigatorVM.MobileTelephone, NavigatorVM.Email);

                        //NavigatorVM.Trainings = GetTrainingsFromDataSet(NavigatorVM.NavigatorID);
                        //NavigatorVM.Languages = GetLanguagesFromDataSet(NavigatorVM.NavigatorID);


                        TempData["FeedbackMessage"] = "Your changes were saved successfully.";
                    }
                    else
                    {
                        
                        int retValue = getProxy().InsertNavigator(User.Identity.Name,
                            NavigatorVM.Region, NavigatorVM.Type, NavigatorVM.LastName, NavigatorVM.FirstName,
                            NavigatorVM.Address1, NavigatorVM.Address2, NavigatorVM.City,
                            NavigatorVM.State, NavigatorVM.Zip, businessTel,
                            NavigatorVM.MobileTelephone, NavigatorVM.Email);

                        //NavigatorVM.Trainings = GetTrainingsFromDataSet(NavigatorVM.NavigatorID);
                        //NavigatorVM.Languages = GetLanguagesFromDataSet(NavigatorVM.NavigatorID);
                        TempData["FeedbackMessage"] = "Your data was saved successfully.";
                    }

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }

            }
            else
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
            }


            return View(NavigatorVM);
        }

        /*
        [NonAction]
        public void TrainingUpdate(NavigatorTraining TrainingModel)
        {
            
            try
            {
                int retValue = getProxy().UpdateNavigatorTraining(TrainingModel.TrainingID, TrainingModel.FK_NavigatorID,
                            TrainingModel.CourseName, TrainingModel.Organization, TrainingModel.CompletionDt, TrainingModel.Certificate);
            }
            catch
            {
                throw;
            }
                
            
        }



        [NonAction]
        public void TrainingInsert(NavigatorTraining TrainingModel)
        {

            try
            {
                int retValue = getProxy().InsertNavigatorTraining(TrainingModel.FK_NavigatorID,
                            TrainingModel.CourseName, TrainingModel.Organization, TrainingModel.CompletionDt, TrainingModel.Certificate);
            }
            catch
            {
                throw;
            }


        }
        */










        public ActionResult TrainingUpdate(NavigatorTraining TrainingModel)
        {
            int retValue = 0;
            try
            {
                retValue = getProxy().UpdateNavigatorTraining(TrainingModel.TrainingID, TrainingModel.FK_NavigatorID,
                           TrainingModel.CourseName, TrainingModel.Organization, TrainingModel.CompletionDt, TrainingModel.Certificate);
            }
            catch
            {
                throw;
            }
            return Json(retValue, JsonRequestBehavior.AllowGet);
        }

        //[NonAction]
        //public void TrainingInsert(NavigatorTraining TrainingModel)
        public void TrainingInsert(NavigatorTraining TrainingModel)
        {
            //int retValue = 0;
            try
            {
                int retValue = getProxy().InsertNavigatorTraining(TrainingModel.FK_NavigatorID,
                             TrainingModel.CourseName, TrainingModel.Organization, TrainingModel.CompletionDt, TrainingModel.Certificate);
            }
            catch
            {
                throw;
            }
            //return Json(retValue, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadFiles()
        {
            HttpFileCollectionBase files = Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFileBase file = files[i];
                string fname;

                // Checking for Internet Explorer  
                if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                {
                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                    fname = testfiles[testfiles.Length - 1];
                }
                else
                {
                    fname = file.FileName;
                }

                string fileName = Request.Form["FK_NavigatorID"] + System.IO.Path.GetExtension(fname);
                // Get the complete folder path and store the file inside it.  
                fname = System.IO.Path.Combine(Server.MapPath("~/App_Data/"), fileName);
                string validateFile = System.IO.Path.Combine(Server.MapPath("~/App_Data/"), Request.Form["FK_NavigatorID"]);

                if (System.IO.File.Exists(validateFile + ".docx"))
                {
                    System.IO.File.Delete(validateFile + ".docx");
                }

                if (System.IO.File.Exists(validateFile + ".pdf"))
                {
                    System.IO.File.Delete(validateFile + ".pdf");
                }

                file.SaveAs(fname);
            }
            // Returns message that successfully uploaded  
            return Json("File Uploaded Successfully!");
        }

        public ActionResult ValidateUploadedFile(string navigatorId, string uploadedFile)
        {
            if (string.IsNullOrWhiteSpace(uploadedFile))
            {
                return Json("nofile", JsonRequestBehavior.AllowGet);
            }
            string extension = System.IO.Path.GetExtension(uploadedFile);
            if (extension.ToLower().Trim() != ".docx" && extension.ToLower().Trim() != ".pdf")
            {
                return Json("Please upload docx or pdf files only.", JsonRequestBehavior.AllowGet);
            }
            else
            {
                string fname = System.IO.Path.Combine(Server.MapPath("~/App_Data/"), navigatorId);
                if (System.IO.File.Exists(fname + ".docx"))
                {
                    return Json("File is already exists, Do you want to override the file.", JsonRequestBehavior.AllowGet);
                }
                else if (System.IO.File.Exists(fname + ".pdf"))
                {
                    return Json("File is already exists, Do you want to override the file.", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("valid", JsonRequestBehavior.AllowGet);
                }
            }
        }







































        //[NonAction]
        //public void LanguageUpdate(NavigatorLanguages LanguageModel)
        //{
        //    try
        //    {
        //        int retValue = getProxy().UpdateNavigatorLanguage(LanguageModel.NavigatorLanguageID, LanguageModel.FK_NavigatorID,
        //                    LanguageModel.LanguageCode, LanguageModel.SpeakingScore, LanguageModel.ListeningScore, LanguageModel.ReadingScore,
        //                    LanguageModel.WritingScore);
        //    }
        //    catch
        //    {
        //        throw;
        //    }

        //}

        //[NonAction]
        //public void LanguageInsert(NavigatorLanguages LanguageModel)
        //{

        //    try
        //    {
        //        int retValue = getProxy().InsertNavigatorLanguage(LanguageModel.FK_NavigatorID,
        //                    LanguageModel.LanguageCode, LanguageModel.SpeakingScore, LanguageModel.ListeningScore, LanguageModel.ReadingScore,
        //                    LanguageModel.WritingScore);
        //    }
        //    catch
        //    {
        //        throw;
        //    }


        //}





        public ActionResult LanguageUpdate(NavigatorLanguages LanguageModel)
        {
            int retValue = 0;

            try
            {
                retValue = getProxy().UpdateNavigatorLanguage(LanguageModel.NavigatorLanguageID, LanguageModel.FK_NavigatorID,
                           LanguageModel.LanguageCode, LanguageModel.SpeakingScore, LanguageModel.ListeningScore, LanguageModel.ReadingScore,
                           LanguageModel.WritingScore);
            }
            catch
            {
                throw;
            }
            return Json(retValue, JsonRequestBehavior.AllowGet);
        }


        public ActionResult LanguageInsert(NavigatorLanguages LanguageModel)
        {
            int retValue = 0;
            try
            {
                LanguageModel.DateCreated = DateTime.Now;

                retValue = getProxy().InsertNavigatorLanguage(LanguageModel.FK_NavigatorID,
                           LanguageModel.LanguageCode, LanguageModel.SpeakingScore, LanguageModel.ListeningScore, LanguageModel.ReadingScore,
                           LanguageModel.WritingScore);
            }
            catch
            {
                throw;
            }
            return Json(retValue, JsonRequestBehavior.AllowGet);
        }
        private bool IfNavigatorExists()
        {

            DataSet NavigatorDS;

            try
            {
                NavigatorDS = getProxy().GetNavigator(User.Identity.Name);
                //Check if there are any records

                if (NavigatorDS.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

        }


        #region Get Navigator Type Dropdown

        [NonAction]
        public static List<SelectListItem> GetNavigatorTypeDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            ls.Add(new SelectListItem() { Text = "Health Educator", Value = "1" });
            ls.Add(new SelectListItem() { Text = "Clinical Coordinator", Value = "2" });
            ls.Add(new SelectListItem() { Text = "CHW", Value = "3" });
            ls.Add(new SelectListItem() { Text = "Other", Value = "4" });

            return ls;
        }


        #endregion

        #region  Get Navigator Language Dropdown
        public static List<SelectListItem> GetNavigatorLanguages()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            ls.Add(new SelectListItem() { Text = "Spanish", Value = "1" });
            ls.Add(new SelectListItem() { Text = "Combodian/Khmer", Value = "2" });
            ls.Add(new SelectListItem() { Text = "Vietnamese", Value = "3" });
            ls.Add(new SelectListItem() { Text = "Hmong", Value = "4" });
            ls.Add(new SelectListItem() { Text = "Cantonese Chinese", Value = "5" });
            ls.Add(new SelectListItem() { Text = "Mandarin Chinese", Value = "6" });
            ls.Add(new SelectListItem() { Text = "Korean", Value = "7" });
            ls.Add(new SelectListItem() { Text = "Tagalog", Value = "8" });
            ls.Add(new SelectListItem() { Text = "Russian", Value = "9" });
            ls.Add(new SelectListItem() { Text = "Hindi", Value = "10" });

            return ls;
        }
        #endregion

        #region Get Region Dropdown

        [NonAction]
        public static List<SelectListItem> GetRegionDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            //ls.Add(new SelectListItem() { Text = "--Select Region--", Value = "0" });
            ls.Add(new SelectListItem() { Text = "Region 1", Value = "1" });
            ls.Add(new SelectListItem() { Text = "Region 2", Value = "2" });
            ls.Add(new SelectListItem() { Text = "Region 3", Value = "3" });
            ls.Add(new SelectListItem() { Text = "Region 4", Value = "4" });
            ls.Add(new SelectListItem() { Text = "Region 5", Value = "5" });
            ls.Add(new SelectListItem() { Text = "Region 6", Value = "6" });
            ls.Add(new SelectListItem() { Text = "Region 7", Value = "7" });
            ls.Add(new SelectListItem() { Text = "Region 8", Value = "8" });
            ls.Add(new SelectListItem() { Text = "Region 9", Value = "9" });
            ls.Add(new SelectListItem() { Text = "Region 10", Value = "10" });
            ls.Add(new SelectListItem() { Text = "Multiple", Value = "99" });


            return ls;
        }
        #endregion

        #region Get Navigators from DataSet
        private List<NavigatorViewModel> GetNavigatorFromDataSet(string DomainName)
        {
            List<NavigatorViewModel> lsNavigators = new List<NavigatorViewModel>();

            try
            {
                DataSet navigatorsDS = getProxy().GetNavigator(DomainName);
                if (navigatorsDS.Tables[0].Rows.Count > 0)
                {
                    for (var index = 0; index < navigatorsDS.Tables[0].Rows.Count; index++)
                    {
                        lsNavigators.Add(new NavigatorViewModel()
                        {
                            NavigatorID = (int)navigatorsDS.Tables[0].Rows[index]["NavigatorID"],
                            DomainName = navigatorsDS.Tables[0].Rows[index]["DomainName"].ToString(),
                            Region = Convert.IsDBNull(navigatorsDS.Tables[0].Rows[index]["Region"]) ? 0 : Convert.ToInt32(navigatorsDS.Tables[0].Rows[index]["Region"]),
                            Type = (int)navigatorsDS.Tables[0].Rows[index]["Type"],
                            LastName = navigatorsDS.Tables[0].Rows[index]["LastName"].ToString(),
                            FirstName = navigatorsDS.Tables[0].Rows[index]["FirstName"].ToString(),
                            Address1 = navigatorsDS.Tables[0].Rows[index]["Address1"].ToString(),
                            Address2 = navigatorsDS.Tables[0].Rows[index]["Address2"].ToString(),
                            City = navigatorsDS.Tables[0].Rows[index]["City"].ToString(),
                            State = navigatorsDS.Tables[0].Rows[index]["State"].ToString(),
                            Zip = navigatorsDS.Tables[0].Rows[index]["Zip"].ToString(),
                            BusinessTelephone = navigatorsDS.Tables[0].Rows[index]["BusinessTelephone"].ToString(),
                            MobileTelephone = navigatorsDS.Tables[0].Rows[index]["MobileTelephone"].ToString(),
                            Email = navigatorsDS.Tables[0].Rows[index]["Email"].ToString()
                        });
                    }
                }

            }

            catch
            {
                throw;
            }

            return lsNavigators;
        }

        #endregion

        #region Get Trainings from DataSet
        private List<NavigatorTraining> GetTrainingsFromDataSet(int NavigatorID)
        {
            List<NavigatorTraining> lsTrainings = new List<NavigatorTraining>();

            try
            {
                DataSet trainingsDS = getProxy().GetTrainingsList(NavigatorID);
                if (trainingsDS.Tables[0].Rows.Count > 0)
                {
                    for (var index = 0; index < trainingsDS.Tables[0].Rows.Count; index++)
                    {
                        lsTrainings.Add(new NavigatorTraining()
                        {
                            TrainingID = (int)trainingsDS.Tables[0].Rows[index]["TrainingID"],
                            FK_NavigatorID = (int)trainingsDS.Tables[0].Rows[index]["FK_NavigatorID"],
                            CourseName = trainingsDS.Tables[0].Rows[index]["CourseName"].ToString(),
                            Organization = trainingsDS.Tables[0].Rows[index]["Organization"].ToString(),
                            CompletionDt = (DateTime)trainingsDS.Tables[0].Rows[index]["CompletionDt"],
                            Certificate = (bool)trainingsDS.Tables[0].Rows[index]["Certificate"]
                        });
                    }
                }

            }

            catch
            {
                throw;
            }

            return lsTrainings;
        }

        #endregion

        #region Get Trainings from DataSet
        private List<NavigatorLanguages> GetLanguagesFromDataSet(int NavigatorID)
        {
            List<NavigatorLanguages> lsLanguages = new List<NavigatorLanguages>();

            try
            {
                DataSet languagesDS = getProxy().GetNavigatorLanguagesList(NavigatorID);
                if (languagesDS.Tables[0].Rows.Count > 0)
                {
                    for (var index = 0; index < languagesDS.Tables[0].Rows.Count; index++)
                    {
                        lsLanguages.Add(new NavigatorLanguages()
                        {
                            NavigatorLanguageID = (int)languagesDS.Tables[0].Rows[index]["NavigatorLanguageID"],
                            FK_NavigatorID = (int)languagesDS.Tables[0].Rows[index]["FK_NavigatorID"],
                            LanguageCode = (int)languagesDS.Tables[0].Rows[index]["LanguageCode"],
                            LanguageName = languagesDS.Tables[0].Rows[index]["LanguageName"].ToString(),
                            SpeakingScore = (int)languagesDS.Tables[0].Rows[index]["SpeakingScore"],
                            ListeningScore = (int)languagesDS.Tables[0].Rows[index]["ListeningScore"],
                            ReadingScore = (int)languagesDS.Tables[0].Rows[index]["ReadingScore"],
                            WritingScore = (int)languagesDS.Tables[0].Rows[index]["WritingScore"]
                        });
                    }
                }

            }

            catch
            {
                throw;
            }

            return lsLanguages;
        }

        #endregion

        #region Get Search Recipients 
        private List<tServiceRecipient> GetSearchRecipients(int? NSID, string LastName, DateTime? DOB)
        {
            List<tServiceRecipient> lsRecipients = new List<tServiceRecipient>();
            try
            {
                DataSet recipientsDS = getProxy().GetSearchRecipients(NSID, LastName, DOB);
                for (var index = 0; index < recipientsDS.Tables[0].Rows.Count; index++)
                {

                    lsRecipients.Add(new tServiceRecipient()
                    {
                        NSRecipientID = (int)recipientsDS.Tables[0].Rows[index]["NSRecipientID"],
                        FK_NavigatorID = (int)recipientsDS.Tables[0].Rows[index]["FK_NavigatorID"],
                        FK_RecipID = recipientsDS.Tables[0].Rows[index]["FK_RecipID"].ToString(),
                        LastName = recipientsDS.Tables[0].Rows[index]["LastName"].ToString(),
                        FirstName = recipientsDS.Tables[0].Rows[index]["FirstName"].ToString(),
                        MiddleInitial = recipientsDS.Tables[0].Rows[index]["MiddleInitial"].ToString(),
                        DOB = (DateTime)recipientsDS.Tables[0].Rows[index]["DOB"],
                        Address1 = recipientsDS.Tables[0].Rows[index]["Address1"].ToString(),
                        Address2 = recipientsDS.Tables[0].Rows[index]["Address2"].ToString(),
                        City = recipientsDS.Tables[0].Rows[index]["City"].ToString(),
                        State = recipientsDS.Tables[0].Rows[index]["State"].ToString(),
                        Zip = recipientsDS.Tables[0].Rows[index]["Zip"].ToString(),
                        Phone1 = recipientsDS.Tables[0].Rows[index]["Phone1"].ToString(),
                        P1PhoneType = (short)recipientsDS.Tables[0].Rows[index]["P1PhoneType"],
                        P1PersonalMessage = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["P1PersonalMessage"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["P1PersonalMessage"],
                        Phone2 = recipientsDS.Tables[0].Rows[index]["Phone2"].ToString(),
                        P2PhoneType = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["P2PhoneType"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["P2PhoneType"],
                        P2PersonalMessage = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["P2PersonalMessage"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["P2PersonalMessage"],
                        MotherMaidenName = recipientsDS.Tables[0].Rows[index]["MotherMaidenName"].ToString(),
                        Email = recipientsDS.Tables[0].Rows[index]["Email"].ToString(),
                        SSN = recipientsDS.Tables[0].Rows[index]["SSN"].ToString(),
                        ImmigrantStatus = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["ImmigrantStatus"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["ImmigrantStatus"],
                        CountryOfOrigin = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CountryOfOrigin"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["CountryOfOrigin"],
                        Gender = recipientsDS.Tables[0].Rows[index]["Gender"].ToString(),
                        Ethnicity = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["Ethnicity"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["Ethnicity"],
                        PrimaryLanguage = (short)recipientsDS.Tables[0].Rows[index]["PrimaryLanguage"],
                        AgreeToSpeakEnglish = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["AgreeToSpeakEnglish"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["AgreeToSpeakEnglish"],
                        CaregiverName = recipientsDS.Tables[0].Rows[index]["CaregiverName"].ToString(),
                        Relationship = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["Relationship"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["Relationship"],
                        RelationshipOther = recipientsDS.Tables[0].Rows[index]["RelationshipOther"].ToString(),
                        CaregiverPhone = recipientsDS.Tables[0].Rows[index]["CaregiverPhone"].ToString(),
                        CaregiverPhoneType = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CaregiverPhoneType"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["CaregiverPhoneType"],
                        CaregiverPhonePersonalMessage = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CaregiverPhonePersonalMessage"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["CaregiverPhonePersonalMessage"],
                        CaregiverEmail = recipientsDS.Tables[0].Rows[index]["CaregiverEmail"].ToString(),
                        CaregiverContactPreference = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CaregiverContactPreference"]) ? 0 : (int)recipientsDS.Tables[0].Rows[index]["CaregiverContactPreference"],
                        Comments = recipientsDS.Tables[0].Rows[index]["Comments"].ToString(),
                        xtag = recipientsDS.Tables[0].Rows[index]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["DateCreated"]) ? DateTime.MinValue : (DateTime)recipientsDS.Tables[0].Rows[index]["DateCreated"],
                        CreatedBy = recipientsDS.Tables[0].Rows[index]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["LastUpdated"]) ? DateTime.MinValue : (DateTime)recipientsDS.Tables[0].Rows[index]["LastUpdated"],
                        UpdatedBy = recipientsDS.Tables[0].Rows[index]["UpdatedBy"].ToString()
                    });
                }

            }
            catch
            {
                throw;
            }

            return lsRecipients;
        }

        #endregion


    }
}