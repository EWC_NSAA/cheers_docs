﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EWC_NSAA.Controllers
{
    public class PartnersCollaboratorsController : Controller
    {
        // GET: PartnersCollaborators
        public ActionResult PartnersCollaboratorList()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult _PartnersSearch()
        {
            return PartialView();
        }

        public ActionResult _List()
        {
            return PartialView();
        }

        public ActionResult _Edit(int id)
        {
            return PartialView();
        }

        public ActionResult _Deactivate(int id)
        {
            return PartialView();
        }

        public static List<SelectListItem> GetCityDropDown()
        {
            List<SelectListItem> cityList = new List<SelectListItem>();
            cityList.Add(new SelectListItem { Text = "Houston", Value = "1" });
            cityList.Add(new SelectListItem { Text = "Texas", Value = "2" });
            cityList.Add(new SelectListItem { Text = "Newyork", Value = "3" });
            cityList.Add(new SelectListItem { Text = "California", Value = "4" });
            return cityList;
        }

        public static List<SelectListItem> GetServiceDropDown()
        {
            List<SelectListItem> cityList = new List<SelectListItem>();
            cityList.Add(new SelectListItem { Text = "Service Type 1", Value = "1" });
            cityList.Add(new SelectListItem { Text = "Service Type 2", Value = "2" });
            cityList.Add(new SelectListItem { Text = "Service Type 3", Value = "3" });
            cityList.Add(new SelectListItem { Text = "Service Type 4", Value = "4" });
            return cityList;
        }

        public static List<SelectListItem> GetCountyDropDown()
        {
            List<SelectListItem> cityList = new List<SelectListItem>();
            cityList.Add(new SelectListItem { Text = "Morris", Value = "1" });
            cityList.Add(new SelectListItem { Text = "Brooklyn", Value = "2" });
            cityList.Add(new SelectListItem { Text = "Los Angeles", Value = "3" });
            cityList.Add(new SelectListItem { Text = "Fairfax", Value = "4" });
            return cityList;
        }

        public static List<SelectListItem> GetCategoryDropDown()
        {
            List<SelectListItem> cityList = new List<SelectListItem>();
            cityList.Add(new SelectListItem { Text = "Category 1", Value = "1" });
            cityList.Add(new SelectListItem { Text = "Category 2", Value = "2" });
            cityList.Add(new SelectListItem { Text = "Category 3", Value = "3" });
            cityList.Add(new SelectListItem { Text = "Category 4", Value = "4" });
            return cityList;
        }

        public static List<SelectListItem> GetPopulationDropDown()
        {
            List<SelectListItem> cityList = new List<SelectListItem>();
            cityList.Add(new SelectListItem { Text = "Population 1", Value = "1" });
            cityList.Add(new SelectListItem { Text = "Population 2", Value = "2" });
            cityList.Add(new SelectListItem { Text = "Population 3", Value = "3" });
            cityList.Add(new SelectListItem { Text = "Population 4", Value = "4" });
            return cityList;
        }
    }
}