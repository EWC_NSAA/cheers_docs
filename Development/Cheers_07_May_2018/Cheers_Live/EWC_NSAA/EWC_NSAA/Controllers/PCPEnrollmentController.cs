﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EWC_NSAA.Controllers
{
    public class PCPEnrollmentController : Controller
    {
        // GET: PCPEnrollment
        public ActionResult Index()
        {
            return View();
        }
        // GET: Pre Enrollment
        public ActionResult PreEnrollment()
        {
            return View();
        }
        // GET: Enrollment
        public ActionResult Enrollment()
        {
            return View();
        }
        // GET: Management
        public ActionResult Management()
        {
            return View();
        }
    }
}