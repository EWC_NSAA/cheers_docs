﻿using EWC_NSAA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EWC_NSAA.ViewModels
{
    public class HECViewModel
    {
        public HECActivity HECActivity { get; set; }
        public HECDetails HECDetails { get; set; }
        public HECCommunityEvents HECCommunityEvents { get; set; }
        public HECParticipant HECParticipant { get; set; }
       

    }
}