﻿using EWC_NSAA.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EWC_NSAA.Models
{
    public class HECDetails
    {
        public string HECIncidentNumber { get; set; }
        public DateTime HECIncidentDateStamp { get; set; }
        [Display(Name ="Date")]
        public DateTime HECActivityDate { get; set; }
        [Display(Name = "Activity Type")]
        public int HECActivityType { get; set; }
        [Display(Name = "Activity Type")]
        public string OtherActivityType { get; set; }
        [Display(Name = "Activity Cancelled")]
        public bool HECActivityCancelled { get; set; } = false;
        [Display(Name = "Reason for Cancellation")]
        public string HECactivityCancellationReason { get; set; }
        [Display(Name = "Name")]
        public string HECNameOrPurpose { get; set; }
        [Display(Name = "Type of Activity")]
        public int HECTypeOfActivity { get; set; }
        [Display(Name = "Collaborator")]
        public int HECCollaborator { get; set; }
        [Display(Name = "Contribution")]
        public int HECCollaboratorContribution { get; set; }
        public string OtherCollaboratorContribution { get; set; }
        public string HECActivityAddress1 { get; set; }
        public string HECActivityAddress2 { get; set; }
        [Display(Name = "City")]
        public string HECActivityCity { get; set; }
        [Display(Name = "Zip")]
        public string HECActivityZip { get; set; }
        [Display(Name = "CHW")]
        public int HECCHW { get; set; }
        [Display(Name = "Other Attendee")]
        public string HECOtherAttendees { get; set; }
        [Display(Name = "Population")]
        public int HECPopulation { get; set; }
        [Display(Name = "Language")]
        public int HECLanguage { get; set; }
        [Display(Name = "Discussed")]
        public int HECDiscussed { get; set; }
        [Display(Name = "Other Discussed")]
        public string OtherDiscussed { get; set; }
        [Display(Name = "Test")]
        public int HECPrePostTest { get; set; }
        [Display(Name = "Result")]
        public int HECResult { get; set; }
        [Display(Name = "Other Result")]
        public string OtherResult { get; set; }

        [Display(Name = "My Role")]
        public int HECMyRole { get; set; }
        [Display(Name = "Other My Role")]
        public string OtherMyRole { get; set; }
        [Display(Name = "Confirm Attendance")]
        public bool HECConfirmAttendance { get; set; }
        [Display(Name = "Travel")]
        public bool HECActivityTravel { get; set; }
        [Display(Name = "Notes")]
        public string HECActivityNotes { get; set; }
    }
}