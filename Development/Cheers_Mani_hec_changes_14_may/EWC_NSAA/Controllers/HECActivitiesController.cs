﻿using EWC_NSAA.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EWC_NSAA.Controllers
{
    public class HECActivitiesController : Controller
    {
        // GET: HECActivities
        public ActionResult Index()
        {
            var model = new HECViewModel();
            model.HECActivity = new Models.HECActivity();
            model.HECDetails = new Models.HECDetails();
            model.HECCommunityEvents = new Models.HECCommunityEvents();
            model.HECParticipant = new Models.HECParticipant();
            return View(model);
        }

        // GET: HECActivities/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: HECActivities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HECActivities/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: HECActivities/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: HECActivities/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: HECActivities/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: HECActivities/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
