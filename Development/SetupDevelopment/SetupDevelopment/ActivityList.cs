﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetupDevelopment
{
    
    public static class  ActivityRepository
    {
        public static Dictionary<string, IActivity> actitities;
        static ActivityRepository()
        {
            actitities = new Dictionary<string, IActivity>();
            actitities.Add("CreateDeploymentFolder", new CreateDeploymentFolder());
            actitities.Add("CreateDbScripts", new CreateDbScripts());
            actitities.Add("ApplyDatabaseChanges", new ApplyDatabaseChanges());
        }
        public static IActivity GetActivity(string activityName)
        {
            return actitities[activityName];
        }
    } 
}
