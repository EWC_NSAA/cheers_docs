﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetupDevelopment
{
    public interface IActivity
    {
   
        
        void Init();
        void Execute();
        string ActivityName { get; }
    }
}
