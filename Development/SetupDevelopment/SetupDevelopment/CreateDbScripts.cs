﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;


using System.Configuration;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.Diagnostics;
using System.Collections.Generic;
using System.Xml.Linq;

namespace SetupDevelopment
{
   
    
    public class CreateDbScripts : IActivity
    {
        private string _databaseScriptBasePath = string.Empty;
        
        public string ActivityName{ get { return "Create Database Scripts"; } }

        private string _DepFolderNamePrefix = string.Empty;
        private string _releaseVersion = string.Empty;
        private string _releaseBasePath = string.Empty;
        private DateTime _LastDeploymentDate = DateTime.Now;
        private bool _ConsiderFullscripts = false;
        public void Execute()
        {
            try
            {
               string[] allDbdirs= Directory.GetDirectories(_databaseScriptBasePath).OrderBy(s=>s).ToArray();

               string releaseFolder = $@"{_releaseBasePath}\{_DepFolderNamePrefix}{_releaseVersion}\Database";

                if (Directory.Exists(releaseFolder))
                    Directory.Delete(releaseFolder);
                Directory.CreateDirectory(releaseFolder);

                foreach (string script in allDbdirs)
                {
                    string filename = new DirectoryInfo(script).Name;
                    GenerateScripts(script, releaseFolder, $"{filename}.sql");
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Not able to update the database.But the scripts might have generated. So please try executing the scripts manually in your database. Please see the exception{ex.ToString()}");
            }
        }

        public void Init()
        {
            try
            {
                _databaseScriptBasePath = ConfigurationManager.AppSettings["DatabaseScriptBasePath"].ToString();
                _DepFolderNamePrefix = ConfigurationManager.AppSettings["DepFolderNamePrefix"].ToString();
                _releaseVersion = ConfigurationManager.AppSettings["CurrentVersion"].ToString();
                _releaseBasePath = ConfigurationManager.AppSettings["ReleaseBaseFolder"].ToString();
                _LastDeploymentDate = Convert.ToDateTime(ConfigurationManager.AppSettings["LastDeploymentDate"].ToString());
                _ConsiderFullscripts = Convert.ToBoolean(ConfigurationManager.AppSettings["ConsiderFullscripts"].ToString());
            }
            catch (Exception ex)
            {
                throw new Exception("Init Operation failed"+ex.ToString());
            }            
        }
        public void GenerateScripts(string scriptSourcePath,string scriptTargetPath, string scriptTargetFileName)
        {           
            
            StringBuilder sb = new StringBuilder();
            string targetFile = $"{ scriptTargetPath}\\{scriptTargetFileName}";
            if (File.Exists(targetFile))
                File.Delete(targetFile);

            var files = Directory.GetFiles(scriptSourcePath, "*.sql", SearchOption.AllDirectories).OrderBy(f => f);

            foreach (var file in files)
            {
                var fileinfo = new FileInfo(file);
                if (fileinfo.LastWriteTime > _LastDeploymentDate && !_ConsiderFullscripts)
                {
                    var fileContent = File.ReadAllText(file);
                    sb.AppendLine(fileContent);
                    sb.AppendLine("--*************************************************************************");
                }             
            }
            File.WriteAllText($"{ scriptTargetPath}\\{scriptTargetFileName}", sb.ToString());
        }
    }
}
