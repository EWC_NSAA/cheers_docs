﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetupDevelopment
{
    public class CreateDeploymentFolder : IActivity
    {
        private string _DepFolderNamePrefix = string.Empty;
        private string _releaseVersion = string.Empty;
        private string _releaseBasePath = string.Empty;    
        public string ActivityName
        {
            get
            {
                return "Create Deployment Folder";
            }
        }

        public void Execute()
        {
            string releaseFolder = $@"{_releaseBasePath}\{_DepFolderNamePrefix}{_releaseVersion}";
            if (!Directory.Exists(_releaseBasePath))
                Directory.CreateDirectory(_releaseBasePath);
           
            if (Directory.Exists(releaseFolder))
                Directory.Delete(releaseFolder);

            Directory.CreateDirectory(releaseFolder);
            
        }

        public void Init()
        {
            _DepFolderNamePrefix = ConfigurationManager.AppSettings["DepFolderNamePrefix"].ToString();
            _releaseVersion = ConfigurationManager.AppSettings["CurrentVersion"].ToString();
            _releaseBasePath = ConfigurationManager.AppSettings["ReleaseBaseFolder"].ToString();            
        }
    }
}
