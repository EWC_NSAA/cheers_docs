﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SetupDevelopment
{
    public class ApplyDatabaseChanges : IActivity
    {
        public string ActivityName
        {
            get
            {
                return "Apply Database Changes";
            }           
        }

        private string _databaseScriptBasePath = string.Empty;
        private string _sqlTempPath = string.Empty;
        private string[] Scripts { get; set; }
        public void Execute()
        {
            string appName = ConfigurationManager.AppSettings["SqlCmdpath"].ToString();
            string SqlInstance = ConfigurationManager.AppSettings["SqlServerInstance"].ToString();
            string DatabaseName = ConfigurationManager.AppSettings["DatabaseName"].ToString();

            if (Directory.Exists(_sqlTempPath))
                Directory.Delete(_sqlTempPath);
           
            Directory.CreateDirectory(_sqlTempPath);

            string[] allDbdirs = Directory.GetDirectories(_databaseScriptBasePath).OrderBy(s => s).ToArray();

            foreach (string script in allDbdirs)
            {
                string filename = new DirectoryInfo(script).Name;
                CreateDbScripts cDb = new CreateDbScripts();
                cDb.GenerateScripts(script, _sqlTempPath, $"{filename}.sql");               
            }


            Scripts = Directory.GetFiles(_sqlTempPath);

            foreach (string sqlScript in Scripts)
            {
               
                Process process = new Process();
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.FileName = appName;
                process.StartInfo.Arguments = $"-S {SqlInstance} -d {DatabaseName} -i {sqlScript}";

                process.Start();
                process.WaitForExit();
            }
            Directory.Delete(_sqlTempPath,true);
        }

        public void Init()
        {           
            _databaseScriptBasePath = ConfigurationManager.AppSettings["DatabaseScriptBasePath"].ToString();
            _sqlTempPath = ConfigurationManager.AppSettings["SqlTempPath"].ToString();
        }
    }
}
