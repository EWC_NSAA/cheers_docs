﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetupDevelopment
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                foreach (string activityName in args)
                {
                    
                    var activity = ActivityRepository.GetActivity(activityName);
                    Console.WriteLine($"Executing the {activity.ActivityName}. Please wait.....");
                    activity.Init();
                    activity.Execute();
                    Console.WriteLine($"{activity.ActivityName} completed successfully.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Console.ReadKey();
        }
    }
}
