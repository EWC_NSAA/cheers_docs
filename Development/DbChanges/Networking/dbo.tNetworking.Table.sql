/****** Object:  Table [dbo].[tNetworking]    Script Date: 3/24/2018 11:10:12 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNetworking]') AND type in (N'U'))
DROP TABLE [dbo].[tNetworking]
GO
/****** Object:  Table [dbo].[tNetworking]    Script Date: 3/24/2018 11:10:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tNetworking]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tNetworking](
	[NetworkingId] [int] IDENTITY(1,1) NOT NULL,
	[Startdate] [datetime] NULL,
	[Enddate] [datetime] NULL,
	[NameOrPurpose] [nvarchar](200) NULL,
	[Collaborator] [int] NULL,
	[CollaboratorOther] [nvarchar](100) NULL,
	[Address1] [nvarchar](50) NULL,
	[Address2] [nvarchar](50) NULL,
	[City] [varchar](50) NULL,
	[Zip] [int] NULL,
	[Role] [int] NULL,
	[ConfirmAttendance] [bit] NULL,
	[AnnocementDoc] [nvarchar](300) NULL,
	[Travel] [bit] NULL,
	[Notes] [nvarchar](500) NULL,
 CONSTRAINT [PK_tNetworking] PRIMARY KEY CLUSTERED 
(
	[NetworkingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
