/****** Object:  StoredProcedure [dbo].[usp_UpdatePartnersCollaborator]    Script Date: 3/24/2018 3:44:50 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdatePartnersCollaborator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdatePartnersCollaborator]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdatePartnersCollaborator]    Script Date: 3/24/2018 3:44:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdatePartnersCollaborator]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdatePartnersCollaborator] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_UpdatePartnersCollaborator]
@PartnersCollaboratorsId int,
@NameofOrg1	nvarchar(200),
@NameOfOrg2	nvarchar(200),
@Abbreviation	nvarchar(100),
@Address1	nvarchar(100),
@Address2	nvarchar(100),
@City	nvarchar(100),
@ZipCode int,
@Phone	varchar(15),
@Email	varchar(100),
@WebAddress	varchar	(250),

@FirstName	nvarchar(100),
@LastName	nvarchar(100),
@Title	nvarchar(20),
@Contact_Phone	varchar(15),
@Ext	int,
@Contact_Email	varchar(100),
@DateOfLastContact	datetime,

@OrgType1	int	,
@OrgType2	int	,
@ServiceCategory1	int	,
@ServiceCaregory2	int	,
@ServicePopulation1	int	,
@ServicePopulation2	int	,
@Language1	int	,
@Language2	int,
@Area	varchar	(50),
@Region	int	,
@Country1	int	,
@Country2	int	

AS
BEGIN

IF ( @PartnersCollaboratorsId<>null)
BEGIN

UPDATE
tPartnersCollaborators
SET NameofOrg1=@NameofOrg1,
NameOfOrg2=@NameOfOrg2,
Abbreviation=@Abbreviation,
Address1=@Address1,
Address2=@Address2,
City=@City,
ZipCode=@ZipCode,
Phone=@Phone,
Email=@Email,
WebAddress= @WebAddress
WHERE PartnersCollaboratorsId = @PartnersCollaboratorsId


UPDATE tPartnersCollaboratorsContact
sET FirstName=@FirstName,
LastName=@LastName,
Title=@Title,
Phone=@Contact_Phone,
Ext=@Ext,
Email=@Contact_Email,
DateOfLastContact=@DateOfLastContact
WHERE PartnersCollaboratorsId = @PartnersCollaboratorsId


UPDATE tPartnersCollaboratorsService
SET OrgType1=@OrgType1,
OrgType2=@OrgType2,
ServiceCategory1=@ServiceCategory1,
ServiceCaregory2=@ServiceCaregory2,
ServicePopulation1=@ServicePopulation1,
ServicePopulation2=@ServicePopulation2,
Language1=@Language1,
Language2=@Language2,
Area=@Area,
Region=@Region,
Country1=@Country1,
Country2=@Country2

WHERE PartnersCollaboratorsId = @PartnersCollaboratorsId
END
END
GO
