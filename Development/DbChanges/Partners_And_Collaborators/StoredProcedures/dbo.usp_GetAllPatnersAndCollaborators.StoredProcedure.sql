/****** Object:  StoredProcedure [dbo].[usp_GetAllPatnersAndCollaborators]    Script Date: 3/24/2018 3:44:50 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAllPatnersAndCollaborators]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetAllPatnersAndCollaborators]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAllPatnersAndCollaborators]    Script Date: 3/24/2018 3:44:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAllPatnersAndCollaborators]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetAllPatnersAndCollaborators] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_GetAllPatnersAndCollaborators]
AS
BEGIN

SELECT pmaster.*,pcontract.*,pservice.* from [dbo].[tPartnersCollaborators] pmaster
left outer join [dbo].[tPartnersCollaboratorsContact] pcontract on pmaster.PartnersCollaboratorsId = pcontract.PartnersCollaboratorsId
left outer join [dbo].[tPartnersCollaboratorsService] pservice on pmaster.PartnersCollaboratorsId = pservice.PartnersCollaboratorsId

END
GO
