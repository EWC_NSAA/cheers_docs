/****** Object:  StoredProcedure [dbo].[usp_InsertPartnersCollaborator]    Script Date: 3/24/2018 3:44:50 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertPartnersCollaborator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertPartnersCollaborator]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertPartnersCollaborator]    Script Date: 3/24/2018 3:44:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertPartnersCollaborator]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertPartnersCollaborator] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_InsertPartnersCollaborator]
@NameofOrg1	nvarchar(200),
@NameOfOrg2	nvarchar(200),
@Abbreviation	nvarchar(100),
@Address1	nvarchar(100),
@Address2	nvarchar(100),
@City	nvarchar(100),
@ZipCode int,
@Phone	varchar(15),
@Email	varchar(100),
@WebAddress	varchar	(250),

@FirstName	nvarchar(100),
@LastName	nvarchar(100),
@Title	nvarchar(20),
@Contact_Phone	varchar(15),
@Ext	int,
@Contact_Email	varchar(100),
@DateOfLastContact	datetime,

@OrgType1	int	,
@OrgType2	int	,
@ServiceCategory1	int	,
@ServiceCaregory2	int	,
@ServicePopulation1	int	,
@ServicePopulation2	int	,
@Language1	int	,
@Language2	int,
@Area	varchar	(50),
@Region	int	,
@Country1	int	,
@Country2	int	

AS
BEGIN
DECLARE @PartnersCollaboratorsId int
INSERT INTO tPartnersCollaborators(NameofOrg1,NameOfOrg2,Abbreviation,Address1,Address2,City,ZipCode,Phone,Email,WebAddress)
VALUES(@NameofOrg1,@NameOfOrg2,@Abbreviation,@Address1,@Address2,@City,@ZipCode,@Phone,@Email,@WebAddress)

SET @PartnersCollaboratorsId= SCOPE_IDENTITY()

IF ( @PartnersCollaboratorsId<>null)
BEGIN

INSERT INTO tPartnersCollaboratorsContact(PartnersCollaboratorsId,FirstName,LastName,Title,Phone,Ext,Email,DateOfLastContact)
VALUES (@PartnersCollaboratorsId,@FirstName,@LastName,@Title,@Contact_Phone,@Ext,@Contact_Email,@DateOfLastContact)
END


INSERT INTO tPartnersCollaboratorsService(PartnersCollaboratorsId,OrgType1,OrgType2,ServiceCategory1,ServiceCaregory2,ServicePopulation1,
ServicePopulation2,Language1,Language2,Area,Region,Country1,Country2)
VALUES(@PartnersCollaboratorsId,@OrgType1,@OrgType2,@ServiceCategory1,@ServiceCaregory2,@ServicePopulation1,
@ServicePopulation2,@Language1,@Language2,@Area,@Region,@Country1,@Country2)

END

GO
