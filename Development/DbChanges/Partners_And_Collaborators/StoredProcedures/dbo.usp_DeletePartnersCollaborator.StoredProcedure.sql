/****** Object:  StoredProcedure [dbo].[usp_DeletePartnersCollaborator]    Script Date: 3/24/2018 3:44:50 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DeletePartnersCollaborator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_DeletePartnersCollaborator]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeletePartnersCollaborator]    Script Date: 3/24/2018 3:44:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DeletePartnersCollaborator]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_DeletePartnersCollaborator] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_DeletePartnersCollaborator]
@PartnersCollaboratorsId int
AS
BEGIN
DELETE FROM [dbo].[tPartnersCollaboratorsContact] where PartnersCollaboratorsId = @PartnersCollaboratorsId
DELETE FROM [dbo].[tPartnersCollaboratorsService] where PartnersCollaboratorsId = @PartnersCollaboratorsId
DELETE FROM [dbo].[tPartnersCollaborators] where PartnersCollaboratorsId = @PartnersCollaboratorsId
END
GO
