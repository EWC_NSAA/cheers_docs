IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tPartnersCollaboratorsContact_tPartnersCollaborators]') AND parent_object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsContact]'))
ALTER TABLE [dbo].[tPartnersCollaboratorsContact] DROP CONSTRAINT [FK_tPartnersCollaboratorsContact_tPartnersCollaborators]
GO
/****** Object:  Table [dbo].[tPartnersCollaboratorsContact]    Script Date: 3/24/2018 11:03:23 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsContact]') AND type in (N'U'))
DROP TABLE [dbo].[tPartnersCollaboratorsContact]
GO
/****** Object:  Table [dbo].[tPartnersCollaboratorsContact]    Script Date: 3/24/2018 11:03:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsContact]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tPartnersCollaboratorsContact](
	[PartnersCollaboratorsId] [int] NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Title] [nvarchar](10) NULL,
	[Phone] [varchar](15) NULL,
	[Ext] [int] NULL,
	[Email] [varchar](100) NULL,
	[DateOfLastContact] [datetime] NULL,
 CONSTRAINT [PK_tPartnersCollaboratorsContact] PRIMARY KEY CLUSTERED 
(
	[PartnersCollaboratorsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tPartnersCollaboratorsContact_tPartnersCollaborators]') AND parent_object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsContact]'))
ALTER TABLE [dbo].[tPartnersCollaboratorsContact]  WITH CHECK ADD  CONSTRAINT [FK_tPartnersCollaboratorsContact_tPartnersCollaborators] FOREIGN KEY([PartnersCollaboratorsId])
REFERENCES [dbo].[tPartnersCollaborators] ([PartnersCollaboratorsId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tPartnersCollaboratorsContact_tPartnersCollaborators]') AND parent_object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsContact]'))
ALTER TABLE [dbo].[tPartnersCollaboratorsContact] CHECK CONSTRAINT [FK_tPartnersCollaboratorsContact_tPartnersCollaborators]
GO
