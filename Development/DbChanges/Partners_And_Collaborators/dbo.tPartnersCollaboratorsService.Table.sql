IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tPartnersCollaboratorsService_tPartnersCollaborators]') AND parent_object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsService]'))
ALTER TABLE [dbo].[tPartnersCollaboratorsService] DROP CONSTRAINT [FK_tPartnersCollaboratorsService_tPartnersCollaborators]
GO
/****** Object:  Table [dbo].[tPartnersCollaboratorsService]    Script Date: 3/24/2018 11:03:23 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsService]') AND type in (N'U'))
DROP TABLE [dbo].[tPartnersCollaboratorsService]
GO
/****** Object:  Table [dbo].[tPartnersCollaboratorsService]    Script Date: 3/24/2018 11:03:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsService]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tPartnersCollaboratorsService](
	[PartnersCollaboratorsId] [int] NOT NULL,
	[OrgType1] [int] NULL,
	[OrgType2] [int] NULL,
	[ServiceCategory1] [int] NULL,
	[ServiceCaregory2] [int] NULL,
	[ServicePopulation1] [int] NULL,
	[ServicePopulation2] [int] NULL,
	[Language1] [int] NULL,
	[Language2] [int] NULL,
	[Area] [varchar](50) NULL,
	[Region] [int] NULL,
	[Country1] [int] NULL,
	[Country2] [int] NULL,
 CONSTRAINT [PK_tPartnersCollaboratorsService] PRIMARY KEY CLUSTERED 
(
	[PartnersCollaboratorsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tPartnersCollaboratorsService_tPartnersCollaborators]') AND parent_object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsService]'))
ALTER TABLE [dbo].[tPartnersCollaboratorsService]  WITH CHECK ADD  CONSTRAINT [FK_tPartnersCollaboratorsService_tPartnersCollaborators] FOREIGN KEY([PartnersCollaboratorsId])
REFERENCES [dbo].[tPartnersCollaborators] ([PartnersCollaboratorsId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tPartnersCollaboratorsService_tPartnersCollaborators]') AND parent_object_id = OBJECT_ID(N'[dbo].[tPartnersCollaboratorsService]'))
ALTER TABLE [dbo].[tPartnersCollaboratorsService] CHECK CONSTRAINT [FK_tPartnersCollaboratorsService_tPartnersCollaborators]
GO
