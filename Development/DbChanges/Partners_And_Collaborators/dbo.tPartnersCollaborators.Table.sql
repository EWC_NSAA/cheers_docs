/****** Object:  Table [dbo].[tPartnersCollaborators]    Script Date: 3/24/2018 11:03:23 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tPartnersCollaborators]') AND type in (N'U'))
DROP TABLE [dbo].[tPartnersCollaborators]
GO
/****** Object:  Table [dbo].[tPartnersCollaborators]    Script Date: 3/24/2018 11:03:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tPartnersCollaborators]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tPartnersCollaborators](
	[PartnersCollaboratorsId] [int] IDENTITY(1,1) NOT NULL,
	[NameofOrg1] [nvarchar](100) NULL,
	[NameOfOrg2] [nvarchar](100) NULL,
	[Abbreviation] [nvarchar](50) NULL,
	[Address1] [nvarchar](50) NULL,
	[Address2] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[ZipCode] [int] NULL,
	[Phone] [varchar](15) NULL,
	[Email] [varchar](100) NULL,
	[WebAddress] [varchar](250) NULL,
 CONSTRAINT [PK_tPartnersCollaborators] PRIMARY KEY CLUSTERED 
(
	[PartnersCollaboratorsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
