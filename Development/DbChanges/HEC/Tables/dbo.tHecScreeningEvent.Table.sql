IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecScreeningEvent_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecScreeningEvent]'))
ALTER TABLE [dbo].[tHecScreeningEvent] DROP CONSTRAINT [FK_tHecScreeningEvent_tHecActivities]
GO
/****** Object:  Table [dbo].[tHecScreeningEvent]    Script Date: 3/19/2018 11:58:03 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecScreeningEvent]') AND type in (N'U'))
DROP TABLE [dbo].[tHecScreeningEvent]
GO
/****** Object:  Table [dbo].[tHecScreeningEvent]    Script Date: 3/19/2018 11:58:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecScreeningEvent]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tHecScreeningEvent](
	[ActivityId] [int] NOT NULL,
	[BusinessName] [nvarchar](100) NULL,
	[NPI] [varchar](50) NULL,
	[ProviderFirstName] [nvarchar](50) NULL,
	[ProviderLastName] [nvarchar](50) NULL,
	[EwcProvider] [smallint] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecScreeningEvent_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecScreeningEvent]'))
ALTER TABLE [dbo].[tHecScreeningEvent]  WITH CHECK ADD  CONSTRAINT [FK_tHecScreeningEvent_tHecActivities] FOREIGN KEY([ActivityId])
REFERENCES [dbo].[tHecActivities] ([ActivityId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecScreeningEvent_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecScreeningEvent]'))
ALTER TABLE [dbo].[tHecScreeningEvent] CHECK CONSTRAINT [FK_tHecScreeningEvent_tHecActivities]
GO
