/****** Object:  Table [dbo].[tHecActivities]    Script Date: 3/19/2018 11:58:03 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecActivities]') AND type in (N'U'))
DROP TABLE [dbo].[tHecActivities]
GO
/****** Object:  Table [dbo].[tHecActivities]    Script Date: 3/19/2018 11:58:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecActivities]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tHecActivities](
	[ActivityId] [int] IDENTITY(1,1) NOT NULL,
	[ActivityDate] [datetime] NULL,
	[ActivityType] [smallint] NULL,
	[NameOrPurpose] [varchar](250) NULL,
	[CollaboratorId] [int] NULL,
	[CollaboratorContributionId] [int] NULL,
	[Address1] [nvarchar](100) NULL,
	[Address2] [nvarchar](100) NULL,
	[CityId] [int] NULL,
	[Zip] [varchar](5) NULL,
	[CHW] [int] NULL,
	[OtherAttendee] [nvarchar](100) NULL,
	[Population] [varchar](50) NULL,
	[LanguageId] [int] NULL,
	[Discussed] [int] NULL,
	[PrePostTest] [int] NULL,
	[Result] [int] NULL,
	[MyRole] [int] NULL,
	[Attendee] [bit] NULL,
	[AnnoucementDoc] [varchar](300) NULL,
	[Travel] [bit] NULL,
	[Notes] [nvarchar](250) NULL,
 CONSTRAINT [PK_tHecActivities] PRIMARY KEY CLUSTERED 
(
	[ActivityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
