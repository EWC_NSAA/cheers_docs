IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecActivityParticipantInfo_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecActivityParticipantInfo]'))
ALTER TABLE [dbo].[tHecActivityParticipantInfo] DROP CONSTRAINT [FK_tHecActivityParticipantInfo_tHecActivities]
GO
/****** Object:  Table [dbo].[tHecActivityParticipantInfo]    Script Date: 3/19/2018 11:58:03 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecActivityParticipantInfo]') AND type in (N'U'))
DROP TABLE [dbo].[tHecActivityParticipantInfo]
GO
/****** Object:  Table [dbo].[tHecActivityParticipantInfo]    Script Date: 3/19/2018 11:58:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tHecActivityParticipantInfo]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tHecActivityParticipantInfo](
	[ActivityId] [int] NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Month] [smallint] NULL,
	[Year] [int] NULL,
	[RaceOrEthinicity] [int] NULL,
	[Address1] [nvarchar](100) NULL,
	[Address2] [nvarchar](100) NULL,
	[CityId] [int] NULL,
	[Zip] [varchar](5) NULL,
	[Telephone] [varchar](50) NULL,
	[Email] [varchar](150) NULL,
	[LanguageId] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecActivityParticipantInfo_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecActivityParticipantInfo]'))
ALTER TABLE [dbo].[tHecActivityParticipantInfo]  WITH CHECK ADD  CONSTRAINT [FK_tHecActivityParticipantInfo_tHecActivities] FOREIGN KEY([ActivityId])
REFERENCES [dbo].[tHecActivities] ([ActivityId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tHecActivityParticipantInfo_tHecActivities]') AND parent_object_id = OBJECT_ID(N'[dbo].[tHecActivityParticipantInfo]'))
ALTER TABLE [dbo].[tHecActivityParticipantInfo] CHECK CONSTRAINT [FK_tHecActivityParticipantInfo_tHecActivities]
GO
