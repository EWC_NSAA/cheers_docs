/****** Object:  StoredProcedure [dbo].[usp_InsertHECActivity]    Script Date: 3/24/2018 2:57:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertHECActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertHECActivity]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertHECActivity]    Script Date: 3/24/2018 2:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertHECActivity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_InsertHECActivity] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_InsertHECActivity]
@ActivityDate	datetime,	
@ActivityType	smallint,
@NameOrPurpose	varchar(250),	
@CollaboratorId	int,	
@CollaboratorContributionId	int,	
@Address1	nvarchar(100),
@Address2	nvarchar(100),	
@CityId	int,	
@Zip	varchar(5),	
@CHW	int,
@OtherAttendee	nvarchar(100),	
@Population	varchar(50),	
@LanguageId	int,	
@Discussed	int,	
@PrePostTest	int,	
@Result	int,	
@MyRole	int,	
@Attendee	bit,	
@AnnoucementDoc	varchar(300),	
@Travel	bit,	
@Notes	nvarchar(250),

@FirstName	nvarchar(50),	
@LastName	nvarchar(50),	
@Month	smallint,	
@Year	int,	
@RaceOrEthinicity	int,	
@Part_Info_Address1	nvarchar(100),	
@Part_Info_Address2	nvarchar(100),	
@Part_Info_CityId	int,
@Part_Info_Zip	varchar(5),	
@Telephone	varchar(50),	
@Email	varchar(150),	
@Part_Info_LanguageId	int,	

@BusinessName	nvarchar(100),	
@NPI	varchar(50),	
@ProviderFirstName	nvarchar(50),	
@ProviderLastName	nvarchar(50),	
@EwcProvider	smallint

AS
Begin

DECLARE @ActivityId int = null
INSERT INTO tHecActivities( ActivityDate,  ActivityType, NameOrPurpose,  CollaboratorId,  CollaboratorContributionId,  Address1,  Address2,  CityId,  Zip,  CHW,  OtherAttendee,
 Population,  LanguageId,  Discussed,  PrePostTest,  Result,  MyRole,  Attendee,  AnnoucementDoc,  Travel,  Notes)
 VALUES (@ActivityDate,@ActivityType	,@NameOrPurpose	,@CollaboratorId	,@CollaboratorContributionId	,@Address1	,@Address2	,@CityId	,	
@Zip	,@CHW	,@OtherAttendee	,	@Population	,	@LanguageId	,@Discussed	,@PrePostTest	,	@Result	,@MyRole	,@Attendee	,	@AnnoucementDoc	,	
@Travel	,	@Notes)

set @ActivityId = SCOPE_IDENTITY()

IF(@ActivityId<>NULL)
Begin
INSERT INTO tHecActivityParticipantInfo(ActivityId,FirstName,LastName,Month,Year,RaceOrEthinicity,Address1,Address2,CityId,Zip,Telephone,Email,LanguageId)
VALUES (@ActivityId,@FirstName,@LastName	,@Month	,@Year	,@RaceOrEthinicity	,	@Part_Info_Address1	,@Part_Info_Address2,	@Part_Info_CityId	,
@Part_Info_Zip,	@Telephone,@Email	,@Part_Info_LanguageId)

INSERT INTO tHecScreeningEvent(ActivityId,BusinessName,NPI,ProviderFirstName,ProviderLastName,EwcProvider)
VALUES (@ActivityId,@BusinessName,@NPI,@ProviderFirstName,@ProviderLastName,@EwcProvider)


END



END
GO
