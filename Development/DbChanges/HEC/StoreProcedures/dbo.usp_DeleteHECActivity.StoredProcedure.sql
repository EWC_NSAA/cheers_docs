/****** Object:  StoredProcedure [dbo].[usp_DeleteHECActivity]    Script Date: 3/24/2018 2:57:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DeleteHECActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_DeleteHECActivity]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteHECActivity]    Script Date: 3/24/2018 2:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DeleteHECActivity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_DeleteHECActivity] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_DeleteHECActivity]
@ActivityId int
AS
BEGIN
 IF(@ActivityId<>Null)
 Begin
	DELETE fROM tHecScreeningEvent WHERE ActivityId = @ActivityId
	DELETE FROM tHecActivityParticipantInfo WHERE ActivityId = @ActivityId
	DELETE FROM tHecActivities WHERE ActivityId = @ActivityId
 End
END
GO
