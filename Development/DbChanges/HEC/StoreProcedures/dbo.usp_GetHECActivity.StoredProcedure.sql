/****** Object:  StoredProcedure [dbo].[usp_GetHECActivity]    Script Date: 3/24/2018 2:57:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetHECActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetHECActivity]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetHECActivity]    Script Date: 3/24/2018 2:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetHECActivity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetHECActivity] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_GetHECActivity]
@ActivityId int
AS
BEGIN

IF(@ActivityId<>NUll)
Begin
SELECT hecmaster.*, hecpartInfo.*,hecscrnevent.* FROM tHecActivities hecmaster
left outer join tHecActivityParticipantInfo hecpartInfo on hecmaster.ActivityId = hecpartInfo.ActivityId
left outer join tHecScreeningEvent hecscrnevent on hecmaster.ActivityId = hecscrnevent.ActivityId
where hecmaster.ActivityId = @ActivityId
END
END
GO
