/****** Object:  StoredProcedure [dbo].[usp_UpdateHECActivity]    Script Date: 3/24/2018 2:57:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateHECActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateHECActivity]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateHECActivity]    Script Date: 3/24/2018 2:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateHECActivity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_UpdateHECActivity] AS' 
END
GO
ALTER PROCEDURE [dbo].[usp_UpdateHECActivity]
@ActivityId int,
@ActivityDate	datetime,	
@ActivityType	smallint,
@NameOrPurpose	varchar(250),	
@CollaboratorId	int,	
@CollaboratorContributionId	int,	
@Address1	nvarchar(100),
@Address2	nvarchar(100),	
@CityId	int,	
@Zip	varchar(5),	
@CHW	int,
@OtherAttendee	nvarchar(100),	
@Population	varchar(50),	
@LanguageId	int,	
@Discussed	int,	
@PrePostTest	int,	
@Result	int,	
@MyRole	int,	
@Attendee	bit,	
@AnnoucementDoc	varchar(300),	
@Travel	bit,	
@Notes	nvarchar(250),

@FirstName	nvarchar(50),	
@LastName	nvarchar(50),	
@Month	smallint,	
@Year	int,	
@RaceOrEthinicity	int,	
@Part_Info_Address1	nvarchar(100),	
@Part_Info_Address2	nvarchar(100),	
@Part_Info_CityId	int,
@Part_Info_Zip	varchar(5),	
@Telephone	varchar(50),	
@Email	varchar(150),	
@Part_Info_LanguageId	int,	

@BusinessName	nvarchar(100),	
@NPI	varchar(50),	
@ProviderFirstName	nvarchar(50),	
@ProviderLastName	nvarchar(50),	
@EwcProvider	smallint

AS
Begin

IF(@ActivityId<>NULL)
Begin

UPDATE tHecActivities
SET ActivityDate=@ActivityDate ,
  ActivityType=@ActivityType, 
  NameOrPurpose=@NameOrPurpose,  
  CollaboratorId=@CollaboratorId,  
  CollaboratorContributionId=@CollaboratorContributionId,  
  Address1=@Address1,  
  Address2=@Address2,  
  CityId=@CityId,  
  Zip=@Zip,  
  CHW=@CHW,  
  OtherAttendee=@OtherAttendee,
 Population=@Population,  
 LanguageId=@LanguageId,  
 Discussed=@Discussed,  
 PrePostTest=@PrePostTest,  
 Result=@Result,  
 MyRole=@MyRole,  
 Attendee=@Attendee,  
 AnnoucementDoc=@AnnoucementDoc,  
 Travel=@Travel,  
 Notes=@Notes
 where ActivityId = @ActivityId

UPDATE tHecActivityParticipantInfo
set FirstName=@FirstName,
LastName=@LastName,
Month=@Month,
Year=@Year,
RaceOrEthinicity=@RaceOrEthinicity,
Address1=@Part_Info_Address1,
Address2=@Part_Info_Address2,
CityId=@Part_Info_CityId,
Zip=@Part_Info_Zip,
Telephone=@Telephone,
Email=@Email,
LanguageId=@Part_Info_LanguageId


UPDATE tHecScreeningEvent
SET BusinessName=@BusinessName,
NPI=@NPI,
ProviderFirstName=@ProviderFirstName,
ProviderLastName=@ProviderLastName,
EwcProvider=@EwcProvider 
where ActivityId = @ActivityId


END



END
GO
